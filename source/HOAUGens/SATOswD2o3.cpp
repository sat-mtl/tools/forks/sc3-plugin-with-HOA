/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD2o3"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider1;
	float fRec1[2];
	float fRec2[3];
	FAUSTFLOAT fHslider2;
	float fRec3[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fRec10[3];
	float fRec11[3];
	float fRec12[3];
	float fRec13[3];
	float fRec14[3];
	float fConst5;
	float fConst6;
	float fConst7;
	float fRec9[2];
	float fRec7[2];
	float fRec6[2];
	float fRec4[2];
	float fConst8;
	float fConst9;
	float fConst10;
	float fConst11;
	float fRec24[3];
	float fRec25[3];
	float fRec26[3];
	float fRec27[3];
	float fRec28[3];
	float fRec29[3];
	float fRec30[3];
	float fConst12;
	float fConst13;
	float fRec23[2];
	float fRec21[2];
	float fRec20[2];
	float fRec18[2];
	float fRec17[2];
	float fRec15[2];
	float fConst14;
	float fConst15;
	float fRec34[3];
	float fRec35[3];
	float fRec36[3];
	float fConst16;
	float fRec33[2];
	float fRec31[2];
	int IOTA;
	float fVec0[1024];
	int iConst17;
	float fConst18;
	float fConst19;
	float fConst20;
	float fConst21;
	float fConst22;
	float fConst23;
	float fRec45[2];
	float fRec43[2];
	float fRec42[2];
	float fRec40[2];
	float fRec39[2];
	float fRec37[2];
	float fConst24;
	float fConst25;
	float fConst26;
	float fRec48[2];
	float fRec46[2];
	float fConst27;
	float fConst28;
	float fConst29;
	float fConst30;
	float fRec54[2];
	float fRec52[2];
	float fRec51[2];
	float fRec49[2];
	float fVec1[512];
	int iConst31;
	float fConst32;
	float fConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fRec63[2];
	float fRec61[2];
	float fRec60[2];
	float fRec58[2];
	float fRec57[2];
	float fRec55[2];
	float fConst38;
	float fConst39;
	float fConst40;
	float fRec66[2];
	float fRec64[2];
	float fConst41;
	float fConst42;
	float fConst43;
	float fConst44;
	float fRec72[2];
	float fRec70[2];
	float fRec69[2];
	float fRec67[2];
	float fVec2[512];
	int iConst45;
	float fRec81[2];
	float fRec79[2];
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	float fRec84[2];
	float fRec82[2];
	float fRec90[2];
	float fRec88[2];
	float fRec87[2];
	float fRec85[2];
	float fVec3[512];
	float fRec99[2];
	float fRec97[2];
	float fRec96[2];
	float fRec94[2];
	float fRec93[2];
	float fRec91[2];
	float fRec102[2];
	float fRec100[2];
	float fRec108[2];
	float fRec106[2];
	float fRec105[2];
	float fRec103[2];
	float fVec4[512];
	float fRec117[2];
	float fRec115[2];
	float fRec114[2];
	float fRec112[2];
	float fRec111[2];
	float fRec109[2];
	float fRec120[2];
	float fRec118[2];
	float fRec126[2];
	float fRec124[2];
	float fRec123[2];
	float fRec121[2];
	float fVec5[512];
	float fRec135[2];
	float fRec133[2];
	float fRec132[2];
	float fRec130[2];
	float fRec129[2];
	float fRec127[2];
	float fRec138[2];
	float fRec136[2];
	float fRec144[2];
	float fRec142[2];
	float fRec141[2];
	float fRec139[2];
	float fVec6[512];
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fConst50;
	float fConst51;
	float fRec153[2];
	float fRec151[2];
	float fRec150[2];
	float fRec148[2];
	float fRec147[2];
	float fRec145[2];
	float fConst52;
	float fConst53;
	float fConst54;
	float fRec156[2];
	float fRec154[2];
	float fConst55;
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec162[2];
	float fRec160[2];
	float fRec159[2];
	float fRec157[2];
	float fVec7[512];
	int iConst59;
	float fConst60;
	float fConst61;
	float fConst62;
	float fConst63;
	float fConst64;
	float fConst65;
	float fRec171[2];
	float fRec169[2];
	float fRec168[2];
	float fRec166[2];
	float fRec165[2];
	float fRec163[2];
	float fConst66;
	float fConst67;
	float fConst68;
	float fRec174[2];
	float fRec172[2];
	float fConst69;
	float fConst70;
	float fConst71;
	float fConst72;
	float fRec180[2];
	float fRec178[2];
	float fRec177[2];
	float fRec175[2];
	float fVec8[512];
	int iConst73;
	float fRec189[2];
	float fRec187[2];
	float fRec186[2];
	float fRec184[2];
	float fRec183[2];
	float fRec181[2];
	float fRec192[2];
	float fRec190[2];
	float fRec198[2];
	float fRec196[2];
	float fRec195[2];
	float fRec193[2];
	float fVec9[512];
	float fRec207[2];
	float fRec205[2];
	float fRec204[2];
	float fRec202[2];
	float fRec201[2];
	float fRec199[2];
	float fRec210[2];
	float fRec208[2];
	float fRec216[2];
	float fRec214[2];
	float fRec213[2];
	float fRec211[2];
	float fVec10[512];
	float fRec225[2];
	float fRec223[2];
	float fRec222[2];
	float fRec220[2];
	float fRec219[2];
	float fRec217[2];
	float fRec228[2];
	float fRec226[2];
	float fRec234[2];
	float fRec232[2];
	float fRec231[2];
	float fRec229[2];
	float fVec11[512];
	float fRec243[2];
	float fRec241[2];
	float fRec240[2];
	float fRec238[2];
	float fRec237[2];
	float fRec235[2];
	float fRec246[2];
	float fRec244[2];
	float fRec252[2];
	float fRec250[2];
	float fRec249[2];
	float fRec247[2];
	float fVec12[512];
	float fRec261[2];
	float fRec259[2];
	float fRec258[2];
	float fRec256[2];
	float fRec255[2];
	float fRec253[2];
	float fRec264[2];
	float fRec262[2];
	float fRec270[2];
	float fRec268[2];
	float fRec267[2];
	float fRec265[2];
	float fVec13[512];
	float fRec273[2];
	float fRec271[2];
	float fRec279[2];
	float fRec277[2];
	float fRec276[2];
	float fRec274[2];
	float fRec288[2];
	float fRec286[2];
	float fRec285[2];
	float fRec283[2];
	float fRec282[2];
	float fRec280[2];
	float fVec14[512];
	float fRec291[2];
	float fRec289[2];
	float fRec297[2];
	float fRec295[2];
	float fRec294[2];
	float fRec292[2];
	float fRec306[2];
	float fRec304[2];
	float fRec303[2];
	float fRec301[2];
	float fRec300[2];
	float fRec298[2];
	float fVec15[512];
	float fRec309[2];
	float fRec307[2];
	float fRec315[2];
	float fRec313[2];
	float fRec312[2];
	float fRec310[2];
	float fRec324[2];
	float fRec322[2];
	float fRec321[2];
	float fRec319[2];
	float fRec318[2];
	float fRec316[2];
	float fVec16[512];
	float fRec327[2];
	float fRec325[2];
	float fRec333[2];
	float fRec331[2];
	float fRec330[2];
	float fRec328[2];
	float fRec342[2];
	float fRec340[2];
	float fRec339[2];
	float fRec337[2];
	float fRec336[2];
	float fRec334[2];
	float fVec17[512];
	float fRec345[2];
	float fRec343[2];
	float fRec351[2];
	float fRec349[2];
	float fRec348[2];
	float fRec346[2];
	float fRec360[2];
	float fRec358[2];
	float fRec357[2];
	float fRec355[2];
	float fRec354[2];
	float fRec352[2];
	float fVec18[512];
	float fConst74;
	float fConst75;
	float fConst76;
	float fConst77;
	float fConst78;
	float fConst79;
	float fRec369[2];
	float fRec367[2];
	float fRec366[2];
	float fRec364[2];
	float fRec363[2];
	float fRec361[2];
	float fConst80;
	float fConst81;
	float fConst82;
	float fRec372[2];
	float fRec370[2];
	float fConst83;
	float fConst84;
	float fConst85;
	float fConst86;
	float fRec378[2];
	float fRec376[2];
	float fRec375[2];
	float fRec373[2];
	float fVec19[3];
	int iConst87;
	float fConst88;
	float fConst89;
	float fConst90;
	float fConst91;
	float fConst92;
	float fConst93;
	float fRec387[2];
	float fRec385[2];
	float fRec384[2];
	float fRec382[2];
	float fRec381[2];
	float fRec379[2];
	float fConst94;
	float fConst95;
	float fConst96;
	float fRec390[2];
	float fRec388[2];
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fRec396[2];
	float fRec394[2];
	float fRec393[2];
	float fRec391[2];
	float fRec405[2];
	float fRec403[2];
	float fRec402[2];
	float fRec400[2];
	float fRec399[2];
	float fRec397[2];
	float fRec408[2];
	float fRec406[2];
	float fRec414[2];
	float fRec412[2];
	float fRec411[2];
	float fRec409[2];
	float fRec423[2];
	float fRec421[2];
	float fRec420[2];
	float fRec418[2];
	float fRec417[2];
	float fRec415[2];
	float fRec426[2];
	float fRec424[2];
	float fRec432[2];
	float fRec430[2];
	float fRec429[2];
	float fRec427[2];
	float fVec20[3];
	float fRec441[2];
	float fRec439[2];
	float fRec438[2];
	float fRec436[2];
	float fRec435[2];
	float fRec433[2];
	float fRec444[2];
	float fRec442[2];
	float fRec450[2];
	float fRec448[2];
	float fRec447[2];
	float fRec445[2];
	float fRec459[2];
	float fRec457[2];
	float fRec456[2];
	float fRec454[2];
	float fRec453[2];
	float fRec451[2];
	float fRec462[2];
	float fRec460[2];
	float fRec468[2];
	float fRec466[2];
	float fRec465[2];
	float fRec463[2];
	float fRec477[2];
	float fRec475[2];
	float fRec474[2];
	float fRec472[2];
	float fRec471[2];
	float fRec469[2];
	float fRec480[2];
	float fRec478[2];
	float fRec486[2];
	float fRec484[2];
	float fRec483[2];
	float fRec481[2];
	float fVec21[3];
	float fRec495[2];
	float fRec493[2];
	float fRec492[2];
	float fRec490[2];
	float fRec489[2];
	float fRec487[2];
	float fRec498[2];
	float fRec496[2];
	float fRec504[2];
	float fRec502[2];
	float fRec501[2];
	float fRec499[2];
	float fRec513[2];
	float fRec511[2];
	float fRec510[2];
	float fRec508[2];
	float fRec507[2];
	float fRec505[2];
	float fRec516[2];
	float fRec514[2];
	float fRec522[2];
	float fRec520[2];
	float fRec519[2];
	float fRec517[2];
	float fRec531[2];
	float fRec529[2];
	float fRec528[2];
	float fRec526[2];
	float fRec525[2];
	float fRec523[2];
	float fRec534[2];
	float fRec532[2];
	float fRec540[2];
	float fRec538[2];
	float fRec537[2];
	float fRec535[2];
	float fVec22[3];
	float fRec549[2];
	float fRec547[2];
	float fRec546[2];
	float fRec544[2];
	float fRec543[2];
	float fRec541[2];
	float fRec552[2];
	float fRec550[2];
	float fRec558[2];
	float fRec556[2];
	float fRec555[2];
	float fRec553[2];
	float fRec567[2];
	float fRec565[2];
	float fRec564[2];
	float fRec562[2];
	float fRec561[2];
	float fRec559[2];
	float fRec570[2];
	float fRec568[2];
	float fRec576[2];
	float fRec574[2];
	float fRec573[2];
	float fRec571[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD2o3");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 16;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((((1371.83777f / fConst2) + 64.1522675f) / fConst2) + 1.0f);
		fConst4 = (1.0f / fConst3);
		fConst5 = mydsp_faustpower2_f(fConst2);
		fConst6 = (0.0f - (5487.35107f / (fConst5 * fConst3)));
		fConst7 = (0.0f - (((5487.35107f / fConst2) + 128.304535f) / (fConst2 * fConst3)));
		fConst8 = ((49.6578178f / fConst2) + 1.0f);
		fConst9 = (0.0f - (99.3156357f / (fConst2 * fConst8)));
		fConst10 = ((((2953.7644f / fConst2) + 78.6467133f) / fConst2) + 1.0f);
		fConst11 = (1.0f / (fConst8 * fConst10));
		fConst12 = (0.0f - (11815.0576f / (fConst5 * fConst10)));
		fConst13 = (0.0f - (((11815.0576f / fConst2) + 157.293427f) / (fConst2 * fConst10)));
		fConst14 = ((21.3840885f / fConst2) + 1.0f);
		fConst15 = (1.0f / fConst14);
		fConst16 = (0.0f - (42.768177f / (fConst2 * fConst14)));
		iConst17 = int(((0.003076792f * float(iConst0)) + 0.5f));
		fConst18 = ((48.2801056f / fConst2) + 1.0f);
		fConst19 = (0.0f - (96.5602112f / (fConst2 * fConst18)));
		fConst20 = ((((2792.13892f / fConst2) + 76.4647293f) / fConst2) + 1.0f);
		fConst21 = (1.0f / (fConst18 * fConst20));
		fConst22 = (0.0f - (11168.5557f / (fConst5 * fConst20)));
		fConst23 = (0.0f - (((11168.5557f / fConst2) + 152.929459f) / (fConst2 * fConst20)));
		fConst24 = ((20.7908058f / fConst2) + 1.0f);
		fConst25 = (1.0f / fConst24);
		fConst26 = (0.0f - (41.5816116f / (fConst2 * fConst24)));
		fConst27 = ((((1296.77283f / fConst2) + 62.3724174f) / fConst2) + 1.0f);
		fConst28 = (1.0f / fConst27);
		fConst29 = (0.0f - (5187.09131f / (fConst5 * fConst27)));
		fConst30 = (0.0f - (((5187.09131f / fConst2) + 124.744835f) / (fConst2 * fConst27)));
		iConst31 = int(((0.00240957108f * float(iConst0)) + 0.5f));
		fConst32 = ((48.2918053f / fConst2) + 1.0f);
		fConst33 = (0.0f - (96.5836105f / (fConst2 * fConst32)));
		fConst34 = ((((2793.49243f / fConst2) + 76.4832611f) / fConst2) + 1.0f);
		fConst35 = (1.0f / (fConst32 * fConst34));
		fConst36 = (0.0f - (11173.9697f / (fConst5 * fConst34)));
		fConst37 = (0.0f - (((11173.9697f / fConst2) + 152.966522f) / (fConst2 * fConst34)));
		fConst38 = ((20.795845f / fConst2) + 1.0f);
		fConst39 = (1.0f / fConst38);
		fConst40 = (0.0f - (41.5916901f / (fConst2 * fConst38)));
		fConst41 = ((((1297.40149f / fConst2) + 62.3875351f) / fConst2) + 1.0f);
		fConst42 = (1.0f / fConst41);
		fConst43 = (0.0f - (5189.60596f / (fConst41 * fConst5)));
		fConst44 = (0.0f - (((5189.60596f / fConst2) + 124.77507f) / (fConst41 * fConst2)));
		iConst45 = int(((0.00241539837f * float(iConst0)) + 0.5f));
		fConst46 = ((46.6577682f / fConst2) + 1.0f);
		fConst47 = (0.0f - (93.3155365f / (fConst46 * fConst2)));
		fConst48 = ((((2607.64526f / fConst2) + 73.8953171f) / fConst2) + 1.0f);
		fConst49 = (1.0f / (fConst46 * fConst48));
		fConst50 = (0.0f - (10430.5811f / (fConst48 * fConst5)));
		fConst51 = (0.0f - (((10430.5811f / fConst2) + 147.790634f) / (fConst48 * fConst2)));
		fConst52 = ((20.0921803f / fConst2) + 1.0f);
		fConst53 = (1.0f / fConst52);
		fConst54 = (0.0f - (40.1843605f / (fConst52 * fConst2)));
		fConst55 = ((((1211.08716f / fConst2) + 60.2765388f) / fConst2) + 1.0f);
		fConst56 = (1.0f / fConst55);
		fConst57 = (0.0f - (4844.34863f / (fConst5 * fConst55)));
		fConst58 = (0.0f - (((4844.34863f / fConst2) + 120.553078f) / (fConst2 * fConst55)));
		iConst59 = int(((0.00157335959f * float(iConst0)) + 0.5f));
		fConst60 = ((46.6304703f / fConst2) + 1.0f);
		fConst61 = (0.0f - (93.2609406f / (fConst2 * fConst60)));
		fConst62 = ((((2604.59473f / fConst2) + 73.8520813f) / fConst2) + 1.0f);
		fConst63 = (1.0f / (fConst60 * fConst62));
		fConst64 = (0.0f - (10418.3789f / (fConst5 * fConst62)));
		fConst65 = (0.0f - (((10418.3789f / fConst2) + 147.704163f) / (fConst2 * fConst62)));
		fConst66 = ((20.0804253f / fConst2) + 1.0f);
		fConst67 = (1.0f / fConst66);
		fConst68 = (0.0f - (40.1608505f / (fConst2 * fConst66)));
		fConst69 = ((((1209.67041f / fConst2) + 60.2412758f) / fConst2) + 1.0f);
		fConst70 = (1.0f / fConst69);
		fConst71 = (0.0f - (4838.68164f / (fConst5 * fConst69)));
		fConst72 = (0.0f - (((4838.68164f / fConst2) + 120.482552f) / (fConst2 * fConst69)));
		iConst73 = int(((0.00155879138f * float(iConst0)) + 0.5f));
		fConst74 = ((43.8929367f / fConst2) + 1.0f);
		fConst75 = (0.0f - (87.7858734f / (fConst2 * fConst74)));
		fConst76 = ((((2307.7561f / fConst2) + 69.5164566f) / fConst2) + 1.0f);
		fConst77 = (1.0f / (fConst74 * fConst76));
		fConst78 = (0.0f - (9231.02441f / (fConst5 * fConst76)));
		fConst79 = (0.0f - (((9231.02441f / fConst2) + 139.032913f) / (fConst2 * fConst76)));
		fConst80 = ((18.9015656f / fConst2) + 1.0f);
		fConst81 = (0.0f - (37.8031311f / (fConst2 * fConst80)));
		fConst82 = (1.0f / fConst80);
		fConst83 = ((((1071.8075f / fConst2) + 56.7046967f) / fConst2) + 1.0f);
		fConst84 = (1.0f / fConst83);
		fConst85 = (0.0f - (4287.22998f / (fConst83 * fConst5)));
		fConst86 = (0.0f - (((4287.22998f / fConst2) + 113.409393f) / (fConst83 * fConst2)));
		iConst87 = int(((5.82725761e-06f * float(iConst0)) + 0.5f));
		fConst88 = ((43.8832703f / fConst2) + 1.0f);
		fConst89 = (0.0f - (87.7665405f / (fConst2 * fConst88)));
		fConst90 = ((((2306.73975f / fConst2) + 69.5011444f) / fConst2) + 1.0f);
		fConst91 = (1.0f / (fConst88 * fConst90));
		fConst92 = (0.0f - (9226.95898f / (fConst5 * fConst90)));
		fConst93 = (0.0f - (((9226.95898f / fConst2) + 139.002289f) / (fConst2 * fConst90)));
		fConst94 = ((18.8974018f / fConst2) + 1.0f);
		fConst95 = (1.0f / fConst94);
		fConst96 = (0.0f - (37.7948036f / (fConst94 * fConst2)));
		fConst97 = ((((1071.33545f / fConst2) + 56.6922073f) / fConst2) + 1.0f);
		fConst98 = (1.0f / fConst97);
		fConst99 = (0.0f - (4285.3418f / (fConst5 * fConst97)));
		fConst100 = (0.0f - (((4285.3418f / fConst2) + 113.384415f) / (fConst2 * fConst97)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec3[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec10[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec11[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec12[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec13[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec14[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 2); l9 = (l9 + 1)) {
			fRec9[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			fRec7[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			fRec6[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 2); l12 = (l12 + 1)) {
			fRec4[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec24[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 3); l14 = (l14 + 1)) {
			fRec25[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 3); l15 = (l15 + 1)) {
			fRec26[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 3); l16 = (l16 + 1)) {
			fRec27[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 3); l17 = (l17 + 1)) {
			fRec28[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 3); l18 = (l18 + 1)) {
			fRec29[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 3); l19 = (l19 + 1)) {
			fRec30[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec23[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec21[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec20[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec18[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec17[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 2); l25 = (l25 + 1)) {
			fRec15[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec34[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec35[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec36[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 2); l29 = (l29 + 1)) {
			fRec33[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 2); l30 = (l30 + 1)) {
			fRec31[l30] = 0.0f;
			
		}
		IOTA = 0;
		for (int l31 = 0; (l31 < 1024); l31 = (l31 + 1)) {
			fVec0[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec45[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec43[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec42[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec40[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec39[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec37[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec48[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2); l39 = (l39 + 1)) {
			fRec46[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec54[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec52[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 2); l42 = (l42 + 1)) {
			fRec51[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 2); l43 = (l43 + 1)) {
			fRec49[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 512); l44 = (l44 + 1)) {
			fVec1[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 2); l45 = (l45 + 1)) {
			fRec63[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec61[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec60[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 2); l48 = (l48 + 1)) {
			fRec58[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec57[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec55[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec66[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec64[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec72[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec70[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec69[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec67[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 512); l57 = (l57 + 1)) {
			fVec2[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec81[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec79[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec78[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec76[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec75[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec73[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec84[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec82[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec90[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec88[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec87[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 2); l69 = (l69 + 1)) {
			fRec85[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 512); l70 = (l70 + 1)) {
			fVec3[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec99[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec97[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec96[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec94[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec93[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec91[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec102[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec100[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec108[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec106[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec105[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec103[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 512); l83 = (l83 + 1)) {
			fVec4[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec117[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec115[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec114[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec112[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec111[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec109[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec120[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec118[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec126[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec124[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec123[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec121[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 512); l96 = (l96 + 1)) {
			fVec5[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec135[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec133[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec132[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec130[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec129[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec127[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec138[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec136[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec144[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec142[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec141[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec139[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 512); l109 = (l109 + 1)) {
			fVec6[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec153[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec151[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec150[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec148[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec147[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec145[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec156[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec154[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec162[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec160[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec159[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec157[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 512); l122 = (l122 + 1)) {
			fVec7[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec171[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec169[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec168[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec166[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec165[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec163[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec174[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec172[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2); l131 = (l131 + 1)) {
			fRec180[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec178[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec177[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec175[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 512); l135 = (l135 + 1)) {
			fVec8[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec189[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec187[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec186[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec184[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec183[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec181[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec192[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec190[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec198[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec196[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec195[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec193[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 512); l148 = (l148 + 1)) {
			fVec9[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec207[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec205[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec204[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec202[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec201[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec199[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec210[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec208[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec216[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec214[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec213[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec211[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 512); l161 = (l161 + 1)) {
			fVec10[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2); l162 = (l162 + 1)) {
			fRec225[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec223[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec222[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec220[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec219[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec217[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec228[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec226[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec234[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec232[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec231[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec229[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 512); l174 = (l174 + 1)) {
			fVec11[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec243[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec241[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec240[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec238[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec237[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec235[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec246[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec244[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec252[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec250[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec249[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec247[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 512); l187 = (l187 + 1)) {
			fVec12[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec261[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 2); l189 = (l189 + 1)) {
			fRec259[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec258[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec256[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec255[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2); l193 = (l193 + 1)) {
			fRec253[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec264[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 2); l195 = (l195 + 1)) {
			fRec262[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec270[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec268[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec267[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec265[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 512); l200 = (l200 + 1)) {
			fVec13[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec273[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec271[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec279[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec277[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec276[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec274[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec288[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec286[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec285[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec283[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec282[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec280[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 512); l213 = (l213 + 1)) {
			fVec14[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec291[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec289[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 2); l216 = (l216 + 1)) {
			fRec297[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec295[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec294[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec292[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec306[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec304[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec303[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 2); l223 = (l223 + 1)) {
			fRec301[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 2); l224 = (l224 + 1)) {
			fRec300[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec298[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 512); l226 = (l226 + 1)) {
			fVec15[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec309[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec307[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 2); l229 = (l229 + 1)) {
			fRec315[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec313[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec312[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec310[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec324[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec322[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec321[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec319[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 2); l237 = (l237 + 1)) {
			fRec318[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 2); l238 = (l238 + 1)) {
			fRec316[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 512); l239 = (l239 + 1)) {
			fVec16[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec327[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec325[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec333[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 2); l243 = (l243 + 1)) {
			fRec331[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec330[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec328[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec342[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec340[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec339[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec337[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 2); l250 = (l250 + 1)) {
			fRec336[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec334[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 512); l252 = (l252 + 1)) {
			fVec17[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec345[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 2); l254 = (l254 + 1)) {
			fRec343[l254] = 0.0f;
			
		}
		for (int l255 = 0; (l255 < 2); l255 = (l255 + 1)) {
			fRec351[l255] = 0.0f;
			
		}
		for (int l256 = 0; (l256 < 2); l256 = (l256 + 1)) {
			fRec349[l256] = 0.0f;
			
		}
		for (int l257 = 0; (l257 < 2); l257 = (l257 + 1)) {
			fRec348[l257] = 0.0f;
			
		}
		for (int l258 = 0; (l258 < 2); l258 = (l258 + 1)) {
			fRec346[l258] = 0.0f;
			
		}
		for (int l259 = 0; (l259 < 2); l259 = (l259 + 1)) {
			fRec360[l259] = 0.0f;
			
		}
		for (int l260 = 0; (l260 < 2); l260 = (l260 + 1)) {
			fRec358[l260] = 0.0f;
			
		}
		for (int l261 = 0; (l261 < 2); l261 = (l261 + 1)) {
			fRec357[l261] = 0.0f;
			
		}
		for (int l262 = 0; (l262 < 2); l262 = (l262 + 1)) {
			fRec355[l262] = 0.0f;
			
		}
		for (int l263 = 0; (l263 < 2); l263 = (l263 + 1)) {
			fRec354[l263] = 0.0f;
			
		}
		for (int l264 = 0; (l264 < 2); l264 = (l264 + 1)) {
			fRec352[l264] = 0.0f;
			
		}
		for (int l265 = 0; (l265 < 512); l265 = (l265 + 1)) {
			fVec18[l265] = 0.0f;
			
		}
		for (int l266 = 0; (l266 < 2); l266 = (l266 + 1)) {
			fRec369[l266] = 0.0f;
			
		}
		for (int l267 = 0; (l267 < 2); l267 = (l267 + 1)) {
			fRec367[l267] = 0.0f;
			
		}
		for (int l268 = 0; (l268 < 2); l268 = (l268 + 1)) {
			fRec366[l268] = 0.0f;
			
		}
		for (int l269 = 0; (l269 < 2); l269 = (l269 + 1)) {
			fRec364[l269] = 0.0f;
			
		}
		for (int l270 = 0; (l270 < 2); l270 = (l270 + 1)) {
			fRec363[l270] = 0.0f;
			
		}
		for (int l271 = 0; (l271 < 2); l271 = (l271 + 1)) {
			fRec361[l271] = 0.0f;
			
		}
		for (int l272 = 0; (l272 < 2); l272 = (l272 + 1)) {
			fRec372[l272] = 0.0f;
			
		}
		for (int l273 = 0; (l273 < 2); l273 = (l273 + 1)) {
			fRec370[l273] = 0.0f;
			
		}
		for (int l274 = 0; (l274 < 2); l274 = (l274 + 1)) {
			fRec378[l274] = 0.0f;
			
		}
		for (int l275 = 0; (l275 < 2); l275 = (l275 + 1)) {
			fRec376[l275] = 0.0f;
			
		}
		for (int l276 = 0; (l276 < 2); l276 = (l276 + 1)) {
			fRec375[l276] = 0.0f;
			
		}
		for (int l277 = 0; (l277 < 2); l277 = (l277 + 1)) {
			fRec373[l277] = 0.0f;
			
		}
		for (int l278 = 0; (l278 < 3); l278 = (l278 + 1)) {
			fVec19[l278] = 0.0f;
			
		}
		for (int l279 = 0; (l279 < 2); l279 = (l279 + 1)) {
			fRec387[l279] = 0.0f;
			
		}
		for (int l280 = 0; (l280 < 2); l280 = (l280 + 1)) {
			fRec385[l280] = 0.0f;
			
		}
		for (int l281 = 0; (l281 < 2); l281 = (l281 + 1)) {
			fRec384[l281] = 0.0f;
			
		}
		for (int l282 = 0; (l282 < 2); l282 = (l282 + 1)) {
			fRec382[l282] = 0.0f;
			
		}
		for (int l283 = 0; (l283 < 2); l283 = (l283 + 1)) {
			fRec381[l283] = 0.0f;
			
		}
		for (int l284 = 0; (l284 < 2); l284 = (l284 + 1)) {
			fRec379[l284] = 0.0f;
			
		}
		for (int l285 = 0; (l285 < 2); l285 = (l285 + 1)) {
			fRec390[l285] = 0.0f;
			
		}
		for (int l286 = 0; (l286 < 2); l286 = (l286 + 1)) {
			fRec388[l286] = 0.0f;
			
		}
		for (int l287 = 0; (l287 < 2); l287 = (l287 + 1)) {
			fRec396[l287] = 0.0f;
			
		}
		for (int l288 = 0; (l288 < 2); l288 = (l288 + 1)) {
			fRec394[l288] = 0.0f;
			
		}
		for (int l289 = 0; (l289 < 2); l289 = (l289 + 1)) {
			fRec393[l289] = 0.0f;
			
		}
		for (int l290 = 0; (l290 < 2); l290 = (l290 + 1)) {
			fRec391[l290] = 0.0f;
			
		}
		for (int l291 = 0; (l291 < 2); l291 = (l291 + 1)) {
			fRec405[l291] = 0.0f;
			
		}
		for (int l292 = 0; (l292 < 2); l292 = (l292 + 1)) {
			fRec403[l292] = 0.0f;
			
		}
		for (int l293 = 0; (l293 < 2); l293 = (l293 + 1)) {
			fRec402[l293] = 0.0f;
			
		}
		for (int l294 = 0; (l294 < 2); l294 = (l294 + 1)) {
			fRec400[l294] = 0.0f;
			
		}
		for (int l295 = 0; (l295 < 2); l295 = (l295 + 1)) {
			fRec399[l295] = 0.0f;
			
		}
		for (int l296 = 0; (l296 < 2); l296 = (l296 + 1)) {
			fRec397[l296] = 0.0f;
			
		}
		for (int l297 = 0; (l297 < 2); l297 = (l297 + 1)) {
			fRec408[l297] = 0.0f;
			
		}
		for (int l298 = 0; (l298 < 2); l298 = (l298 + 1)) {
			fRec406[l298] = 0.0f;
			
		}
		for (int l299 = 0; (l299 < 2); l299 = (l299 + 1)) {
			fRec414[l299] = 0.0f;
			
		}
		for (int l300 = 0; (l300 < 2); l300 = (l300 + 1)) {
			fRec412[l300] = 0.0f;
			
		}
		for (int l301 = 0; (l301 < 2); l301 = (l301 + 1)) {
			fRec411[l301] = 0.0f;
			
		}
		for (int l302 = 0; (l302 < 2); l302 = (l302 + 1)) {
			fRec409[l302] = 0.0f;
			
		}
		for (int l303 = 0; (l303 < 2); l303 = (l303 + 1)) {
			fRec423[l303] = 0.0f;
			
		}
		for (int l304 = 0; (l304 < 2); l304 = (l304 + 1)) {
			fRec421[l304] = 0.0f;
			
		}
		for (int l305 = 0; (l305 < 2); l305 = (l305 + 1)) {
			fRec420[l305] = 0.0f;
			
		}
		for (int l306 = 0; (l306 < 2); l306 = (l306 + 1)) {
			fRec418[l306] = 0.0f;
			
		}
		for (int l307 = 0; (l307 < 2); l307 = (l307 + 1)) {
			fRec417[l307] = 0.0f;
			
		}
		for (int l308 = 0; (l308 < 2); l308 = (l308 + 1)) {
			fRec415[l308] = 0.0f;
			
		}
		for (int l309 = 0; (l309 < 2); l309 = (l309 + 1)) {
			fRec426[l309] = 0.0f;
			
		}
		for (int l310 = 0; (l310 < 2); l310 = (l310 + 1)) {
			fRec424[l310] = 0.0f;
			
		}
		for (int l311 = 0; (l311 < 2); l311 = (l311 + 1)) {
			fRec432[l311] = 0.0f;
			
		}
		for (int l312 = 0; (l312 < 2); l312 = (l312 + 1)) {
			fRec430[l312] = 0.0f;
			
		}
		for (int l313 = 0; (l313 < 2); l313 = (l313 + 1)) {
			fRec429[l313] = 0.0f;
			
		}
		for (int l314 = 0; (l314 < 2); l314 = (l314 + 1)) {
			fRec427[l314] = 0.0f;
			
		}
		for (int l315 = 0; (l315 < 3); l315 = (l315 + 1)) {
			fVec20[l315] = 0.0f;
			
		}
		for (int l316 = 0; (l316 < 2); l316 = (l316 + 1)) {
			fRec441[l316] = 0.0f;
			
		}
		for (int l317 = 0; (l317 < 2); l317 = (l317 + 1)) {
			fRec439[l317] = 0.0f;
			
		}
		for (int l318 = 0; (l318 < 2); l318 = (l318 + 1)) {
			fRec438[l318] = 0.0f;
			
		}
		for (int l319 = 0; (l319 < 2); l319 = (l319 + 1)) {
			fRec436[l319] = 0.0f;
			
		}
		for (int l320 = 0; (l320 < 2); l320 = (l320 + 1)) {
			fRec435[l320] = 0.0f;
			
		}
		for (int l321 = 0; (l321 < 2); l321 = (l321 + 1)) {
			fRec433[l321] = 0.0f;
			
		}
		for (int l322 = 0; (l322 < 2); l322 = (l322 + 1)) {
			fRec444[l322] = 0.0f;
			
		}
		for (int l323 = 0; (l323 < 2); l323 = (l323 + 1)) {
			fRec442[l323] = 0.0f;
			
		}
		for (int l324 = 0; (l324 < 2); l324 = (l324 + 1)) {
			fRec450[l324] = 0.0f;
			
		}
		for (int l325 = 0; (l325 < 2); l325 = (l325 + 1)) {
			fRec448[l325] = 0.0f;
			
		}
		for (int l326 = 0; (l326 < 2); l326 = (l326 + 1)) {
			fRec447[l326] = 0.0f;
			
		}
		for (int l327 = 0; (l327 < 2); l327 = (l327 + 1)) {
			fRec445[l327] = 0.0f;
			
		}
		for (int l328 = 0; (l328 < 2); l328 = (l328 + 1)) {
			fRec459[l328] = 0.0f;
			
		}
		for (int l329 = 0; (l329 < 2); l329 = (l329 + 1)) {
			fRec457[l329] = 0.0f;
			
		}
		for (int l330 = 0; (l330 < 2); l330 = (l330 + 1)) {
			fRec456[l330] = 0.0f;
			
		}
		for (int l331 = 0; (l331 < 2); l331 = (l331 + 1)) {
			fRec454[l331] = 0.0f;
			
		}
		for (int l332 = 0; (l332 < 2); l332 = (l332 + 1)) {
			fRec453[l332] = 0.0f;
			
		}
		for (int l333 = 0; (l333 < 2); l333 = (l333 + 1)) {
			fRec451[l333] = 0.0f;
			
		}
		for (int l334 = 0; (l334 < 2); l334 = (l334 + 1)) {
			fRec462[l334] = 0.0f;
			
		}
		for (int l335 = 0; (l335 < 2); l335 = (l335 + 1)) {
			fRec460[l335] = 0.0f;
			
		}
		for (int l336 = 0; (l336 < 2); l336 = (l336 + 1)) {
			fRec468[l336] = 0.0f;
			
		}
		for (int l337 = 0; (l337 < 2); l337 = (l337 + 1)) {
			fRec466[l337] = 0.0f;
			
		}
		for (int l338 = 0; (l338 < 2); l338 = (l338 + 1)) {
			fRec465[l338] = 0.0f;
			
		}
		for (int l339 = 0; (l339 < 2); l339 = (l339 + 1)) {
			fRec463[l339] = 0.0f;
			
		}
		for (int l340 = 0; (l340 < 2); l340 = (l340 + 1)) {
			fRec477[l340] = 0.0f;
			
		}
		for (int l341 = 0; (l341 < 2); l341 = (l341 + 1)) {
			fRec475[l341] = 0.0f;
			
		}
		for (int l342 = 0; (l342 < 2); l342 = (l342 + 1)) {
			fRec474[l342] = 0.0f;
			
		}
		for (int l343 = 0; (l343 < 2); l343 = (l343 + 1)) {
			fRec472[l343] = 0.0f;
			
		}
		for (int l344 = 0; (l344 < 2); l344 = (l344 + 1)) {
			fRec471[l344] = 0.0f;
			
		}
		for (int l345 = 0; (l345 < 2); l345 = (l345 + 1)) {
			fRec469[l345] = 0.0f;
			
		}
		for (int l346 = 0; (l346 < 2); l346 = (l346 + 1)) {
			fRec480[l346] = 0.0f;
			
		}
		for (int l347 = 0; (l347 < 2); l347 = (l347 + 1)) {
			fRec478[l347] = 0.0f;
			
		}
		for (int l348 = 0; (l348 < 2); l348 = (l348 + 1)) {
			fRec486[l348] = 0.0f;
			
		}
		for (int l349 = 0; (l349 < 2); l349 = (l349 + 1)) {
			fRec484[l349] = 0.0f;
			
		}
		for (int l350 = 0; (l350 < 2); l350 = (l350 + 1)) {
			fRec483[l350] = 0.0f;
			
		}
		for (int l351 = 0; (l351 < 2); l351 = (l351 + 1)) {
			fRec481[l351] = 0.0f;
			
		}
		for (int l352 = 0; (l352 < 3); l352 = (l352 + 1)) {
			fVec21[l352] = 0.0f;
			
		}
		for (int l353 = 0; (l353 < 2); l353 = (l353 + 1)) {
			fRec495[l353] = 0.0f;
			
		}
		for (int l354 = 0; (l354 < 2); l354 = (l354 + 1)) {
			fRec493[l354] = 0.0f;
			
		}
		for (int l355 = 0; (l355 < 2); l355 = (l355 + 1)) {
			fRec492[l355] = 0.0f;
			
		}
		for (int l356 = 0; (l356 < 2); l356 = (l356 + 1)) {
			fRec490[l356] = 0.0f;
			
		}
		for (int l357 = 0; (l357 < 2); l357 = (l357 + 1)) {
			fRec489[l357] = 0.0f;
			
		}
		for (int l358 = 0; (l358 < 2); l358 = (l358 + 1)) {
			fRec487[l358] = 0.0f;
			
		}
		for (int l359 = 0; (l359 < 2); l359 = (l359 + 1)) {
			fRec498[l359] = 0.0f;
			
		}
		for (int l360 = 0; (l360 < 2); l360 = (l360 + 1)) {
			fRec496[l360] = 0.0f;
			
		}
		for (int l361 = 0; (l361 < 2); l361 = (l361 + 1)) {
			fRec504[l361] = 0.0f;
			
		}
		for (int l362 = 0; (l362 < 2); l362 = (l362 + 1)) {
			fRec502[l362] = 0.0f;
			
		}
		for (int l363 = 0; (l363 < 2); l363 = (l363 + 1)) {
			fRec501[l363] = 0.0f;
			
		}
		for (int l364 = 0; (l364 < 2); l364 = (l364 + 1)) {
			fRec499[l364] = 0.0f;
			
		}
		for (int l365 = 0; (l365 < 2); l365 = (l365 + 1)) {
			fRec513[l365] = 0.0f;
			
		}
		for (int l366 = 0; (l366 < 2); l366 = (l366 + 1)) {
			fRec511[l366] = 0.0f;
			
		}
		for (int l367 = 0; (l367 < 2); l367 = (l367 + 1)) {
			fRec510[l367] = 0.0f;
			
		}
		for (int l368 = 0; (l368 < 2); l368 = (l368 + 1)) {
			fRec508[l368] = 0.0f;
			
		}
		for (int l369 = 0; (l369 < 2); l369 = (l369 + 1)) {
			fRec507[l369] = 0.0f;
			
		}
		for (int l370 = 0; (l370 < 2); l370 = (l370 + 1)) {
			fRec505[l370] = 0.0f;
			
		}
		for (int l371 = 0; (l371 < 2); l371 = (l371 + 1)) {
			fRec516[l371] = 0.0f;
			
		}
		for (int l372 = 0; (l372 < 2); l372 = (l372 + 1)) {
			fRec514[l372] = 0.0f;
			
		}
		for (int l373 = 0; (l373 < 2); l373 = (l373 + 1)) {
			fRec522[l373] = 0.0f;
			
		}
		for (int l374 = 0; (l374 < 2); l374 = (l374 + 1)) {
			fRec520[l374] = 0.0f;
			
		}
		for (int l375 = 0; (l375 < 2); l375 = (l375 + 1)) {
			fRec519[l375] = 0.0f;
			
		}
		for (int l376 = 0; (l376 < 2); l376 = (l376 + 1)) {
			fRec517[l376] = 0.0f;
			
		}
		for (int l377 = 0; (l377 < 2); l377 = (l377 + 1)) {
			fRec531[l377] = 0.0f;
			
		}
		for (int l378 = 0; (l378 < 2); l378 = (l378 + 1)) {
			fRec529[l378] = 0.0f;
			
		}
		for (int l379 = 0; (l379 < 2); l379 = (l379 + 1)) {
			fRec528[l379] = 0.0f;
			
		}
		for (int l380 = 0; (l380 < 2); l380 = (l380 + 1)) {
			fRec526[l380] = 0.0f;
			
		}
		for (int l381 = 0; (l381 < 2); l381 = (l381 + 1)) {
			fRec525[l381] = 0.0f;
			
		}
		for (int l382 = 0; (l382 < 2); l382 = (l382 + 1)) {
			fRec523[l382] = 0.0f;
			
		}
		for (int l383 = 0; (l383 < 2); l383 = (l383 + 1)) {
			fRec534[l383] = 0.0f;
			
		}
		for (int l384 = 0; (l384 < 2); l384 = (l384 + 1)) {
			fRec532[l384] = 0.0f;
			
		}
		for (int l385 = 0; (l385 < 2); l385 = (l385 + 1)) {
			fRec540[l385] = 0.0f;
			
		}
		for (int l386 = 0; (l386 < 2); l386 = (l386 + 1)) {
			fRec538[l386] = 0.0f;
			
		}
		for (int l387 = 0; (l387 < 2); l387 = (l387 + 1)) {
			fRec537[l387] = 0.0f;
			
		}
		for (int l388 = 0; (l388 < 2); l388 = (l388 + 1)) {
			fRec535[l388] = 0.0f;
			
		}
		for (int l389 = 0; (l389 < 3); l389 = (l389 + 1)) {
			fVec22[l389] = 0.0f;
			
		}
		for (int l390 = 0; (l390 < 2); l390 = (l390 + 1)) {
			fRec549[l390] = 0.0f;
			
		}
		for (int l391 = 0; (l391 < 2); l391 = (l391 + 1)) {
			fRec547[l391] = 0.0f;
			
		}
		for (int l392 = 0; (l392 < 2); l392 = (l392 + 1)) {
			fRec546[l392] = 0.0f;
			
		}
		for (int l393 = 0; (l393 < 2); l393 = (l393 + 1)) {
			fRec544[l393] = 0.0f;
			
		}
		for (int l394 = 0; (l394 < 2); l394 = (l394 + 1)) {
			fRec543[l394] = 0.0f;
			
		}
		for (int l395 = 0; (l395 < 2); l395 = (l395 + 1)) {
			fRec541[l395] = 0.0f;
			
		}
		for (int l396 = 0; (l396 < 2); l396 = (l396 + 1)) {
			fRec552[l396] = 0.0f;
			
		}
		for (int l397 = 0; (l397 < 2); l397 = (l397 + 1)) {
			fRec550[l397] = 0.0f;
			
		}
		for (int l398 = 0; (l398 < 2); l398 = (l398 + 1)) {
			fRec558[l398] = 0.0f;
			
		}
		for (int l399 = 0; (l399 < 2); l399 = (l399 + 1)) {
			fRec556[l399] = 0.0f;
			
		}
		for (int l400 = 0; (l400 < 2); l400 = (l400 + 1)) {
			fRec555[l400] = 0.0f;
			
		}
		for (int l401 = 0; (l401 < 2); l401 = (l401 + 1)) {
			fRec553[l401] = 0.0f;
			
		}
		for (int l402 = 0; (l402 < 2); l402 = (l402 + 1)) {
			fRec567[l402] = 0.0f;
			
		}
		for (int l403 = 0; (l403 < 2); l403 = (l403 + 1)) {
			fRec565[l403] = 0.0f;
			
		}
		for (int l404 = 0; (l404 < 2); l404 = (l404 + 1)) {
			fRec564[l404] = 0.0f;
			
		}
		for (int l405 = 0; (l405 < 2); l405 = (l405 + 1)) {
			fRec562[l405] = 0.0f;
			
		}
		for (int l406 = 0; (l406 < 2); l406 = (l406 + 1)) {
			fRec561[l406] = 0.0f;
			
		}
		for (int l407 = 0; (l407 < 2); l407 = (l407 + 1)) {
			fRec559[l407] = 0.0f;
			
		}
		for (int l408 = 0; (l408 < 2); l408 = (l408 + 1)) {
			fRec570[l408] = 0.0f;
			
		}
		for (int l409 = 0; (l409 < 2); l409 = (l409 + 1)) {
			fRec568[l409] = 0.0f;
			
		}
		for (int l410 = 0; (l410 < 2); l410 = (l410 + 1)) {
			fRec576[l410] = 0.0f;
			
		}
		for (int l411 = 0; (l411 < 2); l411 = (l411 + 1)) {
			fRec574[l411] = 0.0f;
			
		}
		for (int l412 = 0; (l412 < 2); l412 = (l412 + 1)) {
			fRec573[l412] = 0.0f;
			
		}
		for (int l413 = 0; (l413 < 2); l413 = (l413 + 1)) {
			fRec571[l413] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD2o3");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec1[0] = (fSlow1 + (0.999000013f * fRec1[1]));
			float fTemp0 = tanf((fConst1 * fRec1[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec2[0] = (float(input0[i]) - (((fRec2[2] * fTemp2) + (2.0f * (fRec2[1] * fTemp3))) / fTemp4));
			fRec3[0] = (fSlow2 + (0.999000013f * fRec3[1]));
			float fTemp5 = (fRec3[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec2[2] + (fRec2[0] + (2.0f * fRec2[1])))) / fTemp5) + (fRec3[0] * (0.0f - ((fRec2[1] * fTemp6) + ((fRec2[0] + fRec2[2]) / fTemp4)))));
			fRec10[0] = (float(input4[i]) - (((fTemp2 * fRec10[2]) + (2.0f * (fTemp3 * fRec10[1]))) / fTemp4));
			float fTemp8 = (((fTemp1 * (fRec10[2] + (fRec10[0] + (2.0f * fRec10[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec10[1]) + ((fRec10[0] + fRec10[2]) / fTemp4))))));
			fRec11[0] = (float(input6[i]) - (((fTemp2 * fRec11[2]) + (2.0f * (fTemp3 * fRec11[1]))) / fTemp4));
			float fTemp9 = (((fTemp1 * (fRec11[2] + (fRec11[0] + (2.0f * fRec11[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec11[1]) + ((fRec11[0] + fRec11[2]) / fTemp4))))));
			fRec12[0] = (float(input7[i]) - (((fTemp2 * fRec12[2]) + (2.0f * (fTemp3 * fRec12[1]))) / fTemp4));
			float fTemp10 = (((fTemp1 * (fRec12[2] + (fRec12[0] + (2.0f * fRec12[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec12[1]) + ((fRec12[0] + fRec12[2]) / fTemp4))))));
			fRec13[0] = (float(input5[i]) - (((fTemp2 * fRec13[2]) + (2.0f * (fTemp3 * fRec13[1]))) / fTemp4));
			float fTemp11 = (((fTemp1 * (fRec13[2] + (fRec13[0] + (2.0f * fRec13[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec13[1]) + ((fRec13[0] + fRec13[2]) / fTemp4))))));
			fRec14[0] = (float(input8[i]) - (((fTemp2 * fRec14[2]) + (2.0f * (fTemp3 * fRec14[1]))) / fTemp4));
			float fTemp12 = (((fTemp1 * (fRec14[2] + (fRec14[0] + (2.0f * fRec14[1])))) / fTemp5) + (0.612333596f * (fRec3[0] * (0.0f - ((fTemp6 * fRec14[1]) + ((fRec14[0] + fRec14[2]) / fTemp4))))));
			float fTemp13 = (fConst4 * ((((8.81689994e-06f * fTemp8) + (0.103228964f * fTemp9)) + (5.07200014e-07f * fTemp10)) - ((7.52200003e-06f * fTemp11) + (4.11000019e-06f * fTemp12))));
			float fTemp14 = (fConst6 * fRec4[1]);
			float fTemp15 = (fConst7 * fRec7[1]);
			fRec9[0] = (fTemp13 + (fTemp14 + (fRec9[1] + fTemp15)));
			fRec7[0] = fRec9[0];
			float fRec8 = ((fTemp15 + fTemp14) + fTemp13);
			fRec6[0] = (fRec7[0] + fRec6[1]);
			fRec4[0] = fRec6[0];
			float fRec5 = fRec8;
			float fTemp16 = (fConst9 * fRec15[1]);
			fRec24[0] = (float(input9[i]) - (((fTemp2 * fRec24[2]) + (2.0f * (fTemp3 * fRec24[1]))) / fTemp4));
			float fTemp17 = (((fTemp1 * (fRec24[2] + (fRec24[0] + (2.0f * fRec24[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec24[1]) + ((fRec24[0] + fRec24[2]) / fTemp4))))));
			fRec25[0] = (float(input10[i]) - (((fTemp2 * fRec25[2]) + (2.0f * (fTemp3 * fRec25[1]))) / fTemp4));
			float fTemp18 = (((fTemp1 * (fRec25[2] + (fRec25[0] + (2.0f * fRec25[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec25[1]) + ((fRec25[0] + fRec25[2]) / fTemp4))))));
			fRec26[0] = (float(input12[i]) - (((fTemp2 * fRec26[2]) + (2.0f * (fTemp3 * fRec26[1]))) / fTemp4));
			float fTemp19 = (((fTemp1 * (fRec26[2] + (fRec26[0] + (2.0f * fRec26[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec26[1]) + ((fRec26[0] + fRec26[2]) / fTemp4))))));
			fRec27[0] = (float(input13[i]) - (((fTemp2 * fRec27[2]) + (2.0f * (fTemp3 * fRec27[1]))) / fTemp4));
			float fTemp20 = (((fTemp1 * (fRec27[2] + (fRec27[0] + (2.0f * fRec27[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec27[1]) + ((fRec27[0] + fRec27[2]) / fTemp4))))));
			fRec28[0] = (float(input15[i]) - (((fTemp2 * fRec28[2]) + (2.0f * (fTemp3 * fRec28[1]))) / fTemp4));
			float fTemp21 = (((fTemp1 * (fRec28[2] + (fRec28[0] + (2.0f * fRec28[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec28[1]) + ((fRec28[0] + fRec28[2]) / fTemp4))))));
			fRec29[0] = (float(input11[i]) - (((fTemp2 * fRec29[2]) + (2.0f * (fTemp3 * fRec29[1]))) / fTemp4));
			float fTemp22 = (((fTemp1 * (fRec29[2] + (fRec29[0] + (2.0f * fRec29[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec29[1]) + ((fRec29[0] + fRec29[2]) / fTemp4))))));
			fRec30[0] = (float(input14[i]) - (((fTemp2 * fRec30[2]) + (2.0f * (fTemp3 * fRec30[1]))) / fTemp4));
			float fTemp23 = (((fTemp1 * (fRec30[2] + (fRec30[0] + (2.0f * fRec30[1])))) / fTemp5) + (0.304746985f * (fRec3[0] * (0.0f - ((fTemp6 * fRec30[1]) + ((fRec30[0] + fRec30[2]) / fTemp4))))));
			float fTemp24 = (fConst11 * ((((((2.70700014e-07f * fTemp17) + (1.88841004e-05f * fTemp18)) + (0.0943125188f * fTemp19)) + (7.84300028e-07f * fTemp20)) + (8.95399978e-07f * fTemp21)) - ((8.87769966e-06f * fTemp22) + (8.26339965e-06f * fTemp23))));
			float fTemp25 = (fConst12 * fRec18[1]);
			float fTemp26 = (fConst13 * fRec21[1]);
			fRec23[0] = (fTemp24 + (fTemp25 + (fRec23[1] + fTemp26)));
			fRec21[0] = fRec23[0];
			float fRec22 = ((fTemp26 + fTemp25) + fTemp24);
			fRec20[0] = (fRec21[0] + fRec20[1]);
			fRec18[0] = fRec20[0];
			float fRec19 = fRec22;
			fRec17[0] = (fTemp16 + (fRec19 + fRec17[1]));
			fRec15[0] = fRec17[0];
			float fRec16 = (fRec19 + fTemp16);
			fRec34[0] = (float(input2[i]) - (((fTemp2 * fRec34[2]) + (2.0f * (fTemp3 * fRec34[1]))) / fTemp4));
			float fTemp27 = (((fTemp1 * (fRec34[2] + (fRec34[0] + (2.0f * fRec34[1])))) / fTemp5) + (0.861136317f * (fRec3[0] * (0.0f - ((fTemp6 * fRec34[1]) + ((fRec34[0] + fRec34[2]) / fTemp4))))));
			fRec35[0] = (float(input3[i]) - (((fTemp2 * fRec35[2]) + (2.0f * (fTemp3 * fRec35[1]))) / fTemp4));
			float fTemp28 = (((fTemp1 * (fRec35[2] + (fRec35[0] + (2.0f * fRec35[1])))) / fTemp5) + (0.861136317f * (fRec3[0] * (0.0f - ((fTemp6 * fRec35[1]) + ((fRec35[0] + fRec35[2]) / fTemp4))))));
			fRec36[0] = (float(input1[i]) - (((fTemp2 * fRec36[2]) + (2.0f * (fTemp3 * fRec36[1]))) / fTemp4));
			float fTemp29 = (((fTemp1 * (fRec36[2] + (fRec36[0] + (2.0f * fRec36[1])))) / fTemp5) + (0.861136317f * (fRec3[0] * (0.0f - ((fTemp6 * fRec36[1]) + ((fRec36[0] + fRec36[2]) / fTemp4))))));
			float fTemp30 = (fConst15 * (((0.0941376761f * fTemp27) + (2.42800013e-07f * fTemp28)) - (4.15470004e-06f * fTemp29)));
			float fTemp31 = (fConst16 * fRec31[1]);
			fRec33[0] = (fTemp30 + (fRec33[1] + fTemp31));
			fRec31[0] = fRec33[0];
			float fRec32 = (fTemp31 + fTemp30);
			fVec0[(IOTA & 1023)] = ((0.0588310584f * fTemp7) + (fRec5 + (fRec16 + fRec32)));
			output0[i] = FAUSTFLOAT((0.883713245f * (fRec0[0] * fVec0[((IOTA - iConst17) & 1023)])));
			float fTemp32 = (fConst19 * fRec37[1]);
			float fTemp33 = (fConst21 * (((((0.0280165877f * fTemp18) + (0.0582673401f * fTemp22)) + (0.000949303096f * fTemp19)) + (0.017944064f * fTemp20)) - ((0.0117041916f * fTemp21) + ((0.00536028529f * fTemp17) + (0.0338784568f * fTemp23)))));
			float fTemp34 = (fConst22 * fRec40[1]);
			float fTemp35 = (fConst23 * fRec43[1]);
			fRec45[0] = (fTemp33 + (fTemp34 + (fRec45[1] + fTemp35)));
			fRec43[0] = fRec45[0];
			float fRec44 = ((fTemp35 + fTemp34) + fTemp33);
			fRec42[0] = (fRec43[0] + fRec42[1]);
			fRec40[0] = fRec42[0];
			float fRec41 = fRec44;
			fRec39[0] = (fTemp32 + (fRec41 + fRec39[1]));
			fRec37[0] = fRec39[0];
			float fRec38 = (fRec41 + fTemp32);
			float fTemp36 = (fConst25 * (((0.0389746763f * fTemp29) + (0.0524411313f * fTemp27)) + (0.0137809403f * fTemp28)));
			float fTemp37 = (fConst26 * fRec46[1]);
			fRec48[0] = (fTemp36 + (fRec48[1] + fTemp37));
			fRec46[0] = fRec48[0];
			float fRec47 = (fTemp37 + fTemp36);
			float fTemp38 = (fConst28 * (((((0.0163057726f * fTemp8) + (0.0618029423f * fTemp11)) + (0.0322771296f * fTemp9)) + (0.0209300108f * fTemp10)) - (0.0186568219f * fTemp12)));
			float fTemp39 = (fConst29 * fRec49[1]);
			float fTemp40 = (fConst30 * fRec52[1]);
			fRec54[0] = (fTemp38 + (fTemp39 + (fRec54[1] + fTemp40)));
			fRec52[0] = fRec54[0];
			float fRec53 = ((fTemp40 + fTemp39) + fTemp38);
			fRec51[0] = (fRec52[0] + fRec51[1]);
			fRec49[0] = fRec51[0];
			float fRec50 = fRec53;
			fVec1[(IOTA & 511)] = ((0.0407811999f * fTemp7) + (fRec38 + (fRec47 + fRec50)));
			output1[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec1[((IOTA - iConst31) & 511)])));
			float fTemp41 = (fConst33 * fRec55[1]);
			float fTemp42 = (fConst35 * (((0.00843413826f * fTemp21) + (((0.00843882281f * fTemp17) + (0.045639351f * fTemp22)) + (1.07461001e-05f * fTemp23))) - (((0.0472712144f * fTemp18) + (0.0032499393f * fTemp19)) + (0.045648586f * fTemp20))));
			float fTemp43 = (fConst36 * fRec58[1]);
			float fTemp44 = (fConst37 * fRec61[1]);
			fRec63[0] = (fTemp42 + (fTemp43 + (fRec63[1] + fTemp44)));
			fRec61[0] = fRec63[0];
			float fRec62 = ((fTemp44 + fTemp43) + fTemp42);
			fRec60[0] = (fRec61[0] + fRec60[1]);
			fRec58[0] = fRec60[0];
			float fRec59 = fRec62;
			fRec57[0] = (fTemp41 + (fRec59 + fRec57[1]));
			fRec55[0] = fRec57[0];
			float fRec56 = (fRec59 + fTemp41);
			float fTemp45 = (fConst39 * (((0.0329679698f * fTemp29) + (0.0571265034f * fTemp27)) - (0.0329778865f * fTemp28)));
			float fTemp46 = (fConst40 * fRec64[1]);
			fRec66[0] = (fTemp45 + (fRec66[1] + fTemp46));
			fRec64[0] = fRec66[0];
			float fRec65 = (fTemp46 + fTemp45);
			float fTemp47 = (fConst42 * ((((0.0510057807f * fTemp11) + (0.0325682648f * fTemp9)) + (7.47690001e-06f * fTemp12)) - ((0.026926782f * fTemp8) + (0.0510190837f * fTemp10))));
			float fTemp48 = (fConst43 * fRec67[1]);
			float fTemp49 = (fConst44 * fRec70[1]);
			fRec72[0] = (fTemp47 + (fTemp48 + (fRec72[1] + fTemp49)));
			fRec70[0] = fRec72[0];
			float fRec71 = ((fTemp49 + fTemp48) + fTemp47);
			fRec69[0] = (fRec70[0] + fRec69[1]);
			fRec67[0] = fRec69[0];
			float fRec68 = fRec71;
			fVec2[(IOTA & 511)] = ((0.0453824513f * fTemp7) + (fRec56 + (fRec65 + fRec68)));
			output2[i] = FAUSTFLOAT((0.90871048f * (fRec0[0] * fVec2[((IOTA - iConst45) & 511)])));
			float fTemp50 = (fConst19 * fRec73[1]);
			float fTemp51 = (fConst21 * ((((0.0280273985f * fTemp18) + (0.000924167107f * fTemp19)) + (0.0338808298f * fTemp23)) - ((((0.0117095737f * fTemp17) + (0.0179393496f * fTemp22)) + (0.0582761504f * fTemp20)) + (0.00534620928f * fTemp21))));
			float fTemp52 = (fConst22 * fRec76[1]);
			float fTemp53 = (fConst23 * fRec79[1]);
			fRec81[0] = (fTemp51 + (fTemp52 + (fRec81[1] + fTemp53)));
			fRec79[0] = fRec81[0];
			float fRec80 = ((fTemp53 + fTemp52) + fTemp51);
			fRec78[0] = (fRec79[0] + fRec78[1]);
			fRec76[0] = fRec78[0];
			float fRec77 = fRec80;
			fRec75[0] = (fTemp50 + (fRec77 + fRec75[1]));
			fRec73[0] = fRec75[0];
			float fRec74 = (fRec77 + fTemp50);
			float fTemp54 = (fConst25 * ((0.0524600223f * fTemp27) - ((0.0137928454f * fTemp29) + (0.0389987528f * fTemp28))));
			float fTemp55 = (fConst26 * fRec82[1]);
			fRec84[0] = (fTemp54 + (fRec84[1] + fTemp55));
			fRec82[0] = fRec84[0];
			float fRec83 = (fTemp55 + fTemp54);
			float fTemp56 = (fConst28 * ((((0.0163196493f * fTemp8) + (0.0322710611f * fTemp9)) + (0.0186589565f * fTemp12)) - ((0.0209395979f * fTemp11) + (0.0618302934f * fTemp10))));
			float fTemp57 = (fConst29 * fRec85[1]);
			float fTemp58 = (fConst30 * fRec88[1]);
			fRec90[0] = (fTemp56 + (fTemp57 + (fRec90[1] + fTemp58)));
			fRec88[0] = fRec90[0];
			float fRec89 = ((fTemp58 + fTemp57) + fTemp56);
			fRec87[0] = (fRec88[0] + fRec87[1]);
			fRec85[0] = fRec87[0];
			float fRec86 = fRec89;
			fVec3[(IOTA & 511)] = ((0.0408032164f * fTemp7) + (fRec74 + (fRec83 + fRec86)));
			output3[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec3[((IOTA - iConst31) & 511)])));
			float fTemp59 = (fConst19 * fRec91[1]);
			float fTemp60 = (fConst21 * (((((0.00859549083f * fTemp17) + (0.0197129045f * fTemp18)) + (0.00515312981f * fTemp19)) + (0.0086075291f * fTemp21)) - (((0.0553654023f * fTemp22) + (0.0148442732f * fTemp20)) + (0.0341094136f * fTemp23))));
			float fTemp61 = (fConst22 * fRec94[1]);
			float fTemp62 = (fConst23 * fRec97[1]);
			fRec99[0] = (fTemp60 + (fTemp61 + (fRec99[1] + fTemp62)));
			fRec97[0] = fRec99[0];
			float fRec98 = ((fTemp62 + fTemp61) + fTemp60);
			fRec96[0] = (fRec97[0] + fRec96[1]);
			fRec94[0] = fRec96[0];
			float fRec95 = fRec98;
			fRec93[0] = (fTemp59 + (fRec95 + fRec93[1]));
			fRec91[0] = fRec93[0];
			float fRec92 = (fRec95 + fTemp59);
			float fTemp63 = (fConst25 * ((0.0477688611f * fTemp27) - ((0.0345717557f * fTemp29) + (0.00927145034f * fTemp28))));
			float fTemp64 = (fConst26 * fRec100[1]);
			fRec102[0] = (fTemp63 + (fRec102[1] + fTemp64));
			fRec100[0] = fRec102[0];
			float fRec101 = (fTemp64 + fTemp63);
			float fTemp65 = (fConst28 * (((0.01084367f * fTemp8) + (0.0319992863f * fTemp9)) - (((0.0561082587f * fTemp11) + (0.0150459614f * fTemp10)) + (0.0187615324f * fTemp12))));
			float fTemp66 = (fConst29 * fRec103[1]);
			float fTemp67 = (fConst30 * fRec106[1]);
			fRec108[0] = (fTemp65 + (fTemp66 + (fRec108[1] + fTemp67)));
			fRec106[0] = fRec108[0];
			float fRec107 = ((fTemp67 + fTemp66) + fTemp65);
			fRec105[0] = (fRec106[0] + fRec105[1]);
			fRec103[0] = fRec105[0];
			float fRec104 = fRec107;
			fVec4[(IOTA & 511)] = ((0.0361875407f * fTemp7) + (fRec92 + (fRec101 + fRec104)));
			output4[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec4[((IOTA - iConst31) & 511)])));
			float fTemp68 = (fConst33 * fRec109[1]);
			float fTemp69 = (fConst35 * (((0.0456504859f * fTemp20) + (1.72595992e-05f * fTemp23)) - (((((0.00845677312f * fTemp17) + (0.0472821817f * fTemp18)) + (0.0456476621f * fTemp22)) + (0.003242946f * fTemp19)) + (0.00843393337f * fTemp21))));
			float fTemp70 = (fConst36 * fRec112[1]);
			float fTemp71 = (fConst37 * fRec115[1]);
			fRec117[0] = (fTemp69 + (fTemp70 + (fRec117[1] + fTemp71)));
			fRec115[0] = fRec117[0];
			float fRec116 = ((fTemp71 + fTemp70) + fTemp69);
			fRec114[0] = (fRec115[0] + fRec114[1]);
			fRec112[0] = fRec114[0];
			float fRec113 = fRec116;
			fRec111[0] = (fTemp68 + (fRec113 + fRec111[1]));
			fRec109[0] = fRec111[0];
			float fRec110 = (fRec113 + fTemp68);
			float fTemp72 = (fConst39 * (((0.0571384393f * fTemp27) + (0.032984715f * fTemp28)) - (0.0329728834f * fTemp29)));
			float fTemp73 = (fConst40 * fRec118[1]);
			fRec120[0] = (fTemp72 + (fRec120[1] + fTemp73));
			fRec118[0] = fRec120[0];
			float fRec119 = (fTemp73 + fTemp72);
			float fTemp74 = (fConst42 * ((((0.0325779691f * fTemp9) + (0.0510265306f * fTemp10)) + (1.39711001e-05f * fTemp12)) - ((0.0269363876f * fTemp8) + (0.0510135181f * fTemp11))));
			float fTemp75 = (fConst43 * fRec121[1]);
			float fTemp76 = (fConst44 * fRec124[1]);
			fRec126[0] = (fTemp74 + (fTemp75 + (fRec126[1] + fTemp76)));
			fRec124[0] = fRec126[0];
			float fRec125 = ((fTemp76 + fTemp75) + fTemp74);
			fRec123[0] = (fRec124[0] + fRec123[1]);
			fRec121[0] = fRec123[0];
			float fRec122 = fRec125;
			fVec5[(IOTA & 511)] = ((0.04539131f * fTemp7) + (fRec110 + (fRec119 + fRec122)));
			output5[i] = FAUSTFLOAT((0.90871048f * (fRec0[0] * fVec5[((IOTA - iConst45) & 511)])));
			float fTemp77 = (fConst21 * (((((((0.00860633329f * fTemp17) + (0.0197001658f * fTemp18)) + (0.0148308799f * fTemp22)) + (0.00513769081f * fTemp19)) + (0.0553685278f * fTemp20)) + (0.0341309495f * fTemp23)) + (0.00860711467f * fTemp21)));
			float fTemp78 = (fConst23 * fRec133[1]);
			float fTemp79 = (fConst22 * fRec130[1]);
			fRec135[0] = (((fTemp77 + fRec135[1]) + fTemp78) + fTemp79);
			fRec133[0] = fRec135[0];
			float fRec134 = ((fTemp77 + fTemp78) + fTemp79);
			fRec132[0] = (fRec133[0] + fRec132[1]);
			fRec130[0] = fRec132[0];
			float fRec131 = fRec134;
			float fTemp80 = (fConst19 * fRec127[1]);
			fRec129[0] = ((fRec131 + fRec129[1]) + fTemp80);
			fRec127[0] = fRec129[0];
			float fRec128 = (fRec131 + fTemp80);
			float fTemp81 = (fConst25 * (((0.00926380046f * fTemp29) + (0.0477699935f * fTemp27)) + (0.0345857032f * fTemp28)));
			float fTemp82 = (fConst26 * fRec136[1]);
			fRec138[0] = (fTemp81 + (fRec138[1] + fTemp82));
			fRec136[0] = fRec138[0];
			float fRec137 = (fTemp82 + fTemp81);
			float fTemp83 = (fConst28 * (((((0.0108376313f * fTemp8) + (0.0150328474f * fTemp11)) + (0.0319898091f * fTemp9)) + (0.0561233684f * fTemp10)) + (0.0187769048f * fTemp12)));
			float fTemp84 = (fConst29 * fRec139[1]);
			float fTemp85 = (fConst30 * fRec142[1]);
			fRec144[0] = (fTemp83 + (fTemp84 + (fRec144[1] + fTemp85)));
			fRec142[0] = fRec144[0];
			float fRec143 = ((fTemp85 + fTemp84) + fTemp83);
			fRec141[0] = (fRec142[0] + fRec141[1]);
			fRec139[0] = fRec141[0];
			float fRec140 = fRec143;
			fVec6[(IOTA & 511)] = ((fRec128 + (fRec137 + fRec140)) + (0.0361924917f * fTemp7));
			output6[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec6[((IOTA - iConst31) & 511)])));
			float fTemp86 = (fConst47 * fRec145[1]);
			float fTemp87 = (fConst49 * ((0.00330808479f * fTemp21) - ((((((0.0327155329f * fTemp17) + (0.00467471452f * fTemp18)) + (0.00268839579f * fTemp22)) + (0.0242720768f * fTemp19)) + (0.00141779461f * fTemp20)) + (0.0364895128f * fTemp23))));
			float fTemp88 = (fConst50 * fRec148[1]);
			float fTemp89 = (fConst51 * fRec151[1]);
			fRec153[0] = (fTemp87 + (fTemp88 + (fRec153[1] + fTemp89)));
			fRec151[0] = fRec153[0];
			float fRec152 = ((fTemp89 + fTemp88) + fTemp87);
			fRec150[0] = (fRec151[0] + fRec150[1]);
			fRec148[0] = fRec150[0];
			float fRec149 = fRec152;
			fRec147[0] = (fTemp86 + (fRec149 + fRec147[1]));
			fRec145[0] = fRec147[0];
			float fRec146 = (fRec149 + fTemp86);
			float fTemp90 = (fConst53 * (((0.0394506529f * fTemp29) + (0.0185638908f * fTemp27)) - (0.00184053963f * fTemp28)));
			float fTemp91 = (fConst54 * fRec154[1]);
			fRec156[0] = (fTemp90 + (fRec156[1] + fTemp91));
			fRec154[0] = fRec156[0];
			float fRec155 = (fTemp91 + fTemp90);
			float fTemp92 = (fConst56 * ((0.034612678f * fTemp11) - ((((0.00298444019f * fTemp8) + (0.0117430286f * fTemp9)) + (0.00246379874f * fTemp10)) + (0.0370986424f * fTemp12))));
			float fTemp93 = (fConst57 * fRec157[1]);
			float fTemp94 = (fConst58 * fRec160[1]);
			fRec162[0] = (fTemp92 + (fTemp93 + (fRec162[1] + fTemp94)));
			fRec160[0] = fRec162[0];
			float fRec161 = ((fTemp94 + fTemp93) + fTemp92);
			fRec159[0] = (fRec160[0] + fRec159[1]);
			fRec157[0] = fRec159[0];
			float fRec158 = fRec161;
			fVec7[(IOTA & 511)] = ((0.0261618514f * fTemp7) + (fRec146 + (fRec155 + fRec158)));
			output7[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec7[((IOTA - iConst59) & 511)])));
			float fTemp95 = (fConst61 * fRec163[1]);
			float fTemp96 = (fConst63 * ((((0.00126608519f * fTemp17) + (0.0128872748f * fTemp20)) + (0.045305524f * fTemp21)) - ((((0.0274303351f * fTemp18) + (0.0228283945f * fTemp22)) + (0.0225832518f * fTemp19)) + (0.0141941234f * fTemp23))));
			float fTemp97 = (fConst64 * fRec166[1]);
			float fTemp98 = (fConst65 * fRec169[1]);
			fRec171[0] = (fTemp96 + (fTemp97 + (fRec171[1] + fTemp98)));
			fRec169[0] = fRec171[0];
			float fRec170 = ((fTemp98 + fTemp97) + fTemp96);
			fRec168[0] = (fRec169[0] + fRec168[1]);
			fRec166[0] = fRec168[0];
			float fRec167 = fRec170;
			fRec165[0] = (fTemp95 + (fRec167 + fRec165[1]));
			fRec163[0] = fRec165[0];
			float fRec164 = (fRec167 + fTemp95);
			float fTemp99 = (fConst67 * (((0.0445195064f * fTemp29) + (0.0137387449f * fTemp27)) - (0.0263461135f * fTemp28)));
			float fTemp100 = (fConst68 * fRec172[1]);
			fRec174[0] = (fTemp99 + (fRec174[1] + fTemp100));
			fRec172[0] = fRec174[0];
			float fRec173 = (fTemp100 + fTemp99);
			float fTemp101 = (fConst70 * ((0.0233319849f * fTemp11) - ((((0.0444219857f * fTemp8) + (0.0256232228f * fTemp9)) + (0.0142473914f * fTemp10)) + (0.0244926121f * fTemp12))));
			float fTemp102 = (fConst71 * fRec175[1]);
			float fTemp103 = (fConst72 * fRec178[1]);
			fRec180[0] = (fTemp101 + (fTemp102 + (fRec180[1] + fTemp103)));
			fRec178[0] = fRec180[0];
			float fRec179 = ((fTemp103 + fTemp102) + fTemp101);
			fRec177[0] = (fRec178[0] + fRec177[1]);
			fRec175[0] = fRec177[0];
			float fRec176 = fRec179;
			fVec8[(IOTA & 511)] = ((0.0323272236f * fTemp7) + (fRec164 + (fRec173 + fRec176)));
			output8[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec8[((IOTA - iConst73) & 511)])));
			float fTemp104 = (fConst61 * fRec181[1]);
			float fTemp105 = (fConst63 * (((0.0143728498f * fTemp23) + ((0.0368976034f * fTemp17) + (0.0165556017f * fTemp20))) - ((((0.0247016866f * fTemp18) + (0.00657225167f * fTemp22)) + (0.0205141604f * fTemp19)) + (0.00747417286f * fTemp21))));
			float fTemp106 = (fConst64 * fRec184[1]);
			float fTemp107 = (fConst65 * fRec187[1]);
			fRec189[0] = (fTemp105 + (fTemp106 + (fRec189[1] + fTemp107)));
			fRec187[0] = fRec189[0];
			float fRec188 = ((fTemp107 + fTemp106) + fTemp105);
			fRec186[0] = (fRec187[0] + fRec186[1]);
			fRec184[0] = fRec186[0];
			float fRec185 = fRec188;
			fRec183[0] = (fTemp104 + (fRec185 + fRec183[1]));
			fRec181[0] = fRec183[0];
			float fRec182 = (fRec185 + fTemp104);
			float fTemp108 = (fConst67 * (((0.0186304655f * fTemp29) + (0.0127383061f * fTemp27)) - (0.0369097739f * fTemp28)));
			float fTemp109 = (fConst68 * fRec190[1]);
			fRec192[0] = (fTemp108 + (fRec192[1] + fTemp109));
			fRec190[0] = fRec192[0];
			float fRec191 = (fTemp109 + fTemp108);
			float fTemp110 = (fConst70 * (((0.012669703f * fTemp11) + (0.0246489197f * fTemp12)) - (((0.0326845907f * fTemp8) + (0.019024346f * fTemp9)) + (0.0218561292f * fTemp10))));
			float fTemp111 = (fConst71 * fRec193[1]);
			float fTemp112 = (fConst72 * fRec196[1]);
			fRec198[0] = (fTemp110 + (fTemp111 + (fRec198[1] + fTemp112)));
			fRec196[0] = fRec198[0];
			float fRec197 = ((fTemp112 + fTemp111) + fTemp110);
			fRec195[0] = (fRec196[0] + fRec195[1]);
			fRec193[0] = fRec195[0];
			float fRec194 = fRec197;
			fVec9[(IOTA & 511)] = ((0.0259500761f * fTemp7) + (fRec182 + (fRec191 + fRec194)));
			output9[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec9[((IOTA - iConst73) & 511)])));
			float fTemp113 = (fConst47 * fRec199[1]);
			float fTemp114 = (fConst49 * (((0.0364695266f * fTemp23) + (((0.00330042141f * fTemp17) + (0.00141847541f * fTemp22)) + (0.00270266877f * fTemp20))) - (((0.00467247749f * fTemp18) + (0.0242545679f * fTemp19)) + (0.0327177942f * fTemp21))));
			float fTemp115 = (fConst50 * fRec202[1]);
			float fTemp116 = (fConst51 * fRec205[1]);
			fRec207[0] = (fTemp114 + (fTemp115 + (fRec207[1] + fTemp116)));
			fRec205[0] = fRec207[0];
			float fRec206 = ((fTemp116 + fTemp115) + fTemp114);
			fRec204[0] = (fRec205[0] + fRec204[1]);
			fRec202[0] = fRec204[0];
			float fRec203 = fRec206;
			fRec201[0] = (fTemp113 + (fRec203 + fRec201[1]));
			fRec199[0] = fRec201[0];
			float fRec200 = (fRec203 + fTemp113);
			float fTemp117 = (fConst53 * (((0.00183700956f * fTemp29) + (0.0185472425f * fTemp27)) - (0.0394348688f * fTemp28)));
			float fTemp118 = (fConst54 * fRec208[1]);
			fRec210[0] = (fTemp117 + (fRec210[1] + fTemp118));
			fRec208[0] = fRec210[0];
			float fRec209 = (fTemp118 + fTemp117);
			float fTemp119 = (fConst56 * (((0.00246185367f * fTemp11) + (0.0370914005f * fTemp12)) - (((0.00297827949f * fTemp8) + (0.0117452387f * fTemp9)) + (0.0345855802f * fTemp10))));
			float fTemp120 = (fConst57 * fRec211[1]);
			float fTemp121 = (fConst58 * fRec214[1]);
			fRec216[0] = (fTemp119 + (fTemp120 + (fRec216[1] + fTemp121)));
			fRec214[0] = fRec216[0];
			float fRec215 = ((fTemp121 + fTemp120) + fTemp119);
			fRec213[0] = (fRec214[0] + fRec213[1]);
			fRec211[0] = fRec213[0];
			float fRec212 = fRec215;
			fVec10[(IOTA & 511)] = ((0.0261483416f * fTemp7) + (fRec200 + (fRec209 + fRec212)));
			output10[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec10[((IOTA - iConst59) & 511)])));
			float fTemp122 = (fConst61 * fRec217[1]);
			float fTemp123 = (fConst63 * (((0.0168354791f * fTemp23) + (((0.0232635159f * fTemp18) + (0.00700940285f * fTemp22)) + (0.0163048115f * fTemp20))) - (((0.0368799195f * fTemp17) + (0.0205050856f * fTemp19)) + (0.0100207152f * fTemp21))));
			float fTemp124 = (fConst64 * fRec220[1]);
			float fTemp125 = (fConst65 * fRec223[1]);
			fRec225[0] = (fTemp123 + (fTemp124 + (fRec225[1] + fTemp125)));
			fRec223[0] = fRec225[0];
			float fRec224 = ((fTemp125 + fTemp124) + fTemp123);
			fRec222[0] = (fRec223[0] + fRec222[1]);
			fRec220[0] = fRec222[0];
			float fRec221 = fRec224;
			fRec219[0] = (fTemp122 + (fRec221 + fRec219[1]));
			fRec217[0] = fRec219[0];
			float fRec218 = (fRec221 + fTemp122);
			float fTemp126 = (fConst67 * ((0.0127308108f * fTemp27) - ((0.0176555794f * fTemp29) + (0.0374553464f * fTemp28))));
			float fTemp127 = (fConst68 * fRec226[1]);
			fRec228[0] = (fTemp126 + (fRec228[1] + fTemp127));
			fRec226[0] = fRec228[0];
			float fRec227 = (fTemp127 + fTemp126);
			float fTemp128 = (fConst70 * (((0.0316663086f * fTemp8) + (0.0263799429f * fTemp12)) - (((0.011496013f * fTemp11) + (0.0190213211f * fTemp9)) + (0.0225178245f * fTemp10))));
			float fTemp129 = (fConst71 * fRec229[1]);
			float fTemp130 = (fConst72 * fRec232[1]);
			fRec234[0] = (fTemp128 + (fTemp129 + (fRec234[1] + fTemp130)));
			fRec232[0] = fRec234[0];
			float fRec233 = ((fTemp130 + fTemp129) + fTemp128);
			fRec231[0] = (fRec232[0] + fRec231[1]);
			fRec229[0] = fRec231[0];
			float fRec230 = fRec233;
			fVec11[(IOTA & 511)] = ((0.0259405281f * fTemp7) + (fRec218 + (fRec227 + fRec230)));
			output11[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec11[((IOTA - iConst73) & 511)])));
			float fTemp131 = (fConst63 * (((((0.0379067026f * fTemp18) + (0.0179398041f * fTemp22)) + (0.008722906f * fTemp20)) + (0.0495633371f * fTemp21)) - ((0.0164802484f * fTemp23) + ((0.0033218693f * fTemp17) + (0.0284428578f * fTemp19)))));
			float fTemp132 = (fConst65 * fRec241[1]);
			float fTemp133 = (fConst64 * fRec238[1]);
			fRec243[0] = (((fTemp131 + fRec243[1]) + fTemp132) + fTemp133);
			fRec241[0] = fRec243[0];
			float fRec242 = ((fTemp131 + fTemp132) + fTemp133);
			fRec240[0] = (fRec241[0] + fRec240[1]);
			fRec238[0] = fRec240[0];
			float fRec239 = fRec242;
			float fTemp134 = (fConst61 * fRec235[1]);
			fRec237[0] = ((fRec239 + fRec237[1]) + fTemp134);
			fRec235[0] = fRec237[0];
			float fRec236 = (fRec239 + fTemp134);
			float fTemp135 = (fConst67 * ((0.0205770936f * fTemp27) - ((0.0513798967f * fTemp29) + (0.0317948349f * fTemp28))));
			float fTemp136 = (fConst68 * fRec244[1]);
			fRec246[0] = (fTemp135 + (fRec246[1] + fTemp136));
			fRec244[0] = fRec246[0];
			float fRec245 = (fTemp136 + fTemp135);
			float fTemp137 = (fConst70 * ((0.0511338785f * fTemp8) - ((((0.0323468074f * fTemp11) + (0.024969304f * fTemp9)) + (0.0215231236f * fTemp10)) + (0.026064856f * fTemp12))));
			float fTemp138 = (fConst71 * fRec247[1]);
			float fTemp139 = (fConst72 * fRec250[1]);
			fRec252[0] = (fTemp137 + (fTemp138 + (fRec252[1] + fTemp139)));
			fRec250[0] = fRec252[0];
			float fRec251 = ((fTemp139 + fTemp138) + fTemp137);
			fRec249[0] = (fRec250[0] + fRec249[1]);
			fRec247[0] = fRec249[0];
			float fRec248 = fRec251;
			fVec12[(IOTA & 511)] = ((fRec236 + (fRec245 + fRec248)) + (0.0389448367f * fTemp7));
			output12[i] = FAUSTFLOAT((0.941085756f * (fVec12[((IOTA - iConst73) & 511)] * fRec0[0])));
			float fTemp140 = (fConst47 * fRec253[1]);
			float fTemp141 = (fConst49 * ((((0.0327142365f * fTemp17) + (0.00268353708f * fTemp22)) + (0.00141352706f * fTemp20)) - ((0.00332705281f * fTemp21) + (((0.00468599051f * fTemp18) + (0.0242712181f * fTemp19)) + (0.0364912525f * fTemp23)))));
			float fTemp142 = (fConst50 * fRec256[1]);
			float fTemp143 = (fConst51 * fRec259[1]);
			fRec261[0] = (fTemp141 + (fTemp142 + (fRec261[1] + fTemp143)));
			fRec259[0] = fRec261[0];
			float fRec260 = ((fTemp143 + fTemp142) + fTemp141);
			fRec258[0] = (fRec258[1] + fRec259[0]);
			fRec256[0] = fRec258[0];
			float fRec257 = fRec260;
			fRec255[0] = ((fRec255[1] + fTemp140) + fRec257);
			fRec253[0] = fRec255[0];
			float fRec254 = (fTemp140 + fRec257);
			float fTemp144 = (fConst53 * (((0.0185660571f * fTemp27) + (0.00184788718f * fTemp28)) - (0.0394531265f * fTemp29)));
			float fTemp145 = (fConst54 * fRec262[1]);
			fRec264[0] = (fTemp144 + (fRec264[1] + fTemp145));
			fRec262[0] = fRec264[0];
			float fRec263 = (fTemp145 + fTemp144);
			float fTemp146 = (fConst56 * ((0.00246844557f * fTemp10) - ((((0.0029989176f * fTemp8) + (0.0346155316f * fTemp11)) + (0.0117413951f * fTemp9)) + (0.0370995589f * fTemp12))));
			float fTemp147 = (fConst57 * fRec265[1]);
			float fTemp148 = (fConst58 * fRec268[1]);
			fRec270[0] = (fTemp146 + (fTemp147 + (fRec270[1] + fTemp148)));
			fRec268[0] = fRec270[0];
			float fRec269 = ((fTemp148 + fTemp147) + fTemp146);
			fRec267[0] = (fRec267[1] + fRec268[0]);
			fRec265[0] = fRec267[0];
			float fRec266 = fRec269;
			fVec13[(IOTA & 511)] = ((0.0261641443f * fTemp7) + ((fRec254 + fRec263) + fRec266));
			output13[i] = FAUSTFLOAT((0.940535188f * (fVec13[((IOTA - iConst59) & 511)] * fRec0[0])));
			float fTemp149 = (fConst67 * (((0.0127271321f * fTemp27) + (0.0186162814f * fTemp28)) - (0.0369095467f * fTemp29)));
			float fTemp150 = (fConst68 * fRec271[1]);
			fRec273[0] = (fTemp149 + (fRec273[1] + fTemp150));
			fRec271[0] = fRec273[0];
			float fRec272 = (fTemp150 + fTemp149);
			float fTemp151 = (fConst70 * ((0.0126558729f * fTemp10) - ((((0.0326672122f * fTemp8) + (0.0218386911f * fTemp11)) + (0.0190253519f * fTemp9)) + (0.0246668719f * fTemp12))));
			float fTemp152 = (fConst71 * fRec274[1]);
			float fTemp153 = (fConst72 * fRec277[1]);
			fRec279[0] = (fTemp151 + (fTemp152 + (fRec279[1] + fTemp153)));
			fRec277[0] = fRec279[0];
			float fRec278 = ((fTemp153 + fTemp152) + fTemp151);
			fRec276[0] = (fRec276[1] + fRec277[0]);
			fRec274[0] = fRec276[0];
			float fRec275 = fRec278;
			float fTemp154 = (fConst61 * fRec280[1]);
			float fTemp155 = (fConst63 * (((0.0075061284f * fTemp17) + (0.0165674873f * fTemp22)) - ((0.0368934311f * fTemp21) + ((((0.0246765427f * fTemp18) + (0.02049651f * fTemp19)) + (0.00656695059f * fTemp20)) + (0.0143680032f * fTemp23)))));
			float fTemp156 = (fConst64 * fRec283[1]);
			float fTemp157 = (fConst65 * fRec286[1]);
			fRec288[0] = (fTemp155 + (fTemp156 + (fRec288[1] + fTemp157)));
			fRec286[0] = fRec288[0];
			float fRec287 = ((fTemp157 + fTemp156) + fTemp155);
			fRec285[0] = (fRec285[1] + fRec286[0]);
			fRec283[0] = fRec285[0];
			float fRec284 = fRec287;
			fRec282[0] = ((fRec282[1] + fTemp154) + fRec284);
			fRec280[0] = fRec282[0];
			float fRec281 = (fTemp154 + fRec284);
			fVec14[(IOTA & 511)] = ((0.0259447377f * fTemp7) + ((fRec272 + fRec275) + fRec281));
			output14[i] = FAUSTFLOAT((0.941085756f * (fVec14[((IOTA - iConst73) & 511)] * fRec0[0])));
			float fTemp158 = (fConst67 * (((0.0137484306f * fTemp27) + (0.0445271879f * fTemp28)) - (0.0263485294f * fTemp29)));
			float fTemp159 = (fConst68 * fRec289[1]);
			fRec291[0] = (fTemp158 + (fRec291[1] + fTemp159));
			fRec289[0] = fRec291[0];
			float fRec290 = (fTemp159 + fTemp158);
			float fTemp160 = (fConst70 * (((0.023348514f * fTemp10) + (0.0244967658f * fTemp12)) - (((0.0444244258f * fTemp8) + (0.0142523237f * fTemp11)) + (0.0256209057f * fTemp9))));
			float fTemp161 = (fConst72 * fRec295[1]);
			float fTemp162 = (fConst71 * fRec292[1]);
			fRec297[0] = (((fTemp160 + fRec297[1]) + fTemp161) + fTemp162);
			fRec295[0] = fRec297[0];
			float fRec296 = ((fTemp160 + fTemp161) + fTemp162);
			fRec294[0] = (fRec295[0] + fRec294[1]);
			fRec292[0] = fRec294[0];
			float fRec293 = fRec296;
			float fTemp163 = (fConst61 * fRec298[1]);
			float fTemp164 = (fConst63 * (((0.0128821284f * fTemp22) + (0.014208056f * fTemp23)) - (((((0.0453056693f * fTemp17) + (0.0274373721f * fTemp18)) + (0.0225955732f * fTemp19)) + (0.0228183232f * fTemp20)) + (0.00126505631f * fTemp21))));
			float fTemp165 = (fConst64 * fRec301[1]);
			float fTemp166 = (fConst65 * fRec304[1]);
			fRec306[0] = (fTemp164 + (fTemp165 + (fRec306[1] + fTemp166)));
			fRec304[0] = fRec306[0];
			float fRec305 = ((fTemp166 + fTemp165) + fTemp164);
			fRec303[0] = (fRec303[1] + fRec304[0]);
			fRec301[0] = fRec303[0];
			float fRec302 = fRec305;
			fRec300[0] = ((fRec300[1] + fTemp163) + fRec302);
			fRec298[0] = fRec300[0];
			float fRec299 = (fTemp163 + fRec302);
			fVec15[(IOTA & 511)] = (((fRec290 + fRec293) + fRec299) + (0.0323338211f * fTemp7));
			output15[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec15[((IOTA - iConst73) & 511)])));
			float fTemp167 = (fConst53 * (((0.0185498931f * fTemp27) + (0.0394535773f * fTemp28)) - (0.00184174115f * fTemp29)));
			float fTemp168 = (fConst54 * fRec307[1]);
			fRec309[0] = (fTemp167 + (fRec309[1] + fTemp168));
			fRec307[0] = fRec309[0];
			float fRec308 = (fTemp168 + fTemp167);
			float fTemp169 = (fConst56 * (((0.0345902257f * fTemp10) + (0.0371084623f * fTemp12)) - (((0.00298590446f * fTemp8) + (0.00246753893f * fTemp11)) + (0.0117560346f * fTemp9))));
			float fTemp170 = (fConst57 * fRec310[1]);
			float fTemp171 = (fConst58 * fRec313[1]);
			fRec315[0] = (fTemp169 + (fTemp170 + (fRec315[1] + fTemp171)));
			fRec313[0] = fRec315[0];
			float fRec314 = ((fTemp171 + fTemp170) + fTemp169);
			fRec312[0] = (fRec312[1] + fRec313[0]);
			fRec310[0] = fRec312[0];
			float fRec311 = fRec314;
			float fTemp172 = (fConst47 * fRec316[1]);
			float fTemp173 = (fConst49 * (((0.03647165f * fTemp23) + (0.0327316634f * fTemp21)) - (((((0.00330823287f * fTemp17) + (0.00468324311f * fTemp18)) + (0.00142077159f * fTemp22)) + (0.0242597535f * fTemp19)) + (0.00271490379f * fTemp20))));
			float fTemp174 = (fConst50 * fRec319[1]);
			float fTemp175 = (fConst51 * fRec322[1]);
			fRec324[0] = (fTemp173 + (fTemp174 + (fRec324[1] + fTemp175)));
			fRec322[0] = fRec324[0];
			float fRec323 = ((fTemp175 + fTemp174) + fTemp173);
			fRec321[0] = (fRec321[1] + fRec322[0]);
			fRec319[0] = fRec321[0];
			float fRec320 = fRec323;
			fRec318[0] = ((fRec318[1] + fTemp172) + fRec320);
			fRec316[0] = fRec318[0];
			float fRec317 = (fTemp172 + fRec320);
			fVec16[(IOTA & 511)] = ((0.0261601638f * fTemp7) + ((fRec308 + fRec311) + fRec317));
			output16[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec16[((IOTA - iConst59) & 511)])));
			float fTemp176 = (fConst67 * (((0.0318024792f * fTemp29) + (0.0205848552f * fTemp27)) + (0.0513721406f * fTemp28)));
			float fTemp177 = (fConst68 * fRec325[1]);
			fRec327[0] = (fTemp176 + (fRec327[1] + fTemp177));
			fRec325[0] = fRec327[0];
			float fRec326 = (fTemp177 + fTemp176);
			float fTemp178 = (fConst70 * (((((0.0511377268f * fTemp8) + (0.0215313174f * fTemp11)) + (0.032357011f * fTemp10)) + (0.0260451864f * fTemp12)) - (0.0249631405f * fTemp9)));
			float fTemp179 = (fConst71 * fRec328[1]);
			float fTemp180 = (fConst72 * fRec331[1]);
			fRec333[0] = (fTemp178 + (fTemp179 + (fRec333[1] + fTemp180)));
			fRec331[0] = fRec333[0];
			float fRec332 = ((fTemp180 + fTemp179) + fTemp178);
			fRec330[0] = (fRec330[1] + fRec331[0]);
			fRec328[0] = fRec330[0];
			float fRec329 = fRec332;
			float fTemp181 = (fConst61 * fRec334[1]);
			float fTemp182 = (fConst63 * ((((0.0495547503f * fTemp17) + (0.0379152559f * fTemp18)) + (0.016481569f * fTemp23)) - ((((0.00872249622f * fTemp22) + (0.0284533314f * fTemp19)) + (0.0179251432f * fTemp20)) + (0.00334320101f * fTemp21))));
			float fTemp183 = (fConst64 * fRec337[1]);
			float fTemp184 = (fConst65 * fRec340[1]);
			fRec342[0] = (fTemp182 + (fTemp183 + (fRec342[1] + fTemp184)));
			fRec340[0] = fRec342[0];
			float fRec341 = ((fTemp184 + fTemp183) + fTemp182);
			fRec339[0] = (fRec339[1] + fRec340[0]);
			fRec337[0] = fRec339[0];
			float fRec338 = fRec341;
			fRec336[0] = ((fRec336[1] + fTemp181) + fRec338);
			fRec334[0] = fRec336[0];
			float fRec335 = (fTemp181 + fRec338);
			fVec17[(IOTA & 511)] = ((0.038944684f * fTemp7) + ((fRec326 + fRec329) + fRec335));
			output17[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec17[((IOTA - iConst73) & 511)])));
			float fTemp185 = (fConst67 * (((0.0374534018f * fTemp29) + (0.0127278063f * fTemp27)) + (0.0176651441f * fTemp28)));
			float fTemp186 = (fConst68 * fRec343[1]);
			fRec345[0] = (fTemp185 + (fRec345[1] + fTemp186));
			fRec343[0] = fRec345[0];
			float fRec344 = (fTemp186 + fTemp185);
			float fTemp187 = (fConst70 * ((((0.0316778682f * fTemp8) + (0.0225088652f * fTemp11)) + (0.0115001611f * fTemp10)) - ((0.0190254468f * fTemp9) + (0.0263673887f * fTemp12))));
			float fTemp188 = (fConst71 * fRec346[1]);
			float fTemp189 = (fConst72 * fRec349[1]);
			fRec351[0] = (fTemp187 + (fTemp188 + (fRec351[1] + fTemp189)));
			fRec349[0] = fRec351[0];
			float fRec350 = ((fTemp189 + fTemp188) + fTemp187);
			fRec348[0] = (fRec348[1] + fRec349[0]);
			fRec346[0] = fRec348[0];
			float fRec347 = fRec350;
			float fTemp190 = (fConst63 * ((0.0232655909f * fTemp18) - ((0.0368838497f * fTemp21) + (((((0.0100020012f * fTemp17) + (0.0163094383f * fTemp22)) + (0.0205040593f * fTemp19)) + (0.00701622479f * fTemp20)) + (0.0168153476f * fTemp23)))));
			float fTemp191 = (fConst64 * fRec355[1]);
			float fTemp192 = (fConst65 * fRec358[1]);
			fRec360[0] = (fTemp190 + (fTemp191 + (fRec360[1] + fTemp192)));
			fRec358[0] = fRec360[0];
			float fRec359 = ((fTemp192 + fTemp191) + fTemp190);
			fRec357[0] = (fRec357[1] + fRec358[0]);
			fRec355[0] = fRec357[0];
			float fRec356 = fRec359;
			float fTemp193 = (fConst61 * fRec352[1]);
			fRec354[0] = (fRec356 + (fRec354[1] + fTemp193));
			fRec352[0] = fRec354[0];
			float fRec353 = (fTemp193 + fRec356);
			fVec18[(IOTA & 511)] = ((0.0259419568f * fTemp7) + ((fRec344 + fRec347) + fRec353));
			output18[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec18[((IOTA - iConst73) & 511)])));
			float fTemp194 = (fConst75 * fRec361[1]);
			float fTemp195 = (fConst77 * (((1.10768997e-05f * fTemp21) + (((9.5419e-06f * fTemp18) + (0.0177717861f * fTemp19)) + (0.0421755761f * fTemp23))) - (((0.0680814683f * fTemp17) + (0.01865343f * fTemp22)) + (4.70539999e-06f * fTemp20))));
			float fTemp196 = (fConst78 * fRec364[1]);
			float fTemp197 = (fConst79 * fRec367[1]);
			fRec369[0] = (fTemp195 + (fTemp196 + (fRec369[1] + fTemp197)));
			fRec367[0] = fRec369[0];
			float fRec368 = ((fTemp197 + fTemp196) + fTemp195);
			fRec366[0] = (fRec367[0] + fRec366[1]);
			fRec364[0] = fRec366[0];
			float fRec365 = fRec368;
			fRec363[0] = (fTemp194 + (fRec365 + fRec363[1]));
			fRec361[0] = fRec363[0];
			float fRec362 = (fRec365 + fTemp194);
			float fTemp198 = (fConst81 * fRec370[1]);
			float fTemp199 = (fConst82 * ((0.0827766433f * fTemp29) - ((0.0273921341f * fTemp27) + (7.02950001e-06f * fTemp28))));
			fRec372[0] = ((fRec372[1] + fTemp198) + fTemp199);
			fRec370[0] = fRec372[0];
			float fRec371 = (fTemp198 + fTemp199);
			float fTemp200 = (fConst84 * ((7.08849984e-06f * fTemp10) - ((((1.05427998e-05f * fTemp8) + (0.0438432321f * fTemp11)) + (0.0262416285f * fTemp9)) + (0.0779453963f * fTemp12))));
			float fTemp201 = (fConst85 * fRec373[1]);
			float fTemp202 = (fConst86 * fRec376[1]);
			fRec378[0] = (fTemp200 + (fTemp201 + (fRec378[1] + fTemp202)));
			fRec376[0] = fRec378[0];
			float fRec377 = ((fTemp202 + fTemp201) + fTemp200);
			fRec375[0] = (fRec375[1] + fRec376[0]);
			fRec373[0] = fRec375[0];
			float fRec374 = fRec377;
			fVec19[0] = ((0.055423025f * fTemp7) + (fRec362 + (fRec371 + fRec374)));
			output19[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec19[iConst87])));
			float fTemp203 = (fConst89 * fRec379[1]);
			float fTemp204 = (fConst91 * (((0.0524279959f * fTemp21) + ((0.0237775631f * fTemp23) + (((0.0411787964f * fTemp18) + (0.0224569999f * fTemp19)) + (0.00144205324f * fTemp20)))) - ((3.28360011e-06f * fTemp17) + (0.00249966024f * fTemp22))));
			float fTemp205 = (fConst92 * fRec382[1]);
			float fTemp206 = (fConst93 * fRec385[1]);
			fRec387[0] = (fTemp204 + (fTemp205 + (fRec387[1] + fTemp206)));
			fRec385[0] = fRec387[0];
			float fRec386 = ((fTemp206 + fTemp205) + fTemp204);
			fRec384[0] = (fRec385[0] + fRec384[1]);
			fRec382[0] = fRec384[0];
			float fRec383 = fRec386;
			fRec381[0] = (fTemp203 + (fRec383 + fRec381[1]));
			fRec379[0] = fRec381[0];
			float fRec380 = (fRec383 + fTemp203);
			float fTemp207 = (fConst95 * ((0.0546924546f * fTemp29) - ((0.0296233296f * fTemp27) + (0.0315752216f * fTemp28))));
			float fTemp208 = (fConst96 * fRec388[1]);
			fRec390[0] = (fTemp207 + (fRec390[1] + fTemp208));
			fRec388[0] = fRec390[0];
			float fRec389 = (fTemp208 + fTemp207);
			float fTemp209 = (fConst98 * ((0.0242707804f * fTemp10) - ((((0.0511472188f * fTemp8) + (0.0420402549f * fTemp11)) + (0.0140019646f * fTemp9)) + (0.0295328442f * fTemp12))));
			float fTemp210 = (fConst99 * fRec391[1]);
			float fTemp211 = (fConst100 * fRec394[1]);
			fRec396[0] = (fTemp209 + (fTemp210 + (fRec396[1] + fTemp211)));
			fRec394[0] = fRec396[0];
			float fRec395 = ((fTemp211 + fTemp210) + fTemp209);
			fRec393[0] = (fRec394[0] + fRec393[1]);
			fRec391[0] = fRec393[0];
			float fRec392 = fRec395;
			output20[i] = FAUSTFLOAT((fRec0[0] * ((0.0434079953f * fTemp7) + (fRec380 + (fRec389 + fRec392)))));
			float fTemp212 = (fConst89 * fRec397[1]);
			float fTemp213 = (fConst91 * ((((((0.0602632426f * fTemp17) + (0.0381556787f * fTemp18)) + (0.0201204065f * fTemp19)) + (0.00823509041f * fTemp20)) + (0.00822857302f * fTemp21)) - ((0.00728519401f * fTemp22) + (0.0236924179f * fTemp23))));
			float fTemp214 = (fConst92 * fRec400[1]);
			float fTemp215 = (fConst93 * fRec403[1]);
			fRec405[0] = (fTemp213 + (fTemp214 + (fRec405[1] + fTemp215)));
			fRec403[0] = fRec405[0];
			float fRec404 = ((fTemp215 + fTemp214) + fTemp213);
			fRec402[0] = (fRec403[0] + fRec402[1]);
			fRec400[0] = fRec402[0];
			float fRec401 = fRec404;
			fRec399[0] = (fTemp212 + (fRec401 + fRec399[1]));
			fRec397[0] = fRec399[0];
			float fRec398 = (fRec401 + fTemp212);
			float fTemp216 = (fConst95 * ((0.0388471968f * fTemp29) - ((0.0285362061f * fTemp27) + (0.0618594699f * fTemp28))));
			float fTemp217 = (fConst96 * fRec406[1]);
			fRec408[0] = (fTemp216 + (fRec408[1] + fTemp217));
			fRec406[0] = fRec408[0];
			float fRec407 = (fTemp217 + fTemp216);
			float fTemp218 = (fConst98 * (((0.0403285064f * fTemp10) + (0.0293631945f * fTemp12)) - (((0.0621728152f * fTemp8) + (0.0226049349f * fTemp11)) + (0.0201213304f * fTemp9))));
			float fTemp219 = (fConst99 * fRec409[1]);
			float fTemp220 = (fConst100 * fRec412[1]);
			fRec414[0] = (fTemp218 + (fTemp219 + (fRec414[1] + fTemp220)));
			fRec412[0] = fRec414[0];
			float fRec413 = ((fTemp220 + fTemp219) + fTemp218);
			fRec411[0] = (fRec412[0] + fRec411[1]);
			fRec409[0] = fRec411[0];
			float fRec410 = fRec413;
			output21[i] = FAUSTFLOAT((fRec0[0] * ((0.0494420342f * fTemp7) + (fRec398 + (fRec407 + fRec410)))));
			float fTemp221 = (fConst75 * fRec415[1]);
			float fTemp222 = (fConst77 * (((((6.41730003e-06f * fTemp17) + (1.38580003e-06f * fTemp18)) + (0.0177688692f * fTemp19)) + (0.0186544098f * fTemp20)) - ((0.0680964366f * fTemp21) + ((3.25449992e-06f * fTemp22) + (0.0421651751f * fTemp23)))));
			float fTemp223 = (fConst78 * fRec418[1]);
			float fTemp224 = (fConst79 * fRec421[1]);
			fRec423[0] = (fTemp222 + (fTemp223 + (fRec423[1] + fTemp224)));
			fRec421[0] = fRec423[0];
			float fRec422 = ((fTemp224 + fTemp223) + fTemp222);
			fRec420[0] = (fRec421[0] + fRec420[1]);
			fRec418[0] = fRec420[0];
			float fRec419 = fRec422;
			fRec417[0] = (fTemp221 + (fRec419 + fRec417[1]));
			fRec415[0] = fRec417[0];
			float fRec416 = (fRec419 + fTemp221);
			float fTemp225 = (fConst82 * ((2.4369001e-06f * fTemp29) - ((0.0273813531f * fTemp27) + (0.082773298f * fTemp28))));
			float fTemp226 = (fConst81 * fRec424[1]);
			fRec426[0] = (fTemp225 + (fRec426[1] + fTemp226));
			fRec424[0] = fRec426[0];
			float fRec425 = (fTemp226 + fTemp225);
			float fTemp227 = (fConst84 * (((0.0438312702f * fTemp10) + (0.0779497474f * fTemp12)) - (((5.10100017e-06f * fTemp8) + (1.33499995e-07f * fTemp11)) + (0.026245553f * fTemp9))));
			float fTemp228 = (fConst85 * fRec427[1]);
			float fTemp229 = (fConst86 * fRec430[1]);
			fRec432[0] = (fTemp227 + (fTemp228 + (fRec432[1] + fTemp229)));
			fRec430[0] = fRec432[0];
			float fRec431 = ((fTemp229 + fTemp228) + fTemp227);
			fRec429[0] = (fRec430[0] + fRec429[1]);
			fRec427[0] = fRec429[0];
			float fRec428 = fRec431;
			fVec20[0] = ((0.0554167405f * fTemp7) + (fRec416 + (fRec425 + fRec428)));
			output22[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec20[iConst87])));
			float fTemp230 = (fConst89 * fRec433[1]);
			float fTemp231 = (fConst91 * (((((0.00730328076f * fTemp22) + (0.020125458f * fTemp19)) + (0.00825409591f * fTemp20)) + (0.00823290274f * fTemp21)) - (((0.0602913089f * fTemp17) + (0.0381317139f * fTemp18)) + (0.0236870889f * fTemp23))));
			float fTemp232 = (fConst92 * fRec436[1]);
			float fTemp233 = (fConst93 * fRec439[1]);
			fRec441[0] = (fTemp231 + (fTemp232 + (fRec441[1] + fTemp233)));
			fRec439[0] = fRec441[0];
			float fRec440 = ((fTemp233 + fTemp232) + fTemp231);
			fRec438[0] = (fRec439[0] + fRec438[1]);
			fRec436[0] = fRec438[0];
			float fRec437 = fRec440;
			fRec435[0] = (fTemp230 + (fRec437 + fRec435[1]));
			fRec433[0] = fRec435[0];
			float fRec434 = (fRec437 + fTemp230);
			float fTemp234 = (fConst95 * (0.0f - (((0.0388576426f * fTemp29) + (0.0285068844f * fTemp27)) + (0.0618772283f * fTemp28))));
			float fTemp235 = (fConst96 * fRec442[1]);
			fRec444[0] = (fTemp234 + (fRec444[1] + fTemp235));
			fRec442[0] = fRec444[0];
			float fRec443 = (fTemp235 + fTemp234);
			float fTemp236 = (fConst98 * (((((0.0621965267f * fTemp8) + (0.0225855913f * fTemp11)) + (0.0403064527f * fTemp10)) + (0.0293740258f * fTemp12)) - (0.0201558322f * fTemp9)));
			float fTemp237 = (fConst99 * fRec445[1]);
			float fTemp238 = (fConst100 * fRec448[1]);
			fRec450[0] = (fTemp236 + (fTemp237 + (fRec450[1] + fTemp238)));
			fRec448[0] = fRec450[0];
			float fRec449 = ((fTemp238 + fTemp237) + fTemp236);
			fRec447[0] = (fRec448[0] + fRec447[1]);
			fRec445[0] = fRec447[0];
			float fRec446 = fRec449;
			output23[i] = FAUSTFLOAT((fRec0[0] * ((0.0494453609f * fTemp7) + (fRec434 + (fRec443 + fRec446)))));
			float fTemp239 = (fConst89 * fRec451[1]);
			float fTemp240 = (fConst91 * (((0.0524208769f * fTemp21) + ((0.0237827767f * fTemp23) + ((((7.98969995e-06f * fTemp17) + (0.00248825992f * fTemp22)) + (0.0224375706f * fTemp19)) + (0.00143079273f * fTemp20)))) - (0.0411872156f * fTemp18)));
			float fTemp241 = (fConst92 * fRec454[1]);
			float fTemp242 = (fConst93 * fRec457[1]);
			fRec459[0] = (fTemp240 + (fTemp241 + (fRec459[1] + fTemp242)));
			fRec457[0] = fRec459[0];
			float fRec458 = ((fTemp242 + fTemp241) + fTemp240);
			fRec456[0] = (fRec457[0] + fRec456[1]);
			fRec454[0] = fRec456[0];
			float fRec455 = fRec458;
			fRec453[0] = (fTemp239 + (fRec455 + fRec453[1]));
			fRec451[0] = fRec453[0];
			float fRec452 = (fRec455 + fTemp239);
			float fTemp243 = (fConst95 * (0.0f - (((0.0546927527f * fTemp29) + (0.0296443459f * fTemp27)) + (0.0315747596f * fTemp28))));
			float fTemp244 = (fConst96 * fRec460[1]);
			fRec462[0] = (fTemp243 + (fRec462[1] + fTemp244));
			fRec460[0] = fRec462[0];
			float fRec461 = (fTemp244 + fTemp243);
			float fTemp245 = (fConst98 * ((((0.0511424206f * fTemp8) + (0.0420513675f * fTemp11)) + (0.0242786594f * fTemp10)) - ((0.0139776776f * fTemp9) + (0.0295330938f * fTemp12))));
			float fTemp246 = (fConst99 * fRec463[1]);
			float fTemp247 = (fConst100 * fRec466[1]);
			fRec468[0] = (fTemp245 + (fTemp246 + (fRec468[1] + fTemp247)));
			fRec466[0] = fRec468[0];
			float fRec467 = ((fTemp247 + fTemp246) + fTemp245);
			fRec465[0] = (fRec466[0] + fRec465[1]);
			fRec463[0] = fRec465[0];
			float fRec464 = fRec467;
			output24[i] = FAUSTFLOAT((fRec0[0] * ((0.0434169397f * fTemp7) + (fRec452 + (fRec461 + fRec464)))));
			float fTemp248 = (fConst75 * fRec469[1]);
			float fTemp249 = (fConst77 * (((((((0.0680911094f * fTemp17) + (7.35240019e-06f * fTemp18)) + (0.018660184f * fTemp22)) + (0.0177974422f * fTemp19)) + (1.59659999e-06f * fTemp20)) + (0.0421827324f * fTemp23)) + (1.19404003e-05f * fTemp21)));
			float fTemp250 = (fConst78 * fRec472[1]);
			float fTemp251 = (fConst79 * fRec475[1]);
			fRec477[0] = (fTemp249 + (fTemp250 + (fRec477[1] + fTemp251)));
			fRec475[0] = fRec477[0];
			float fRec476 = ((fTemp251 + fTemp250) + fTemp249);
			fRec474[0] = (fRec475[0] + fRec474[1]);
			fRec472[0] = fRec474[0];
			float fRec473 = fRec476;
			fRec471[0] = (fTemp248 + (fRec471[1] + fRec473));
			fRec469[0] = fRec471[0];
			float fRec470 = (fRec473 + fTemp248);
			float fTemp252 = (fConst82 * (0.0f - (((0.0827651322f * fTemp29) + (0.0273843631f * fTemp27)) + (5.92059996e-06f * fTemp28))));
			float fTemp253 = (fConst81 * fRec478[1]);
			fRec480[0] = (fTemp252 + (fRec480[1] + fTemp253));
			fRec478[0] = fRec480[0];
			float fRec479 = (fTemp253 + fTemp252);
			float fTemp254 = (fConst84 * (((1.03394996e-05f * fTemp8) + (0.0438491851f * fTemp11)) - (((0.026253948f * fTemp9) + (2.64510004e-06f * fTemp10)) + (0.0779431164f * fTemp12))));
			float fTemp255 = (fConst86 * fRec484[1]);
			float fTemp256 = (fConst85 * fRec481[1]);
			fRec486[0] = (((fTemp254 + fRec486[1]) + fTemp255) + fTemp256);
			fRec484[0] = fRec486[0];
			float fRec485 = ((fTemp254 + fTemp255) + fTemp256);
			fRec483[0] = (fRec484[0] + fRec483[1]);
			fRec481[0] = fRec483[0];
			float fRec482 = fRec485;
			fVec21[0] = (((fRec470 + fRec479) + fRec482) + (0.0554073155f * fTemp7));
			output25[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec21[iConst87])));
			float fTemp257 = (fConst89 * fRec487[1]);
			float fTemp258 = (fConst91 * (((((0.038121108f * fTemp18) + (0.0082456246f * fTemp22)) + (0.0200931113f * fTemp19)) + (0.0236905683f * fTemp23)) - (((0.0082371328f * fTemp17) + (0.00729924766f * fTemp20)) + (0.060279958f * fTemp21))));
			float fTemp259 = (fConst92 * fRec490[1]);
			float fTemp260 = (fConst93 * fRec493[1]);
			fRec495[0] = (fTemp258 + (fTemp259 + (fRec495[1] + fTemp260)));
			fRec493[0] = fRec495[0];
			float fRec494 = ((fTemp260 + fTemp259) + fTemp258);
			fRec492[0] = (fRec493[0] + fRec492[1]);
			fRec490[0] = fRec492[0];
			float fRec491 = fRec494;
			fRec489[0] = (fTemp257 + (fRec491 + fRec489[1]));
			fRec487[0] = fRec489[0];
			float fRec488 = (fRec491 + fTemp257);
			float fTemp261 = (fConst95 * ((0.0388509668f * fTemp28) - ((0.0618637428f * fTemp29) + (0.0285150502f * fTemp27))));
			float fTemp262 = (fConst96 * fRec496[1]);
			fRec498[0] = (fTemp261 + (fRec498[1] + fTemp262));
			fRec496[0] = fRec498[0];
			float fRec497 = (fTemp262 + fTemp261);
			float fTemp263 = (fConst98 * ((0.0403008051f * fTemp11) - ((((0.0621853285f * fTemp8) + (0.0201301072f * fTemp9)) + (0.0225778446f * fTemp10)) + (0.0293648373f * fTemp12))));
			float fTemp264 = (fConst99 * fRec499[1]);
			float fTemp265 = (fConst100 * fRec502[1]);
			fRec504[0] = (fTemp263 + (fTemp264 + (fRec504[1] + fTemp265)));
			fRec502[0] = fRec504[0];
			float fRec503 = ((fTemp265 + fTemp264) + fTemp263);
			fRec501[0] = (fRec502[0] + fRec501[1]);
			fRec499[0] = fRec501[0];
			float fRec500 = fRec503;
			output26[i] = FAUSTFLOAT((fRec0[0] * ((0.0494432151f * fTemp7) + (fRec488 + (fRec497 + fRec500)))));
			float fTemp266 = (fConst89 * fRec505[1]);
			float fTemp267 = (fConst91 * (((((0.0412002578f * fTemp18) + (0.00143468869f * fTemp22)) + (0.0224596411f * fTemp19)) + (1.64367993e-05f * fTemp21)) - (((0.0524200052f * fTemp17) + (0.00248971907f * fTemp20)) + (0.0237927921f * fTemp23))));
			float fTemp268 = (fConst92 * fRec508[1]);
			float fTemp269 = (fConst93 * fRec511[1]);
			fRec513[0] = (fTemp267 + (fTemp268 + (fRec513[1] + fTemp269)));
			fRec511[0] = fRec513[0];
			float fRec512 = ((fTemp269 + fTemp268) + fTemp267);
			fRec510[0] = (fRec511[0] + fRec510[1]);
			fRec508[0] = fRec510[0];
			float fRec509 = fRec512;
			fRec507[0] = (fTemp266 + (fRec509 + fRec507[1]));
			fRec505[0] = fRec507[0];
			float fRec506 = (fRec509 + fTemp266);
			float fTemp270 = (fConst95 * ((0.0547024459f * fTemp28) - ((0.0315749347f * fTemp29) + (0.0296466146f * fTemp27))));
			float fTemp271 = (fConst96 * fRec514[1]);
			fRec516[0] = (fTemp270 + (fRec516[1] + fTemp271));
			fRec514[0] = fRec516[0];
			float fRec515 = (fTemp271 + fTemp270);
			float fTemp272 = (fConst98 * (((0.0242839344f * fTemp11) + (0.0295415837f * fTemp12)) - (((0.0511430465f * fTemp8) + (0.0139899021f * fTemp9)) + (0.0420673341f * fTemp10))));
			float fTemp273 = (fConst99 * fRec517[1]);
			float fTemp274 = (fConst100 * fRec520[1]);
			fRec522[0] = (fTemp272 + (fTemp273 + (fRec522[1] + fTemp274)));
			fRec520[0] = fRec522[0];
			float fRec521 = ((fTemp274 + fTemp273) + fTemp272);
			fRec519[0] = (fRec520[0] + fRec519[1]);
			fRec517[0] = fRec519[0];
			float fRec518 = fRec521;
			output27[i] = FAUSTFLOAT((fRec0[0] * ((0.0434193648f * fTemp7) + (fRec506 + (fRec515 + fRec518)))));
			float fTemp275 = (fConst75 * fRec523[1]);
			float fTemp276 = (fConst77 * ((((2.98569989e-06f * fTemp22) + (0.0177803766f * fTemp19)) + (0.0680936277f * fTemp21)) - ((((7.58089982e-06f * fTemp17) + (1.33274998e-05f * fTemp18)) + (0.0186686739f * fTemp20)) + (0.0421624407f * fTemp23))));
			float fTemp277 = (fConst78 * fRec526[1]);
			float fTemp278 = (fConst79 * fRec529[1]);
			fRec531[0] = (fTemp276 + (fTemp277 + (fRec531[1] + fTemp278)));
			fRec529[0] = fRec531[0];
			float fRec530 = ((fTemp278 + fTemp277) + fTemp276);
			fRec528[0] = (fRec529[0] + fRec528[1]);
			fRec526[0] = fRec528[0];
			float fRec527 = fRec530;
			fRec525[0] = (fTemp275 + (fRec527 + fRec525[1]));
			fRec523[0] = fRec525[0];
			float fRec524 = (fRec527 + fTemp275);
			float fTemp279 = (fConst82 * ((0.0827706978f * fTemp28) - ((2.25269991e-06f * fTemp29) + (0.0273699835f * fTemp27))));
			float fTemp280 = (fConst81 * fRec532[1]);
			fRec534[0] = (fTemp279 + (fRec534[1] + fTemp280));
			fRec532[0] = fRec534[0];
			float fRec533 = (fTemp280 + fTemp279);
			float fTemp281 = (fConst84 * ((0.0779483095f * fTemp12) - ((((5.11959979e-06f * fTemp8) + (6.8147001e-06f * fTemp11)) + (0.0262604319f * fTemp9)) + (0.0438237339f * fTemp10))));
			float fTemp282 = (fConst85 * fRec535[1]);
			float fTemp283 = (fConst86 * fRec538[1]);
			fRec540[0] = (fTemp281 + (fTemp282 + (fRec540[1] + fTemp283)));
			fRec538[0] = fRec540[0];
			float fRec539 = ((fTemp283 + fTemp282) + fTemp281);
			fRec537[0] = (fRec538[0] + fRec537[1]);
			fRec535[0] = fRec537[0];
			float fRec536 = fRec539;
			fVec22[0] = ((0.0554104783f * fTemp7) + (fRec524 + (fRec533 + fRec536)));
			output28[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec22[iConst87])));
			float fTemp284 = (fConst89 * fRec541[1]);
			float fTemp285 = (fConst91 * ((((0.0524359122f * fTemp17) + (0.0224289857f * fTemp19)) + (3.17443009e-05f * fTemp21)) - ((((0.0411784872f * fTemp18) + (0.00143745751f * fTemp22)) + (0.00250173663f * fTemp20)) + (0.023784969f * fTemp23))));
			float fTemp286 = (fConst92 * fRec544[1]);
			float fTemp287 = (fConst93 * fRec547[1]);
			fRec549[0] = (fTemp285 + (fTemp286 + (fRec549[1] + fTemp287)));
			fRec547[0] = fRec549[0];
			float fRec548 = ((fTemp287 + fTemp286) + fTemp285);
			fRec546[0] = (fRec547[0] + fRec546[1]);
			fRec544[0] = fRec546[0];
			float fRec545 = fRec548;
			fRec543[0] = (fTemp284 + (fRec545 + fRec543[1]));
			fRec541[0] = fRec543[0];
			float fRec542 = (fRec545 + fTemp284);
			float fTemp288 = (fConst95 * (((0.0315765515f * fTemp29) + (0.0547167361f * fTemp28)) - (0.0296458863f * fTemp27)));
			float fTemp289 = (fConst96 * fRec550[1]);
			fRec552[0] = (fTemp288 + (fRec552[1] + fTemp289));
			fRec550[0] = fRec552[0];
			float fRec551 = (fTemp289 + fTemp288);
			float fTemp290 = (fConst98 * (((0.0511500239f * fTemp8) + (0.0295588356f * fTemp12)) - (((0.0242726207f * fTemp11) + (0.0139865186f * fTemp9)) + (0.0420513824f * fTemp10))));
			float fTemp291 = (fConst99 * fRec553[1]);
			float fTemp292 = (fConst100 * fRec556[1]);
			fRec558[0] = (fTemp290 + (fTemp291 + (fRec558[1] + fTemp292)));
			fRec556[0] = fRec558[0];
			float fRec557 = ((fTemp292 + fTemp291) + fTemp290);
			fRec555[0] = (fRec556[0] + fRec555[1]);
			fRec553[0] = fRec555[0];
			float fRec554 = fRec557;
			output29[i] = FAUSTFLOAT((fRec0[0] * ((0.0434320159f * fTemp7) + (fRec542 + (fRec551 + fRec554)))));
			float fTemp293 = (fConst89 * fRec559[1]);
			float fTemp294 = (fConst91 * ((((0.00823547225f * fTemp17) + (0.0201231521f * fTemp19)) + (0.0236865543f * fTemp23)) - ((((0.0381439999f * fTemp18) + (0.00823130645f * fTemp22)) + (0.00729331141f * fTemp20)) + (0.0602695309f * fTemp21))));
			float fTemp295 = (fConst92 * fRec562[1]);
			float fTemp296 = (fConst93 * fRec565[1]);
			fRec567[0] = (fTemp294 + (fTemp295 + (fRec567[1] + fTemp296)));
			fRec565[0] = fRec567[0];
			float fRec566 = ((fTemp296 + fTemp295) + fTemp294);
			fRec564[0] = (fRec565[0] + fRec564[1]);
			fRec562[0] = fRec564[0];
			float fRec563 = fRec566;
			fRec561[0] = (fTemp293 + (fRec563 + fRec561[1]));
			fRec559[0] = fRec561[0];
			float fRec560 = (fRec563 + fTemp293);
			float fTemp297 = (fConst95 * (((0.0618486144f * fTemp29) + (0.0388411246f * fTemp28)) - (0.028516816f * fTemp27)));
			float fTemp298 = (fConst96 * fRec568[1]);
			fRec570[0] = (fTemp297 + (fRec570[1] + fTemp298));
			fRec568[0] = fRec570[0];
			float fRec569 = (fTemp298 + fTemp297);
			float fTemp299 = (fConst98 * ((0.0621710345f * fTemp8) - ((((0.0403146707f * fTemp11) + (0.0201298986f * fTemp9)) + (0.022593135f * fTemp10)) + (0.0293579157f * fTemp12))));
			float fTemp300 = (fConst99 * fRec571[1]);
			float fTemp301 = (fConst100 * fRec574[1]);
			fRec576[0] = (fTemp299 + (fTemp300 + (fRec576[1] + fTemp301)));
			fRec574[0] = fRec576[0];
			float fRec575 = ((fTemp301 + fTemp300) + fTemp299);
			fRec573[0] = (fRec574[0] + fRec573[1]);
			fRec571[0] = fRec573[0];
			float fRec572 = fRec575;
			output30[i] = FAUSTFLOAT((fRec0[0] * ((0.0494268872f * fTemp7) + (fRec560 + (fRec569 + fRec572)))));
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			fRec10[2] = fRec10[1];
			fRec10[1] = fRec10[0];
			fRec11[2] = fRec11[1];
			fRec11[1] = fRec11[0];
			fRec12[2] = fRec12[1];
			fRec12[1] = fRec12[0];
			fRec13[2] = fRec13[1];
			fRec13[1] = fRec13[0];
			fRec14[2] = fRec14[1];
			fRec14[1] = fRec14[0];
			fRec9[1] = fRec9[0];
			fRec7[1] = fRec7[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec26[2] = fRec26[1];
			fRec26[1] = fRec26[0];
			fRec27[2] = fRec27[1];
			fRec27[1] = fRec27[0];
			fRec28[2] = fRec28[1];
			fRec28[1] = fRec28[0];
			fRec29[2] = fRec29[1];
			fRec29[1] = fRec29[0];
			fRec30[2] = fRec30[1];
			fRec30[1] = fRec30[0];
			fRec23[1] = fRec23[0];
			fRec21[1] = fRec21[0];
			fRec20[1] = fRec20[0];
			fRec18[1] = fRec18[0];
			fRec17[1] = fRec17[0];
			fRec15[1] = fRec15[0];
			fRec34[2] = fRec34[1];
			fRec34[1] = fRec34[0];
			fRec35[2] = fRec35[1];
			fRec35[1] = fRec35[0];
			fRec36[2] = fRec36[1];
			fRec36[1] = fRec36[0];
			fRec33[1] = fRec33[0];
			fRec31[1] = fRec31[0];
			IOTA = (IOTA + 1);
			fRec45[1] = fRec45[0];
			fRec43[1] = fRec43[0];
			fRec42[1] = fRec42[0];
			fRec40[1] = fRec40[0];
			fRec39[1] = fRec39[0];
			fRec37[1] = fRec37[0];
			fRec48[1] = fRec48[0];
			fRec46[1] = fRec46[0];
			fRec54[1] = fRec54[0];
			fRec52[1] = fRec52[0];
			fRec51[1] = fRec51[0];
			fRec49[1] = fRec49[0];
			fRec63[1] = fRec63[0];
			fRec61[1] = fRec61[0];
			fRec60[1] = fRec60[0];
			fRec58[1] = fRec58[0];
			fRec57[1] = fRec57[0];
			fRec55[1] = fRec55[0];
			fRec66[1] = fRec66[0];
			fRec64[1] = fRec64[0];
			fRec72[1] = fRec72[0];
			fRec70[1] = fRec70[0];
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fRec84[1] = fRec84[0];
			fRec82[1] = fRec82[0];
			fRec90[1] = fRec90[0];
			fRec88[1] = fRec88[0];
			fRec87[1] = fRec87[0];
			fRec85[1] = fRec85[0];
			fRec99[1] = fRec99[0];
			fRec97[1] = fRec97[0];
			fRec96[1] = fRec96[0];
			fRec94[1] = fRec94[0];
			fRec93[1] = fRec93[0];
			fRec91[1] = fRec91[0];
			fRec102[1] = fRec102[0];
			fRec100[1] = fRec100[0];
			fRec108[1] = fRec108[0];
			fRec106[1] = fRec106[0];
			fRec105[1] = fRec105[0];
			fRec103[1] = fRec103[0];
			fRec117[1] = fRec117[0];
			fRec115[1] = fRec115[0];
			fRec114[1] = fRec114[0];
			fRec112[1] = fRec112[0];
			fRec111[1] = fRec111[0];
			fRec109[1] = fRec109[0];
			fRec120[1] = fRec120[0];
			fRec118[1] = fRec118[0];
			fRec126[1] = fRec126[0];
			fRec124[1] = fRec124[0];
			fRec123[1] = fRec123[0];
			fRec121[1] = fRec121[0];
			fRec135[1] = fRec135[0];
			fRec133[1] = fRec133[0];
			fRec132[1] = fRec132[0];
			fRec130[1] = fRec130[0];
			fRec129[1] = fRec129[0];
			fRec127[1] = fRec127[0];
			fRec138[1] = fRec138[0];
			fRec136[1] = fRec136[0];
			fRec144[1] = fRec144[0];
			fRec142[1] = fRec142[0];
			fRec141[1] = fRec141[0];
			fRec139[1] = fRec139[0];
			fRec153[1] = fRec153[0];
			fRec151[1] = fRec151[0];
			fRec150[1] = fRec150[0];
			fRec148[1] = fRec148[0];
			fRec147[1] = fRec147[0];
			fRec145[1] = fRec145[0];
			fRec156[1] = fRec156[0];
			fRec154[1] = fRec154[0];
			fRec162[1] = fRec162[0];
			fRec160[1] = fRec160[0];
			fRec159[1] = fRec159[0];
			fRec157[1] = fRec157[0];
			fRec171[1] = fRec171[0];
			fRec169[1] = fRec169[0];
			fRec168[1] = fRec168[0];
			fRec166[1] = fRec166[0];
			fRec165[1] = fRec165[0];
			fRec163[1] = fRec163[0];
			fRec174[1] = fRec174[0];
			fRec172[1] = fRec172[0];
			fRec180[1] = fRec180[0];
			fRec178[1] = fRec178[0];
			fRec177[1] = fRec177[0];
			fRec175[1] = fRec175[0];
			fRec189[1] = fRec189[0];
			fRec187[1] = fRec187[0];
			fRec186[1] = fRec186[0];
			fRec184[1] = fRec184[0];
			fRec183[1] = fRec183[0];
			fRec181[1] = fRec181[0];
			fRec192[1] = fRec192[0];
			fRec190[1] = fRec190[0];
			fRec198[1] = fRec198[0];
			fRec196[1] = fRec196[0];
			fRec195[1] = fRec195[0];
			fRec193[1] = fRec193[0];
			fRec207[1] = fRec207[0];
			fRec205[1] = fRec205[0];
			fRec204[1] = fRec204[0];
			fRec202[1] = fRec202[0];
			fRec201[1] = fRec201[0];
			fRec199[1] = fRec199[0];
			fRec210[1] = fRec210[0];
			fRec208[1] = fRec208[0];
			fRec216[1] = fRec216[0];
			fRec214[1] = fRec214[0];
			fRec213[1] = fRec213[0];
			fRec211[1] = fRec211[0];
			fRec225[1] = fRec225[0];
			fRec223[1] = fRec223[0];
			fRec222[1] = fRec222[0];
			fRec220[1] = fRec220[0];
			fRec219[1] = fRec219[0];
			fRec217[1] = fRec217[0];
			fRec228[1] = fRec228[0];
			fRec226[1] = fRec226[0];
			fRec234[1] = fRec234[0];
			fRec232[1] = fRec232[0];
			fRec231[1] = fRec231[0];
			fRec229[1] = fRec229[0];
			fRec243[1] = fRec243[0];
			fRec241[1] = fRec241[0];
			fRec240[1] = fRec240[0];
			fRec238[1] = fRec238[0];
			fRec237[1] = fRec237[0];
			fRec235[1] = fRec235[0];
			fRec246[1] = fRec246[0];
			fRec244[1] = fRec244[0];
			fRec252[1] = fRec252[0];
			fRec250[1] = fRec250[0];
			fRec249[1] = fRec249[0];
			fRec247[1] = fRec247[0];
			fRec261[1] = fRec261[0];
			fRec259[1] = fRec259[0];
			fRec258[1] = fRec258[0];
			fRec256[1] = fRec256[0];
			fRec255[1] = fRec255[0];
			fRec253[1] = fRec253[0];
			fRec264[1] = fRec264[0];
			fRec262[1] = fRec262[0];
			fRec270[1] = fRec270[0];
			fRec268[1] = fRec268[0];
			fRec267[1] = fRec267[0];
			fRec265[1] = fRec265[0];
			fRec273[1] = fRec273[0];
			fRec271[1] = fRec271[0];
			fRec279[1] = fRec279[0];
			fRec277[1] = fRec277[0];
			fRec276[1] = fRec276[0];
			fRec274[1] = fRec274[0];
			fRec288[1] = fRec288[0];
			fRec286[1] = fRec286[0];
			fRec285[1] = fRec285[0];
			fRec283[1] = fRec283[0];
			fRec282[1] = fRec282[0];
			fRec280[1] = fRec280[0];
			fRec291[1] = fRec291[0];
			fRec289[1] = fRec289[0];
			fRec297[1] = fRec297[0];
			fRec295[1] = fRec295[0];
			fRec294[1] = fRec294[0];
			fRec292[1] = fRec292[0];
			fRec306[1] = fRec306[0];
			fRec304[1] = fRec304[0];
			fRec303[1] = fRec303[0];
			fRec301[1] = fRec301[0];
			fRec300[1] = fRec300[0];
			fRec298[1] = fRec298[0];
			fRec309[1] = fRec309[0];
			fRec307[1] = fRec307[0];
			fRec315[1] = fRec315[0];
			fRec313[1] = fRec313[0];
			fRec312[1] = fRec312[0];
			fRec310[1] = fRec310[0];
			fRec324[1] = fRec324[0];
			fRec322[1] = fRec322[0];
			fRec321[1] = fRec321[0];
			fRec319[1] = fRec319[0];
			fRec318[1] = fRec318[0];
			fRec316[1] = fRec316[0];
			fRec327[1] = fRec327[0];
			fRec325[1] = fRec325[0];
			fRec333[1] = fRec333[0];
			fRec331[1] = fRec331[0];
			fRec330[1] = fRec330[0];
			fRec328[1] = fRec328[0];
			fRec342[1] = fRec342[0];
			fRec340[1] = fRec340[0];
			fRec339[1] = fRec339[0];
			fRec337[1] = fRec337[0];
			fRec336[1] = fRec336[0];
			fRec334[1] = fRec334[0];
			fRec345[1] = fRec345[0];
			fRec343[1] = fRec343[0];
			fRec351[1] = fRec351[0];
			fRec349[1] = fRec349[0];
			fRec348[1] = fRec348[0];
			fRec346[1] = fRec346[0];
			fRec360[1] = fRec360[0];
			fRec358[1] = fRec358[0];
			fRec357[1] = fRec357[0];
			fRec355[1] = fRec355[0];
			fRec354[1] = fRec354[0];
			fRec352[1] = fRec352[0];
			fRec369[1] = fRec369[0];
			fRec367[1] = fRec367[0];
			fRec366[1] = fRec366[0];
			fRec364[1] = fRec364[0];
			fRec363[1] = fRec363[0];
			fRec361[1] = fRec361[0];
			fRec372[1] = fRec372[0];
			fRec370[1] = fRec370[0];
			fRec378[1] = fRec378[0];
			fRec376[1] = fRec376[0];
			fRec375[1] = fRec375[0];
			fRec373[1] = fRec373[0];
			fVec19[2] = fVec19[1];
			fVec19[1] = fVec19[0];
			fRec387[1] = fRec387[0];
			fRec385[1] = fRec385[0];
			fRec384[1] = fRec384[0];
			fRec382[1] = fRec382[0];
			fRec381[1] = fRec381[0];
			fRec379[1] = fRec379[0];
			fRec390[1] = fRec390[0];
			fRec388[1] = fRec388[0];
			fRec396[1] = fRec396[0];
			fRec394[1] = fRec394[0];
			fRec393[1] = fRec393[0];
			fRec391[1] = fRec391[0];
			fRec405[1] = fRec405[0];
			fRec403[1] = fRec403[0];
			fRec402[1] = fRec402[0];
			fRec400[1] = fRec400[0];
			fRec399[1] = fRec399[0];
			fRec397[1] = fRec397[0];
			fRec408[1] = fRec408[0];
			fRec406[1] = fRec406[0];
			fRec414[1] = fRec414[0];
			fRec412[1] = fRec412[0];
			fRec411[1] = fRec411[0];
			fRec409[1] = fRec409[0];
			fRec423[1] = fRec423[0];
			fRec421[1] = fRec421[0];
			fRec420[1] = fRec420[0];
			fRec418[1] = fRec418[0];
			fRec417[1] = fRec417[0];
			fRec415[1] = fRec415[0];
			fRec426[1] = fRec426[0];
			fRec424[1] = fRec424[0];
			fRec432[1] = fRec432[0];
			fRec430[1] = fRec430[0];
			fRec429[1] = fRec429[0];
			fRec427[1] = fRec427[0];
			fVec20[2] = fVec20[1];
			fVec20[1] = fVec20[0];
			fRec441[1] = fRec441[0];
			fRec439[1] = fRec439[0];
			fRec438[1] = fRec438[0];
			fRec436[1] = fRec436[0];
			fRec435[1] = fRec435[0];
			fRec433[1] = fRec433[0];
			fRec444[1] = fRec444[0];
			fRec442[1] = fRec442[0];
			fRec450[1] = fRec450[0];
			fRec448[1] = fRec448[0];
			fRec447[1] = fRec447[0];
			fRec445[1] = fRec445[0];
			fRec459[1] = fRec459[0];
			fRec457[1] = fRec457[0];
			fRec456[1] = fRec456[0];
			fRec454[1] = fRec454[0];
			fRec453[1] = fRec453[0];
			fRec451[1] = fRec451[0];
			fRec462[1] = fRec462[0];
			fRec460[1] = fRec460[0];
			fRec468[1] = fRec468[0];
			fRec466[1] = fRec466[0];
			fRec465[1] = fRec465[0];
			fRec463[1] = fRec463[0];
			fRec477[1] = fRec477[0];
			fRec475[1] = fRec475[0];
			fRec474[1] = fRec474[0];
			fRec472[1] = fRec472[0];
			fRec471[1] = fRec471[0];
			fRec469[1] = fRec469[0];
			fRec480[1] = fRec480[0];
			fRec478[1] = fRec478[0];
			fRec486[1] = fRec486[0];
			fRec484[1] = fRec484[0];
			fRec483[1] = fRec483[0];
			fRec481[1] = fRec481[0];
			fVec21[2] = fVec21[1];
			fVec21[1] = fVec21[0];
			fRec495[1] = fRec495[0];
			fRec493[1] = fRec493[0];
			fRec492[1] = fRec492[0];
			fRec490[1] = fRec490[0];
			fRec489[1] = fRec489[0];
			fRec487[1] = fRec487[0];
			fRec498[1] = fRec498[0];
			fRec496[1] = fRec496[0];
			fRec504[1] = fRec504[0];
			fRec502[1] = fRec502[0];
			fRec501[1] = fRec501[0];
			fRec499[1] = fRec499[0];
			fRec513[1] = fRec513[0];
			fRec511[1] = fRec511[0];
			fRec510[1] = fRec510[0];
			fRec508[1] = fRec508[0];
			fRec507[1] = fRec507[0];
			fRec505[1] = fRec505[0];
			fRec516[1] = fRec516[0];
			fRec514[1] = fRec514[0];
			fRec522[1] = fRec522[0];
			fRec520[1] = fRec520[0];
			fRec519[1] = fRec519[0];
			fRec517[1] = fRec517[0];
			fRec531[1] = fRec531[0];
			fRec529[1] = fRec529[0];
			fRec528[1] = fRec528[0];
			fRec526[1] = fRec526[0];
			fRec525[1] = fRec525[0];
			fRec523[1] = fRec523[0];
			fRec534[1] = fRec534[0];
			fRec532[1] = fRec532[0];
			fRec540[1] = fRec540[0];
			fRec538[1] = fRec538[0];
			fRec537[1] = fRec537[0];
			fRec535[1] = fRec535[0];
			fVec22[2] = fVec22[1];
			fVec22[1] = fVec22[0];
			fRec549[1] = fRec549[0];
			fRec547[1] = fRec547[0];
			fRec546[1] = fRec546[0];
			fRec544[1] = fRec544[0];
			fRec543[1] = fRec543[0];
			fRec541[1] = fRec541[0];
			fRec552[1] = fRec552[0];
			fRec550[1] = fRec550[0];
			fRec558[1] = fRec558[0];
			fRec556[1] = fRec556[0];
			fRec555[1] = fRec555[0];
			fRec553[1] = fRec553[0];
			fRec567[1] = fRec567[0];
			fRec565[1] = fRec565[0];
			fRec564[1] = fRec564[0];
			fRec562[1] = fRec562[0];
			fRec561[1] = fRec561[0];
			fRec559[1] = fRec559[0];
			fRec570[1] = fRec570[0];
			fRec568[1] = fRec568[0];
			fRec576[1] = fRec576[0];
			fRec574[1] = fRec574[0];
			fRec573[1] = fRec573[0];
			fRec571[1] = fRec571[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
