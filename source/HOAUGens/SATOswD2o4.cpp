/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD2o4"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider1;
	float fRec1[2];
	float fRec2[3];
	FAUSTFLOAT fHslider2;
	float fRec3[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fRec7[3];
	float fRec8[3];
	float fRec9[3];
	float fConst5;
	float fRec6[2];
	float fRec4[2];
	float fConst6;
	float fConst7;
	float fConst8;
	float fRec19[3];
	float fRec20[3];
	float fRec21[3];
	float fRec22[3];
	float fRec23[3];
	float fRec24[3];
	float fRec25[3];
	float fConst9;
	float fConst10;
	float fConst11;
	float fRec18[2];
	float fRec16[2];
	float fRec15[2];
	float fRec13[2];
	float fConst12;
	float fRec12[2];
	float fRec10[2];
	float fConst13;
	float fConst14;
	float fConst15;
	float fConst16;
	float fConst17;
	float fRec38[3];
	float fRec39[3];
	float fRec40[3];
	float fRec41[3];
	float fRec42[3];
	float fRec43[3];
	float fRec44[3];
	float fRec45[3];
	float fRec46[3];
	float fConst18;
	float fConst19;
	float fRec37[2];
	float fRec35[2];
	float fRec34[2];
	float fRec32[2];
	float fRec31[2];
	float fRec29[2];
	float fRec28[2];
	float fRec26[2];
	float fConst20;
	float fConst21;
	float fRec53[3];
	float fRec54[3];
	float fRec55[3];
	float fRec56[3];
	float fRec57[3];
	float fConst22;
	float fConst23;
	float fRec52[2];
	float fRec50[2];
	float fRec49[2];
	float fRec47[2];
	int IOTA;
	float fVec0[1024];
	int iConst24;
	float fConst25;
	float fConst26;
	float fConst27;
	float fConst28;
	float fConst29;
	float fConst30;
	float fConst31;
	float fRec69[2];
	float fRec67[2];
	float fRec66[2];
	float fRec64[2];
	float fRec63[2];
	float fRec61[2];
	float fRec60[2];
	float fRec58[2];
	float fConst32;
	float fConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	float fRec72[2];
	float fRec70[2];
	float fConst38;
	float fConst39;
	float fConst40;
	float fRec81[2];
	float fRec79[2];
	float fConst41;
	float fConst42;
	float fConst43;
	float fConst44;
	float fRec87[2];
	float fRec85[2];
	float fRec84[2];
	float fRec82[2];
	float fVec1[512];
	int iConst45;
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fConst50;
	float fConst51;
	float fConst52;
	float fRec99[2];
	float fRec97[2];
	float fRec96[2];
	float fRec94[2];
	float fRec93[2];
	float fRec91[2];
	float fRec90[2];
	float fRec88[2];
	float fConst53;
	float fConst54;
	float fConst55;
	float fConst56;
	float fConst57;
	float fRec108[2];
	float fRec106[2];
	float fRec105[2];
	float fRec103[2];
	float fConst58;
	float fRec102[2];
	float fRec100[2];
	float fConst59;
	float fConst60;
	float fConst61;
	float fRec111[2];
	float fRec109[2];
	float fConst62;
	float fConst63;
	float fConst64;
	float fConst65;
	float fRec117[2];
	float fRec115[2];
	float fRec114[2];
	float fRec112[2];
	float fVec2[512];
	int iConst66;
	float fRec129[2];
	float fRec127[2];
	float fRec126[2];
	float fRec124[2];
	float fRec123[2];
	float fRec121[2];
	float fRec120[2];
	float fRec118[2];
	float fRec138[2];
	float fRec136[2];
	float fRec135[2];
	float fRec133[2];
	float fRec132[2];
	float fRec130[2];
	float fRec141[2];
	float fRec139[2];
	float fRec147[2];
	float fRec145[2];
	float fRec144[2];
	float fRec142[2];
	float fVec3[512];
	float fRec150[2];
	float fRec148[2];
	float fRec156[2];
	float fRec154[2];
	float fRec153[2];
	float fRec151[2];
	float fRec165[2];
	float fRec163[2];
	float fRec162[2];
	float fRec160[2];
	float fRec159[2];
	float fRec157[2];
	float fRec177[2];
	float fRec175[2];
	float fRec174[2];
	float fRec172[2];
	float fRec171[2];
	float fRec169[2];
	float fRec168[2];
	float fRec166[2];
	float fVec4[512];
	float fRec180[2];
	float fRec178[2];
	float fRec186[2];
	float fRec184[2];
	float fRec183[2];
	float fRec181[2];
	float fRec195[2];
	float fRec193[2];
	float fRec192[2];
	float fRec190[2];
	float fRec189[2];
	float fRec187[2];
	float fRec207[2];
	float fRec205[2];
	float fRec204[2];
	float fRec202[2];
	float fRec201[2];
	float fRec199[2];
	float fRec198[2];
	float fRec196[2];
	float fVec5[512];
	float fRec210[2];
	float fRec208[2];
	float fRec216[2];
	float fRec214[2];
	float fRec213[2];
	float fRec211[2];
	float fRec225[2];
	float fRec223[2];
	float fRec222[2];
	float fRec220[2];
	float fRec219[2];
	float fRec217[2];
	float fRec237[2];
	float fRec235[2];
	float fRec234[2];
	float fRec232[2];
	float fRec231[2];
	float fRec229[2];
	float fRec228[2];
	float fRec226[2];
	float fVec6[512];
	float fConst67;
	float fConst68;
	float fConst69;
	float fRec240[2];
	float fRec238[2];
	float fConst70;
	float fConst71;
	float fConst72;
	float fConst73;
	float fRec246[2];
	float fRec244[2];
	float fRec243[2];
	float fRec241[2];
	float fConst74;
	float fConst75;
	float fConst76;
	float fConst77;
	float fConst78;
	float fConst79;
	float fRec255[2];
	float fRec253[2];
	float fRec252[2];
	float fRec250[2];
	float fRec249[2];
	float fRec247[2];
	float fConst80;
	float fConst81;
	float fConst82;
	float fConst83;
	float fConst84;
	float fConst85;
	float fConst86;
	float fRec267[2];
	float fRec265[2];
	float fRec264[2];
	float fRec262[2];
	float fRec261[2];
	float fRec259[2];
	float fRec258[2];
	float fRec256[2];
	float fVec7[512];
	int iConst87;
	float fConst88;
	float fConst89;
	float fConst90;
	float fConst91;
	float fRec273[2];
	float fRec271[2];
	float fRec270[2];
	float fRec268[2];
	float fConst92;
	float fConst93;
	float fConst94;
	float fRec276[2];
	float fRec274[2];
	float fConst95;
	float fConst96;
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fRec285[2];
	float fRec283[2];
	float fRec282[2];
	float fRec280[2];
	float fRec279[2];
	float fRec277[2];
	float fConst101;
	float fConst102;
	float fConst103;
	float fConst104;
	float fConst105;
	float fConst106;
	float fConst107;
	float fRec297[2];
	float fRec295[2];
	float fRec294[2];
	float fRec292[2];
	float fRec291[2];
	float fRec289[2];
	float fRec288[2];
	float fRec286[2];
	float fVec8[512];
	int iConst108;
	float fRec309[2];
	float fRec307[2];
	float fRec306[2];
	float fRec304[2];
	float fRec303[2];
	float fRec301[2];
	float fRec300[2];
	float fRec298[2];
	float fRec318[2];
	float fRec316[2];
	float fRec315[2];
	float fRec313[2];
	float fRec312[2];
	float fRec310[2];
	float fRec321[2];
	float fRec319[2];
	float fRec327[2];
	float fRec325[2];
	float fRec324[2];
	float fRec322[2];
	float fVec9[512];
	float fRec339[2];
	float fRec337[2];
	float fRec336[2];
	float fRec334[2];
	float fRec333[2];
	float fRec331[2];
	float fRec330[2];
	float fRec328[2];
	float fRec348[2];
	float fRec346[2];
	float fRec345[2];
	float fRec343[2];
	float fRec342[2];
	float fRec340[2];
	float fRec351[2];
	float fRec349[2];
	float fRec357[2];
	float fRec355[2];
	float fRec354[2];
	float fRec352[2];
	float fVec10[512];
	float fRec366[2];
	float fRec364[2];
	float fRec363[2];
	float fRec361[2];
	float fRec360[2];
	float fRec358[2];
	float fRec372[2];
	float fRec370[2];
	float fRec369[2];
	float fRec367[2];
	float fRec384[2];
	float fRec382[2];
	float fRec381[2];
	float fRec379[2];
	float fRec378[2];
	float fRec376[2];
	float fRec375[2];
	float fRec373[2];
	float fRec387[2];
	float fRec385[2];
	float fVec11[512];
	float fRec399[2];
	float fRec397[2];
	float fRec396[2];
	float fRec394[2];
	float fRec393[2];
	float fRec391[2];
	float fRec390[2];
	float fRec388[2];
	float fRec408[2];
	float fRec406[2];
	float fRec405[2];
	float fRec403[2];
	float fRec402[2];
	float fRec400[2];
	float fRec411[2];
	float fRec409[2];
	float fRec417[2];
	float fRec415[2];
	float fRec414[2];
	float fRec412[2];
	float fVec12[512];
	float fRec423[2];
	float fRec421[2];
	float fRec420[2];
	float fRec418[2];
	float fRec426[2];
	float fRec424[2];
	float fRec435[2];
	float fRec433[2];
	float fRec432[2];
	float fRec430[2];
	float fRec429[2];
	float fRec427[2];
	float fRec447[2];
	float fRec445[2];
	float fRec444[2];
	float fRec442[2];
	float fRec441[2];
	float fRec439[2];
	float fRec438[2];
	float fRec436[2];
	float fVec13[512];
	float fRec459[2];
	float fRec457[2];
	float fRec456[2];
	float fRec454[2];
	float fRec453[2];
	float fRec451[2];
	float fRec450[2];
	float fRec448[2];
	float fRec468[2];
	float fRec466[2];
	float fRec465[2];
	float fRec463[2];
	float fRec462[2];
	float fRec460[2];
	float fRec471[2];
	float fRec469[2];
	float fRec477[2];
	float fRec475[2];
	float fRec474[2];
	float fRec472[2];
	float fVec14[512];
	float fRec480[2];
	float fRec478[2];
	float fRec489[2];
	float fRec487[2];
	float fRec486[2];
	float fRec484[2];
	float fRec483[2];
	float fRec481[2];
	float fRec501[2];
	float fRec499[2];
	float fRec498[2];
	float fRec496[2];
	float fRec495[2];
	float fRec493[2];
	float fRec492[2];
	float fRec490[2];
	float fRec507[2];
	float fRec505[2];
	float fRec504[2];
	float fRec502[2];
	float fVec15[512];
	float fRec519[2];
	float fRec517[2];
	float fRec516[2];
	float fRec514[2];
	float fRec513[2];
	float fRec511[2];
	float fRec510[2];
	float fRec508[2];
	float fRec528[2];
	float fRec526[2];
	float fRec525[2];
	float fRec523[2];
	float fRec522[2];
	float fRec520[2];
	float fRec531[2];
	float fRec529[2];
	float fRec537[2];
	float fRec535[2];
	float fRec534[2];
	float fRec532[2];
	float fVec16[512];
	float fRec549[2];
	float fRec547[2];
	float fRec546[2];
	float fRec544[2];
	float fRec543[2];
	float fRec541[2];
	float fRec540[2];
	float fRec538[2];
	float fRec555[2];
	float fRec553[2];
	float fRec552[2];
	float fRec550[2];
	float fRec564[2];
	float fRec562[2];
	float fRec561[2];
	float fRec559[2];
	float fRec558[2];
	float fRec556[2];
	float fRec567[2];
	float fRec565[2];
	float fVec17[512];
	float fRec579[2];
	float fRec577[2];
	float fRec576[2];
	float fRec574[2];
	float fRec573[2];
	float fRec571[2];
	float fRec570[2];
	float fRec568[2];
	float fRec588[2];
	float fRec586[2];
	float fRec585[2];
	float fRec583[2];
	float fRec582[2];
	float fRec580[2];
	float fRec591[2];
	float fRec589[2];
	float fRec597[2];
	float fRec595[2];
	float fRec594[2];
	float fRec592[2];
	float fVec18[512];
	float fConst109;
	float fConst110;
	float fConst111;
	float fConst112;
	float fConst113;
	float fConst114;
	float fConst115;
	float fRec609[2];
	float fRec607[2];
	float fRec606[2];
	float fRec604[2];
	float fRec603[2];
	float fRec601[2];
	float fRec600[2];
	float fRec598[2];
	float fConst116;
	float fConst117;
	float fConst118;
	float fConst119;
	float fConst120;
	float fConst121;
	float fRec618[2];
	float fRec616[2];
	float fRec615[2];
	float fRec613[2];
	float fRec612[2];
	float fRec610[2];
	float fConst122;
	float fConst123;
	float fConst124;
	float fRec621[2];
	float fRec619[2];
	float fConst125;
	float fConst126;
	float fConst127;
	float fConst128;
	float fRec627[2];
	float fRec625[2];
	float fRec624[2];
	float fRec622[2];
	float fVec19[3];
	int iConst129;
	float fConst130;
	float fConst131;
	float fConst132;
	float fConst133;
	float fConst134;
	float fConst135;
	float fRec636[2];
	float fRec634[2];
	float fRec633[2];
	float fRec631[2];
	float fRec630[2];
	float fRec628[2];
	float fConst136;
	float fConst137;
	float fConst138;
	float fConst139;
	float fRec642[2];
	float fRec640[2];
	float fRec639[2];
	float fRec637[2];
	float fConst140;
	float fConst141;
	float fConst142;
	float fConst143;
	float fConst144;
	float fConst145;
	float fConst146;
	float fRec654[2];
	float fRec652[2];
	float fRec651[2];
	float fRec649[2];
	float fRec648[2];
	float fRec646[2];
	float fRec645[2];
	float fRec643[2];
	float fConst147;
	float fConst148;
	float fConst149;
	float fRec657[2];
	float fRec655[2];
	float fRec666[2];
	float fRec664[2];
	float fRec663[2];
	float fRec661[2];
	float fRec660[2];
	float fRec658[2];
	float fRec669[2];
	float fRec667[2];
	float fRec675[2];
	float fRec673[2];
	float fRec672[2];
	float fRec670[2];
	float fRec687[2];
	float fRec685[2];
	float fRec684[2];
	float fRec682[2];
	float fRec681[2];
	float fRec679[2];
	float fRec678[2];
	float fRec676[2];
	float fRec699[2];
	float fRec697[2];
	float fRec696[2];
	float fRec694[2];
	float fRec693[2];
	float fRec691[2];
	float fRec690[2];
	float fRec688[2];
	float fRec708[2];
	float fRec706[2];
	float fRec705[2];
	float fRec703[2];
	float fRec702[2];
	float fRec700[2];
	float fRec711[2];
	float fRec709[2];
	float fRec717[2];
	float fRec715[2];
	float fRec714[2];
	float fRec712[2];
	float fVec20[3];
	float fRec729[2];
	float fRec727[2];
	float fRec726[2];
	float fRec724[2];
	float fRec723[2];
	float fRec721[2];
	float fRec720[2];
	float fRec718[2];
	float fRec732[2];
	float fRec730[2];
	float fRec738[2];
	float fRec736[2];
	float fRec735[2];
	float fRec733[2];
	float fRec747[2];
	float fRec745[2];
	float fRec744[2];
	float fRec742[2];
	float fRec741[2];
	float fRec739[2];
	float fRec753[2];
	float fRec751[2];
	float fRec750[2];
	float fRec748[2];
	float fRec756[2];
	float fRec754[2];
	float fRec765[2];
	float fRec763[2];
	float fRec762[2];
	float fRec760[2];
	float fRec759[2];
	float fRec757[2];
	float fRec777[2];
	float fRec775[2];
	float fRec774[2];
	float fRec772[2];
	float fRec771[2];
	float fRec769[2];
	float fRec768[2];
	float fRec766[2];
	float fRec789[2];
	float fRec787[2];
	float fRec786[2];
	float fRec784[2];
	float fRec783[2];
	float fRec781[2];
	float fRec780[2];
	float fRec778[2];
	float fRec798[2];
	float fRec796[2];
	float fRec795[2];
	float fRec793[2];
	float fRec792[2];
	float fRec790[2];
	float fRec801[2];
	float fRec799[2];
	float fRec807[2];
	float fRec805[2];
	float fRec804[2];
	float fRec802[2];
	float fVec21[3];
	float fRec819[2];
	float fRec817[2];
	float fRec816[2];
	float fRec814[2];
	float fRec813[2];
	float fRec811[2];
	float fRec810[2];
	float fRec808[2];
	float fRec828[2];
	float fRec826[2];
	float fRec825[2];
	float fRec823[2];
	float fRec822[2];
	float fRec820[2];
	float fRec831[2];
	float fRec829[2];
	float fRec837[2];
	float fRec835[2];
	float fRec834[2];
	float fRec832[2];
	float fRec849[2];
	float fRec847[2];
	float fRec846[2];
	float fRec844[2];
	float fRec843[2];
	float fRec841[2];
	float fRec840[2];
	float fRec838[2];
	float fRec858[2];
	float fRec856[2];
	float fRec855[2];
	float fRec853[2];
	float fRec852[2];
	float fRec850[2];
	float fRec861[2];
	float fRec859[2];
	float fRec867[2];
	float fRec865[2];
	float fRec864[2];
	float fRec862[2];
	float fRec879[2];
	float fRec877[2];
	float fRec876[2];
	float fRec874[2];
	float fRec873[2];
	float fRec871[2];
	float fRec870[2];
	float fRec868[2];
	float fRec888[2];
	float fRec886[2];
	float fRec885[2];
	float fRec883[2];
	float fRec882[2];
	float fRec880[2];
	float fRec891[2];
	float fRec889[2];
	float fRec897[2];
	float fRec895[2];
	float fRec894[2];
	float fRec892[2];
	float fVec22[3];
	float fRec909[2];
	float fRec907[2];
	float fRec906[2];
	float fRec904[2];
	float fRec903[2];
	float fRec901[2];
	float fRec900[2];
	float fRec898[2];
	float fRec918[2];
	float fRec916[2];
	float fRec915[2];
	float fRec913[2];
	float fRec912[2];
	float fRec910[2];
	float fRec921[2];
	float fRec919[2];
	float fRec927[2];
	float fRec925[2];
	float fRec924[2];
	float fRec922[2];
	float fRec939[2];
	float fRec937[2];
	float fRec936[2];
	float fRec934[2];
	float fRec933[2];
	float fRec931[2];
	float fRec930[2];
	float fRec928[2];
	float fRec948[2];
	float fRec946[2];
	float fRec945[2];
	float fRec943[2];
	float fRec942[2];
	float fRec940[2];
	float fRec951[2];
	float fRec949[2];
	float fRec957[2];
	float fRec955[2];
	float fRec954[2];
	float fRec952[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD2o4");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 25;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((21.3840885f / fConst2) + 1.0f);
		fConst4 = (1.0f / fConst3);
		fConst5 = (0.0f - (42.768177f / (fConst2 * fConst3)));
		fConst6 = ((49.6578178f / fConst2) + 1.0f);
		fConst7 = ((((2953.7644f / fConst2) + 78.6467133f) / fConst2) + 1.0f);
		fConst8 = (1.0f / (fConst6 * fConst7));
		fConst9 = (0.0f - (((11815.0576f / fConst2) + 157.293427f) / (fConst2 * fConst7)));
		fConst10 = mydsp_faustpower2_f(fConst2);
		fConst11 = (0.0f - (11815.0576f / (fConst10 * fConst7)));
		fConst12 = (0.0f - (99.3156357f / (fConst2 * fConst6)));
		fConst13 = ((((4179.59229f / fConst2) + 123.865646f) / fConst2) + 1.0f);
		fConst14 = (0.0f - (16718.3691f / (fConst10 * fConst13)));
		fConst15 = (0.0f - (((16718.3691f / fConst2) + 247.731293f) / (fConst2 * fConst13)));
		fConst16 = ((((5253.13281f / fConst2) + 89.975235f) / fConst2) + 1.0f);
		fConst17 = (1.0f / (fConst13 * fConst16));
		fConst18 = (0.0f - (21012.5312f / (fConst10 * fConst16)));
		fConst19 = (0.0f - (((21012.5312f / fConst2) + 179.95047f) / (fConst2 * fConst16)));
		fConst20 = ((((1371.83777f / fConst2) + 64.1522675f) / fConst2) + 1.0f);
		fConst21 = (1.0f / fConst20);
		fConst22 = (0.0f - (5487.35107f / (fConst10 * fConst20)));
		fConst23 = (0.0f - (((5487.35107f / fConst2) + 128.304535f) / (fConst2 * fConst20)));
		iConst24 = int(((0.003076792f * float(iConst0)) + 0.5f));
		fConst25 = ((((3950.89111f / fConst2) + 120.429108f) / fConst2) + 1.0f);
		fConst26 = (0.0f - (15803.5645f / (fConst10 * fConst25)));
		fConst27 = (0.0f - (((15803.5645f / fConst2) + 240.858215f) / (fConst2 * fConst25)));
		fConst28 = ((((4965.68896f / fConst2) + 87.4789505f) / fConst2) + 1.0f);
		fConst29 = (1.0f / (fConst25 * fConst28));
		fConst30 = (0.0f - (19862.7559f / (fConst10 * fConst28)));
		fConst31 = (0.0f - (((19862.7559f / fConst2) + 174.957901f) / (fConst2 * fConst28)));
		fConst32 = ((48.2801056f / fConst2) + 1.0f);
		fConst33 = (0.0f - (96.5602112f / (fConst2 * fConst32)));
		fConst34 = ((((2792.13892f / fConst2) + 76.4647293f) / fConst2) + 1.0f);
		fConst35 = (1.0f / (fConst32 * fConst34));
		fConst36 = (0.0f - (11168.5557f / (fConst10 * fConst34)));
		fConst37 = (0.0f - (((11168.5557f / fConst2) + 152.929459f) / (fConst2 * fConst34)));
		fConst38 = ((20.7908058f / fConst2) + 1.0f);
		fConst39 = (1.0f / fConst38);
		fConst40 = (0.0f - (41.5816116f / (fConst2 * fConst38)));
		fConst41 = ((((1296.77283f / fConst2) + 62.3724174f) / fConst2) + 1.0f);
		fConst42 = (1.0f / fConst41);
		fConst43 = (0.0f - (((5187.09131f / fConst2) + 124.744835f) / (fConst2 * fConst41)));
		fConst44 = (0.0f - (5187.09131f / (fConst10 * fConst41)));
		iConst45 = int(((0.00240957108f * float(iConst0)) + 0.5f));
		fConst46 = ((((3952.8064f / fConst2) + 120.45829f) / fConst2) + 1.0f);
		fConst47 = (0.0f - (15811.2256f / (fConst10 * fConst46)));
		fConst48 = (0.0f - (((15811.2256f / fConst2) + 240.91658f) / (fConst2 * fConst46)));
		fConst49 = ((((4968.09668f / fConst2) + 87.5001526f) / fConst2) + 1.0f);
		fConst50 = (1.0f / (fConst46 * fConst49));
		fConst51 = (0.0f - (19872.3867f / (fConst10 * fConst49)));
		fConst52 = (0.0f - (((19872.3867f / fConst2) + 175.000305f) / (fConst2 * fConst49)));
		fConst53 = ((48.2918053f / fConst2) + 1.0f);
		fConst54 = ((((2793.49243f / fConst2) + 76.4832611f) / fConst2) + 1.0f);
		fConst55 = (1.0f / (fConst53 * fConst54));
		fConst56 = (0.0f - (((11173.9697f / fConst2) + 152.966522f) / (fConst2 * fConst54)));
		fConst57 = (0.0f - (11173.9697f / (fConst10 * fConst54)));
		fConst58 = (0.0f - (96.5836105f / (fConst2 * fConst53)));
		fConst59 = ((20.795845f / fConst2) + 1.0f);
		fConst60 = (1.0f / fConst59);
		fConst61 = (0.0f - (41.5916901f / (fConst2 * fConst59)));
		fConst62 = ((((1297.40149f / fConst2) + 62.3875351f) / fConst2) + 1.0f);
		fConst63 = (1.0f / fConst62);
		fConst64 = (0.0f - (5189.60596f / (fConst10 * fConst62)));
		fConst65 = (0.0f - (((5189.60596f / fConst2) + 124.77507f) / (fConst2 * fConst62)));
		iConst66 = int(((0.00241539837f * float(iConst0)) + 0.5f));
		fConst67 = ((20.0921803f / fConst2) + 1.0f);
		fConst68 = (1.0f / fConst67);
		fConst69 = (0.0f - (40.1843605f / (fConst2 * fConst67)));
		fConst70 = ((((1211.08716f / fConst2) + 60.2765388f) / fConst2) + 1.0f);
		fConst71 = (1.0f / fConst70);
		fConst72 = (0.0f - (4844.34863f / (fConst10 * fConst70)));
		fConst73 = (0.0f - (((4844.34863f / fConst2) + 120.553078f) / (fConst2 * fConst70)));
		fConst74 = ((46.6577682f / fConst2) + 1.0f);
		fConst75 = (0.0f - (93.3155365f / (fConst2 * fConst74)));
		fConst76 = ((((2607.64526f / fConst2) + 73.8953171f) / fConst2) + 1.0f);
		fConst77 = (1.0f / (fConst74 * fConst76));
		fConst78 = (0.0f - (10430.5811f / (fConst10 * fConst76)));
		fConst79 = (0.0f - (((10430.5811f / fConst2) + 147.790634f) / (fConst2 * fConst76)));
		fConst80 = ((((3689.83154f / fConst2) + 116.38237f) / fConst2) + 1.0f);
		fConst81 = (0.0f - (14759.3262f / (fConst10 * fConst80)));
		fConst82 = (0.0f - (((14759.3262f / fConst2) + 232.76474f) / (fConst2 * fConst80)));
		fConst83 = ((((4637.57568f / fConst2) + 84.5394287f) / fConst2) + 1.0f);
		fConst84 = (1.0f / (fConst80 * fConst83));
		fConst85 = (0.0f - (18550.3027f / (fConst10 * fConst83)));
		fConst86 = (0.0f - (((18550.3027f / fConst2) + 169.078857f) / (fConst2 * fConst83)));
		iConst87 = int(((0.00157335959f * float(iConst0)) + 0.5f));
		fConst88 = ((((1209.67041f / fConst2) + 60.2412758f) / fConst2) + 1.0f);
		fConst89 = (1.0f / fConst88);
		fConst90 = (0.0f - (4838.68164f / (fConst10 * fConst88)));
		fConst91 = (0.0f - (((4838.68164f / fConst2) + 120.482552f) / (fConst2 * fConst88)));
		fConst92 = ((20.0804253f / fConst2) + 1.0f);
		fConst93 = (1.0f / fConst92);
		fConst94 = (0.0f - (40.1608505f / (fConst2 * fConst92)));
		fConst95 = ((46.6304703f / fConst2) + 1.0f);
		fConst96 = (0.0f - (93.2609406f / (fConst2 * fConst95)));
		fConst97 = ((((2604.59473f / fConst2) + 73.8520813f) / fConst2) + 1.0f);
		fConst98 = (1.0f / (fConst95 * fConst97));
		fConst99 = (0.0f - (10418.3789f / (fConst10 * fConst97)));
		fConst100 = (0.0f - (((10418.3789f / fConst2) + 147.704163f) / (fConst2 * fConst97)));
		fConst101 = ((((3685.51514f / fConst2) + 116.314278f) / fConst2) + 1.0f);
		fConst102 = (0.0f - (14742.0605f / (fConst10 * fConst101)));
		fConst103 = (0.0f - (((14742.0605f / fConst2) + 232.628555f) / (fConst2 * fConst101)));
		fConst104 = ((((4632.15088f / fConst2) + 84.4899673f) / fConst2) + 1.0f);
		fConst105 = (1.0f / (fConst101 * fConst104));
		fConst106 = (0.0f - (18528.6035f / (fConst10 * fConst104)));
		fConst107 = (0.0f - (((18528.6035f / fConst2) + 168.979935f) / (fConst2 * fConst104)));
		iConst108 = int(((0.00155879138f * float(iConst0)) + 0.5f));
		fConst109 = ((((3265.48706f / fConst2) + 109.485825f) / fConst2) + 1.0f);
		fConst110 = (0.0f - (13061.9482f / (fConst10 * fConst109)));
		fConst111 = (0.0f - (((13061.9482f / fConst2) + 218.971649f) / (fConst2 * fConst109)));
		fConst112 = ((((4104.23682f / fConst2) + 79.5298233f) / fConst2) + 1.0f);
		fConst113 = (1.0f / (fConst109 * fConst112));
		fConst114 = (0.0f - (16416.9473f / (fConst10 * fConst112)));
		fConst115 = (0.0f - (((16416.9473f / fConst2) + 159.059647f) / (fConst2 * fConst112)));
		fConst116 = ((43.8929367f / fConst2) + 1.0f);
		fConst117 = (0.0f - (87.7858734f / (fConst2 * fConst116)));
		fConst118 = ((((2307.7561f / fConst2) + 69.5164566f) / fConst2) + 1.0f);
		fConst119 = (1.0f / (fConst116 * fConst118));
		fConst120 = (0.0f - (9231.02441f / (fConst10 * fConst118)));
		fConst121 = (0.0f - (((9231.02441f / fConst2) + 139.032913f) / (fConst2 * fConst118)));
		fConst122 = ((18.9015656f / fConst2) + 1.0f);
		fConst123 = (1.0f / fConst122);
		fConst124 = (0.0f - (37.8031311f / (fConst2 * fConst122)));
		fConst125 = ((((1071.8075f / fConst2) + 56.7046967f) / fConst2) + 1.0f);
		fConst126 = (1.0f / fConst125);
		fConst127 = (0.0f - (4287.22998f / (fConst10 * fConst125)));
		fConst128 = (0.0f - (((4287.22998f / fConst2) + 113.409393f) / (fConst2 * fConst125)));
		iConst129 = int(((5.82725761e-06f * float(iConst0)) + 0.5f));
		fConst130 = ((43.8832703f / fConst2) + 1.0f);
		fConst131 = (0.0f - (87.7665405f / (fConst130 * fConst2)));
		fConst132 = ((((2306.73975f / fConst2) + 69.5011444f) / fConst2) + 1.0f);
		fConst133 = (1.0f / (fConst130 * fConst132));
		fConst134 = (0.0f - (9226.95898f / (fConst132 * fConst10)));
		fConst135 = (0.0f - (((9226.95898f / fConst2) + 139.002289f) / (fConst132 * fConst2)));
		fConst136 = ((((1071.33545f / fConst2) + 56.6922073f) / fConst2) + 1.0f);
		fConst137 = (1.0f / fConst136);
		fConst138 = (0.0f - (4285.3418f / (fConst10 * fConst136)));
		fConst139 = (0.0f - (((4285.3418f / fConst2) + 113.384415f) / (fConst2 * fConst136)));
		fConst140 = ((((3264.04883f / fConst2) + 109.461716f) / fConst2) + 1.0f);
		fConst141 = (0.0f - (13056.1953f / (fConst10 * fConst140)));
		fConst142 = (0.0f - (((13056.1953f / fConst2) + 218.923431f) / (fConst2 * fConst140)));
		fConst143 = ((((4102.4292f / fConst2) + 79.5123062f) / fConst2) + 1.0f);
		fConst144 = (1.0f / (fConst140 * fConst143));
		fConst145 = (0.0f - (16409.7168f / (fConst10 * fConst143)));
		fConst146 = (0.0f - (((16409.7168f / fConst2) + 159.024612f) / (fConst2 * fConst143)));
		fConst147 = ((18.8974018f / fConst2) + 1.0f);
		fConst148 = (1.0f / fConst147);
		fConst149 = (0.0f - (37.7948036f / (fConst147 * fConst2)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec3[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec7[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec8[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec9[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec6[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 2); l8 = (l8 + 1)) {
			fRec4[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec19[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec20[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec21[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec22[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec23[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 3); l14 = (l14 + 1)) {
			fRec24[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 3); l15 = (l15 + 1)) {
			fRec25[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec18[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec16[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec15[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec13[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec12[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec10[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 3); l22 = (l22 + 1)) {
			fRec38[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 3); l23 = (l23 + 1)) {
			fRec39[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 3); l24 = (l24 + 1)) {
			fRec40[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec41[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec42[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec43[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec44[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 3); l29 = (l29 + 1)) {
			fRec45[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 3); l30 = (l30 + 1)) {
			fRec46[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 2); l31 = (l31 + 1)) {
			fRec37[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 2); l32 = (l32 + 1)) {
			fRec35[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 2); l33 = (l33 + 1)) {
			fRec34[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec32[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec31[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec29[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec28[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec26[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 3); l39 = (l39 + 1)) {
			fRec53[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 3); l40 = (l40 + 1)) {
			fRec54[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 3); l41 = (l41 + 1)) {
			fRec55[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 3); l42 = (l42 + 1)) {
			fRec56[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 3); l43 = (l43 + 1)) {
			fRec57[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 2); l44 = (l44 + 1)) {
			fRec52[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 2); l45 = (l45 + 1)) {
			fRec50[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 2); l46 = (l46 + 1)) {
			fRec49[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 2); l47 = (l47 + 1)) {
			fRec47[l47] = 0.0f;
			
		}
		IOTA = 0;
		for (int l48 = 0; (l48 < 1024); l48 = (l48 + 1)) {
			fVec0[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec69[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec67[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec66[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec64[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec63[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec61[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 2); l55 = (l55 + 1)) {
			fRec60[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 2); l56 = (l56 + 1)) {
			fRec58[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 2); l57 = (l57 + 1)) {
			fRec78[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec76[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec75[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 2); l60 = (l60 + 1)) {
			fRec73[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 2); l61 = (l61 + 1)) {
			fRec72[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 2); l62 = (l62 + 1)) {
			fRec70[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 2); l63 = (l63 + 1)) {
			fRec81[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 2); l64 = (l64 + 1)) {
			fRec79[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec87[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec85[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec84[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec82[l68] = 0.0f;
			
		}
		for (int l69 = 0; (l69 < 512); l69 = (l69 + 1)) {
			fVec1[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec99[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec97[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec96[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec94[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec93[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec91[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec90[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec88[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec108[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec106[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec105[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec103[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec102[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec100[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec111[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec109[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec117[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec115[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec114[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec112[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 512); l90 = (l90 + 1)) {
			fVec2[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec129[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec127[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec126[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec124[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec123[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec121[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec120[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec118[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec138[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2); l100 = (l100 + 1)) {
			fRec136[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec135[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec133[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec132[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec130[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec141[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec139[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec147[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec145[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec144[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec142[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 512); l111 = (l111 + 1)) {
			fVec3[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec150[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec148[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec156[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec154[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec153[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec151[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec165[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec163[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec162[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec160[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec159[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec157[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec177[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec175[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec174[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec172[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec171[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec169[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec168[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2); l131 = (l131 + 1)) {
			fRec166[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 512); l132 = (l132 + 1)) {
			fVec4[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec180[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec178[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec186[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec184[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec183[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec181[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec195[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec193[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec192[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec190[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec189[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec187[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec207[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec205[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec204[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec202[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec201[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec199[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec198[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec196[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 512); l153 = (l153 + 1)) {
			fVec5[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec210[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec208[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec216[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec214[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec213[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec211[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec225[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec223[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2); l162 = (l162 + 1)) {
			fRec222[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec220[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec219[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec217[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec237[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec235[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec234[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec232[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec231[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec229[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec228[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec226[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 512); l174 = (l174 + 1)) {
			fVec6[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec240[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec238[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec246[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec244[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec243[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec241[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec255[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec253[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec252[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec250[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec249[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec247[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 2); l187 = (l187 + 1)) {
			fRec267[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec265[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 2); l189 = (l189 + 1)) {
			fRec264[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec262[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec261[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec259[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2); l193 = (l193 + 1)) {
			fRec258[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec256[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 512); l195 = (l195 + 1)) {
			fVec7[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec273[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec271[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec270[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec268[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 2); l200 = (l200 + 1)) {
			fRec276[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec274[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec285[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec283[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec282[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec280[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec279[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec277[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec297[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec295[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec294[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec292[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec291[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 2); l213 = (l213 + 1)) {
			fRec289[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec288[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec286[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 512); l216 = (l216 + 1)) {
			fVec8[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec309[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec307[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec306[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec304[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec303[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec301[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 2); l223 = (l223 + 1)) {
			fRec300[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 2); l224 = (l224 + 1)) {
			fRec298[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec318[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 2); l226 = (l226 + 1)) {
			fRec316[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec315[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec313[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 2); l229 = (l229 + 1)) {
			fRec312[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec310[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec321[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec319[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec327[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec325[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec324[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec322[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 512); l237 = (l237 + 1)) {
			fVec9[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 2); l238 = (l238 + 1)) {
			fRec339[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 2); l239 = (l239 + 1)) {
			fRec337[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec336[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec334[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec333[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 2); l243 = (l243 + 1)) {
			fRec331[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec330[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec328[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec348[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec346[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec345[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec343[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 2); l250 = (l250 + 1)) {
			fRec342[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec340[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 2); l252 = (l252 + 1)) {
			fRec351[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec349[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 2); l254 = (l254 + 1)) {
			fRec357[l254] = 0.0f;
			
		}
		for (int l255 = 0; (l255 < 2); l255 = (l255 + 1)) {
			fRec355[l255] = 0.0f;
			
		}
		for (int l256 = 0; (l256 < 2); l256 = (l256 + 1)) {
			fRec354[l256] = 0.0f;
			
		}
		for (int l257 = 0; (l257 < 2); l257 = (l257 + 1)) {
			fRec352[l257] = 0.0f;
			
		}
		for (int l258 = 0; (l258 < 512); l258 = (l258 + 1)) {
			fVec10[l258] = 0.0f;
			
		}
		for (int l259 = 0; (l259 < 2); l259 = (l259 + 1)) {
			fRec366[l259] = 0.0f;
			
		}
		for (int l260 = 0; (l260 < 2); l260 = (l260 + 1)) {
			fRec364[l260] = 0.0f;
			
		}
		for (int l261 = 0; (l261 < 2); l261 = (l261 + 1)) {
			fRec363[l261] = 0.0f;
			
		}
		for (int l262 = 0; (l262 < 2); l262 = (l262 + 1)) {
			fRec361[l262] = 0.0f;
			
		}
		for (int l263 = 0; (l263 < 2); l263 = (l263 + 1)) {
			fRec360[l263] = 0.0f;
			
		}
		for (int l264 = 0; (l264 < 2); l264 = (l264 + 1)) {
			fRec358[l264] = 0.0f;
			
		}
		for (int l265 = 0; (l265 < 2); l265 = (l265 + 1)) {
			fRec372[l265] = 0.0f;
			
		}
		for (int l266 = 0; (l266 < 2); l266 = (l266 + 1)) {
			fRec370[l266] = 0.0f;
			
		}
		for (int l267 = 0; (l267 < 2); l267 = (l267 + 1)) {
			fRec369[l267] = 0.0f;
			
		}
		for (int l268 = 0; (l268 < 2); l268 = (l268 + 1)) {
			fRec367[l268] = 0.0f;
			
		}
		for (int l269 = 0; (l269 < 2); l269 = (l269 + 1)) {
			fRec384[l269] = 0.0f;
			
		}
		for (int l270 = 0; (l270 < 2); l270 = (l270 + 1)) {
			fRec382[l270] = 0.0f;
			
		}
		for (int l271 = 0; (l271 < 2); l271 = (l271 + 1)) {
			fRec381[l271] = 0.0f;
			
		}
		for (int l272 = 0; (l272 < 2); l272 = (l272 + 1)) {
			fRec379[l272] = 0.0f;
			
		}
		for (int l273 = 0; (l273 < 2); l273 = (l273 + 1)) {
			fRec378[l273] = 0.0f;
			
		}
		for (int l274 = 0; (l274 < 2); l274 = (l274 + 1)) {
			fRec376[l274] = 0.0f;
			
		}
		for (int l275 = 0; (l275 < 2); l275 = (l275 + 1)) {
			fRec375[l275] = 0.0f;
			
		}
		for (int l276 = 0; (l276 < 2); l276 = (l276 + 1)) {
			fRec373[l276] = 0.0f;
			
		}
		for (int l277 = 0; (l277 < 2); l277 = (l277 + 1)) {
			fRec387[l277] = 0.0f;
			
		}
		for (int l278 = 0; (l278 < 2); l278 = (l278 + 1)) {
			fRec385[l278] = 0.0f;
			
		}
		for (int l279 = 0; (l279 < 512); l279 = (l279 + 1)) {
			fVec11[l279] = 0.0f;
			
		}
		for (int l280 = 0; (l280 < 2); l280 = (l280 + 1)) {
			fRec399[l280] = 0.0f;
			
		}
		for (int l281 = 0; (l281 < 2); l281 = (l281 + 1)) {
			fRec397[l281] = 0.0f;
			
		}
		for (int l282 = 0; (l282 < 2); l282 = (l282 + 1)) {
			fRec396[l282] = 0.0f;
			
		}
		for (int l283 = 0; (l283 < 2); l283 = (l283 + 1)) {
			fRec394[l283] = 0.0f;
			
		}
		for (int l284 = 0; (l284 < 2); l284 = (l284 + 1)) {
			fRec393[l284] = 0.0f;
			
		}
		for (int l285 = 0; (l285 < 2); l285 = (l285 + 1)) {
			fRec391[l285] = 0.0f;
			
		}
		for (int l286 = 0; (l286 < 2); l286 = (l286 + 1)) {
			fRec390[l286] = 0.0f;
			
		}
		for (int l287 = 0; (l287 < 2); l287 = (l287 + 1)) {
			fRec388[l287] = 0.0f;
			
		}
		for (int l288 = 0; (l288 < 2); l288 = (l288 + 1)) {
			fRec408[l288] = 0.0f;
			
		}
		for (int l289 = 0; (l289 < 2); l289 = (l289 + 1)) {
			fRec406[l289] = 0.0f;
			
		}
		for (int l290 = 0; (l290 < 2); l290 = (l290 + 1)) {
			fRec405[l290] = 0.0f;
			
		}
		for (int l291 = 0; (l291 < 2); l291 = (l291 + 1)) {
			fRec403[l291] = 0.0f;
			
		}
		for (int l292 = 0; (l292 < 2); l292 = (l292 + 1)) {
			fRec402[l292] = 0.0f;
			
		}
		for (int l293 = 0; (l293 < 2); l293 = (l293 + 1)) {
			fRec400[l293] = 0.0f;
			
		}
		for (int l294 = 0; (l294 < 2); l294 = (l294 + 1)) {
			fRec411[l294] = 0.0f;
			
		}
		for (int l295 = 0; (l295 < 2); l295 = (l295 + 1)) {
			fRec409[l295] = 0.0f;
			
		}
		for (int l296 = 0; (l296 < 2); l296 = (l296 + 1)) {
			fRec417[l296] = 0.0f;
			
		}
		for (int l297 = 0; (l297 < 2); l297 = (l297 + 1)) {
			fRec415[l297] = 0.0f;
			
		}
		for (int l298 = 0; (l298 < 2); l298 = (l298 + 1)) {
			fRec414[l298] = 0.0f;
			
		}
		for (int l299 = 0; (l299 < 2); l299 = (l299 + 1)) {
			fRec412[l299] = 0.0f;
			
		}
		for (int l300 = 0; (l300 < 512); l300 = (l300 + 1)) {
			fVec12[l300] = 0.0f;
			
		}
		for (int l301 = 0; (l301 < 2); l301 = (l301 + 1)) {
			fRec423[l301] = 0.0f;
			
		}
		for (int l302 = 0; (l302 < 2); l302 = (l302 + 1)) {
			fRec421[l302] = 0.0f;
			
		}
		for (int l303 = 0; (l303 < 2); l303 = (l303 + 1)) {
			fRec420[l303] = 0.0f;
			
		}
		for (int l304 = 0; (l304 < 2); l304 = (l304 + 1)) {
			fRec418[l304] = 0.0f;
			
		}
		for (int l305 = 0; (l305 < 2); l305 = (l305 + 1)) {
			fRec426[l305] = 0.0f;
			
		}
		for (int l306 = 0; (l306 < 2); l306 = (l306 + 1)) {
			fRec424[l306] = 0.0f;
			
		}
		for (int l307 = 0; (l307 < 2); l307 = (l307 + 1)) {
			fRec435[l307] = 0.0f;
			
		}
		for (int l308 = 0; (l308 < 2); l308 = (l308 + 1)) {
			fRec433[l308] = 0.0f;
			
		}
		for (int l309 = 0; (l309 < 2); l309 = (l309 + 1)) {
			fRec432[l309] = 0.0f;
			
		}
		for (int l310 = 0; (l310 < 2); l310 = (l310 + 1)) {
			fRec430[l310] = 0.0f;
			
		}
		for (int l311 = 0; (l311 < 2); l311 = (l311 + 1)) {
			fRec429[l311] = 0.0f;
			
		}
		for (int l312 = 0; (l312 < 2); l312 = (l312 + 1)) {
			fRec427[l312] = 0.0f;
			
		}
		for (int l313 = 0; (l313 < 2); l313 = (l313 + 1)) {
			fRec447[l313] = 0.0f;
			
		}
		for (int l314 = 0; (l314 < 2); l314 = (l314 + 1)) {
			fRec445[l314] = 0.0f;
			
		}
		for (int l315 = 0; (l315 < 2); l315 = (l315 + 1)) {
			fRec444[l315] = 0.0f;
			
		}
		for (int l316 = 0; (l316 < 2); l316 = (l316 + 1)) {
			fRec442[l316] = 0.0f;
			
		}
		for (int l317 = 0; (l317 < 2); l317 = (l317 + 1)) {
			fRec441[l317] = 0.0f;
			
		}
		for (int l318 = 0; (l318 < 2); l318 = (l318 + 1)) {
			fRec439[l318] = 0.0f;
			
		}
		for (int l319 = 0; (l319 < 2); l319 = (l319 + 1)) {
			fRec438[l319] = 0.0f;
			
		}
		for (int l320 = 0; (l320 < 2); l320 = (l320 + 1)) {
			fRec436[l320] = 0.0f;
			
		}
		for (int l321 = 0; (l321 < 512); l321 = (l321 + 1)) {
			fVec13[l321] = 0.0f;
			
		}
		for (int l322 = 0; (l322 < 2); l322 = (l322 + 1)) {
			fRec459[l322] = 0.0f;
			
		}
		for (int l323 = 0; (l323 < 2); l323 = (l323 + 1)) {
			fRec457[l323] = 0.0f;
			
		}
		for (int l324 = 0; (l324 < 2); l324 = (l324 + 1)) {
			fRec456[l324] = 0.0f;
			
		}
		for (int l325 = 0; (l325 < 2); l325 = (l325 + 1)) {
			fRec454[l325] = 0.0f;
			
		}
		for (int l326 = 0; (l326 < 2); l326 = (l326 + 1)) {
			fRec453[l326] = 0.0f;
			
		}
		for (int l327 = 0; (l327 < 2); l327 = (l327 + 1)) {
			fRec451[l327] = 0.0f;
			
		}
		for (int l328 = 0; (l328 < 2); l328 = (l328 + 1)) {
			fRec450[l328] = 0.0f;
			
		}
		for (int l329 = 0; (l329 < 2); l329 = (l329 + 1)) {
			fRec448[l329] = 0.0f;
			
		}
		for (int l330 = 0; (l330 < 2); l330 = (l330 + 1)) {
			fRec468[l330] = 0.0f;
			
		}
		for (int l331 = 0; (l331 < 2); l331 = (l331 + 1)) {
			fRec466[l331] = 0.0f;
			
		}
		for (int l332 = 0; (l332 < 2); l332 = (l332 + 1)) {
			fRec465[l332] = 0.0f;
			
		}
		for (int l333 = 0; (l333 < 2); l333 = (l333 + 1)) {
			fRec463[l333] = 0.0f;
			
		}
		for (int l334 = 0; (l334 < 2); l334 = (l334 + 1)) {
			fRec462[l334] = 0.0f;
			
		}
		for (int l335 = 0; (l335 < 2); l335 = (l335 + 1)) {
			fRec460[l335] = 0.0f;
			
		}
		for (int l336 = 0; (l336 < 2); l336 = (l336 + 1)) {
			fRec471[l336] = 0.0f;
			
		}
		for (int l337 = 0; (l337 < 2); l337 = (l337 + 1)) {
			fRec469[l337] = 0.0f;
			
		}
		for (int l338 = 0; (l338 < 2); l338 = (l338 + 1)) {
			fRec477[l338] = 0.0f;
			
		}
		for (int l339 = 0; (l339 < 2); l339 = (l339 + 1)) {
			fRec475[l339] = 0.0f;
			
		}
		for (int l340 = 0; (l340 < 2); l340 = (l340 + 1)) {
			fRec474[l340] = 0.0f;
			
		}
		for (int l341 = 0; (l341 < 2); l341 = (l341 + 1)) {
			fRec472[l341] = 0.0f;
			
		}
		for (int l342 = 0; (l342 < 512); l342 = (l342 + 1)) {
			fVec14[l342] = 0.0f;
			
		}
		for (int l343 = 0; (l343 < 2); l343 = (l343 + 1)) {
			fRec480[l343] = 0.0f;
			
		}
		for (int l344 = 0; (l344 < 2); l344 = (l344 + 1)) {
			fRec478[l344] = 0.0f;
			
		}
		for (int l345 = 0; (l345 < 2); l345 = (l345 + 1)) {
			fRec489[l345] = 0.0f;
			
		}
		for (int l346 = 0; (l346 < 2); l346 = (l346 + 1)) {
			fRec487[l346] = 0.0f;
			
		}
		for (int l347 = 0; (l347 < 2); l347 = (l347 + 1)) {
			fRec486[l347] = 0.0f;
			
		}
		for (int l348 = 0; (l348 < 2); l348 = (l348 + 1)) {
			fRec484[l348] = 0.0f;
			
		}
		for (int l349 = 0; (l349 < 2); l349 = (l349 + 1)) {
			fRec483[l349] = 0.0f;
			
		}
		for (int l350 = 0; (l350 < 2); l350 = (l350 + 1)) {
			fRec481[l350] = 0.0f;
			
		}
		for (int l351 = 0; (l351 < 2); l351 = (l351 + 1)) {
			fRec501[l351] = 0.0f;
			
		}
		for (int l352 = 0; (l352 < 2); l352 = (l352 + 1)) {
			fRec499[l352] = 0.0f;
			
		}
		for (int l353 = 0; (l353 < 2); l353 = (l353 + 1)) {
			fRec498[l353] = 0.0f;
			
		}
		for (int l354 = 0; (l354 < 2); l354 = (l354 + 1)) {
			fRec496[l354] = 0.0f;
			
		}
		for (int l355 = 0; (l355 < 2); l355 = (l355 + 1)) {
			fRec495[l355] = 0.0f;
			
		}
		for (int l356 = 0; (l356 < 2); l356 = (l356 + 1)) {
			fRec493[l356] = 0.0f;
			
		}
		for (int l357 = 0; (l357 < 2); l357 = (l357 + 1)) {
			fRec492[l357] = 0.0f;
			
		}
		for (int l358 = 0; (l358 < 2); l358 = (l358 + 1)) {
			fRec490[l358] = 0.0f;
			
		}
		for (int l359 = 0; (l359 < 2); l359 = (l359 + 1)) {
			fRec507[l359] = 0.0f;
			
		}
		for (int l360 = 0; (l360 < 2); l360 = (l360 + 1)) {
			fRec505[l360] = 0.0f;
			
		}
		for (int l361 = 0; (l361 < 2); l361 = (l361 + 1)) {
			fRec504[l361] = 0.0f;
			
		}
		for (int l362 = 0; (l362 < 2); l362 = (l362 + 1)) {
			fRec502[l362] = 0.0f;
			
		}
		for (int l363 = 0; (l363 < 512); l363 = (l363 + 1)) {
			fVec15[l363] = 0.0f;
			
		}
		for (int l364 = 0; (l364 < 2); l364 = (l364 + 1)) {
			fRec519[l364] = 0.0f;
			
		}
		for (int l365 = 0; (l365 < 2); l365 = (l365 + 1)) {
			fRec517[l365] = 0.0f;
			
		}
		for (int l366 = 0; (l366 < 2); l366 = (l366 + 1)) {
			fRec516[l366] = 0.0f;
			
		}
		for (int l367 = 0; (l367 < 2); l367 = (l367 + 1)) {
			fRec514[l367] = 0.0f;
			
		}
		for (int l368 = 0; (l368 < 2); l368 = (l368 + 1)) {
			fRec513[l368] = 0.0f;
			
		}
		for (int l369 = 0; (l369 < 2); l369 = (l369 + 1)) {
			fRec511[l369] = 0.0f;
			
		}
		for (int l370 = 0; (l370 < 2); l370 = (l370 + 1)) {
			fRec510[l370] = 0.0f;
			
		}
		for (int l371 = 0; (l371 < 2); l371 = (l371 + 1)) {
			fRec508[l371] = 0.0f;
			
		}
		for (int l372 = 0; (l372 < 2); l372 = (l372 + 1)) {
			fRec528[l372] = 0.0f;
			
		}
		for (int l373 = 0; (l373 < 2); l373 = (l373 + 1)) {
			fRec526[l373] = 0.0f;
			
		}
		for (int l374 = 0; (l374 < 2); l374 = (l374 + 1)) {
			fRec525[l374] = 0.0f;
			
		}
		for (int l375 = 0; (l375 < 2); l375 = (l375 + 1)) {
			fRec523[l375] = 0.0f;
			
		}
		for (int l376 = 0; (l376 < 2); l376 = (l376 + 1)) {
			fRec522[l376] = 0.0f;
			
		}
		for (int l377 = 0; (l377 < 2); l377 = (l377 + 1)) {
			fRec520[l377] = 0.0f;
			
		}
		for (int l378 = 0; (l378 < 2); l378 = (l378 + 1)) {
			fRec531[l378] = 0.0f;
			
		}
		for (int l379 = 0; (l379 < 2); l379 = (l379 + 1)) {
			fRec529[l379] = 0.0f;
			
		}
		for (int l380 = 0; (l380 < 2); l380 = (l380 + 1)) {
			fRec537[l380] = 0.0f;
			
		}
		for (int l381 = 0; (l381 < 2); l381 = (l381 + 1)) {
			fRec535[l381] = 0.0f;
			
		}
		for (int l382 = 0; (l382 < 2); l382 = (l382 + 1)) {
			fRec534[l382] = 0.0f;
			
		}
		for (int l383 = 0; (l383 < 2); l383 = (l383 + 1)) {
			fRec532[l383] = 0.0f;
			
		}
		for (int l384 = 0; (l384 < 512); l384 = (l384 + 1)) {
			fVec16[l384] = 0.0f;
			
		}
		for (int l385 = 0; (l385 < 2); l385 = (l385 + 1)) {
			fRec549[l385] = 0.0f;
			
		}
		for (int l386 = 0; (l386 < 2); l386 = (l386 + 1)) {
			fRec547[l386] = 0.0f;
			
		}
		for (int l387 = 0; (l387 < 2); l387 = (l387 + 1)) {
			fRec546[l387] = 0.0f;
			
		}
		for (int l388 = 0; (l388 < 2); l388 = (l388 + 1)) {
			fRec544[l388] = 0.0f;
			
		}
		for (int l389 = 0; (l389 < 2); l389 = (l389 + 1)) {
			fRec543[l389] = 0.0f;
			
		}
		for (int l390 = 0; (l390 < 2); l390 = (l390 + 1)) {
			fRec541[l390] = 0.0f;
			
		}
		for (int l391 = 0; (l391 < 2); l391 = (l391 + 1)) {
			fRec540[l391] = 0.0f;
			
		}
		for (int l392 = 0; (l392 < 2); l392 = (l392 + 1)) {
			fRec538[l392] = 0.0f;
			
		}
		for (int l393 = 0; (l393 < 2); l393 = (l393 + 1)) {
			fRec555[l393] = 0.0f;
			
		}
		for (int l394 = 0; (l394 < 2); l394 = (l394 + 1)) {
			fRec553[l394] = 0.0f;
			
		}
		for (int l395 = 0; (l395 < 2); l395 = (l395 + 1)) {
			fRec552[l395] = 0.0f;
			
		}
		for (int l396 = 0; (l396 < 2); l396 = (l396 + 1)) {
			fRec550[l396] = 0.0f;
			
		}
		for (int l397 = 0; (l397 < 2); l397 = (l397 + 1)) {
			fRec564[l397] = 0.0f;
			
		}
		for (int l398 = 0; (l398 < 2); l398 = (l398 + 1)) {
			fRec562[l398] = 0.0f;
			
		}
		for (int l399 = 0; (l399 < 2); l399 = (l399 + 1)) {
			fRec561[l399] = 0.0f;
			
		}
		for (int l400 = 0; (l400 < 2); l400 = (l400 + 1)) {
			fRec559[l400] = 0.0f;
			
		}
		for (int l401 = 0; (l401 < 2); l401 = (l401 + 1)) {
			fRec558[l401] = 0.0f;
			
		}
		for (int l402 = 0; (l402 < 2); l402 = (l402 + 1)) {
			fRec556[l402] = 0.0f;
			
		}
		for (int l403 = 0; (l403 < 2); l403 = (l403 + 1)) {
			fRec567[l403] = 0.0f;
			
		}
		for (int l404 = 0; (l404 < 2); l404 = (l404 + 1)) {
			fRec565[l404] = 0.0f;
			
		}
		for (int l405 = 0; (l405 < 512); l405 = (l405 + 1)) {
			fVec17[l405] = 0.0f;
			
		}
		for (int l406 = 0; (l406 < 2); l406 = (l406 + 1)) {
			fRec579[l406] = 0.0f;
			
		}
		for (int l407 = 0; (l407 < 2); l407 = (l407 + 1)) {
			fRec577[l407] = 0.0f;
			
		}
		for (int l408 = 0; (l408 < 2); l408 = (l408 + 1)) {
			fRec576[l408] = 0.0f;
			
		}
		for (int l409 = 0; (l409 < 2); l409 = (l409 + 1)) {
			fRec574[l409] = 0.0f;
			
		}
		for (int l410 = 0; (l410 < 2); l410 = (l410 + 1)) {
			fRec573[l410] = 0.0f;
			
		}
		for (int l411 = 0; (l411 < 2); l411 = (l411 + 1)) {
			fRec571[l411] = 0.0f;
			
		}
		for (int l412 = 0; (l412 < 2); l412 = (l412 + 1)) {
			fRec570[l412] = 0.0f;
			
		}
		for (int l413 = 0; (l413 < 2); l413 = (l413 + 1)) {
			fRec568[l413] = 0.0f;
			
		}
		for (int l414 = 0; (l414 < 2); l414 = (l414 + 1)) {
			fRec588[l414] = 0.0f;
			
		}
		for (int l415 = 0; (l415 < 2); l415 = (l415 + 1)) {
			fRec586[l415] = 0.0f;
			
		}
		for (int l416 = 0; (l416 < 2); l416 = (l416 + 1)) {
			fRec585[l416] = 0.0f;
			
		}
		for (int l417 = 0; (l417 < 2); l417 = (l417 + 1)) {
			fRec583[l417] = 0.0f;
			
		}
		for (int l418 = 0; (l418 < 2); l418 = (l418 + 1)) {
			fRec582[l418] = 0.0f;
			
		}
		for (int l419 = 0; (l419 < 2); l419 = (l419 + 1)) {
			fRec580[l419] = 0.0f;
			
		}
		for (int l420 = 0; (l420 < 2); l420 = (l420 + 1)) {
			fRec591[l420] = 0.0f;
			
		}
		for (int l421 = 0; (l421 < 2); l421 = (l421 + 1)) {
			fRec589[l421] = 0.0f;
			
		}
		for (int l422 = 0; (l422 < 2); l422 = (l422 + 1)) {
			fRec597[l422] = 0.0f;
			
		}
		for (int l423 = 0; (l423 < 2); l423 = (l423 + 1)) {
			fRec595[l423] = 0.0f;
			
		}
		for (int l424 = 0; (l424 < 2); l424 = (l424 + 1)) {
			fRec594[l424] = 0.0f;
			
		}
		for (int l425 = 0; (l425 < 2); l425 = (l425 + 1)) {
			fRec592[l425] = 0.0f;
			
		}
		for (int l426 = 0; (l426 < 512); l426 = (l426 + 1)) {
			fVec18[l426] = 0.0f;
			
		}
		for (int l427 = 0; (l427 < 2); l427 = (l427 + 1)) {
			fRec609[l427] = 0.0f;
			
		}
		for (int l428 = 0; (l428 < 2); l428 = (l428 + 1)) {
			fRec607[l428] = 0.0f;
			
		}
		for (int l429 = 0; (l429 < 2); l429 = (l429 + 1)) {
			fRec606[l429] = 0.0f;
			
		}
		for (int l430 = 0; (l430 < 2); l430 = (l430 + 1)) {
			fRec604[l430] = 0.0f;
			
		}
		for (int l431 = 0; (l431 < 2); l431 = (l431 + 1)) {
			fRec603[l431] = 0.0f;
			
		}
		for (int l432 = 0; (l432 < 2); l432 = (l432 + 1)) {
			fRec601[l432] = 0.0f;
			
		}
		for (int l433 = 0; (l433 < 2); l433 = (l433 + 1)) {
			fRec600[l433] = 0.0f;
			
		}
		for (int l434 = 0; (l434 < 2); l434 = (l434 + 1)) {
			fRec598[l434] = 0.0f;
			
		}
		for (int l435 = 0; (l435 < 2); l435 = (l435 + 1)) {
			fRec618[l435] = 0.0f;
			
		}
		for (int l436 = 0; (l436 < 2); l436 = (l436 + 1)) {
			fRec616[l436] = 0.0f;
			
		}
		for (int l437 = 0; (l437 < 2); l437 = (l437 + 1)) {
			fRec615[l437] = 0.0f;
			
		}
		for (int l438 = 0; (l438 < 2); l438 = (l438 + 1)) {
			fRec613[l438] = 0.0f;
			
		}
		for (int l439 = 0; (l439 < 2); l439 = (l439 + 1)) {
			fRec612[l439] = 0.0f;
			
		}
		for (int l440 = 0; (l440 < 2); l440 = (l440 + 1)) {
			fRec610[l440] = 0.0f;
			
		}
		for (int l441 = 0; (l441 < 2); l441 = (l441 + 1)) {
			fRec621[l441] = 0.0f;
			
		}
		for (int l442 = 0; (l442 < 2); l442 = (l442 + 1)) {
			fRec619[l442] = 0.0f;
			
		}
		for (int l443 = 0; (l443 < 2); l443 = (l443 + 1)) {
			fRec627[l443] = 0.0f;
			
		}
		for (int l444 = 0; (l444 < 2); l444 = (l444 + 1)) {
			fRec625[l444] = 0.0f;
			
		}
		for (int l445 = 0; (l445 < 2); l445 = (l445 + 1)) {
			fRec624[l445] = 0.0f;
			
		}
		for (int l446 = 0; (l446 < 2); l446 = (l446 + 1)) {
			fRec622[l446] = 0.0f;
			
		}
		for (int l447 = 0; (l447 < 3); l447 = (l447 + 1)) {
			fVec19[l447] = 0.0f;
			
		}
		for (int l448 = 0; (l448 < 2); l448 = (l448 + 1)) {
			fRec636[l448] = 0.0f;
			
		}
		for (int l449 = 0; (l449 < 2); l449 = (l449 + 1)) {
			fRec634[l449] = 0.0f;
			
		}
		for (int l450 = 0; (l450 < 2); l450 = (l450 + 1)) {
			fRec633[l450] = 0.0f;
			
		}
		for (int l451 = 0; (l451 < 2); l451 = (l451 + 1)) {
			fRec631[l451] = 0.0f;
			
		}
		for (int l452 = 0; (l452 < 2); l452 = (l452 + 1)) {
			fRec630[l452] = 0.0f;
			
		}
		for (int l453 = 0; (l453 < 2); l453 = (l453 + 1)) {
			fRec628[l453] = 0.0f;
			
		}
		for (int l454 = 0; (l454 < 2); l454 = (l454 + 1)) {
			fRec642[l454] = 0.0f;
			
		}
		for (int l455 = 0; (l455 < 2); l455 = (l455 + 1)) {
			fRec640[l455] = 0.0f;
			
		}
		for (int l456 = 0; (l456 < 2); l456 = (l456 + 1)) {
			fRec639[l456] = 0.0f;
			
		}
		for (int l457 = 0; (l457 < 2); l457 = (l457 + 1)) {
			fRec637[l457] = 0.0f;
			
		}
		for (int l458 = 0; (l458 < 2); l458 = (l458 + 1)) {
			fRec654[l458] = 0.0f;
			
		}
		for (int l459 = 0; (l459 < 2); l459 = (l459 + 1)) {
			fRec652[l459] = 0.0f;
			
		}
		for (int l460 = 0; (l460 < 2); l460 = (l460 + 1)) {
			fRec651[l460] = 0.0f;
			
		}
		for (int l461 = 0; (l461 < 2); l461 = (l461 + 1)) {
			fRec649[l461] = 0.0f;
			
		}
		for (int l462 = 0; (l462 < 2); l462 = (l462 + 1)) {
			fRec648[l462] = 0.0f;
			
		}
		for (int l463 = 0; (l463 < 2); l463 = (l463 + 1)) {
			fRec646[l463] = 0.0f;
			
		}
		for (int l464 = 0; (l464 < 2); l464 = (l464 + 1)) {
			fRec645[l464] = 0.0f;
			
		}
		for (int l465 = 0; (l465 < 2); l465 = (l465 + 1)) {
			fRec643[l465] = 0.0f;
			
		}
		for (int l466 = 0; (l466 < 2); l466 = (l466 + 1)) {
			fRec657[l466] = 0.0f;
			
		}
		for (int l467 = 0; (l467 < 2); l467 = (l467 + 1)) {
			fRec655[l467] = 0.0f;
			
		}
		for (int l468 = 0; (l468 < 2); l468 = (l468 + 1)) {
			fRec666[l468] = 0.0f;
			
		}
		for (int l469 = 0; (l469 < 2); l469 = (l469 + 1)) {
			fRec664[l469] = 0.0f;
			
		}
		for (int l470 = 0; (l470 < 2); l470 = (l470 + 1)) {
			fRec663[l470] = 0.0f;
			
		}
		for (int l471 = 0; (l471 < 2); l471 = (l471 + 1)) {
			fRec661[l471] = 0.0f;
			
		}
		for (int l472 = 0; (l472 < 2); l472 = (l472 + 1)) {
			fRec660[l472] = 0.0f;
			
		}
		for (int l473 = 0; (l473 < 2); l473 = (l473 + 1)) {
			fRec658[l473] = 0.0f;
			
		}
		for (int l474 = 0; (l474 < 2); l474 = (l474 + 1)) {
			fRec669[l474] = 0.0f;
			
		}
		for (int l475 = 0; (l475 < 2); l475 = (l475 + 1)) {
			fRec667[l475] = 0.0f;
			
		}
		for (int l476 = 0; (l476 < 2); l476 = (l476 + 1)) {
			fRec675[l476] = 0.0f;
			
		}
		for (int l477 = 0; (l477 < 2); l477 = (l477 + 1)) {
			fRec673[l477] = 0.0f;
			
		}
		for (int l478 = 0; (l478 < 2); l478 = (l478 + 1)) {
			fRec672[l478] = 0.0f;
			
		}
		for (int l479 = 0; (l479 < 2); l479 = (l479 + 1)) {
			fRec670[l479] = 0.0f;
			
		}
		for (int l480 = 0; (l480 < 2); l480 = (l480 + 1)) {
			fRec687[l480] = 0.0f;
			
		}
		for (int l481 = 0; (l481 < 2); l481 = (l481 + 1)) {
			fRec685[l481] = 0.0f;
			
		}
		for (int l482 = 0; (l482 < 2); l482 = (l482 + 1)) {
			fRec684[l482] = 0.0f;
			
		}
		for (int l483 = 0; (l483 < 2); l483 = (l483 + 1)) {
			fRec682[l483] = 0.0f;
			
		}
		for (int l484 = 0; (l484 < 2); l484 = (l484 + 1)) {
			fRec681[l484] = 0.0f;
			
		}
		for (int l485 = 0; (l485 < 2); l485 = (l485 + 1)) {
			fRec679[l485] = 0.0f;
			
		}
		for (int l486 = 0; (l486 < 2); l486 = (l486 + 1)) {
			fRec678[l486] = 0.0f;
			
		}
		for (int l487 = 0; (l487 < 2); l487 = (l487 + 1)) {
			fRec676[l487] = 0.0f;
			
		}
		for (int l488 = 0; (l488 < 2); l488 = (l488 + 1)) {
			fRec699[l488] = 0.0f;
			
		}
		for (int l489 = 0; (l489 < 2); l489 = (l489 + 1)) {
			fRec697[l489] = 0.0f;
			
		}
		for (int l490 = 0; (l490 < 2); l490 = (l490 + 1)) {
			fRec696[l490] = 0.0f;
			
		}
		for (int l491 = 0; (l491 < 2); l491 = (l491 + 1)) {
			fRec694[l491] = 0.0f;
			
		}
		for (int l492 = 0; (l492 < 2); l492 = (l492 + 1)) {
			fRec693[l492] = 0.0f;
			
		}
		for (int l493 = 0; (l493 < 2); l493 = (l493 + 1)) {
			fRec691[l493] = 0.0f;
			
		}
		for (int l494 = 0; (l494 < 2); l494 = (l494 + 1)) {
			fRec690[l494] = 0.0f;
			
		}
		for (int l495 = 0; (l495 < 2); l495 = (l495 + 1)) {
			fRec688[l495] = 0.0f;
			
		}
		for (int l496 = 0; (l496 < 2); l496 = (l496 + 1)) {
			fRec708[l496] = 0.0f;
			
		}
		for (int l497 = 0; (l497 < 2); l497 = (l497 + 1)) {
			fRec706[l497] = 0.0f;
			
		}
		for (int l498 = 0; (l498 < 2); l498 = (l498 + 1)) {
			fRec705[l498] = 0.0f;
			
		}
		for (int l499 = 0; (l499 < 2); l499 = (l499 + 1)) {
			fRec703[l499] = 0.0f;
			
		}
		for (int l500 = 0; (l500 < 2); l500 = (l500 + 1)) {
			fRec702[l500] = 0.0f;
			
		}
		for (int l501 = 0; (l501 < 2); l501 = (l501 + 1)) {
			fRec700[l501] = 0.0f;
			
		}
		for (int l502 = 0; (l502 < 2); l502 = (l502 + 1)) {
			fRec711[l502] = 0.0f;
			
		}
		for (int l503 = 0; (l503 < 2); l503 = (l503 + 1)) {
			fRec709[l503] = 0.0f;
			
		}
		for (int l504 = 0; (l504 < 2); l504 = (l504 + 1)) {
			fRec717[l504] = 0.0f;
			
		}
		for (int l505 = 0; (l505 < 2); l505 = (l505 + 1)) {
			fRec715[l505] = 0.0f;
			
		}
		for (int l506 = 0; (l506 < 2); l506 = (l506 + 1)) {
			fRec714[l506] = 0.0f;
			
		}
		for (int l507 = 0; (l507 < 2); l507 = (l507 + 1)) {
			fRec712[l507] = 0.0f;
			
		}
		for (int l508 = 0; (l508 < 3); l508 = (l508 + 1)) {
			fVec20[l508] = 0.0f;
			
		}
		for (int l509 = 0; (l509 < 2); l509 = (l509 + 1)) {
			fRec729[l509] = 0.0f;
			
		}
		for (int l510 = 0; (l510 < 2); l510 = (l510 + 1)) {
			fRec727[l510] = 0.0f;
			
		}
		for (int l511 = 0; (l511 < 2); l511 = (l511 + 1)) {
			fRec726[l511] = 0.0f;
			
		}
		for (int l512 = 0; (l512 < 2); l512 = (l512 + 1)) {
			fRec724[l512] = 0.0f;
			
		}
		for (int l513 = 0; (l513 < 2); l513 = (l513 + 1)) {
			fRec723[l513] = 0.0f;
			
		}
		for (int l514 = 0; (l514 < 2); l514 = (l514 + 1)) {
			fRec721[l514] = 0.0f;
			
		}
		for (int l515 = 0; (l515 < 2); l515 = (l515 + 1)) {
			fRec720[l515] = 0.0f;
			
		}
		for (int l516 = 0; (l516 < 2); l516 = (l516 + 1)) {
			fRec718[l516] = 0.0f;
			
		}
		for (int l517 = 0; (l517 < 2); l517 = (l517 + 1)) {
			fRec732[l517] = 0.0f;
			
		}
		for (int l518 = 0; (l518 < 2); l518 = (l518 + 1)) {
			fRec730[l518] = 0.0f;
			
		}
		for (int l519 = 0; (l519 < 2); l519 = (l519 + 1)) {
			fRec738[l519] = 0.0f;
			
		}
		for (int l520 = 0; (l520 < 2); l520 = (l520 + 1)) {
			fRec736[l520] = 0.0f;
			
		}
		for (int l521 = 0; (l521 < 2); l521 = (l521 + 1)) {
			fRec735[l521] = 0.0f;
			
		}
		for (int l522 = 0; (l522 < 2); l522 = (l522 + 1)) {
			fRec733[l522] = 0.0f;
			
		}
		for (int l523 = 0; (l523 < 2); l523 = (l523 + 1)) {
			fRec747[l523] = 0.0f;
			
		}
		for (int l524 = 0; (l524 < 2); l524 = (l524 + 1)) {
			fRec745[l524] = 0.0f;
			
		}
		for (int l525 = 0; (l525 < 2); l525 = (l525 + 1)) {
			fRec744[l525] = 0.0f;
			
		}
		for (int l526 = 0; (l526 < 2); l526 = (l526 + 1)) {
			fRec742[l526] = 0.0f;
			
		}
		for (int l527 = 0; (l527 < 2); l527 = (l527 + 1)) {
			fRec741[l527] = 0.0f;
			
		}
		for (int l528 = 0; (l528 < 2); l528 = (l528 + 1)) {
			fRec739[l528] = 0.0f;
			
		}
		for (int l529 = 0; (l529 < 2); l529 = (l529 + 1)) {
			fRec753[l529] = 0.0f;
			
		}
		for (int l530 = 0; (l530 < 2); l530 = (l530 + 1)) {
			fRec751[l530] = 0.0f;
			
		}
		for (int l531 = 0; (l531 < 2); l531 = (l531 + 1)) {
			fRec750[l531] = 0.0f;
			
		}
		for (int l532 = 0; (l532 < 2); l532 = (l532 + 1)) {
			fRec748[l532] = 0.0f;
			
		}
		for (int l533 = 0; (l533 < 2); l533 = (l533 + 1)) {
			fRec756[l533] = 0.0f;
			
		}
		for (int l534 = 0; (l534 < 2); l534 = (l534 + 1)) {
			fRec754[l534] = 0.0f;
			
		}
		for (int l535 = 0; (l535 < 2); l535 = (l535 + 1)) {
			fRec765[l535] = 0.0f;
			
		}
		for (int l536 = 0; (l536 < 2); l536 = (l536 + 1)) {
			fRec763[l536] = 0.0f;
			
		}
		for (int l537 = 0; (l537 < 2); l537 = (l537 + 1)) {
			fRec762[l537] = 0.0f;
			
		}
		for (int l538 = 0; (l538 < 2); l538 = (l538 + 1)) {
			fRec760[l538] = 0.0f;
			
		}
		for (int l539 = 0; (l539 < 2); l539 = (l539 + 1)) {
			fRec759[l539] = 0.0f;
			
		}
		for (int l540 = 0; (l540 < 2); l540 = (l540 + 1)) {
			fRec757[l540] = 0.0f;
			
		}
		for (int l541 = 0; (l541 < 2); l541 = (l541 + 1)) {
			fRec777[l541] = 0.0f;
			
		}
		for (int l542 = 0; (l542 < 2); l542 = (l542 + 1)) {
			fRec775[l542] = 0.0f;
			
		}
		for (int l543 = 0; (l543 < 2); l543 = (l543 + 1)) {
			fRec774[l543] = 0.0f;
			
		}
		for (int l544 = 0; (l544 < 2); l544 = (l544 + 1)) {
			fRec772[l544] = 0.0f;
			
		}
		for (int l545 = 0; (l545 < 2); l545 = (l545 + 1)) {
			fRec771[l545] = 0.0f;
			
		}
		for (int l546 = 0; (l546 < 2); l546 = (l546 + 1)) {
			fRec769[l546] = 0.0f;
			
		}
		for (int l547 = 0; (l547 < 2); l547 = (l547 + 1)) {
			fRec768[l547] = 0.0f;
			
		}
		for (int l548 = 0; (l548 < 2); l548 = (l548 + 1)) {
			fRec766[l548] = 0.0f;
			
		}
		for (int l549 = 0; (l549 < 2); l549 = (l549 + 1)) {
			fRec789[l549] = 0.0f;
			
		}
		for (int l550 = 0; (l550 < 2); l550 = (l550 + 1)) {
			fRec787[l550] = 0.0f;
			
		}
		for (int l551 = 0; (l551 < 2); l551 = (l551 + 1)) {
			fRec786[l551] = 0.0f;
			
		}
		for (int l552 = 0; (l552 < 2); l552 = (l552 + 1)) {
			fRec784[l552] = 0.0f;
			
		}
		for (int l553 = 0; (l553 < 2); l553 = (l553 + 1)) {
			fRec783[l553] = 0.0f;
			
		}
		for (int l554 = 0; (l554 < 2); l554 = (l554 + 1)) {
			fRec781[l554] = 0.0f;
			
		}
		for (int l555 = 0; (l555 < 2); l555 = (l555 + 1)) {
			fRec780[l555] = 0.0f;
			
		}
		for (int l556 = 0; (l556 < 2); l556 = (l556 + 1)) {
			fRec778[l556] = 0.0f;
			
		}
		for (int l557 = 0; (l557 < 2); l557 = (l557 + 1)) {
			fRec798[l557] = 0.0f;
			
		}
		for (int l558 = 0; (l558 < 2); l558 = (l558 + 1)) {
			fRec796[l558] = 0.0f;
			
		}
		for (int l559 = 0; (l559 < 2); l559 = (l559 + 1)) {
			fRec795[l559] = 0.0f;
			
		}
		for (int l560 = 0; (l560 < 2); l560 = (l560 + 1)) {
			fRec793[l560] = 0.0f;
			
		}
		for (int l561 = 0; (l561 < 2); l561 = (l561 + 1)) {
			fRec792[l561] = 0.0f;
			
		}
		for (int l562 = 0; (l562 < 2); l562 = (l562 + 1)) {
			fRec790[l562] = 0.0f;
			
		}
		for (int l563 = 0; (l563 < 2); l563 = (l563 + 1)) {
			fRec801[l563] = 0.0f;
			
		}
		for (int l564 = 0; (l564 < 2); l564 = (l564 + 1)) {
			fRec799[l564] = 0.0f;
			
		}
		for (int l565 = 0; (l565 < 2); l565 = (l565 + 1)) {
			fRec807[l565] = 0.0f;
			
		}
		for (int l566 = 0; (l566 < 2); l566 = (l566 + 1)) {
			fRec805[l566] = 0.0f;
			
		}
		for (int l567 = 0; (l567 < 2); l567 = (l567 + 1)) {
			fRec804[l567] = 0.0f;
			
		}
		for (int l568 = 0; (l568 < 2); l568 = (l568 + 1)) {
			fRec802[l568] = 0.0f;
			
		}
		for (int l569 = 0; (l569 < 3); l569 = (l569 + 1)) {
			fVec21[l569] = 0.0f;
			
		}
		for (int l570 = 0; (l570 < 2); l570 = (l570 + 1)) {
			fRec819[l570] = 0.0f;
			
		}
		for (int l571 = 0; (l571 < 2); l571 = (l571 + 1)) {
			fRec817[l571] = 0.0f;
			
		}
		for (int l572 = 0; (l572 < 2); l572 = (l572 + 1)) {
			fRec816[l572] = 0.0f;
			
		}
		for (int l573 = 0; (l573 < 2); l573 = (l573 + 1)) {
			fRec814[l573] = 0.0f;
			
		}
		for (int l574 = 0; (l574 < 2); l574 = (l574 + 1)) {
			fRec813[l574] = 0.0f;
			
		}
		for (int l575 = 0; (l575 < 2); l575 = (l575 + 1)) {
			fRec811[l575] = 0.0f;
			
		}
		for (int l576 = 0; (l576 < 2); l576 = (l576 + 1)) {
			fRec810[l576] = 0.0f;
			
		}
		for (int l577 = 0; (l577 < 2); l577 = (l577 + 1)) {
			fRec808[l577] = 0.0f;
			
		}
		for (int l578 = 0; (l578 < 2); l578 = (l578 + 1)) {
			fRec828[l578] = 0.0f;
			
		}
		for (int l579 = 0; (l579 < 2); l579 = (l579 + 1)) {
			fRec826[l579] = 0.0f;
			
		}
		for (int l580 = 0; (l580 < 2); l580 = (l580 + 1)) {
			fRec825[l580] = 0.0f;
			
		}
		for (int l581 = 0; (l581 < 2); l581 = (l581 + 1)) {
			fRec823[l581] = 0.0f;
			
		}
		for (int l582 = 0; (l582 < 2); l582 = (l582 + 1)) {
			fRec822[l582] = 0.0f;
			
		}
		for (int l583 = 0; (l583 < 2); l583 = (l583 + 1)) {
			fRec820[l583] = 0.0f;
			
		}
		for (int l584 = 0; (l584 < 2); l584 = (l584 + 1)) {
			fRec831[l584] = 0.0f;
			
		}
		for (int l585 = 0; (l585 < 2); l585 = (l585 + 1)) {
			fRec829[l585] = 0.0f;
			
		}
		for (int l586 = 0; (l586 < 2); l586 = (l586 + 1)) {
			fRec837[l586] = 0.0f;
			
		}
		for (int l587 = 0; (l587 < 2); l587 = (l587 + 1)) {
			fRec835[l587] = 0.0f;
			
		}
		for (int l588 = 0; (l588 < 2); l588 = (l588 + 1)) {
			fRec834[l588] = 0.0f;
			
		}
		for (int l589 = 0; (l589 < 2); l589 = (l589 + 1)) {
			fRec832[l589] = 0.0f;
			
		}
		for (int l590 = 0; (l590 < 2); l590 = (l590 + 1)) {
			fRec849[l590] = 0.0f;
			
		}
		for (int l591 = 0; (l591 < 2); l591 = (l591 + 1)) {
			fRec847[l591] = 0.0f;
			
		}
		for (int l592 = 0; (l592 < 2); l592 = (l592 + 1)) {
			fRec846[l592] = 0.0f;
			
		}
		for (int l593 = 0; (l593 < 2); l593 = (l593 + 1)) {
			fRec844[l593] = 0.0f;
			
		}
		for (int l594 = 0; (l594 < 2); l594 = (l594 + 1)) {
			fRec843[l594] = 0.0f;
			
		}
		for (int l595 = 0; (l595 < 2); l595 = (l595 + 1)) {
			fRec841[l595] = 0.0f;
			
		}
		for (int l596 = 0; (l596 < 2); l596 = (l596 + 1)) {
			fRec840[l596] = 0.0f;
			
		}
		for (int l597 = 0; (l597 < 2); l597 = (l597 + 1)) {
			fRec838[l597] = 0.0f;
			
		}
		for (int l598 = 0; (l598 < 2); l598 = (l598 + 1)) {
			fRec858[l598] = 0.0f;
			
		}
		for (int l599 = 0; (l599 < 2); l599 = (l599 + 1)) {
			fRec856[l599] = 0.0f;
			
		}
		for (int l600 = 0; (l600 < 2); l600 = (l600 + 1)) {
			fRec855[l600] = 0.0f;
			
		}
		for (int l601 = 0; (l601 < 2); l601 = (l601 + 1)) {
			fRec853[l601] = 0.0f;
			
		}
		for (int l602 = 0; (l602 < 2); l602 = (l602 + 1)) {
			fRec852[l602] = 0.0f;
			
		}
		for (int l603 = 0; (l603 < 2); l603 = (l603 + 1)) {
			fRec850[l603] = 0.0f;
			
		}
		for (int l604 = 0; (l604 < 2); l604 = (l604 + 1)) {
			fRec861[l604] = 0.0f;
			
		}
		for (int l605 = 0; (l605 < 2); l605 = (l605 + 1)) {
			fRec859[l605] = 0.0f;
			
		}
		for (int l606 = 0; (l606 < 2); l606 = (l606 + 1)) {
			fRec867[l606] = 0.0f;
			
		}
		for (int l607 = 0; (l607 < 2); l607 = (l607 + 1)) {
			fRec865[l607] = 0.0f;
			
		}
		for (int l608 = 0; (l608 < 2); l608 = (l608 + 1)) {
			fRec864[l608] = 0.0f;
			
		}
		for (int l609 = 0; (l609 < 2); l609 = (l609 + 1)) {
			fRec862[l609] = 0.0f;
			
		}
		for (int l610 = 0; (l610 < 2); l610 = (l610 + 1)) {
			fRec879[l610] = 0.0f;
			
		}
		for (int l611 = 0; (l611 < 2); l611 = (l611 + 1)) {
			fRec877[l611] = 0.0f;
			
		}
		for (int l612 = 0; (l612 < 2); l612 = (l612 + 1)) {
			fRec876[l612] = 0.0f;
			
		}
		for (int l613 = 0; (l613 < 2); l613 = (l613 + 1)) {
			fRec874[l613] = 0.0f;
			
		}
		for (int l614 = 0; (l614 < 2); l614 = (l614 + 1)) {
			fRec873[l614] = 0.0f;
			
		}
		for (int l615 = 0; (l615 < 2); l615 = (l615 + 1)) {
			fRec871[l615] = 0.0f;
			
		}
		for (int l616 = 0; (l616 < 2); l616 = (l616 + 1)) {
			fRec870[l616] = 0.0f;
			
		}
		for (int l617 = 0; (l617 < 2); l617 = (l617 + 1)) {
			fRec868[l617] = 0.0f;
			
		}
		for (int l618 = 0; (l618 < 2); l618 = (l618 + 1)) {
			fRec888[l618] = 0.0f;
			
		}
		for (int l619 = 0; (l619 < 2); l619 = (l619 + 1)) {
			fRec886[l619] = 0.0f;
			
		}
		for (int l620 = 0; (l620 < 2); l620 = (l620 + 1)) {
			fRec885[l620] = 0.0f;
			
		}
		for (int l621 = 0; (l621 < 2); l621 = (l621 + 1)) {
			fRec883[l621] = 0.0f;
			
		}
		for (int l622 = 0; (l622 < 2); l622 = (l622 + 1)) {
			fRec882[l622] = 0.0f;
			
		}
		for (int l623 = 0; (l623 < 2); l623 = (l623 + 1)) {
			fRec880[l623] = 0.0f;
			
		}
		for (int l624 = 0; (l624 < 2); l624 = (l624 + 1)) {
			fRec891[l624] = 0.0f;
			
		}
		for (int l625 = 0; (l625 < 2); l625 = (l625 + 1)) {
			fRec889[l625] = 0.0f;
			
		}
		for (int l626 = 0; (l626 < 2); l626 = (l626 + 1)) {
			fRec897[l626] = 0.0f;
			
		}
		for (int l627 = 0; (l627 < 2); l627 = (l627 + 1)) {
			fRec895[l627] = 0.0f;
			
		}
		for (int l628 = 0; (l628 < 2); l628 = (l628 + 1)) {
			fRec894[l628] = 0.0f;
			
		}
		for (int l629 = 0; (l629 < 2); l629 = (l629 + 1)) {
			fRec892[l629] = 0.0f;
			
		}
		for (int l630 = 0; (l630 < 3); l630 = (l630 + 1)) {
			fVec22[l630] = 0.0f;
			
		}
		for (int l631 = 0; (l631 < 2); l631 = (l631 + 1)) {
			fRec909[l631] = 0.0f;
			
		}
		for (int l632 = 0; (l632 < 2); l632 = (l632 + 1)) {
			fRec907[l632] = 0.0f;
			
		}
		for (int l633 = 0; (l633 < 2); l633 = (l633 + 1)) {
			fRec906[l633] = 0.0f;
			
		}
		for (int l634 = 0; (l634 < 2); l634 = (l634 + 1)) {
			fRec904[l634] = 0.0f;
			
		}
		for (int l635 = 0; (l635 < 2); l635 = (l635 + 1)) {
			fRec903[l635] = 0.0f;
			
		}
		for (int l636 = 0; (l636 < 2); l636 = (l636 + 1)) {
			fRec901[l636] = 0.0f;
			
		}
		for (int l637 = 0; (l637 < 2); l637 = (l637 + 1)) {
			fRec900[l637] = 0.0f;
			
		}
		for (int l638 = 0; (l638 < 2); l638 = (l638 + 1)) {
			fRec898[l638] = 0.0f;
			
		}
		for (int l639 = 0; (l639 < 2); l639 = (l639 + 1)) {
			fRec918[l639] = 0.0f;
			
		}
		for (int l640 = 0; (l640 < 2); l640 = (l640 + 1)) {
			fRec916[l640] = 0.0f;
			
		}
		for (int l641 = 0; (l641 < 2); l641 = (l641 + 1)) {
			fRec915[l641] = 0.0f;
			
		}
		for (int l642 = 0; (l642 < 2); l642 = (l642 + 1)) {
			fRec913[l642] = 0.0f;
			
		}
		for (int l643 = 0; (l643 < 2); l643 = (l643 + 1)) {
			fRec912[l643] = 0.0f;
			
		}
		for (int l644 = 0; (l644 < 2); l644 = (l644 + 1)) {
			fRec910[l644] = 0.0f;
			
		}
		for (int l645 = 0; (l645 < 2); l645 = (l645 + 1)) {
			fRec921[l645] = 0.0f;
			
		}
		for (int l646 = 0; (l646 < 2); l646 = (l646 + 1)) {
			fRec919[l646] = 0.0f;
			
		}
		for (int l647 = 0; (l647 < 2); l647 = (l647 + 1)) {
			fRec927[l647] = 0.0f;
			
		}
		for (int l648 = 0; (l648 < 2); l648 = (l648 + 1)) {
			fRec925[l648] = 0.0f;
			
		}
		for (int l649 = 0; (l649 < 2); l649 = (l649 + 1)) {
			fRec924[l649] = 0.0f;
			
		}
		for (int l650 = 0; (l650 < 2); l650 = (l650 + 1)) {
			fRec922[l650] = 0.0f;
			
		}
		for (int l651 = 0; (l651 < 2); l651 = (l651 + 1)) {
			fRec939[l651] = 0.0f;
			
		}
		for (int l652 = 0; (l652 < 2); l652 = (l652 + 1)) {
			fRec937[l652] = 0.0f;
			
		}
		for (int l653 = 0; (l653 < 2); l653 = (l653 + 1)) {
			fRec936[l653] = 0.0f;
			
		}
		for (int l654 = 0; (l654 < 2); l654 = (l654 + 1)) {
			fRec934[l654] = 0.0f;
			
		}
		for (int l655 = 0; (l655 < 2); l655 = (l655 + 1)) {
			fRec933[l655] = 0.0f;
			
		}
		for (int l656 = 0; (l656 < 2); l656 = (l656 + 1)) {
			fRec931[l656] = 0.0f;
			
		}
		for (int l657 = 0; (l657 < 2); l657 = (l657 + 1)) {
			fRec930[l657] = 0.0f;
			
		}
		for (int l658 = 0; (l658 < 2); l658 = (l658 + 1)) {
			fRec928[l658] = 0.0f;
			
		}
		for (int l659 = 0; (l659 < 2); l659 = (l659 + 1)) {
			fRec948[l659] = 0.0f;
			
		}
		for (int l660 = 0; (l660 < 2); l660 = (l660 + 1)) {
			fRec946[l660] = 0.0f;
			
		}
		for (int l661 = 0; (l661 < 2); l661 = (l661 + 1)) {
			fRec945[l661] = 0.0f;
			
		}
		for (int l662 = 0; (l662 < 2); l662 = (l662 + 1)) {
			fRec943[l662] = 0.0f;
			
		}
		for (int l663 = 0; (l663 < 2); l663 = (l663 + 1)) {
			fRec942[l663] = 0.0f;
			
		}
		for (int l664 = 0; (l664 < 2); l664 = (l664 + 1)) {
			fRec940[l664] = 0.0f;
			
		}
		for (int l665 = 0; (l665 < 2); l665 = (l665 + 1)) {
			fRec951[l665] = 0.0f;
			
		}
		for (int l666 = 0; (l666 < 2); l666 = (l666 + 1)) {
			fRec949[l666] = 0.0f;
			
		}
		for (int l667 = 0; (l667 < 2); l667 = (l667 + 1)) {
			fRec957[l667] = 0.0f;
			
		}
		for (int l668 = 0; (l668 < 2); l668 = (l668 + 1)) {
			fRec955[l668] = 0.0f;
			
		}
		for (int l669 = 0; (l669 < 2); l669 = (l669 + 1)) {
			fRec954[l669] = 0.0f;
			
		}
		for (int l670 = 0; (l670 < 2); l670 = (l670 + 1)) {
			fRec952[l670] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD2o4");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec1[0] = (fSlow1 + (0.999000013f * fRec1[1]));
			float fTemp0 = tanf((fConst1 * fRec1[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec2[0] = (float(input0[i]) - (((fRec2[2] * fTemp2) + (2.0f * (fTemp3 * fRec2[1]))) / fTemp4));
			fRec3[0] = (fSlow2 + (0.999000013f * fRec3[1]));
			float fTemp5 = (fRec3[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec2[2] + (fRec2[0] + (2.0f * fRec2[1])))) / fTemp5) + (fRec3[0] * (0.0f - ((fTemp6 * fRec2[1]) + ((fRec2[0] + fRec2[2]) / fTemp4)))));
			fRec7[0] = (float(input2[i]) - (((fTemp2 * fRec7[2]) + (2.0f * (fTemp3 * fRec7[1]))) / fTemp4));
			float fTemp8 = (((fTemp1 * (fRec7[2] + (fRec7[0] + (2.0f * fRec7[1])))) / fTemp5) + (0.906179845f * (fRec3[0] * (0.0f - ((fTemp6 * fRec7[1]) + ((fRec7[0] + fRec7[2]) / fTemp4))))));
			fRec8[0] = (float(input3[i]) - (((fTemp2 * fRec8[2]) + (2.0f * (fTemp3 * fRec8[1]))) / fTemp4));
			float fTemp9 = (((fTemp1 * (fRec8[2] + (fRec8[0] + (2.0f * fRec8[1])))) / fTemp5) + (0.906179845f * (fRec3[0] * (0.0f - ((fTemp6 * fRec8[1]) + ((fRec8[0] + fRec8[2]) / fTemp4))))));
			fRec9[0] = (float(input1[i]) - (((fTemp2 * fRec9[2]) + (2.0f * (fTemp3 * fRec9[1]))) / fTemp4));
			float fTemp10 = (((fTemp1 * (fRec9[2] + (fRec9[0] + (2.0f * fRec9[1])))) / fTemp5) + (0.906179845f * (fRec3[0] * (0.0f - ((fTemp6 * fRec9[1]) + ((fRec9[0] + fRec9[2]) / fTemp4))))));
			float fTemp11 = (fConst4 * (((0.0941376761f * fTemp8) + (2.42800013e-07f * fTemp9)) - (4.15470004e-06f * fTemp10)));
			float fTemp12 = (fConst5 * fRec4[1]);
			fRec6[0] = (fTemp11 + (fRec6[1] + fTemp12));
			fRec4[0] = fRec6[0];
			float fRec5 = (fTemp12 + fTemp11);
			fRec19[0] = (float(input13[i]) - (((fTemp2 * fRec19[2]) + (2.0f * (fTemp3 * fRec19[1]))) / fTemp4));
			float fTemp13 = (((fTemp1 * ((fRec19[0] + (2.0f * fRec19[1])) + fRec19[2])) / fTemp5) + (0.50103116f * ((0.0f - ((fTemp6 * fRec19[1]) + ((fRec19[0] + fRec19[2]) / fTemp4))) * fRec3[0])));
			fRec20[0] = (float(input15[i]) - (((fRec20[2] * fTemp2) + (2.0f * (fRec20[1] * fTemp3))) / fTemp4));
			float fTemp14 = (((fTemp1 * (fRec20[2] + (fRec20[0] + (2.0f * fRec20[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fRec20[1] * fTemp6) + ((fRec20[2] + fRec20[0]) / fTemp4))))));
			fRec21[0] = (float(input9[i]) - (((fTemp2 * fRec21[2]) + (2.0f * (fTemp3 * fRec21[1]))) / fTemp4));
			float fTemp15 = (((fTemp1 * (fRec21[2] + (fRec21[0] + (2.0f * fRec21[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec21[1]) + ((fRec21[0] + fRec21[2]) / fTemp4))))));
			fRec22[0] = (float(input10[i]) - (((fTemp2 * fRec22[2]) + (2.0f * (fTemp3 * fRec22[1]))) / fTemp4));
			float fTemp16 = (((fTemp1 * (fRec22[2] + (fRec22[0] + (2.0f * fRec22[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec22[1]) + ((fRec22[0] + fRec22[2]) / fTemp4))))));
			fRec23[0] = (float(input12[i]) - (((fTemp2 * fRec23[2]) + (2.0f * (fTemp3 * fRec23[1]))) / fTemp4));
			float fTemp17 = (((fTemp1 * (fRec23[2] + (fRec23[0] + (2.0f * fRec23[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec23[1]) + ((fRec23[0] + fRec23[2]) / fTemp4))))));
			fRec24[0] = (float(input14[i]) - (((fTemp2 * fRec24[2]) + (2.0f * (fTemp3 * fRec24[1]))) / fTemp4));
			float fTemp18 = ((((fRec24[2] + (fRec24[0] + (2.0f * fRec24[1]))) * fTemp1) / fTemp5) + (0.50103116f * ((0.0f - ((fTemp6 * fRec24[1]) + ((fRec24[0] + fRec24[2]) / fTemp4))) * fRec3[0])));
			fRec25[0] = (float(input11[i]) - (((fTemp2 * fRec25[2]) + (2.0f * (fTemp3 * fRec25[1]))) / fTemp4));
			float fTemp19 = (((fTemp1 * (fRec25[2] + (fRec25[0] + (2.0f * fRec25[1])))) / fTemp5) + (0.50103116f * (fRec3[0] * (0.0f - ((fTemp6 * fRec25[1]) + ((fRec25[0] + fRec25[2]) / fTemp4))))));
			float fTemp20 = (fConst8 * ((((((7.84300028e-07f * fTemp13) + (8.95399978e-07f * fTemp14)) + (2.70700014e-07f * fTemp15)) + (1.88841004e-05f * fTemp16)) + (0.0943125188f * fTemp17)) - ((8.26339965e-06f * fTemp18) + (8.87769966e-06f * fTemp19))));
			float fTemp21 = (fConst9 * fRec16[1]);
			float fTemp22 = (fConst11 * fRec13[1]);
			fRec18[0] = (((fTemp20 + fRec18[1]) + fTemp21) + fTemp22);
			fRec16[0] = fRec18[0];
			float fRec17 = ((fTemp20 + fTemp21) + fTemp22);
			fRec15[0] = (fRec16[0] + fRec15[1]);
			fRec13[0] = fRec15[0];
			float fRec14 = fRec17;
			float fTemp23 = (fConst12 * fRec10[1]);
			fRec12[0] = ((fRec14 + fRec12[1]) + fTemp23);
			fRec10[0] = fRec12[0];
			float fRec11 = (fRec14 + fTemp23);
			float fTemp24 = (fConst14 * fRec26[1]);
			float fTemp25 = (fConst15 * fRec29[1]);
			fRec38[0] = (float(input17[i]) - (((fTemp2 * fRec38[2]) + (2.0f * (fTemp3 * fRec38[1]))) / fTemp4));
			float fTemp26 = (((fTemp1 * (fRec38[2] + (fRec38[0] + (2.0f * fRec38[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec38[1]) + ((fRec38[0] + fRec38[2]) / fTemp4))))));
			fRec39[0] = (float(input18[i]) - (((fTemp2 * fRec39[2]) + (2.0f * (fTemp3 * fRec39[1]))) / fTemp4));
			float fTemp27 = (((fTemp1 * (fRec39[2] + (fRec39[0] + (2.0f * fRec39[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec39[1]) + ((fRec39[0] + fRec39[2]) / fTemp4))))));
			fRec40[0] = (float(input20[i]) - (((fTemp2 * fRec40[2]) + (2.0f * (fTemp3 * fRec40[1]))) / fTemp4));
			float fTemp28 = (((fTemp1 * (fRec40[2] + (fRec40[0] + (2.0f * fRec40[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec40[1]) + ((fRec40[0] + fRec40[2]) / fTemp4))))));
			fRec41[0] = (float(input21[i]) - (((fTemp2 * fRec41[2]) + (2.0f * (fTemp3 * fRec41[1]))) / fTemp4));
			float fTemp29 = (((fTemp1 * (fRec41[2] + (fRec41[0] + (2.0f * fRec41[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec41[1]) + ((fRec41[0] + fRec41[2]) / fTemp4))))));
			fRec42[0] = (float(input23[i]) - (((fTemp2 * fRec42[2]) + (2.0f * (fTemp3 * fRec42[1]))) / fTemp4));
			float fTemp30 = (((fTemp1 * (fRec42[2] + (fRec42[0] + (2.0f * fRec42[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec42[1]) + ((fRec42[0] + fRec42[2]) / fTemp4))))));
			fRec43[0] = (float(input24[i]) - (((fTemp2 * fRec43[2]) + (2.0f * (fTemp3 * fRec43[1]))) / fTemp4));
			float fTemp31 = (((fTemp1 * (fRec43[2] + (fRec43[0] + (2.0f * fRec43[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec43[1]) + ((fRec43[0] + fRec43[2]) / fTemp4))))));
			fRec44[0] = (float(input16[i]) - (((fTemp2 * fRec44[2]) + (2.0f * (fTemp3 * fRec44[1]))) / fTemp4));
			float fTemp32 = (((fTemp1 * (fRec44[2] + (fRec44[0] + (2.0f * fRec44[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec44[1]) + ((fRec44[0] + fRec44[2]) / fTemp4))))));
			fRec45[0] = (float(input19[i]) - (((fTemp2 * fRec45[2]) + (2.0f * (fTemp3 * fRec45[1]))) / fTemp4));
			float fTemp33 = (((fTemp1 * (fRec45[2] + (fRec45[0] + (2.0f * fRec45[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec45[1]) + ((fRec45[0] + fRec45[2]) / fTemp4))))));
			fRec46[0] = (float(input22[i]) - (((fTemp2 * fRec46[2]) + (2.0f * (fTemp3 * fRec46[1]))) / fTemp4));
			float fTemp34 = (((fTemp1 * (fRec46[2] + (fRec46[0] + (2.0f * fRec46[1])))) / fTemp5) + (0.245735466f * (fRec3[0] * (0.0f - ((fTemp6 * fRec46[1]) + ((fRec46[0] + fRec46[2]) / fTemp4))))));
			float fTemp35 = (fConst17 * (((((((6.81100005e-07f * fTemp26) + (2.75337006e-05f * fTemp27)) + (0.0733396858f * fTemp28)) + (1.06230004e-06f * fTemp29)) + (2.10329995e-06f * fTemp30)) + (5.68999994e-08f * fTemp31)) - (((2.01100011e-06f * fTemp32) + (7.4579998e-06f * fTemp33)) + (1.08580998e-05f * fTemp34))));
			float fTemp36 = (fConst18 * fRec32[1]);
			float fTemp37 = (fConst19 * fRec35[1]);
			fRec37[0] = (fTemp35 + (fTemp36 + (fRec37[1] + fTemp37)));
			fRec35[0] = fRec37[0];
			float fRec36 = ((fTemp37 + fTemp36) + fTemp35);
			fRec34[0] = (fRec35[0] + fRec34[1]);
			fRec32[0] = fRec34[0];
			float fRec33 = fRec36;
			fRec31[0] = (fTemp24 + (fTemp25 + (fRec33 + fRec31[1])));
			fRec29[0] = fRec31[0];
			float fRec30 = (fTemp24 + (fRec33 + fTemp25));
			fRec28[0] = (fRec29[0] + fRec28[1]);
			fRec26[0] = fRec28[0];
			float fRec27 = fRec30;
			fRec53[0] = (float(input4[i]) - (((fTemp2 * fRec53[2]) + (2.0f * (fTemp3 * fRec53[1]))) / fTemp4));
			float fTemp38 = (((fTemp1 * (fRec53[2] + (fRec53[0] + (2.0f * fRec53[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec53[1]) + ((fRec53[0] + fRec53[2]) / fTemp4))))));
			fRec54[0] = (float(input6[i]) - (((fTemp2 * fRec54[2]) + (2.0f * (fTemp3 * fRec54[1]))) / fTemp4));
			float fTemp39 = (((fTemp1 * (fRec54[2] + (fRec54[0] + (2.0f * fRec54[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec54[1]) + ((fRec54[0] + fRec54[2]) / fTemp4))))));
			fRec55[0] = (float(input7[i]) - (((fTemp2 * fRec55[2]) + (2.0f * (fTemp3 * fRec55[1]))) / fTemp4));
			float fTemp40 = (((fTemp1 * (fRec55[2] + (fRec55[0] + (2.0f * fRec55[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec55[1]) + ((fRec55[0] + fRec55[2]) / fTemp4))))));
			fRec56[0] = (float(input5[i]) - (((fTemp2 * fRec56[2]) + (2.0f * (fTemp3 * fRec56[1]))) / fTemp4));
			float fTemp41 = (((fTemp1 * (fRec56[2] + (fRec56[0] + (2.0f * fRec56[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec56[1]) + ((fRec56[0] + fRec56[2]) / fTemp4))))));
			fRec57[0] = (float(input8[i]) - (((fTemp2 * fRec57[2]) + (2.0f * (fTemp3 * fRec57[1]))) / fTemp4));
			float fTemp42 = (((fTemp1 * (fRec57[2] + (fRec57[0] + (2.0f * fRec57[1])))) / fTemp5) + (0.731742859f * (fRec3[0] * (0.0f - ((fTemp6 * fRec57[1]) + ((fRec57[0] + fRec57[2]) / fTemp4))))));
			float fTemp43 = (fConst21 * ((((8.81689994e-06f * fTemp38) + (0.103228964f * fTemp39)) + (5.07200014e-07f * fTemp40)) - ((7.52200003e-06f * fTemp41) + (4.11000019e-06f * fTemp42))));
			float fTemp44 = (fConst22 * fRec47[1]);
			float fTemp45 = (fConst23 * fRec50[1]);
			fRec52[0] = (fTemp43 + (fTemp44 + (fRec52[1] + fTemp45)));
			fRec50[0] = fRec52[0];
			float fRec51 = ((fTemp45 + fTemp44) + fTemp43);
			fRec49[0] = (fRec50[0] + fRec49[1]);
			fRec47[0] = fRec49[0];
			float fRec48 = fRec51;
			fVec0[(IOTA & 1023)] = ((0.0588310584f * fTemp7) + (fRec5 + (fRec11 + (fRec27 + fRec48))));
			output0[i] = FAUSTFLOAT((0.883713245f * (fRec0[0] * fVec0[((IOTA - iConst24) & 1023)])));
			float fTemp46 = (fConst26 * fRec58[1]);
			float fTemp47 = (fConst27 * fRec61[1]);
			float fTemp48 = (fConst29 * ((((0.0290305931f * fTemp27) + (0.0329333097f * fTemp33)) + (0.00758319721f * fTemp29)) - ((0.000230128499f * fTemp31) + ((0.021826975f * fTemp30) + ((((0.00565015245f * fTemp32) + (0.0110148005f * fTemp26)) + (0.0211727768f * fTemp28)) + (0.0388294756f * fTemp34))))));
			float fTemp49 = (fConst30 * fRec64[1]);
			float fTemp50 = (fConst31 * fRec67[1]);
			fRec69[0] = (fTemp48 + (fTemp49 + (fRec69[1] + fTemp50)));
			fRec67[0] = fRec69[0];
			float fRec68 = ((fTemp50 + fTemp49) + fTemp48);
			fRec66[0] = (fRec67[0] + fRec66[1]);
			fRec64[0] = fRec66[0];
			float fRec65 = fRec68;
			fRec63[0] = (fTemp46 + (fTemp47 + (fRec65 + fRec63[1])));
			fRec61[0] = fRec63[0];
			float fRec62 = (fTemp46 + (fRec65 + fTemp47));
			fRec60[0] = (fRec61[0] + fRec60[1]);
			fRec58[0] = fRec60[0];
			float fRec59 = fRec62;
			float fTemp51 = (fConst33 * fRec70[1]);
			float fTemp52 = (fConst35 * (((0.000949303096f * fTemp17) + ((0.0582673401f * fTemp19) + ((0.0280165877f * fTemp16) + (0.017944064f * fTemp13)))) - ((0.00536028529f * fTemp15) + ((0.0338784568f * fTemp18) + (0.0117041916f * fTemp14)))));
			float fTemp53 = (fConst36 * fRec73[1]);
			float fTemp54 = (fConst37 * fRec76[1]);
			fRec78[0] = (fTemp52 + (fTemp53 + (fRec78[1] + fTemp54)));
			fRec76[0] = fRec78[0];
			float fRec77 = ((fTemp54 + fTemp53) + fTemp52);
			fRec75[0] = (fRec76[0] + fRec75[1]);
			fRec73[0] = fRec75[0];
			float fRec74 = fRec77;
			fRec72[0] = (fTemp51 + (fRec74 + fRec72[1]));
			fRec70[0] = fRec72[0];
			float fRec71 = (fRec74 + fTemp51);
			float fTemp55 = (fConst39 * (((0.0389746763f * fTemp10) + (0.0524411313f * fTemp8)) + (0.0137809403f * fTemp9)));
			float fTemp56 = (fConst40 * fRec79[1]);
			fRec81[0] = (fTemp55 + (fRec81[1] + fTemp56));
			fRec79[0] = fRec81[0];
			float fRec80 = (fTemp56 + fTemp55);
			float fTemp57 = (fConst42 * (((0.0209300108f * fTemp40) + (((0.0163057726f * fTemp38) + (0.0618029423f * fTemp41)) + (0.0322771296f * fTemp39))) - (0.0186568219f * fTemp42)));
			float fTemp58 = (fConst43 * fRec85[1]);
			float fTemp59 = (fConst44 * fRec82[1]);
			fRec87[0] = (((fTemp57 + fRec87[1]) + fTemp58) + fTemp59);
			fRec85[0] = fRec87[0];
			float fRec86 = ((fTemp57 + fTemp58) + fTemp59);
			fRec84[0] = (fRec85[0] + fRec84[1]);
			fRec82[0] = fRec84[0];
			float fRec83 = fRec86;
			fVec1[(IOTA & 511)] = ((fRec59 + (fRec71 + (fRec80 + fRec83))) + (0.0407811999f * fTemp7));
			output1[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec1[((IOTA - iConst45) & 511)])));
			float fTemp60 = (fConst47 * fRec88[1]);
			float fTemp61 = (fConst48 * fRec91[1]);
			float fTemp62 = (fConst50 * (((0.0161915477f * fTemp30) + ((((2.36300002e-06f * fTemp32) + (0.0161989685f * fTemp26)) + (0.0223312788f * fTemp33)) + (8.23059963e-06f * fTemp34))) - (((0.0223343018f * fTemp29) + ((0.0510637052f * fTemp27) + (0.0261863358f * fTemp28))) + (0.00291420706f * fTemp31))));
			float fTemp63 = (fConst51 * fRec94[1]);
			float fTemp64 = (fConst52 * fRec97[1]);
			fRec99[0] = (fTemp62 + (fTemp63 + (fRec99[1] + fTemp64)));
			fRec97[0] = fRec99[0];
			float fRec98 = ((fTemp64 + fTemp63) + fTemp62);
			fRec96[0] = (fRec97[0] + fRec96[1]);
			fRec94[0] = fRec96[0];
			float fRec95 = fRec98;
			fRec93[0] = (fTemp60 + (fTemp61 + (fRec95 + fRec93[1])));
			fRec91[0] = fRec93[0];
			float fRec92 = (fTemp60 + (fRec95 + fTemp61));
			fRec90[0] = (fRec91[0] + fRec90[1]);
			fRec88[0] = fRec90[0];
			float fRec89 = fRec92;
			float fTemp65 = (fConst55 * (((((1.07461001e-05f * fTemp18) + (0.00843413826f * fTemp14)) + (0.00843882281f * fTemp15)) + (0.045639351f * fTemp19)) - (((0.045648586f * fTemp13) + (0.0472712144f * fTemp16)) + (0.0032499393f * fTemp17))));
			float fTemp66 = (fConst56 * fRec106[1]);
			float fTemp67 = (fConst57 * fRec103[1]);
			fRec108[0] = (((fTemp65 + fRec108[1]) + fTemp66) + fTemp67);
			fRec106[0] = fRec108[0];
			float fRec107 = ((fTemp65 + fTemp66) + fTemp67);
			fRec105[0] = (fRec106[0] + fRec105[1]);
			fRec103[0] = fRec105[0];
			float fRec104 = fRec107;
			float fTemp68 = (fConst58 * fRec100[1]);
			fRec102[0] = ((fRec104 + fRec102[1]) + fTemp68);
			fRec100[0] = fRec102[0];
			float fRec101 = (fRec104 + fTemp68);
			float fTemp69 = (fConst60 * (((0.0329679698f * fTemp10) + (0.0571265034f * fTemp8)) - (0.0329778865f * fTemp9)));
			float fTemp70 = (fConst61 * fRec109[1]);
			fRec111[0] = (fTemp69 + (fRec111[1] + fTemp70));
			fRec109[0] = fRec111[0];
			float fRec110 = (fTemp70 + fTemp69);
			float fTemp71 = (fConst63 * ((((0.0510057807f * fTemp41) + (0.0325682648f * fTemp39)) + (7.47690001e-06f * fTemp42)) - ((0.026926782f * fTemp38) + (0.0510190837f * fTemp40))));
			float fTemp72 = (fConst64 * fRec112[1]);
			float fTemp73 = (fConst65 * fRec115[1]);
			fRec117[0] = (fTemp71 + (fTemp72 + (fRec117[1] + fTemp73)));
			fRec115[0] = fRec117[0];
			float fRec116 = ((fTemp73 + fTemp72) + fTemp71);
			fRec114[0] = (fRec115[0] + fRec114[1]);
			fRec112[0] = fRec114[0];
			float fRec113 = fRec116;
			fVec2[(IOTA & 511)] = ((fRec89 + (fRec101 + (fRec110 + fRec113))) + (0.0453824513f * fTemp7));
			output2[i] = FAUSTFLOAT((0.90871048f * (fRec0[0] * fVec2[((IOTA - iConst66) & 511)])));
			float fTemp74 = (fConst29 * ((((0.00564554473f * fTemp32) + (0.0290222298f * fTemp27)) + (0.0388304405f * fTemp34)) - ((0.000246917189f * fTemp31) + (((0.0329207405f * fTemp29) + (((0.0218268633f * fTemp26) + (0.00756875705f * fTemp33)) + (0.0211945809f * fTemp28))) + (0.0109938625f * fTemp30)))));
			float fTemp75 = (fConst30 * fRec124[1]);
			float fTemp76 = (fConst31 * fRec127[1]);
			fRec129[0] = (fTemp74 + (fTemp75 + (fRec129[1] + fTemp76)));
			fRec127[0] = fRec129[0];
			float fRec128 = ((fTemp76 + fTemp75) + fTemp74);
			fRec126[0] = (fRec126[1] + fRec127[0]);
			fRec124[0] = fRec126[0];
			float fRec125 = fRec128;
			float fTemp77 = (fConst27 * fRec121[1]);
			float fTemp78 = (fConst26 * fRec118[1]);
			fRec123[0] = (((fRec125 + fRec123[1]) + fTemp77) + fTemp78);
			fRec121[0] = fRec123[0];
			float fRec122 = ((fRec125 + fTemp77) + fTemp78);
			fRec120[0] = (fRec121[0] + fRec120[1]);
			fRec118[0] = fRec120[0];
			float fRec119 = fRec122;
			float fTemp79 = (fConst33 * fRec130[1]);
			float fTemp80 = (fConst35 * (((0.000924167107f * fTemp17) + ((0.0280273985f * fTemp16) + (0.0338808298f * fTemp18))) - ((0.0179393496f * fTemp19) + ((0.0117095737f * fTemp15) + ((0.0582761504f * fTemp13) + (0.00534620928f * fTemp14))))));
			float fTemp81 = (fConst36 * fRec133[1]);
			float fTemp82 = (fConst37 * fRec136[1]);
			fRec138[0] = (fTemp80 + (fTemp81 + (fRec138[1] + fTemp82)));
			fRec136[0] = fRec138[0];
			float fRec137 = ((fTemp82 + fTemp81) + fTemp80);
			fRec135[0] = (fRec136[0] + fRec135[1]);
			fRec133[0] = fRec135[0];
			float fRec134 = fRec137;
			fRec132[0] = (fTemp79 + (fRec134 + fRec132[1]));
			fRec130[0] = fRec132[0];
			float fRec131 = (fRec134 + fTemp79);
			float fTemp83 = (fConst39 * ((0.0524600223f * fTemp8) - ((0.0137928454f * fTemp10) + (0.0389987528f * fTemp9))));
			float fTemp84 = (fConst40 * fRec139[1]);
			fRec141[0] = (fTemp83 + (fRec141[1] + fTemp84));
			fRec139[0] = fRec141[0];
			float fRec140 = (fTemp84 + fTemp83);
			float fTemp85 = (fConst42 * ((((0.0163196493f * fTemp38) + (0.0322710611f * fTemp39)) + (0.0186589565f * fTemp42)) - ((0.0209395979f * fTemp41) + (0.0618302934f * fTemp40))));
			float fTemp86 = (fConst44 * fRec142[1]);
			float fTemp87 = (fConst43 * fRec145[1]);
			fRec147[0] = (fTemp85 + (fTemp86 + (fRec147[1] + fTemp87)));
			fRec145[0] = fRec147[0];
			float fRec146 = ((fTemp87 + fTemp86) + fTemp85);
			fRec144[0] = (fRec145[0] + fRec144[1]);
			fRec142[0] = fRec144[0];
			float fRec143 = fRec146;
			fVec3[(IOTA & 511)] = ((fRec119 + (fRec131 + (fRec140 + fRec143))) + (0.0408032164f * fTemp7));
			output3[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec3[((IOTA - iConst45) & 511)])));
			float fTemp88 = (fConst39 * ((0.0477688611f * fTemp8) - ((0.0345717557f * fTemp10) + (0.00927145034f * fTemp9))));
			float fTemp89 = (fConst40 * fRec148[1]);
			fRec150[0] = (fTemp88 + (fRec150[1] + fTemp89));
			fRec148[0] = fRec150[0];
			float fRec149 = (fTemp89 + fTemp88);
			float fTemp90 = (fConst42 * (((0.01084367f * fTemp38) + (0.0319992863f * fTemp39)) - (((0.0561082587f * fTemp41) + (0.0150459614f * fTemp40)) + (0.0187615324f * fTemp42))));
			float fTemp91 = (fConst44 * fRec151[1]);
			float fTemp92 = (fConst43 * fRec154[1]);
			fRec156[0] = (fTemp90 + (fTemp91 + (fRec156[1] + fTemp92)));
			fRec154[0] = fRec156[0];
			float fRec155 = ((fTemp92 + fTemp91) + fTemp90);
			fRec153[0] = (fRec153[1] + fRec154[0]);
			fRec151[0] = fRec153[0];
			float fRec152 = fRec155;
			float fTemp93 = (fConst33 * fRec157[1]);
			float fTemp94 = (fConst35 * (((0.00515312981f * fTemp17) + ((0.0197129045f * fTemp16) + ((0.00859549083f * fTemp15) + (0.0086075291f * fTemp14)))) - ((0.0553654023f * fTemp19) + ((0.0148442732f * fTemp13) + (0.0341094136f * fTemp18)))));
			float fTemp95 = (fConst36 * fRec160[1]);
			float fTemp96 = (fConst37 * fRec163[1]);
			fRec165[0] = (fTemp94 + (fTemp95 + (fRec165[1] + fTemp96)));
			fRec163[0] = fRec165[0];
			float fRec164 = ((fTemp96 + fTemp95) + fTemp94);
			fRec162[0] = (fRec162[1] + fRec163[0]);
			fRec160[0] = fRec162[0];
			float fRec161 = fRec164;
			fRec159[0] = ((fRec159[1] + fTemp93) + fRec161);
			fRec157[0] = fRec159[0];
			float fRec158 = (fTemp93 + fRec161);
			float fTemp97 = (fConst26 * fRec166[1]);
			float fTemp98 = (fConst27 * fRec169[1]);
			float fTemp99 = (fConst29 * (((0.00329727889f * fTemp31) + (((0.0165957827f * fTemp26) + (0.0226213746f * fTemp27)) + (0.0166170541f * fTemp30))) - ((0.0391468108f * fTemp34) + ((((0.00572317466f * fTemp32) + (0.0347619914f * fTemp33)) + (0.0161701348f * fTemp28)) + (0.00931585766f * fTemp29)))));
			float fTemp100 = (fConst30 * fRec172[1]);
			float fTemp101 = (fConst31 * fRec175[1]);
			fRec177[0] = (fTemp99 + (fTemp100 + (fRec177[1] + fTemp101)));
			fRec175[0] = fRec177[0];
			float fRec176 = ((fTemp101 + fTemp100) + fTemp99);
			fRec174[0] = (fRec174[1] + fRec175[0]);
			fRec172[0] = fRec174[0];
			float fRec173 = fRec176;
			fRec171[0] = ((fTemp97 + (fRec171[1] + fTemp98)) + fRec173);
			fRec169[0] = fRec171[0];
			float fRec170 = ((fTemp98 + fTemp97) + fRec173);
			fRec168[0] = (fRec168[1] + fRec169[0]);
			fRec166[0] = fRec168[0];
			float fRec167 = fRec170;
			fVec4[(IOTA & 511)] = ((0.0361875407f * fTemp7) + (((fRec149 + fRec152) + fRec158) + fRec167));
			output4[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec4[((IOTA - iConst45) & 511)])));
			float fTemp102 = (fConst60 * (((0.0571384393f * fTemp8) + (0.032984715f * fTemp9)) - (0.0329728834f * fTemp10)));
			float fTemp103 = (fConst61 * fRec178[1]);
			fRec180[0] = (fTemp102 + (fRec180[1] + fTemp103));
			fRec178[0] = fRec180[0];
			float fRec179 = (fTemp103 + fTemp102);
			float fTemp104 = (fConst63 * ((((0.0325779691f * fTemp39) + (0.0510265306f * fTemp40)) + (1.39711001e-05f * fTemp42)) - ((0.0269363876f * fTemp38) + (0.0510135181f * fTemp41))));
			float fTemp105 = (fConst64 * fRec181[1]);
			float fTemp106 = (fConst65 * fRec184[1]);
			fRec186[0] = (fTemp104 + (fTemp105 + (fRec186[1] + fTemp106)));
			fRec184[0] = fRec186[0];
			float fRec185 = ((fTemp106 + fTemp105) + fTemp104);
			fRec183[0] = (fRec183[1] + fRec184[0]);
			fRec181[0] = fRec183[0];
			float fRec182 = fRec185;
			float fTemp107 = (fConst58 * fRec187[1]);
			float fTemp108 = (fConst55 * (((0.0456504859f * fTemp13) + (1.72595992e-05f * fTemp18)) - ((0.003242946f * fTemp17) + ((0.0456476621f * fTemp19) + ((0.0472821817f * fTemp16) + ((0.00845677312f * fTemp15) + (0.00843393337f * fTemp14)))))));
			float fTemp109 = (fConst57 * fRec190[1]);
			float fTemp110 = (fConst56 * fRec193[1]);
			fRec195[0] = (fTemp108 + (fTemp109 + (fRec195[1] + fTemp110)));
			fRec193[0] = fRec195[0];
			float fRec194 = ((fTemp110 + fTemp109) + fTemp108);
			fRec192[0] = (fRec192[1] + fRec193[0]);
			fRec190[0] = fRec192[0];
			float fRec191 = fRec194;
			fRec189[0] = ((fRec189[1] + fTemp107) + fRec191);
			fRec187[0] = fRec189[0];
			float fRec188 = (fTemp107 + fRec191);
			float fTemp111 = (fConst47 * fRec196[1]);
			float fTemp112 = (fConst48 * fRec199[1]);
			float fTemp113 = (fConst50 * (((0.0223305579f * fTemp29) + (7.6399001e-06f * fTemp34)) - ((0.0029269685f * fTemp31) + ((((((1.70050007e-05f * fTemp32) + (0.0162210763f * fTemp26)) + (0.0510658734f * fTemp27)) + (0.0223399028f * fTemp33)) + (0.026180204f * fTemp28)) + (0.0161926318f * fTemp30)))));
			float fTemp114 = (fConst51 * fRec202[1]);
			float fTemp115 = (fConst52 * fRec205[1]);
			fRec207[0] = (fTemp113 + (fTemp114 + (fRec207[1] + fTemp115)));
			fRec205[0] = fRec207[0];
			float fRec206 = ((fTemp115 + fTemp114) + fTemp113);
			fRec204[0] = (fRec204[1] + fRec205[0]);
			fRec202[0] = fRec204[0];
			float fRec203 = fRec206;
			fRec201[0] = ((fTemp111 + (fRec201[1] + fTemp112)) + fRec203);
			fRec199[0] = fRec201[0];
			float fRec200 = ((fTemp112 + fTemp111) + fRec203);
			fRec198[0] = (fRec198[1] + fRec199[0]);
			fRec196[0] = fRec198[0];
			float fRec197 = fRec200;
			fVec5[(IOTA & 511)] = ((0.04539131f * fTemp7) + (((fRec179 + fRec182) + fRec188) + fRec197));
			output5[i] = FAUSTFLOAT((0.90871048f * (fRec0[0] * fVec5[((IOTA - iConst66) & 511)])));
			float fTemp116 = (fConst39 * (((0.00926380046f * fTemp10) + (0.0477699935f * fTemp8)) + (0.0345857032f * fTemp9)));
			float fTemp117 = (fConst40 * fRec208[1]);
			fRec210[0] = (fTemp116 + (fRec210[1] + fTemp117));
			fRec208[0] = fRec210[0];
			float fRec209 = (fTemp117 + fTemp116);
			float fTemp118 = (fConst42 * (((((0.0108376313f * fTemp38) + (0.0150328474f * fTemp41)) + (0.0319898091f * fTemp39)) + (0.0561233684f * fTemp40)) + (0.0187769048f * fTemp42)));
			float fTemp119 = (fConst44 * fRec211[1]);
			float fTemp120 = (fConst43 * fRec214[1]);
			fRec216[0] = (fTemp118 + (fTemp119 + (fRec216[1] + fTemp120)));
			fRec214[0] = fRec216[0];
			float fRec215 = ((fTemp120 + fTemp119) + fTemp118);
			fRec213[0] = (fRec213[1] + fRec214[0]);
			fRec211[0] = fRec213[0];
			float fRec212 = fRec215;
			float fTemp121 = (fConst33 * fRec217[1]);
			float fTemp122 = (fConst35 * ((0.00513769081f * fTemp17) + ((0.0148308799f * fTemp19) + ((0.0197001658f * fTemp16) + ((0.00860633329f * fTemp15) + ((0.00860711467f * fTemp14) + ((0.0553685278f * fTemp13) + (0.0341309495f * fTemp18))))))));
			float fTemp123 = (fConst36 * fRec220[1]);
			float fTemp124 = (fConst37 * fRec223[1]);
			fRec225[0] = (fTemp122 + (fTemp123 + (fRec225[1] + fTemp124)));
			fRec223[0] = fRec225[0];
			float fRec224 = ((fTemp124 + fTemp123) + fTemp122);
			fRec222[0] = (fRec222[1] + fRec223[0]);
			fRec220[0] = fRec222[0];
			float fRec221 = fRec224;
			fRec219[0] = ((fRec219[1] + fTemp121) + fRec221);
			fRec217[0] = fRec219[0];
			float fRec218 = (fTemp121 + fRec221);
			float fTemp125 = (fConst26 * fRec226[1]);
			float fTemp126 = (fConst27 * fRec229[1]);
			float fTemp127 = (fConst29 * (((0.00330297952f * fTemp31) + ((0.016613761f * fTemp30) + ((0.0391614065f * fTemp34) + (((((0.00572440261f * fTemp32) + (0.0166129041f * fTemp26)) + (0.0226047412f * fTemp27)) + (0.00930862501f * fTemp33)) + (0.0347524434f * fTemp29))))) - (0.0161808208f * fTemp28)));
			float fTemp128 = (fConst30 * fRec232[1]);
			float fTemp129 = (fConst31 * fRec235[1]);
			fRec237[0] = (fTemp127 + (fTemp128 + (fRec237[1] + fTemp129)));
			fRec235[0] = fRec237[0];
			float fRec236 = ((fTemp129 + fTemp128) + fTemp127);
			fRec234[0] = (fRec234[1] + fRec235[0]);
			fRec232[0] = fRec234[0];
			float fRec233 = fRec236;
			fRec231[0] = ((fTemp125 + (fRec231[1] + fTemp126)) + fRec233);
			fRec229[0] = fRec231[0];
			float fRec230 = ((fTemp126 + fTemp125) + fRec233);
			fRec228[0] = (fRec228[1] + fRec229[0]);
			fRec226[0] = fRec228[0];
			float fRec227 = fRec230;
			fVec6[(IOTA & 511)] = ((0.0361924917f * fTemp7) + (((fRec209 + fRec212) + fRec218) + fRec227));
			output6[i] = FAUSTFLOAT((0.908930719f * (fRec0[0] * fVec6[((IOTA - iConst45) & 511)])));
			float fTemp130 = (fConst68 * (((0.0394506529f * fTemp10) + (0.0185638908f * fTemp8)) - (0.00184053963f * fTemp9)));
			float fTemp131 = (fConst69 * fRec238[1]);
			fRec240[0] = (fTemp130 + (fRec240[1] + fTemp131));
			fRec238[0] = fRec240[0];
			float fRec239 = (fTemp131 + fTemp130);
			float fTemp132 = (fConst71 * ((0.034612678f * fTemp41) - ((((0.00298444019f * fTemp38) + (0.0117430286f * fTemp39)) + (0.00246379874f * fTemp40)) + (0.0370986424f * fTemp42))));
			float fTemp133 = (fConst72 * fRec241[1]);
			float fTemp134 = (fConst73 * fRec244[1]);
			fRec246[0] = (fTemp132 + (fTemp133 + (fRec246[1] + fTemp134)));
			fRec244[0] = fRec246[0];
			float fRec245 = ((fTemp134 + fTemp133) + fTemp132);
			fRec243[0] = (fRec243[1] + fRec244[0]);
			fRec241[0] = fRec243[0];
			float fRec242 = fRec245;
			float fTemp135 = (fConst75 * fRec247[1]);
			float fTemp136 = (fConst77 * ((0.00330808479f * fTemp14) - ((0.0242720768f * fTemp17) + ((0.00268839579f * fTemp19) + ((0.00467471452f * fTemp16) + ((0.0327155329f * fTemp15) + ((0.00141779461f * fTemp13) + (0.0364895128f * fTemp18))))))));
			float fTemp137 = (fConst78 * fRec250[1]);
			float fTemp138 = (fConst79 * fRec253[1]);
			fRec255[0] = (fTemp136 + (fTemp137 + (fRec255[1] + fTemp138)));
			fRec253[0] = fRec255[0];
			float fRec254 = ((fTemp138 + fTemp137) + fTemp136);
			fRec252[0] = (fRec252[1] + fRec253[0]);
			fRec250[0] = fRec252[0];
			float fRec251 = fRec254;
			fRec249[0] = ((fRec249[1] + fTemp135) + fRec251);
			fRec247[0] = fRec249[0];
			float fRec248 = (fTemp135 + fRec251);
			float fTemp139 = (fConst81 * fRec256[1]);
			float fTemp140 = (fConst82 * fRec259[1]);
			float fTemp141 = (fConst84 * (((0.0276123788f * fTemp31) + (((0.00299897627f * fTemp32) + (0.000542862981f * fTemp29)) + (0.00580607774f * fTemp30))) - (((((0.0341580957f * fTemp26) + (0.00387094147f * fTemp27)) + (0.0273985285f * fTemp33)) + (0.0089921616f * fTemp28)) + (0.00595043786f * fTemp34))));
			float fTemp142 = (fConst85 * fRec262[1]);
			float fTemp143 = (fConst86 * fRec265[1]);
			fRec267[0] = (fTemp141 + (fTemp142 + (fRec267[1] + fTemp143)));
			fRec265[0] = fRec267[0];
			float fRec266 = ((fTemp143 + fTemp142) + fTemp141);
			fRec264[0] = (fRec264[1] + fRec265[0]);
			fRec262[0] = fRec264[0];
			float fRec263 = fRec266;
			fRec261[0] = ((fTemp139 + (fRec261[1] + fTemp140)) + fRec263);
			fRec259[0] = fRec261[0];
			float fRec260 = ((fTemp140 + fTemp139) + fRec263);
			fRec258[0] = (fRec258[1] + fRec259[0]);
			fRec256[0] = fRec258[0];
			float fRec257 = fRec260;
			fVec7[(IOTA & 511)] = ((0.0261618514f * fTemp7) + (((fRec239 + fRec242) + fRec248) + fRec257));
			output7[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec7[((IOTA - iConst87) & 511)])));
			float fTemp144 = (fConst89 * ((0.0233319849f * fTemp41) - ((((0.0444219857f * fTemp38) + (0.0256232228f * fTemp39)) + (0.0142473914f * fTemp40)) + (0.0244926121f * fTemp42))));
			float fTemp145 = (fConst90 * fRec268[1]);
			float fTemp146 = (fConst91 * fRec271[1]);
			fRec273[0] = (fTemp144 + (fTemp145 + (fRec273[1] + fTemp146)));
			fRec271[0] = fRec273[0];
			float fRec272 = ((fTemp146 + fTemp145) + fTemp144);
			fRec270[0] = (fRec270[1] + fRec271[0]);
			fRec268[0] = fRec270[0];
			float fRec269 = fRec272;
			float fTemp147 = (fConst93 * (((0.0445195064f * fTemp10) + (0.0137387449f * fTemp8)) - (0.0263461135f * fTemp9)));
			float fTemp148 = (fConst94 * fRec274[1]);
			fRec276[0] = (fTemp147 + (fRec276[1] + fTemp148));
			fRec274[0] = fRec276[0];
			float fRec275 = (fTemp148 + fTemp147);
			float fTemp149 = (fConst96 * fRec277[1]);
			float fTemp150 = (fConst98 * (((0.00126608519f * fTemp15) + ((0.0128872748f * fTemp13) + (0.045305524f * fTemp14))) - ((0.0225832518f * fTemp17) + ((0.0228283945f * fTemp19) + ((0.0274303351f * fTemp16) + (0.0141941234f * fTemp18))))));
			float fTemp151 = (fConst99 * fRec280[1]);
			float fTemp152 = (fConst100 * fRec283[1]);
			fRec285[0] = (fTemp150 + (fTemp151 + (fRec285[1] + fTemp152)));
			fRec283[0] = fRec285[0];
			float fRec284 = ((fTemp152 + fTemp151) + fTemp150);
			fRec282[0] = (fRec282[1] + fRec283[0]);
			fRec280[0] = fRec282[0];
			float fRec281 = fRec284;
			fRec279[0] = (fTemp149 + (fRec281 + fRec279[1]));
			fRec277[0] = fRec279[0];
			float fRec278 = (fRec281 + fTemp149);
			float fTemp153 = (fConst102 * fRec286[1]);
			float fTemp154 = (fConst103 * fRec289[1]);
			float fTemp155 = (fConst105 * (((0.0307943691f * fTemp30) + ((0.00927081145f * fTemp34) + ((0.0158457495f * fTemp29) + ((((0.031408146f * fTemp32) + (0.002038331f * fTemp26)) + (0.0142210312f * fTemp27)) + (0.00941270497f * fTemp28))))) - ((0.0267490074f * fTemp33) + (0.0197014492f * fTemp31))));
			float fTemp156 = (fConst106 * fRec292[1]);
			float fTemp157 = (fConst107 * fRec295[1]);
			fRec297[0] = (fTemp155 + (fTemp156 + (fRec297[1] + fTemp157)));
			fRec295[0] = fRec297[0];
			float fRec296 = ((fTemp157 + fTemp156) + fTemp155);
			fRec294[0] = (fRec295[0] + fRec294[1]);
			fRec292[0] = fRec294[0];
			float fRec293 = fRec296;
			fRec291[0] = (fTemp153 + (fTemp154 + (fRec293 + fRec291[1])));
			fRec289[0] = fRec291[0];
			float fRec290 = (fTemp153 + (fRec293 + fTemp154));
			fRec288[0] = (fRec289[0] + fRec288[1]);
			fRec286[0] = fRec288[0];
			float fRec287 = fRec290;
			fVec8[(IOTA & 511)] = ((0.0323272236f * fTemp7) + (fRec269 + (fRec275 + (fRec278 + fRec287))));
			output8[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec8[((IOTA - iConst108) & 511)])));
			float fTemp158 = (fConst102 * fRec298[1]);
			float fTemp159 = (fConst103 * fRec301[1]);
			float fTemp160 = (fConst105 * (((0.0243970491f * fTemp29) + (((0.0287768673f * fTemp26) + (0.00576110976f * fTemp27)) + (0.0038950583f * fTemp28))) - ((0.00756088458f * fTemp31) + ((0.000411854504f * fTemp30) + (((0.0317140669f * fTemp32) + (0.0133470157f * fTemp33)) + (0.00929860771f * fTemp34))))));
			float fTemp161 = (fConst106 * fRec304[1]);
			float fTemp162 = (fConst107 * fRec307[1]);
			fRec309[0] = (fTemp160 + (fTemp161 + (fRec309[1] + fTemp162)));
			fRec307[0] = fRec309[0];
			float fRec308 = ((fTemp162 + fTemp161) + fTemp160);
			fRec306[0] = (fRec307[0] + fRec306[1]);
			fRec304[0] = fRec306[0];
			float fRec305 = fRec308;
			fRec303[0] = (fTemp158 + (fTemp159 + (fRec305 + fRec303[1])));
			fRec301[0] = fRec303[0];
			float fRec302 = (fTemp158 + (fRec305 + fTemp159));
			fRec300[0] = (fRec301[0] + fRec300[1]);
			fRec298[0] = fRec300[0];
			float fRec299 = fRec302;
			float fTemp163 = (fConst96 * fRec310[1]);
			float fTemp164 = (fConst98 * (((0.0368976034f * fTemp15) + ((0.0165556017f * fTemp13) + (0.0143728498f * fTemp18))) - ((0.0205141604f * fTemp17) + ((0.00657225167f * fTemp19) + ((0.0247016866f * fTemp16) + (0.00747417286f * fTemp14))))));
			float fTemp165 = (fConst99 * fRec313[1]);
			float fTemp166 = (fConst100 * fRec316[1]);
			fRec318[0] = (fTemp164 + (fTemp165 + (fRec318[1] + fTemp166)));
			fRec316[0] = fRec318[0];
			float fRec317 = ((fTemp166 + fTemp165) + fTemp164);
			fRec315[0] = (fRec316[0] + fRec315[1]);
			fRec313[0] = fRec315[0];
			float fRec314 = fRec317;
			fRec312[0] = (fTemp163 + (fRec314 + fRec312[1]));
			fRec310[0] = fRec312[0];
			float fRec311 = (fRec314 + fTemp163);
			float fTemp167 = (fConst93 * (((0.0186304655f * fTemp10) + (0.0127383061f * fTemp8)) - (0.0369097739f * fTemp9)));
			float fTemp168 = (fConst94 * fRec319[1]);
			fRec321[0] = (fTemp167 + (fRec321[1] + fTemp168));
			fRec319[0] = fRec321[0];
			float fRec320 = (fTemp168 + fTemp167);
			float fTemp169 = (fConst89 * (((0.012669703f * fTemp41) + (0.0246489197f * fTemp42)) - (((0.0326845907f * fTemp38) + (0.019024346f * fTemp39)) + (0.0218561292f * fTemp40))));
			float fTemp170 = (fConst90 * fRec322[1]);
			float fTemp171 = (fConst91 * fRec325[1]);
			fRec327[0] = (fTemp169 + (fTemp170 + (fRec327[1] + fTemp171)));
			fRec325[0] = fRec327[0];
			float fRec326 = ((fTemp171 + fTemp170) + fTemp169);
			fRec324[0] = (fRec325[0] + fRec324[1]);
			fRec322[0] = fRec324[0];
			float fRec323 = fRec326;
			fVec9[(IOTA & 511)] = ((0.0259500761f * fTemp7) + (fRec299 + (fRec311 + (fRec320 + fRec323))));
			output9[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec9[((IOTA - iConst108) & 511)])));
			float fTemp172 = (fConst81 * fRec328[1]);
			float fTemp173 = (fConst82 * fRec331[1]);
			float fTemp174 = (fConst84 * ((((0.00593551574f * fTemp34) + ((0.00580562465f * fTemp26) + (0.0273841731f * fTemp29))) + (0.0276221391f * fTemp31)) - (((((0.00299062417f * fTemp32) + (0.00387358875f * fTemp27)) + (0.000543645583f * fTemp33)) + (0.00897448137f * fTemp28)) + (0.0341508761f * fTemp30))));
			float fTemp175 = (fConst85 * fRec334[1]);
			float fTemp176 = (fConst86 * fRec337[1]);
			fRec339[0] = (fTemp174 + (fTemp175 + (fRec339[1] + fTemp176)));
			fRec337[0] = fRec339[0];
			float fRec338 = ((fTemp176 + fTemp175) + fTemp174);
			fRec336[0] = (fRec337[0] + fRec336[1]);
			fRec334[0] = fRec336[0];
			float fRec335 = fRec338;
			fRec333[0] = (fTemp172 + (fTemp173 + (fRec335 + fRec333[1])));
			fRec331[0] = fRec333[0];
			float fRec332 = (fTemp172 + (fRec335 + fTemp173));
			fRec330[0] = (fRec331[0] + fRec330[1]);
			fRec328[0] = fRec330[0];
			float fRec329 = fRec332;
			float fTemp177 = (fConst75 * fRec340[1]);
			float fTemp178 = (fConst77 * (((0.00141847541f * fTemp19) + ((0.00330042141f * fTemp15) + ((0.00270266877f * fTemp13) + (0.0364695266f * fTemp18)))) - ((0.0242545679f * fTemp17) + ((0.00467247749f * fTemp16) + (0.0327177942f * fTemp14)))));
			float fTemp179 = (fConst78 * fRec343[1]);
			float fTemp180 = (fConst79 * fRec346[1]);
			fRec348[0] = (fTemp178 + (fTemp179 + (fRec348[1] + fTemp180)));
			fRec346[0] = fRec348[0];
			float fRec347 = ((fTemp180 + fTemp179) + fTemp178);
			fRec345[0] = (fRec346[0] + fRec345[1]);
			fRec343[0] = fRec345[0];
			float fRec344 = fRec347;
			fRec342[0] = (fTemp177 + (fRec344 + fRec342[1]));
			fRec340[0] = fRec342[0];
			float fRec341 = (fRec344 + fTemp177);
			float fTemp181 = (fConst68 * (((0.00183700956f * fTemp10) + (0.0185472425f * fTemp8)) - (0.0394348688f * fTemp9)));
			float fTemp182 = (fConst69 * fRec349[1]);
			fRec351[0] = (fTemp181 + (fRec351[1] + fTemp182));
			fRec349[0] = fRec351[0];
			float fRec350 = (fTemp182 + fTemp181);
			float fTemp183 = (fConst71 * (((0.00246185367f * fTemp41) + (0.0370914005f * fTemp42)) - (((0.00297827949f * fTemp38) + (0.0117452387f * fTemp39)) + (0.0345855802f * fTemp40))));
			float fTemp184 = (fConst72 * fRec352[1]);
			float fTemp185 = (fConst73 * fRec355[1]);
			fRec357[0] = (fTemp183 + (fTemp184 + (fRec357[1] + fTemp185)));
			fRec355[0] = fRec357[0];
			float fRec356 = ((fTemp185 + fTemp184) + fTemp183);
			fRec354[0] = (fRec355[0] + fRec354[1]);
			fRec352[0] = fRec354[0];
			float fRec353 = fRec356;
			fVec10[(IOTA & 511)] = ((0.0261483416f * fTemp7) + (fRec329 + (fRec341 + (fRec350 + fRec353))));
			output10[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec10[((IOTA - iConst87) & 511)])));
			float fTemp186 = (fConst98 * (((((0.0168354791f * fTemp18) + (0.0163048115f * fTemp13)) + (0.0232635159f * fTemp16)) + (0.00700940285f * fTemp19)) - (((0.0100207152f * fTemp14) + (0.0368799195f * fTemp15)) + (0.0205050856f * fTemp17))));
			float fTemp187 = (fConst100 * fRec364[1]);
			float fTemp188 = (fConst99 * fRec361[1]);
			fRec366[0] = (((fTemp186 + fRec366[1]) + fTemp187) + fTemp188);
			fRec364[0] = fRec366[0];
			float fRec365 = ((fTemp186 + fTemp187) + fTemp188);
			fRec363[0] = (fRec364[0] + fRec363[1]);
			fRec361[0] = fRec363[0];
			float fRec362 = fRec365;
			float fTemp189 = (fConst96 * fRec358[1]);
			fRec360[0] = ((fRec362 + fRec360[1]) + fTemp189);
			fRec358[0] = fRec360[0];
			float fRec359 = (fRec362 + fTemp189);
			float fTemp190 = (fConst89 * (((0.0316663086f * fTemp38) + (0.0263799429f * fTemp42)) - (((0.011496013f * fTemp41) + (0.0190213211f * fTemp39)) + (0.0225178245f * fTemp40))));
			float fTemp191 = (fConst90 * fRec367[1]);
			float fTemp192 = (fConst91 * fRec370[1]);
			fRec372[0] = (fTemp190 + (fTemp191 + (fRec372[1] + fTemp192)));
			fRec370[0] = fRec372[0];
			float fRec371 = ((fTemp192 + fTemp191) + fTemp190);
			fRec369[0] = (fRec370[0] + fRec369[1]);
			fRec367[0] = fRec369[0];
			float fRec368 = fRec371;
			float fTemp193 = (fConst102 * fRec373[1]);
			float fTemp194 = (fConst103 * fRec376[1]);
			float fTemp195 = (fConst105 * (((0.0247420892f * fTemp29) + (((0.0330667645f * fTemp32) + (0.0127315633f * fTemp33)) + (0.0039006914f * fTemp28))) - ((0.00518195657f * fTemp31) + ((0.0044993693f * fTemp30) + (((0.0287646987f * fTemp26) + (0.0066821347f * fTemp27)) + (0.00771285361f * fTemp34))))));
			float fTemp196 = (fConst106 * fRec379[1]);
			float fTemp197 = (fConst107 * fRec382[1]);
			fRec384[0] = (fTemp195 + (fTemp196 + (fRec384[1] + fTemp197)));
			fRec382[0] = fRec384[0];
			float fRec383 = ((fTemp197 + fTemp196) + fTemp195);
			fRec381[0] = (fRec382[0] + fRec381[1]);
			fRec379[0] = fRec381[0];
			float fRec380 = fRec383;
			fRec378[0] = (fTemp193 + (fTemp194 + (fRec380 + fRec378[1])));
			fRec376[0] = fRec378[0];
			float fRec377 = (fTemp193 + (fRec380 + fTemp194));
			fRec375[0] = (fRec376[0] + fRec375[1]);
			fRec373[0] = fRec375[0];
			float fRec374 = fRec377;
			float fTemp198 = (fConst93 * ((0.0127308108f * fTemp8) - ((0.0176555794f * fTemp10) + (0.0374553464f * fTemp9))));
			float fTemp199 = (fConst94 * fRec385[1]);
			fRec387[0] = (fTemp198 + (fRec387[1] + fTemp199));
			fRec385[0] = fRec387[0];
			float fRec386 = (fTemp199 + fTemp198);
			fVec11[(IOTA & 511)] = ((fRec359 + (fRec368 + (fRec374 + fRec386))) + (0.0259405281f * fTemp7));
			output11[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec11[((IOTA - iConst108) & 511)])));
			float fTemp200 = (fConst102 * fRec388[1]);
			float fTemp201 = (fConst103 * fRec391[1]);
			float fTemp202 = (fConst105 * (((0.0382165462f * fTemp30) + ((0.00769494474f * fTemp34) + ((0.0174858887f * fTemp29) + ((0.0291910823f * fTemp33) + (0.0020518736f * fTemp28))))) - ((((0.0324732549f * fTemp32) + (0.00581610203f * fTemp26)) + (0.00558697246f * fTemp27)) + (0.0222300626f * fTemp31))));
			float fTemp203 = (fConst106 * fRec394[1]);
			float fTemp204 = (fConst107 * fRec397[1]);
			fRec399[0] = (fTemp202 + (fTemp203 + (fRec399[1] + fTemp204)));
			fRec397[0] = fRec399[0];
			float fRec398 = ((fTemp204 + fTemp203) + fTemp202);
			fRec396[0] = (fRec397[0] + fRec396[1]);
			fRec394[0] = fRec396[0];
			float fRec395 = fRec398;
			fRec393[0] = (fTemp200 + (fTemp201 + (fRec395 + fRec393[1])));
			fRec391[0] = fRec393[0];
			float fRec392 = (fTemp200 + (fRec395 + fTemp201));
			fRec390[0] = (fRec391[0] + fRec390[1]);
			fRec388[0] = fRec390[0];
			float fRec389 = fRec392;
			float fTemp205 = (fConst96 * fRec400[1]);
			float fTemp206 = (fConst98 * (((0.0179398041f * fTemp19) + ((0.0379067026f * fTemp16) + ((0.008722906f * fTemp13) + (0.0495633371f * fTemp14)))) - ((0.0284428578f * fTemp17) + ((0.0033218693f * fTemp15) + (0.0164802484f * fTemp18)))));
			float fTemp207 = (fConst99 * fRec403[1]);
			float fTemp208 = (fConst100 * fRec406[1]);
			fRec408[0] = (fTemp206 + (fTemp207 + (fRec408[1] + fTemp208)));
			fRec406[0] = fRec408[0];
			float fRec407 = ((fTemp208 + fTemp207) + fTemp206);
			fRec405[0] = (fRec406[0] + fRec405[1]);
			fRec403[0] = fRec405[0];
			float fRec404 = fRec407;
			fRec402[0] = (fTemp205 + (fRec404 + fRec402[1]));
			fRec400[0] = fRec402[0];
			float fRec401 = (fRec404 + fTemp205);
			float fTemp209 = (fConst93 * ((0.0205770936f * fTemp8) - ((0.0513798967f * fTemp10) + (0.0317948349f * fTemp9))));
			float fTemp210 = (fConst94 * fRec409[1]);
			fRec411[0] = (fTemp209 + (fRec411[1] + fTemp210));
			fRec409[0] = fRec411[0];
			float fRec410 = (fTemp210 + fTemp209);
			float fTemp211 = (fConst89 * ((0.0511338785f * fTemp38) - ((((0.0323468074f * fTemp41) + (0.024969304f * fTemp39)) + (0.0215231236f * fTemp40)) + (0.026064856f * fTemp42))));
			float fTemp212 = (fConst90 * fRec412[1]);
			float fTemp213 = (fConst91 * fRec415[1]);
			fRec417[0] = (fTemp211 + (fTemp212 + (fRec417[1] + fTemp213)));
			fRec415[0] = fRec417[0];
			float fRec416 = ((fTemp213 + fTemp212) + fTemp211);
			fRec414[0] = (fRec415[0] + fRec414[1]);
			fRec412[0] = fRec414[0];
			float fRec413 = fRec416;
			fVec12[(IOTA & 511)] = ((0.0389448367f * fTemp7) + (fRec389 + (fRec401 + (fRec410 + fRec413))));
			output12[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec12[((IOTA - iConst108) & 511)])));
			float fTemp214 = (fConst71 * ((0.00246844557f * fTemp40) - ((((0.0029989176f * fTemp38) + (0.0346155316f * fTemp41)) + (0.0117413951f * fTemp39)) + (0.0370995589f * fTemp42))));
			float fTemp215 = (fConst72 * fRec418[1]);
			float fTemp216 = (fConst73 * fRec421[1]);
			fRec423[0] = (fTemp214 + (fTemp215 + (fRec423[1] + fTemp216)));
			fRec421[0] = fRec423[0];
			float fRec422 = ((fTemp216 + fTemp215) + fTemp214);
			fRec420[0] = (fRec421[0] + fRec420[1]);
			fRec418[0] = fRec420[0];
			float fRec419 = fRec422;
			float fTemp217 = (fConst68 * (((0.0185660571f * fTemp8) + (0.00184788718f * fTemp9)) - (0.0394531265f * fTemp10)));
			float fTemp218 = (fConst69 * fRec424[1]);
			fRec426[0] = (fTemp217 + (fRec426[1] + fTemp218));
			fRec424[0] = fRec426[0];
			float fRec425 = (fTemp218 + fTemp217);
			float fTemp219 = (fConst75 * fRec427[1]);
			float fTemp220 = (fConst77 * (((0.00268353708f * fTemp19) + ((0.0327142365f * fTemp15) + (0.00141352706f * fTemp13))) - ((0.0242712181f * fTemp17) + ((0.00468599051f * fTemp16) + ((0.0364912525f * fTemp18) + (0.00332705281f * fTemp14))))));
			float fTemp221 = (fConst78 * fRec430[1]);
			float fTemp222 = (fConst79 * fRec433[1]);
			fRec435[0] = (fTemp220 + (fTemp221 + (fRec435[1] + fTemp222)));
			fRec433[0] = fRec435[0];
			float fRec434 = ((fTemp222 + fTemp221) + fTemp220);
			fRec432[0] = (fRec433[0] + fRec432[1]);
			fRec430[0] = fRec432[0];
			float fRec431 = fRec434;
			fRec429[0] = (fTemp219 + (fRec431 + fRec429[1]));
			fRec427[0] = fRec429[0];
			float fRec428 = (fRec431 + fTemp219);
			float fTemp223 = (fConst81 * fRec436[1]);
			float fTemp224 = (fConst82 * fRec439[1]);
			float fTemp225 = (fConst84 * (((((0.00301864557f * fTemp32) + (0.0341576152f * fTemp26)) + (0.0273937713f * fTemp33)) + (0.0276086535f * fTemp31)) - ((0.00582292723f * fTemp30) + ((0.00595741579f * fTemp34) + (((0.00386652979f * fTemp27) + (0.00899475813f * fTemp28)) + (0.000550913624f * fTemp29))))));
			float fTemp226 = (fConst85 * fRec442[1]);
			float fTemp227 = (fConst86 * fRec445[1]);
			fRec447[0] = (fTemp225 + (fTemp226 + (fRec447[1] + fTemp227)));
			fRec445[0] = fRec447[0];
			float fRec446 = ((fTemp227 + fTemp226) + fTemp225);
			fRec444[0] = (fRec445[0] + fRec444[1]);
			fRec442[0] = fRec444[0];
			float fRec443 = fRec446;
			fRec441[0] = (fTemp223 + (fTemp224 + (fRec443 + fRec441[1])));
			fRec439[0] = fRec441[0];
			float fRec440 = (fTemp223 + (fRec443 + fTemp224));
			fRec438[0] = (fRec439[0] + fRec438[1]);
			fRec436[0] = fRec438[0];
			float fRec437 = fRec440;
			fVec13[(IOTA & 511)] = ((0.0261641443f * fTemp7) + (fRec419 + (fRec425 + (fRec428 + fRec437))));
			output13[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec13[((IOTA - iConst87) & 511)])));
			float fTemp228 = (fConst102 * fRec448[1]);
			float fTemp229 = (fConst103 * fRec451[1]);
			float fTemp230 = (fConst105 * (((((((0.0317293145f * fTemp32) + (0.000424335798f * fTemp26)) + (0.00576349348f * fTemp27)) + (0.0243821163f * fTemp33)) + (0.00390488165f * fTemp28)) + (0.00931942556f * fTemp34)) - ((0.00753142405f * fTemp31) + ((0.0133271106f * fTemp29) + (0.028754428f * fTemp30)))));
			float fTemp231 = (fConst106 * fRec454[1]);
			float fTemp232 = (fConst107 * fRec457[1]);
			fRec459[0] = (fTemp230 + (fTemp231 + (fRec459[1] + fTemp232)));
			fRec457[0] = fRec459[0];
			float fRec458 = ((fTemp232 + fTemp231) + fTemp230);
			fRec456[0] = (fRec457[0] + fRec456[1]);
			fRec454[0] = fRec456[0];
			float fRec455 = fRec458;
			fRec453[0] = (fTemp228 + (fTemp229 + (fRec455 + fRec453[1])));
			fRec451[0] = fRec453[0];
			float fRec452 = (fTemp228 + (fRec455 + fTemp229));
			fRec450[0] = (fRec451[0] + fRec450[1]);
			fRec448[0] = fRec450[0];
			float fRec449 = fRec452;
			float fTemp233 = (fConst96 * fRec460[1]);
			float fTemp234 = (fConst98 * (((0.0075061284f * fTemp15) + (0.0165674873f * fTemp19)) - ((0.02049651f * fTemp17) + ((0.0246765427f * fTemp16) + ((0.0368934311f * fTemp14) + ((0.00656695059f * fTemp13) + (0.0143680032f * fTemp18)))))));
			float fTemp235 = (fConst99 * fRec463[1]);
			float fTemp236 = (fConst100 * fRec466[1]);
			fRec468[0] = (fTemp234 + (fTemp235 + (fRec468[1] + fTemp236)));
			fRec466[0] = fRec468[0];
			float fRec467 = ((fTemp236 + fTemp235) + fTemp234);
			fRec465[0] = (fRec466[0] + fRec465[1]);
			fRec463[0] = fRec465[0];
			float fRec464 = fRec467;
			fRec462[0] = (fTemp233 + (fRec464 + fRec462[1]));
			fRec460[0] = fRec462[0];
			float fRec461 = (fRec464 + fTemp233);
			float fTemp237 = (fConst93 * (((0.0127271321f * fTemp8) + (0.0186162814f * fTemp9)) - (0.0369095467f * fTemp10)));
			float fTemp238 = (fConst94 * fRec469[1]);
			fRec471[0] = (fTemp237 + (fRec471[1] + fTemp238));
			fRec469[0] = fRec471[0];
			float fRec470 = (fTemp238 + fTemp237);
			float fTemp239 = (fConst89 * ((0.0126558729f * fTemp40) - ((((0.0326672122f * fTemp38) + (0.0218386911f * fTemp41)) + (0.0190253519f * fTemp39)) + (0.0246668719f * fTemp42))));
			float fTemp240 = (fConst90 * fRec472[1]);
			float fTemp241 = (fConst91 * fRec475[1]);
			fRec477[0] = (fTemp239 + (fTemp240 + (fRec477[1] + fTemp241)));
			fRec475[0] = fRec477[0];
			float fRec476 = ((fTemp241 + fTemp240) + fTemp239);
			fRec474[0] = (fRec475[0] + fRec474[1]);
			fRec472[0] = fRec474[0];
			float fRec473 = fRec476;
			fVec14[(IOTA & 511)] = ((fRec449 + (fRec461 + (fRec470 + fRec473))) + (0.0259447377f * fTemp7));
			output14[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec14[((IOTA - iConst108) & 511)])));
			float fTemp242 = (fConst93 * (((0.0137484306f * fTemp8) + (0.0445271879f * fTemp9)) - (0.0263485294f * fTemp10)));
			float fTemp243 = (fConst94 * fRec478[1]);
			fRec480[0] = (fTemp242 + (fRec480[1] + fTemp243));
			fRec478[0] = fRec480[0];
			float fRec479 = (fTemp243 + fTemp242);
			float fTemp244 = (fConst96 * fRec481[1]);
			float fTemp245 = (fConst98 * (((0.0128821284f * fTemp19) + (0.014208056f * fTemp18)) - ((0.0225955732f * fTemp17) + ((0.0274373721f * fTemp16) + ((0.0453056693f * fTemp15) + ((0.0228183232f * fTemp13) + (0.00126505631f * fTemp14)))))));
			float fTemp246 = (fConst99 * fRec484[1]);
			float fTemp247 = (fConst100 * fRec487[1]);
			fRec489[0] = (fTemp245 + (fTemp246 + (fRec489[1] + fTemp247)));
			fRec487[0] = fRec489[0];
			float fRec488 = ((fTemp247 + fTemp246) + fTemp245);
			fRec486[0] = (fRec487[0] + fRec486[1]);
			fRec484[0] = fRec486[0];
			float fRec485 = fRec488;
			fRec483[0] = (fTemp244 + (fRec485 + fRec483[1]));
			fRec481[0] = fRec483[0];
			float fRec482 = (fRec485 + fTemp244);
			float fTemp248 = (fConst102 * fRec490[1]);
			float fTemp249 = (fConst103 * fRec493[1]);
			float fTemp250 = (fConst105 * ((((0.0142090674f * fTemp27) + (0.0158480611f * fTemp33)) + (0.00939842314f * fTemp28)) - ((0.0197022595f * fTemp31) + ((0.00202640146f * fTemp30) + ((0.0092611704f * fTemp34) + (((0.0314044729f * fTemp32) + (0.0307981484f * fTemp26)) + (0.0267607309f * fTemp29)))))));
			float fTemp251 = (fConst106 * fRec496[1]);
			float fTemp252 = (fConst107 * fRec499[1]);
			fRec501[0] = (fTemp250 + (fTemp251 + (fRec501[1] + fTemp252)));
			fRec499[0] = fRec501[0];
			float fRec500 = ((fTemp252 + fTemp251) + fTemp250);
			fRec498[0] = (fRec499[0] + fRec498[1]);
			fRec496[0] = fRec498[0];
			float fRec497 = fRec500;
			fRec495[0] = (fTemp248 + (fTemp249 + (fRec497 + fRec495[1])));
			fRec493[0] = fRec495[0];
			float fRec494 = (fTemp248 + (fRec497 + fTemp249));
			fRec492[0] = (fRec493[0] + fRec492[1]);
			fRec490[0] = fRec492[0];
			float fRec491 = fRec494;
			float fTemp253 = (fConst89 * (((0.023348514f * fTemp40) + (0.0244967658f * fTemp42)) - (((0.0444244258f * fTemp38) + (0.0142523237f * fTemp41)) + (0.0256209057f * fTemp39))));
			float fTemp254 = (fConst90 * fRec502[1]);
			float fTemp255 = (fConst91 * fRec505[1]);
			fRec507[0] = (fTemp253 + (fTemp254 + (fRec507[1] + fTemp255)));
			fRec505[0] = fRec507[0];
			float fRec506 = ((fTemp255 + fTemp254) + fTemp253);
			fRec504[0] = (fRec504[1] + fRec505[0]);
			fRec502[0] = fRec504[0];
			float fRec503 = fRec506;
			fVec15[(IOTA & 511)] = (((0.0323338211f * fTemp7) + (fRec479 + (fRec482 + fRec491))) + fRec503);
			output15[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec15[((IOTA - iConst108) & 511)])));
			float fTemp256 = (fConst81 * fRec508[1]);
			float fTemp257 = (fConst82 * fRec511[1]);
			float fTemp258 = (fConst84 * (((0.0276331604f * fTemp31) + ((0.0341478623f * fTemp30) + ((0.000545814983f * fTemp33) + (0.00592482649f * fTemp34)))) - (((((0.00299626775f * fTemp32) + (0.00581819238f * fTemp26)) + (0.00388086122f * fTemp27)) + (0.00897001103f * fTemp28)) + (0.0273900591f * fTemp29))));
			float fTemp259 = (fConst85 * fRec514[1]);
			float fTemp260 = (fConst86 * fRec517[1]);
			fRec519[0] = (fTemp258 + (fTemp259 + (fRec519[1] + fTemp260)));
			fRec517[0] = fRec519[0];
			float fRec518 = ((fTemp260 + fTemp259) + fTemp258);
			fRec516[0] = (fRec517[0] + fRec516[1]);
			fRec514[0] = fRec516[0];
			float fRec515 = fRec518;
			fRec513[0] = (fTemp256 + (fTemp257 + (fRec515 + fRec513[1])));
			fRec511[0] = fRec513[0];
			float fRec512 = (fTemp256 + (fRec515 + fTemp257));
			fRec510[0] = (fRec511[0] + fRec510[1]);
			fRec508[0] = fRec510[0];
			float fRec509 = fRec512;
			float fTemp261 = (fConst75 * fRec520[1]);
			float fTemp262 = (fConst77 * (((0.03647165f * fTemp18) + (0.0327316634f * fTemp14)) - ((0.0242597535f * fTemp17) + ((0.00142077159f * fTemp19) + ((0.00468324311f * fTemp16) + ((0.00330823287f * fTemp15) + (0.00271490379f * fTemp13)))))));
			float fTemp263 = (fConst78 * fRec523[1]);
			float fTemp264 = (fConst79 * fRec526[1]);
			fRec528[0] = (fTemp262 + (fTemp263 + (fRec528[1] + fTemp264)));
			fRec526[0] = fRec528[0];
			float fRec527 = ((fTemp264 + fTemp263) + fTemp262);
			fRec525[0] = (fRec526[0] + fRec525[1]);
			fRec523[0] = fRec525[0];
			float fRec524 = fRec527;
			fRec522[0] = (fTemp261 + (fRec524 + fRec522[1]));
			fRec520[0] = fRec522[0];
			float fRec521 = (fRec524 + fTemp261);
			float fTemp265 = (fConst68 * (((0.0185498931f * fTemp8) + (0.0394535773f * fTemp9)) - (0.00184174115f * fTemp10)));
			float fTemp266 = (fConst69 * fRec529[1]);
			fRec531[0] = (fTemp265 + (fRec531[1] + fTemp266));
			fRec529[0] = fRec531[0];
			float fRec530 = (fTemp266 + fTemp265);
			float fTemp267 = (fConst71 * (((0.0345902257f * fTemp40) + (0.0371084623f * fTemp42)) - (((0.00298590446f * fTemp38) + (0.00246753893f * fTemp41)) + (0.0117560346f * fTemp39))));
			float fTemp268 = (fConst72 * fRec532[1]);
			float fTemp269 = (fConst73 * fRec535[1]);
			fRec537[0] = (fTemp267 + (fTemp268 + (fRec537[1] + fTemp269)));
			fRec535[0] = fRec537[0];
			float fRec536 = ((fTemp269 + fTemp268) + fTemp267);
			fRec534[0] = (fRec535[0] + fRec534[1]);
			fRec532[0] = fRec534[0];
			float fRec533 = fRec536;
			fVec16[(IOTA & 511)] = ((0.0261601638f * fTemp7) + (fRec509 + (fRec521 + (fRec530 + fRec533))));
			output16[i] = FAUSTFLOAT((0.940535188f * (fRec0[0] * fVec16[((IOTA - iConst87) & 511)])));
			float fTemp270 = (fConst102 * fRec538[1]);
			float fTemp271 = (fConst103 * fRec541[1]);
			float fTemp272 = (fConst105 * ((((0.0324584916f * fTemp32) + (0.0382162146f * fTemp26)) + (0.00203793449f * fTemp28)) - ((0.0222405121f * fTemp31) + ((0.00581754232f * fTemp30) + ((0.00768063031f * fTemp34) + (((0.00558370119f * fTemp27) + (0.0174922403f * fTemp33)) + (0.0291981883f * fTemp29)))))));
			float fTemp273 = (fConst106 * fRec544[1]);
			float fTemp274 = (fConst107 * fRec547[1]);
			fRec549[0] = (fTemp272 + (fTemp273 + (fRec549[1] + fTemp274)));
			fRec547[0] = fRec549[0];
			float fRec548 = ((fTemp274 + fTemp273) + fTemp272);
			fRec546[0] = (fRec547[0] + fRec546[1]);
			fRec544[0] = fRec546[0];
			float fRec545 = fRec548;
			fRec543[0] = (fTemp270 + (fTemp271 + (fRec545 + fRec543[1])));
			fRec541[0] = fRec543[0];
			float fRec542 = (fTemp270 + (fRec545 + fTemp271));
			fRec540[0] = (fRec541[0] + fRec540[1]);
			fRec538[0] = fRec540[0];
			float fRec539 = fRec542;
			float fTemp275 = (fConst89 * (((((0.0511377268f * fTemp38) + (0.0215313174f * fTemp41)) + (0.032357011f * fTemp40)) + (0.0260451864f * fTemp42)) - (0.0249631405f * fTemp39)));
			float fTemp276 = (fConst90 * fRec550[1]);
			float fTemp277 = (fConst91 * fRec553[1]);
			fRec555[0] = (fTemp275 + (fTemp276 + (fRec555[1] + fTemp277)));
			fRec553[0] = fRec555[0];
			float fRec554 = ((fTemp277 + fTemp276) + fTemp275);
			fRec552[0] = (fRec553[0] + fRec552[1]);
			fRec550[0] = fRec552[0];
			float fRec551 = fRec554;
			float fTemp278 = (fConst96 * fRec556[1]);
			float fTemp279 = (fConst98 * (((0.0379152559f * fTemp16) + ((0.0495547503f * fTemp15) + (0.016481569f * fTemp18))) - ((0.0284533314f * fTemp17) + ((0.00872249622f * fTemp19) + ((0.0179251432f * fTemp13) + (0.00334320101f * fTemp14))))));
			float fTemp280 = (fConst99 * fRec559[1]);
			float fTemp281 = (fConst100 * fRec562[1]);
			fRec564[0] = (fTemp279 + (fTemp280 + (fRec564[1] + fTemp281)));
			fRec562[0] = fRec564[0];
			float fRec563 = ((fTemp281 + fTemp280) + fTemp279);
			fRec561[0] = (fRec562[0] + fRec561[1]);
			fRec559[0] = fRec561[0];
			float fRec560 = fRec563;
			fRec558[0] = (fTemp278 + (fRec560 + fRec558[1]));
			fRec556[0] = fRec558[0];
			float fRec557 = (fRec560 + fTemp278);
			float fTemp282 = (fConst93 * (((0.0318024792f * fTemp10) + (0.0205848552f * fTemp8)) + (0.0513721406f * fTemp9)));
			float fTemp283 = (fConst94 * fRec565[1]);
			fRec567[0] = (fTemp282 + (fRec567[1] + fTemp283));
			fRec565[0] = fRec567[0];
			float fRec566 = (fTemp283 + fTemp282);
			fVec17[(IOTA & 511)] = (((0.038944684f * fTemp7) + (fRec539 + (fRec551 + fRec557))) + fRec566);
			output17[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec17[((IOTA - iConst108) & 511)])));
			float fTemp284 = (fConst102 * fRec568[1]);
			float fTemp285 = (fConst103 * fRec571[1]);
			float fTemp286 = (fConst105 * (((0.00390336011f * fTemp28) + (0.00771646993f * fTemp34)) - ((0.0051958547f * fTemp31) + ((((((0.0330625139f * fTemp32) + (0.00447209831f * fTemp26)) + (0.00669157412f * fTemp27)) + (0.0247380715f * fTemp33)) + (0.0127422307f * fTemp29)) + (0.0287524126f * fTemp30)))));
			float fTemp287 = (fConst106 * fRec574[1]);
			float fTemp288 = (fConst107 * fRec577[1]);
			fRec579[0] = (fTemp286 + (fTemp287 + (fRec579[1] + fTemp288)));
			fRec577[0] = fRec579[0];
			float fRec578 = ((fTemp288 + fTemp287) + fTemp286);
			fRec576[0] = (fRec577[0] + fRec576[1]);
			fRec574[0] = fRec576[0];
			float fRec575 = fRec578;
			fRec573[0] = (fTemp284 + (fTemp285 + (fRec575 + fRec573[1])));
			fRec571[0] = fRec573[0];
			float fRec572 = (fTemp284 + (fRec575 + fTemp285));
			fRec570[0] = (fRec571[0] + fRec570[1]);
			fRec568[0] = fRec570[0];
			float fRec569 = fRec572;
			float fTemp289 = (fConst96 * fRec580[1]);
			float fTemp290 = (fConst98 * ((0.0232655909f * fTemp16) - ((0.0205040593f * fTemp17) + ((0.0163094383f * fTemp19) + ((0.0100020012f * fTemp15) + ((0.0368838497f * fTemp14) + ((0.00701622479f * fTemp13) + (0.0168153476f * fTemp18))))))));
			float fTemp291 = (fConst99 * fRec583[1]);
			float fTemp292 = (fConst100 * fRec586[1]);
			fRec588[0] = (fTemp290 + (fTemp291 + (fRec588[1] + fTemp292)));
			fRec586[0] = fRec588[0];
			float fRec587 = ((fTemp292 + fTemp291) + fTemp290);
			fRec585[0] = (fRec586[0] + fRec585[1]);
			fRec583[0] = fRec585[0];
			float fRec584 = fRec587;
			fRec582[0] = (fTemp289 + (fRec584 + fRec582[1]));
			fRec580[0] = fRec582[0];
			float fRec581 = (fRec584 + fTemp289);
			float fTemp293 = (fConst93 * (((0.0374534018f * fTemp10) + (0.0127278063f * fTemp8)) + (0.0176651441f * fTemp9)));
			float fTemp294 = (fConst94 * fRec589[1]);
			fRec591[0] = (fTemp293 + (fRec591[1] + fTemp294));
			fRec589[0] = fRec591[0];
			float fRec590 = (fTemp294 + fTemp293);
			float fTemp295 = (fConst89 * ((((0.0316778682f * fTemp38) + (0.0225088652f * fTemp41)) + (0.0115001611f * fTemp40)) - ((0.0190254468f * fTemp39) + (0.0263673887f * fTemp42))));
			float fTemp296 = (fConst90 * fRec592[1]);
			float fTemp297 = (fConst91 * fRec595[1]);
			fRec597[0] = (fTemp295 + (fTemp296 + (fRec597[1] + fTemp297)));
			fRec595[0] = fRec597[0];
			float fRec596 = ((fTemp297 + fTemp296) + fTemp295);
			fRec594[0] = (fRec595[0] + fRec594[1]);
			fRec592[0] = fRec594[0];
			float fRec593 = fRec596;
			fVec18[(IOTA & 511)] = ((0.0259419568f * fTemp7) + (fRec569 + (fRec581 + (fRec590 + fRec593))));
			output18[i] = FAUSTFLOAT((0.941085756f * (fRec0[0] * fVec18[((IOTA - iConst108) & 511)])));
			float fTemp298 = (fConst110 * fRec598[1]);
			float fTemp299 = (fConst111 * fRec601[1]);
			float fTemp300 = (fConst113 * ((((0.00793644506f * fTemp34) + ((5.46620004e-06f * fTemp29) + ((((8.97829977e-06f * fTemp32) + (0.0373077318f * fTemp26)) + (0.0110347653f * fTemp33)) + (0.00877523795f * fTemp28)))) + (0.0552211851f * fTemp31)) - ((6.51809978e-06f * fTemp27) + (8.5388001e-06f * fTemp30))));
			float fTemp301 = (fConst114 * fRec604[1]);
			float fTemp302 = (fConst115 * fRec607[1]);
			fRec609[0] = (fTemp300 + (fTemp301 + (fRec609[1] + fTemp302)));
			fRec607[0] = fRec609[0];
			float fRec608 = ((fTemp302 + fTemp301) + fTemp300);
			fRec606[0] = (fRec607[0] + fRec606[1]);
			fRec604[0] = fRec606[0];
			float fRec605 = fRec608;
			fRec603[0] = (fTemp298 + (fTemp299 + (fRec605 + fRec603[1])));
			fRec601[0] = fRec603[0];
			float fRec602 = (fTemp298 + (fRec605 + fTemp299));
			fRec600[0] = (fRec601[0] + fRec600[1]);
			fRec598[0] = fRec600[0];
			float fRec599 = fRec602;
			float fTemp303 = (fConst117 * fRec610[1]);
			float fTemp304 = (fConst119 * (((0.0177717861f * fTemp17) + ((9.5419e-06f * fTemp16) + ((0.0421755761f * fTemp18) + (1.10768997e-05f * fTemp14)))) - ((0.01865343f * fTemp19) + ((0.0680814683f * fTemp15) + (4.70539999e-06f * fTemp13)))));
			float fTemp305 = (fConst120 * fRec613[1]);
			float fTemp306 = (fConst121 * fRec616[1]);
			fRec618[0] = (fTemp304 + (fTemp305 + (fRec618[1] + fTemp306)));
			fRec616[0] = fRec618[0];
			float fRec617 = ((fTemp306 + fTemp305) + fTemp304);
			fRec615[0] = (fRec616[0] + fRec615[1]);
			fRec613[0] = fRec615[0];
			float fRec614 = fRec617;
			fRec612[0] = (fTemp303 + (fRec614 + fRec612[1]));
			fRec610[0] = fRec612[0];
			float fRec611 = (fRec614 + fTemp303);
			float fTemp307 = (fConst123 * ((0.0827766433f * fTemp10) - ((0.0273921341f * fTemp8) + (7.02950001e-06f * fTemp9))));
			float fTemp308 = (fConst124 * fRec619[1]);
			fRec621[0] = (fTemp307 + (fRec621[1] + fTemp308));
			fRec619[0] = fRec621[0];
			float fRec620 = (fTemp308 + fTemp307);
			float fTemp309 = (fConst126 * ((7.08849984e-06f * fTemp40) - ((((1.05427998e-05f * fTemp38) + (0.0438432321f * fTemp41)) + (0.0262416285f * fTemp39)) + (0.0779453963f * fTemp42))));
			float fTemp310 = (fConst127 * fRec622[1]);
			float fTemp311 = (fConst128 * fRec625[1]);
			fRec627[0] = (fTemp309 + (fTemp310 + (fRec627[1] + fTemp311)));
			fRec625[0] = fRec627[0];
			float fRec626 = ((fTemp311 + fTemp310) + fTemp309);
			fRec624[0] = (fRec625[0] + fRec624[1]);
			fRec622[0] = fRec624[0];
			float fRec623 = fRec626;
			fVec19[0] = ((0.055423025f * fTemp7) + (fRec599 + (fRec611 + (fRec620 + fRec623))));
			output19[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec19[iConst129])));
			float fTemp312 = (fConst131 * fRec628[1]);
			float fTemp313 = (fConst133 * (((0.0224569999f * fTemp17) + ((0.0411787964f * fTemp16) + ((0.0524279959f * fTemp14) + ((0.00144205324f * fTemp13) + (0.0237775631f * fTemp18))))) - ((3.28360011e-06f * fTemp15) + (0.00249966024f * fTemp19))));
			float fTemp314 = (fConst134 * fRec631[1]);
			float fTemp315 = (fConst135 * fRec634[1]);
			fRec636[0] = (fTemp313 + (fTemp314 + (fRec636[1] + fTemp315)));
			fRec634[0] = fRec636[0];
			float fRec635 = ((fTemp315 + fTemp314) + fTemp313);
			fRec633[0] = (fRec634[0] + fRec633[1]);
			fRec631[0] = fRec633[0];
			float fRec632 = fRec635;
			fRec630[0] = (fTemp312 + (fRec632 + fRec630[1]));
			fRec628[0] = fRec630[0];
			float fRec629 = (fRec632 + fTemp312);
			float fTemp316 = (fConst137 * ((0.0242707804f * fTemp40) - ((((0.0511472188f * fTemp38) + (0.0420402549f * fTemp41)) + (0.0140019646f * fTemp39)) + (0.0295328442f * fTemp42))));
			float fTemp317 = (fConst138 * fRec637[1]);
			float fTemp318 = (fConst139 * fRec640[1]);
			fRec642[0] = (fTemp316 + (fTemp317 + (fRec642[1] + fTemp318)));
			fRec640[0] = fRec642[0];
			float fRec641 = ((fTemp318 + fTemp317) + fTemp316);
			fRec639[0] = (fRec640[0] + fRec639[1]);
			fRec637[0] = fRec639[0];
			float fRec638 = fRec641;
			float fTemp319 = (fConst141 * fRec643[1]);
			float fTemp320 = (fConst142 * fRec646[1]);
			float fTemp321 = (fConst144 * ((((0.038581226f * fTemp32) + (3.85090016e-06f * fTemp26)) + (0.0160285849f * fTemp33)) - ((0.0222711153f * fTemp31) + ((0.0424897224f * fTemp30) + ((0.00261885161f * fTemp34) + (((0.00453866227f * fTemp27) + (0.00121190224f * fTemp28)) + (0.00925284903f * fTemp29)))))));
			float fTemp322 = (fConst145 * fRec649[1]);
			float fTemp323 = (fConst146 * fRec652[1]);
			fRec654[0] = (fTemp321 + (fTemp322 + (fRec654[1] + fTemp323)));
			fRec652[0] = fRec654[0];
			float fRec653 = ((fTemp323 + fTemp322) + fTemp321);
			fRec651[0] = (fRec652[0] + fRec651[1]);
			fRec649[0] = fRec651[0];
			float fRec650 = fRec653;
			fRec648[0] = (fTemp319 + (fTemp320 + (fRec650 + fRec648[1])));
			fRec646[0] = fRec648[0];
			float fRec647 = (fTemp319 + (fRec650 + fTemp320));
			fRec645[0] = (fRec646[0] + fRec645[1]);
			fRec643[0] = fRec645[0];
			float fRec644 = fRec647;
			float fTemp324 = (fConst148 * ((0.0546924546f * fTemp10) - ((0.0296233296f * fTemp8) + (0.0315752216f * fTemp9))));
			float fTemp325 = (fConst149 * fRec655[1]);
			fRec657[0] = (fTemp324 + (fRec657[1] + fTemp325));
			fRec655[0] = fRec657[0];
			float fRec656 = (fTemp325 + fTemp324);
			output20[i] = FAUSTFLOAT((fRec0[0] * ((0.0434079953f * fTemp7) + (fRec629 + (fRec638 + (fRec644 + fRec656))))));
			float fTemp326 = (fConst131 * fRec658[1]);
			float fTemp327 = (fConst133 * (((0.0201204065f * fTemp17) + ((0.0381556787f * fTemp16) + ((0.0602632426f * fTemp15) + ((0.00823509041f * fTemp13) + (0.00822857302f * fTemp14))))) - ((0.00728519401f * fTemp19) + (0.0236924179f * fTemp18))));
			float fTemp328 = (fConst134 * fRec661[1]);
			float fTemp329 = (fConst135 * fRec664[1]);
			fRec666[0] = (fTemp327 + (fTemp328 + (fRec666[1] + fTemp329)));
			fRec664[0] = fRec666[0];
			float fRec665 = ((fTemp329 + fTemp328) + fTemp327);
			fRec663[0] = (fRec664[0] + fRec663[1]);
			fRec661[0] = fRec663[0];
			float fRec662 = fRec665;
			fRec660[0] = (fTemp326 + (fRec662 + fRec660[1]));
			fRec658[0] = fRec660[0];
			float fRec659 = (fRec662 + fTemp326);
			float fTemp330 = (fConst148 * ((0.0388471968f * fTemp10) - ((0.0285362061f * fTemp8) + (0.0618594699f * fTemp9))));
			float fTemp331 = (fConst149 * fRec667[1]);
			fRec669[0] = (fTemp330 + (fRec669[1] + fTemp331));
			fRec667[0] = fRec669[0];
			float fRec668 = (fTemp331 + fTemp330);
			float fTemp332 = (fConst137 * (((0.0403285064f * fTemp40) + (0.0293631945f * fTemp42)) - (((0.0621728152f * fTemp38) + (0.0226049349f * fTemp41)) + (0.0201213304f * fTemp39))));
			float fTemp333 = (fConst138 * fRec670[1]);
			float fTemp334 = (fConst139 * fRec673[1]);
			fRec675[0] = (fTemp332 + (fTemp333 + (fRec675[1] + fTemp334)));
			fRec673[0] = fRec675[0];
			float fRec674 = ((fTemp334 + fTemp333) + fTemp332);
			fRec672[0] = (fRec673[0] + fRec672[1]);
			fRec670[0] = fRec672[0];
			float fRec671 = fRec674;
			float fTemp335 = (fConst144 * (((0.0023590331f * fTemp30) + ((((0.00317050819f * fTemp27) + (0.00657171477f * fTemp33)) + (0.00380558404f * fTemp28)) + (0.00278478395f * fTemp34))) - ((((0.0381821245f * fTemp32) + (0.0399126671f * fTemp26)) + (0.0132724736f * fTemp29)) + (0.0336249284f * fTemp31))));
			float fTemp336 = (fConst145 * fRec682[1]);
			float fTemp337 = (fConst146 * fRec685[1]);
			fRec687[0] = (fTemp335 + (fTemp336 + (fRec687[1] + fTemp337)));
			fRec685[0] = fRec687[0];
			float fRec686 = ((fTemp337 + fTemp336) + fTemp335);
			fRec684[0] = (fRec684[1] + fRec685[0]);
			fRec682[0] = fRec684[0];
			float fRec683 = fRec686;
			float fTemp338 = (fConst141 * fRec676[1]);
			float fTemp339 = (fConst142 * fRec679[1]);
			fRec681[0] = (fRec683 + (fTemp338 + (fRec681[1] + fTemp339)));
			fRec679[0] = fRec681[0];
			float fRec680 = ((fTemp339 + fTemp338) + fRec683);
			fRec678[0] = (fRec678[1] + fRec679[0]);
			fRec676[0] = fRec678[0];
			float fRec677 = fRec680;
			output21[i] = FAUSTFLOAT((fRec0[0] * ((0.0494420342f * fTemp7) + ((fRec659 + (fRec668 + fRec671)) + fRec677))));
			float fTemp340 = (fConst110 * fRec688[1]);
			float fTemp341 = (fConst111 * fRec691[1]);
			float fTemp342 = (fConst113 * (((0.0552473217f * fTemp31) + ((((4.30930004e-06f * fTemp27) + (1.86939997e-06f * fTemp33)) + (0.0087676188f * fTemp28)) + (0.0372998863f * fTemp30))) - ((0.00793900527f * fTemp34) + (((5.86119995e-06f * fTemp32) + (2.54969996e-06f * fTemp26)) + (0.0110239927f * fTemp29)))));
			float fTemp343 = (fConst114 * fRec694[1]);
			float fTemp344 = (fConst115 * fRec697[1]);
			fRec699[0] = (fTemp342 + (fTemp343 + (fRec699[1] + fTemp344)));
			fRec697[0] = fRec699[0];
			float fRec698 = ((fTemp344 + fTemp343) + fTemp342);
			fRec696[0] = (fRec697[0] + fRec696[1]);
			fRec694[0] = fRec696[0];
			float fRec695 = fRec698;
			fRec693[0] = (fTemp340 + (fTemp341 + (fRec695 + fRec693[1])));
			fRec691[0] = fRec693[0];
			float fRec692 = (fTemp340 + (fRec695 + fTemp341));
			fRec690[0] = (fRec691[0] + fRec690[1]);
			fRec688[0] = fRec690[0];
			float fRec689 = fRec692;
			float fTemp345 = (fConst117 * fRec700[1]);
			float fTemp346 = (fConst119 * (((0.0177688692f * fTemp17) + ((1.38580003e-06f * fTemp16) + ((6.41730003e-06f * fTemp15) + (0.0186544098f * fTemp13)))) - ((3.25449992e-06f * fTemp19) + ((0.0421651751f * fTemp18) + (0.0680964366f * fTemp14)))));
			float fTemp347 = (fConst120 * fRec703[1]);
			float fTemp348 = (fConst121 * fRec706[1]);
			fRec708[0] = (fTemp346 + (fTemp347 + (fRec708[1] + fTemp348)));
			fRec706[0] = fRec708[0];
			float fRec707 = ((fTemp348 + fTemp347) + fTemp346);
			fRec705[0] = (fRec706[0] + fRec705[1]);
			fRec703[0] = fRec705[0];
			float fRec704 = fRec707;
			fRec702[0] = (fTemp345 + (fRec704 + fRec702[1]));
			fRec700[0] = fRec702[0];
			float fRec701 = (fRec704 + fTemp345);
			float fTemp349 = (fConst123 * ((2.4369001e-06f * fTemp10) - ((0.0273813531f * fTemp8) + (0.082773298f * fTemp9))));
			float fTemp350 = (fConst124 * fRec709[1]);
			fRec711[0] = (fTemp349 + (fRec711[1] + fTemp350));
			fRec709[0] = fRec711[0];
			float fRec710 = (fTemp350 + fTemp349);
			float fTemp351 = (fConst126 * (((0.0438312702f * fTemp40) + (0.0779497474f * fTemp42)) - (((5.10100017e-06f * fTemp38) + (1.33499995e-07f * fTemp41)) + (0.026245553f * fTemp39))));
			float fTemp352 = (fConst127 * fRec712[1]);
			float fTemp353 = (fConst128 * fRec715[1]);
			fRec717[0] = (fTemp351 + (fTemp352 + (fRec717[1] + fTemp353)));
			fRec715[0] = fRec717[0];
			float fRec716 = ((fTemp353 + fTemp352) + fTemp351);
			fRec714[0] = (fRec715[0] + fRec714[1]);
			fRec712[0] = fRec714[0];
			float fRec713 = fRec716;
			fVec20[0] = ((0.0554167405f * fTemp7) + (fRec689 + (fRec701 + (fRec710 + fRec713))));
			output22[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec20[iConst129])));
			float fTemp354 = (fConst141 * fRec718[1]);
			float fTemp355 = (fConst142 * fRec721[1]);
			float fTemp356 = (fConst144 * (((0.00236623175f * fTemp30) + ((((0.0382035039f * fTemp32) + (0.0398953296f * fTemp26)) + (0.00378691917f * fTemp28)) + (0.0027830156f * fTemp34))) - ((((0.00318525918f * fTemp27) + (0.00657201698f * fTemp33)) + (0.0132616162f * fTemp29)) + (0.0336430371f * fTemp31))));
			float fTemp357 = (fConst145 * fRec724[1]);
			float fTemp358 = (fConst146 * fRec727[1]);
			fRec729[0] = (fTemp356 + (fTemp357 + (fRec729[1] + fTemp358)));
			fRec727[0] = fRec729[0];
			float fRec728 = ((fTemp358 + fTemp357) + fTemp356);
			fRec726[0] = (fRec727[0] + fRec726[1]);
			fRec724[0] = fRec726[0];
			float fRec725 = fRec728;
			fRec723[0] = (fTemp354 + (fTemp355 + (fRec725 + fRec723[1])));
			fRec721[0] = fRec723[0];
			float fRec722 = (fTemp354 + (fRec725 + fTemp355));
			fRec720[0] = (fRec721[0] + fRec720[1]);
			fRec718[0] = fRec720[0];
			float fRec719 = fRec722;
			float fTemp359 = (fConst148 * (0.0f - (((0.0388576426f * fTemp10) + (0.0285068844f * fTemp8)) + (0.0618772283f * fTemp9))));
			float fTemp360 = (fConst149 * fRec730[1]);
			fRec732[0] = (fTemp359 + (fRec732[1] + fTemp360));
			fRec730[0] = fRec732[0];
			float fRec731 = (fTemp360 + fTemp359);
			float fTemp361 = (fConst137 * (((((0.0621965267f * fTemp38) + (0.0225855913f * fTemp41)) + (0.0403064527f * fTemp40)) + (0.0293740258f * fTemp42)) - (0.0201558322f * fTemp39)));
			float fTemp362 = (fConst138 * fRec733[1]);
			float fTemp363 = (fConst139 * fRec736[1]);
			fRec738[0] = (fTemp361 + (fTemp362 + (fRec738[1] + fTemp363)));
			fRec736[0] = fRec738[0];
			float fRec737 = ((fTemp363 + fTemp362) + fTemp361);
			fRec735[0] = (fRec735[1] + fRec736[0]);
			fRec733[0] = fRec735[0];
			float fRec734 = fRec737;
			float fTemp364 = (fConst131 * fRec739[1]);
			float fTemp365 = (fConst133 * (((0.020125458f * fTemp17) + ((0.00730328076f * fTemp19) + ((0.00825409591f * fTemp13) + (0.00823290274f * fTemp14)))) - ((0.0381317139f * fTemp16) + ((0.0602913089f * fTemp15) + (0.0236870889f * fTemp18)))));
			float fTemp366 = (fConst134 * fRec742[1]);
			float fTemp367 = (fConst135 * fRec745[1]);
			fRec747[0] = (fTemp365 + (fTemp366 + (fRec747[1] + fTemp367)));
			fRec745[0] = fRec747[0];
			float fRec746 = ((fTemp367 + fTemp366) + fTemp365);
			fRec744[0] = (fRec744[1] + fRec745[0]);
			fRec742[0] = fRec744[0];
			float fRec743 = fRec746;
			fRec741[0] = ((fRec741[1] + fTemp364) + fRec743);
			fRec739[0] = fRec741[0];
			float fRec740 = (fTemp364 + fRec743);
			output23[i] = FAUSTFLOAT((fRec0[0] * ((((fRec719 + (0.0494453609f * fTemp7)) + fRec731) + fRec734) + fRec740)));
			float fTemp368 = (fConst137 * ((((0.0511424206f * fTemp38) + (0.0420513675f * fTemp41)) + (0.0242786594f * fTemp40)) - ((0.0139776776f * fTemp39) + (0.0295330938f * fTemp42))));
			float fTemp369 = (fConst138 * fRec748[1]);
			float fTemp370 = (fConst139 * fRec751[1]);
			fRec753[0] = (fTemp368 + (fTemp369 + (fRec753[1] + fTemp370)));
			fRec751[0] = fRec753[0];
			float fRec752 = ((fTemp370 + fTemp369) + fTemp368);
			fRec750[0] = (fRec751[0] + fRec750[1]);
			fRec748[0] = fRec750[0];
			float fRec749 = fRec752;
			float fTemp371 = (fConst148 * (0.0f - (((0.0546927527f * fTemp10) + (0.0296443459f * fTemp8)) + (0.0315747596f * fTemp9))));
			float fTemp372 = (fConst149 * fRec754[1]);
			fRec756[0] = (fTemp371 + (fRec756[1] + fTemp372));
			fRec754[0] = fRec756[0];
			float fRec755 = (fTemp372 + fTemp371);
			float fTemp373 = (fConst131 * fRec757[1]);
			float fTemp374 = (fConst133 * (((0.0224375706f * fTemp17) + ((0.00248825992f * fTemp19) + ((7.98969995e-06f * fTemp15) + ((0.0524208769f * fTemp14) + ((0.00143079273f * fTemp13) + (0.0237827767f * fTemp18)))))) - (0.0411872156f * fTemp16)));
			float fTemp375 = (fConst134 * fRec760[1]);
			float fTemp376 = (fConst135 * fRec763[1]);
			fRec765[0] = (fTemp374 + (fTemp375 + (fRec765[1] + fTemp376)));
			fRec763[0] = fRec765[0];
			float fRec764 = ((fTemp376 + fTemp375) + fTemp374);
			fRec762[0] = (fRec763[0] + fRec762[1]);
			fRec760[0] = fRec762[0];
			float fRec761 = fRec764;
			fRec759[0] = (fTemp373 + (fRec761 + fRec759[1]));
			fRec757[0] = fRec759[0];
			float fRec758 = (fRec761 + fTemp373);
			float fTemp377 = (fConst141 * fRec766[1]);
			float fTemp378 = (fConst142 * fRec769[1]);
			float fTemp379 = (fConst144 * ((0.00454968074f * fTemp27) - ((0.0222604889f * fTemp31) + ((0.0424979739f * fTemp30) + ((0.00262012612f * fTemp34) + (((((0.0385759771f * fTemp32) + (6.03140006e-06f * fTemp26)) + (0.0160222035f * fTemp33)) + (0.00118849357f * fTemp28)) + (0.00924302265f * fTemp29)))))));
			float fTemp380 = (fConst145 * fRec772[1]);
			float fTemp381 = (fConst146 * fRec775[1]);
			fRec777[0] = (fTemp379 + (fTemp380 + (fRec777[1] + fTemp381)));
			fRec775[0] = fRec777[0];
			float fRec776 = ((fTemp381 + fTemp380) + fTemp379);
			fRec774[0] = (fRec775[0] + fRec774[1]);
			fRec772[0] = fRec774[0];
			float fRec773 = fRec776;
			fRec771[0] = (fTemp377 + (fTemp378 + (fRec773 + fRec771[1])));
			fRec769[0] = fRec771[0];
			float fRec770 = (fTemp377 + (fRec773 + fTemp378));
			fRec768[0] = (fRec769[0] + fRec768[1]);
			fRec766[0] = fRec768[0];
			float fRec767 = fRec770;
			output24[i] = FAUSTFLOAT((fRec0[0] * ((0.0434169397f * fTemp7) + (fRec749 + (fRec755 + (fRec758 + fRec767))))));
			float fTemp382 = (fConst110 * fRec778[1]);
			float fTemp383 = (fConst111 * fRec781[1]);
			float fTemp384 = (fConst113 * (((0.0552432239f * fTemp31) + ((1.08373997e-05f * fTemp30) + ((0.00794486888f * fTemp34) + ((0.00876272563f * fTemp28) + (6.72009992e-06f * fTemp29))))) - ((((9.79529977e-06f * fTemp32) + (0.037310347f * fTemp26)) + (2.56399994e-06f * fTemp27)) + (0.0110511202f * fTemp33))));
			float fTemp385 = (fConst114 * fRec784[1]);
			float fTemp386 = (fConst115 * fRec787[1]);
			fRec789[0] = (fTemp384 + (fTemp385 + (fRec789[1] + fTemp386)));
			fRec787[0] = fRec789[0];
			float fRec788 = ((fTemp386 + fTemp385) + fTemp384);
			fRec786[0] = (fRec787[0] + fRec786[1]);
			fRec784[0] = fRec786[0];
			float fRec785 = fRec788;
			fRec783[0] = (fTemp382 + (fTemp383 + (fRec785 + fRec783[1])));
			fRec781[0] = fRec783[0];
			float fRec782 = (fTemp382 + (fRec785 + fTemp383));
			fRec780[0] = (fRec781[0] + fRec780[1]);
			fRec778[0] = fRec780[0];
			float fRec779 = fRec782;
			float fTemp387 = (fConst117 * fRec790[1]);
			float fTemp388 = (fConst119 * ((0.0177974422f * fTemp17) + ((0.018660184f * fTemp19) + ((7.35240019e-06f * fTemp16) + ((0.0680911094f * fTemp15) + ((1.19404003e-05f * fTemp14) + ((1.59659999e-06f * fTemp13) + (0.0421827324f * fTemp18))))))));
			float fTemp389 = (fConst120 * fRec793[1]);
			float fTemp390 = (fConst121 * fRec796[1]);
			fRec798[0] = (fTemp388 + (fTemp389 + (fRec798[1] + fTemp390)));
			fRec796[0] = fRec798[0];
			float fRec797 = ((fTemp390 + fTemp389) + fTemp388);
			fRec795[0] = (fRec796[0] + fRec795[1]);
			fRec793[0] = fRec795[0];
			float fRec794 = fRec797;
			fRec792[0] = (fTemp387 + (fRec794 + fRec792[1]));
			fRec790[0] = fRec792[0];
			float fRec791 = (fRec794 + fTemp387);
			float fTemp391 = (fConst123 * (0.0f - (((0.0827651322f * fTemp10) + (0.0273843631f * fTemp8)) + (5.92059996e-06f * fTemp9))));
			float fTemp392 = (fConst124 * fRec799[1]);
			fRec801[0] = (fTemp391 + (fRec801[1] + fTemp392));
			fRec799[0] = fRec801[0];
			float fRec800 = (fTemp392 + fTemp391);
			float fTemp393 = (fConst126 * (((1.03394996e-05f * fTemp38) + (0.0438491851f * fTemp41)) - (((0.026253948f * fTemp39) + (2.64510004e-06f * fTemp40)) + (0.0779431164f * fTemp42))));
			float fTemp394 = (fConst127 * fRec802[1]);
			float fTemp395 = (fConst128 * fRec805[1]);
			fRec807[0] = (fTemp393 + (fTemp394 + (fRec807[1] + fTemp395)));
			fRec805[0] = fRec807[0];
			float fRec806 = ((fTemp395 + fTemp394) + fTemp393);
			fRec804[0] = (fRec805[0] + fRec804[1]);
			fRec802[0] = fRec804[0];
			float fRec803 = fRec806;
			fVec21[0] = ((0.0554073155f * fTemp7) + (fRec779 + (fRec791 + (fRec800 + fRec803))));
			output25[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec21[iConst129])));
			float fTemp396 = (fConst141 * fRec808[1]);
			float fTemp397 = (fConst142 * fRec811[1]);
			float fTemp398 = (fConst144 * ((((0.0065592546f * fTemp29) + (((0.0381933525f * fTemp32) + (0.00318307895f * fTemp27)) + (0.0038134451f * fTemp28))) + (0.0398955271f * fTemp30)) - ((((0.00237736199f * fTemp26) + (0.0132488534f * fTemp33)) + (0.00278579164f * fTemp34)) + (0.0336444937f * fTemp31))));
			float fTemp399 = (fConst145 * fRec814[1]);
			float fTemp400 = (fConst146 * fRec817[1]);
			fRec819[0] = (fTemp398 + (fTemp399 + (fRec819[1] + fTemp400)));
			fRec817[0] = fRec819[0];
			float fRec818 = ((fTemp400 + fTemp399) + fTemp398);
			fRec816[0] = (fRec817[0] + fRec816[1]);
			fRec814[0] = fRec816[0];
			float fRec815 = fRec818;
			fRec813[0] = (fTemp396 + (fTemp397 + (fRec815 + fRec813[1])));
			fRec811[0] = fRec813[0];
			float fRec812 = (fTemp396 + (fRec815 + fTemp397));
			fRec810[0] = (fRec811[0] + fRec810[1]);
			fRec808[0] = fRec810[0];
			float fRec809 = fRec812;
			float fTemp401 = (fConst131 * fRec820[1]);
			float fTemp402 = (fConst133 * (((0.0200931113f * fTemp17) + ((0.0082456246f * fTemp19) + ((0.038121108f * fTemp16) + (0.0236905683f * fTemp18)))) - ((0.0082371328f * fTemp15) + ((0.00729924766f * fTemp13) + (0.060279958f * fTemp14)))));
			float fTemp403 = (fConst134 * fRec823[1]);
			float fTemp404 = (fConst135 * fRec826[1]);
			fRec828[0] = (fTemp402 + (fTemp403 + (fRec828[1] + fTemp404)));
			fRec826[0] = fRec828[0];
			float fRec827 = ((fTemp404 + fTemp403) + fTemp402);
			fRec825[0] = (fRec826[0] + fRec825[1]);
			fRec823[0] = fRec825[0];
			float fRec824 = fRec827;
			fRec822[0] = (fTemp401 + (fRec824 + fRec822[1]));
			fRec820[0] = fRec822[0];
			float fRec821 = (fRec824 + fTemp401);
			float fTemp405 = (fConst148 * ((0.0388509668f * fTemp9) - ((0.0618637428f * fTemp10) + (0.0285150502f * fTemp8))));
			float fTemp406 = (fConst149 * fRec829[1]);
			fRec831[0] = (fTemp405 + (fRec831[1] + fTemp406));
			fRec829[0] = fRec831[0];
			float fRec830 = (fTemp406 + fTemp405);
			float fTemp407 = (fConst137 * ((0.0403008051f * fTemp41) - ((((0.0621853285f * fTemp38) + (0.0201301072f * fTemp39)) + (0.0225778446f * fTemp40)) + (0.0293648373f * fTemp42))));
			float fTemp408 = (fConst138 * fRec832[1]);
			float fTemp409 = (fConst139 * fRec835[1]);
			fRec837[0] = (fTemp407 + (fTemp408 + (fRec837[1] + fTemp409)));
			fRec835[0] = fRec837[0];
			float fRec836 = ((fTemp409 + fTemp408) + fTemp407);
			fRec834[0] = (fRec835[0] + fRec834[1]);
			fRec832[0] = fRec834[0];
			float fRec833 = fRec836;
			output26[i] = FAUSTFLOAT((fRec0[0] * ((0.0494432151f * fTemp7) + (fRec809 + (fRec821 + (fRec830 + fRec833))))));
			float fTemp410 = (fConst141 * fRec838[1]);
			float fTemp411 = (fConst142 * fRec841[1]);
			float fTemp412 = (fConst144 * (((0.00261830073f * fTemp34) + ((0.0425120033f * fTemp26) + (0.0160307381f * fTemp29))) - ((0.0222494248f * fTemp31) + (((((0.038573917f * fTemp32) + (0.00454684673f * fTemp27)) + (0.00925542228f * fTemp33)) + (0.00119578338f * fTemp28)) + (6.1623e-06f * fTemp30)))));
			float fTemp413 = (fConst145 * fRec844[1]);
			float fTemp414 = (fConst146 * fRec847[1]);
			fRec849[0] = (fTemp412 + (fTemp413 + (fRec849[1] + fTemp414)));
			fRec847[0] = fRec849[0];
			float fRec848 = ((fTemp414 + fTemp413) + fTemp412);
			fRec846[0] = (fRec847[0] + fRec846[1]);
			fRec844[0] = fRec846[0];
			float fRec845 = fRec848;
			fRec843[0] = (fTemp410 + (fTemp411 + (fRec845 + fRec843[1])));
			fRec841[0] = fRec843[0];
			float fRec842 = (fTemp410 + (fRec845 + fTemp411));
			fRec840[0] = (fRec841[0] + fRec840[1]);
			fRec838[0] = fRec840[0];
			float fRec839 = fRec842;
			float fTemp415 = (fConst131 * fRec850[1]);
			float fTemp416 = (fConst133 * (((0.0224596411f * fTemp17) + ((0.00143468869f * fTemp19) + ((0.0412002578f * fTemp16) + (1.64367993e-05f * fTemp14)))) - ((0.0524200052f * fTemp15) + ((0.00248971907f * fTemp13) + (0.0237927921f * fTemp18)))));
			float fTemp417 = (fConst134 * fRec853[1]);
			float fTemp418 = (fConst135 * fRec856[1]);
			fRec858[0] = (fTemp416 + (fTemp417 + (fRec858[1] + fTemp418)));
			fRec856[0] = fRec858[0];
			float fRec857 = ((fTemp418 + fTemp417) + fTemp416);
			fRec855[0] = (fRec856[0] + fRec855[1]);
			fRec853[0] = fRec855[0];
			float fRec854 = fRec857;
			fRec852[0] = (fTemp415 + (fRec854 + fRec852[1]));
			fRec850[0] = fRec852[0];
			float fRec851 = (fRec854 + fTemp415);
			float fTemp419 = (fConst148 * ((0.0547024459f * fTemp9) - ((0.0315749347f * fTemp10) + (0.0296466146f * fTemp8))));
			float fTemp420 = (fConst149 * fRec859[1]);
			fRec861[0] = (fTemp419 + (fRec861[1] + fTemp420));
			fRec859[0] = fRec861[0];
			float fRec860 = (fTemp420 + fTemp419);
			float fTemp421 = (fConst137 * (((0.0242839344f * fTemp41) + (0.0295415837f * fTemp42)) - (((0.0511430465f * fTemp38) + (0.0139899021f * fTemp39)) + (0.0420673341f * fTemp40))));
			float fTemp422 = (fConst138 * fRec862[1]);
			float fTemp423 = (fConst139 * fRec865[1]);
			fRec867[0] = (fTemp421 + (fTemp422 + (fRec867[1] + fTemp423)));
			fRec865[0] = fRec867[0];
			float fRec866 = ((fTemp423 + fTemp422) + fTemp421);
			fRec864[0] = (fRec865[0] + fRec864[1]);
			fRec862[0] = fRec864[0];
			float fRec863 = fRec866;
			output27[i] = FAUSTFLOAT((fRec0[0] * ((0.0434193648f * fTemp7) + (fRec839 + (fRec851 + (fRec860 + fRec863))))));
			float fTemp424 = (fConst110 * fRec868[1]);
			float fTemp425 = (fConst111 * fRec871[1]);
			float fTemp426 = (fConst113 * ((((0.0110338274f * fTemp29) + (((3.54000008e-06f * fTemp27) + (2.90100002e-06f * fTemp33)) + (0.00876111165f * fTemp28))) + (0.0552419834f * fTemp31)) - ((0.0373006202f * fTemp30) + (((8.86260023e-06f * fTemp32) + (1.70736002e-05f * fTemp26)) + (0.0079470966f * fTemp34)))));
			float fTemp427 = (fConst114 * fRec874[1]);
			float fTemp428 = (fConst115 * fRec877[1]);
			fRec879[0] = (fTemp426 + (fTemp427 + (fRec879[1] + fTemp428)));
			fRec877[0] = fRec879[0];
			float fRec878 = ((fTemp428 + fTemp427) + fTemp426);
			fRec876[0] = (fRec877[0] + fRec876[1]);
			fRec874[0] = fRec876[0];
			float fRec875 = fRec878;
			fRec873[0] = (fTemp424 + (fTemp425 + (fRec875 + fRec873[1])));
			fRec871[0] = fRec873[0];
			float fRec872 = (fTemp424 + (fRec875 + fTemp425));
			fRec870[0] = (fRec871[0] + fRec870[1]);
			fRec868[0] = fRec870[0];
			float fRec869 = fRec872;
			float fTemp429 = (fConst117 * fRec880[1]);
			float fTemp430 = (fConst119 * (((0.0177803766f * fTemp17) + ((2.98569989e-06f * fTemp19) + (0.0680936277f * fTemp14))) - ((1.33274998e-05f * fTemp16) + ((7.58089982e-06f * fTemp15) + ((0.0186686739f * fTemp13) + (0.0421624407f * fTemp18))))));
			float fTemp431 = (fConst120 * fRec883[1]);
			float fTemp432 = (fConst121 * fRec886[1]);
			fRec888[0] = (fTemp430 + (fTemp431 + (fRec888[1] + fTemp432)));
			fRec886[0] = fRec888[0];
			float fRec887 = ((fTemp432 + fTemp431) + fTemp430);
			fRec885[0] = (fRec886[0] + fRec885[1]);
			fRec883[0] = fRec885[0];
			float fRec884 = fRec887;
			fRec882[0] = (fTemp429 + (fRec884 + fRec882[1]));
			fRec880[0] = fRec882[0];
			float fRec881 = (fRec884 + fTemp429);
			float fTemp433 = (fConst123 * ((0.0827706978f * fTemp9) - ((2.25269991e-06f * fTemp10) + (0.0273699835f * fTemp8))));
			float fTemp434 = (fConst124 * fRec889[1]);
			fRec891[0] = (fTemp433 + (fRec891[1] + fTemp434));
			fRec889[0] = fRec891[0];
			float fRec890 = (fTemp434 + fTemp433);
			float fTemp435 = (fConst126 * ((0.0779483095f * fTemp42) - ((((5.11959979e-06f * fTemp38) + (6.8147001e-06f * fTemp41)) + (0.0262604319f * fTemp39)) + (0.0438237339f * fTemp40))));
			float fTemp436 = (fConst127 * fRec892[1]);
			float fTemp437 = (fConst128 * fRec895[1]);
			fRec897[0] = (fTemp435 + (fTemp436 + (fRec897[1] + fTemp437)));
			fRec895[0] = fRec897[0];
			float fRec896 = ((fTemp437 + fTemp436) + fTemp435);
			fRec894[0] = (fRec895[0] + fRec894[1]);
			fRec892[0] = fRec894[0];
			float fRec893 = fRec896;
			fVec22[0] = ((0.0554104783f * fTemp7) + (fRec869 + (fRec881 + (fRec890 + fRec893))));
			output28[i] = FAUSTFLOAT((0.999779761f * (fRec0[0] * fVec22[iConst129])));
			float fTemp438 = (fConst141 * fRec898[1]);
			float fTemp439 = (fConst142 * fRec901[1]);
			float fTemp440 = (fConst144 * (((0.00260817772f * fTemp34) + ((((0.0385962762f * fTemp32) + (0.0045394185f * fTemp27)) + (0.00924376585f * fTemp33)) + (0.0160133466f * fTemp29))) - ((0.0222431235f * fTemp31) + (((0.0424894206f * fTemp26) + (0.00117091753f * fTemp28)) + (1.13399001e-05f * fTemp30)))));
			float fTemp441 = (fConst145 * fRec904[1]);
			float fTemp442 = (fConst146 * fRec907[1]);
			fRec909[0] = (fTemp440 + (fTemp441 + (fRec909[1] + fTemp442)));
			fRec907[0] = fRec909[0];
			float fRec908 = ((fTemp442 + fTemp441) + fTemp440);
			fRec906[0] = (fRec907[0] + fRec906[1]);
			fRec904[0] = fRec906[0];
			float fRec905 = fRec908;
			fRec903[0] = (fTemp438 + (fTemp439 + (fRec905 + fRec903[1])));
			fRec901[0] = fRec903[0];
			float fRec902 = (fTemp438 + (fRec905 + fTemp439));
			fRec900[0] = (fRec901[0] + fRec900[1]);
			fRec898[0] = fRec900[0];
			float fRec899 = fRec902;
			float fTemp443 = (fConst131 * fRec910[1]);
			float fTemp444 = (fConst133 * (((0.0224289857f * fTemp17) + ((0.0524359122f * fTemp15) + (3.17443009e-05f * fTemp14))) - ((0.00143745751f * fTemp19) + ((0.0411784872f * fTemp16) + ((0.00250173663f * fTemp13) + (0.023784969f * fTemp18))))));
			float fTemp445 = (fConst134 * fRec913[1]);
			float fTemp446 = (fConst135 * fRec916[1]);
			fRec918[0] = (fTemp444 + (fTemp445 + (fRec918[1] + fTemp446)));
			fRec916[0] = fRec918[0];
			float fRec917 = ((fTemp446 + fTemp445) + fTemp444);
			fRec915[0] = (fRec916[0] + fRec915[1]);
			fRec913[0] = fRec915[0];
			float fRec914 = fRec917;
			fRec912[0] = (fTemp443 + (fRec914 + fRec912[1]));
			fRec910[0] = fRec912[0];
			float fRec911 = (fRec914 + fTemp443);
			float fTemp447 = (fConst148 * (((0.0315765515f * fTemp10) + (0.0547167361f * fTemp9)) - (0.0296458863f * fTemp8)));
			float fTemp448 = (fConst149 * fRec919[1]);
			fRec921[0] = (fTemp447 + (fRec921[1] + fTemp448));
			fRec919[0] = fRec921[0];
			float fRec920 = (fTemp448 + fTemp447);
			float fTemp449 = (fConst137 * (((0.0511500239f * fTemp38) + (0.0295588356f * fTemp42)) - (((0.0242726207f * fTemp41) + (0.0139865186f * fTemp39)) + (0.0420513824f * fTemp40))));
			float fTemp450 = (fConst138 * fRec922[1]);
			float fTemp451 = (fConst139 * fRec925[1]);
			fRec927[0] = (fTemp449 + (fTemp450 + (fRec927[1] + fTemp451)));
			fRec925[0] = fRec927[0];
			float fRec926 = ((fTemp451 + fTemp450) + fTemp449);
			fRec924[0] = (fRec925[0] + fRec924[1]);
			fRec922[0] = fRec924[0];
			float fRec923 = fRec926;
			output29[i] = FAUSTFLOAT((fRec0[0] * ((0.0434320159f * fTemp7) + (fRec899 + (fRec911 + (fRec920 + fRec923))))));
			float fTemp452 = (fConst141 * fRec928[1]);
			float fTemp453 = (fConst142 * fRec931[1]);
			float fTemp454 = (fConst144 * ((((0.00657470152f * fTemp29) + (((0.00235785451f * fTemp26) + (0.0132580223f * fTemp33)) + (0.00378664234f * fTemp28))) + (0.039906919f * fTemp30)) - ((((0.0381918028f * fTemp32) + (0.00317476736f * fTemp27)) + (0.00279365201f * fTemp34)) + (0.0336412415f * fTemp31))));
			float fTemp455 = (fConst145 * fRec934[1]);
			float fTemp456 = (fConst146 * fRec937[1]);
			fRec939[0] = (fTemp454 + (fTemp455 + (fRec939[1] + fTemp456)));
			fRec937[0] = fRec939[0];
			float fRec938 = ((fTemp456 + fTemp455) + fTemp454);
			fRec936[0] = (fRec937[0] + fRec936[1]);
			fRec934[0] = fRec936[0];
			float fRec935 = fRec938;
			fRec933[0] = (fTemp452 + (fTemp453 + (fRec935 + fRec933[1])));
			fRec931[0] = fRec933[0];
			float fRec932 = (fTemp452 + (fRec935 + fTemp453));
			fRec930[0] = (fRec931[0] + fRec930[1]);
			fRec928[0] = fRec930[0];
			float fRec929 = fRec932;
			float fTemp457 = (fConst131 * fRec940[1]);
			float fTemp458 = (fConst133 * (((0.0201231521f * fTemp17) + ((0.00823547225f * fTemp15) + (0.0236865543f * fTemp18))) - ((0.00823130645f * fTemp19) + ((0.0381439999f * fTemp16) + ((0.00729331141f * fTemp13) + (0.0602695309f * fTemp14))))));
			float fTemp459 = (fConst134 * fRec943[1]);
			float fTemp460 = (fConst135 * fRec946[1]);
			fRec948[0] = (fTemp458 + (fTemp459 + (fRec948[1] + fTemp460)));
			fRec946[0] = fRec948[0];
			float fRec947 = ((fTemp460 + fTemp459) + fTemp458);
			fRec945[0] = (fRec946[0] + fRec945[1]);
			fRec943[0] = fRec945[0];
			float fRec944 = fRec947;
			fRec942[0] = (fTemp457 + (fRec944 + fRec942[1]));
			fRec940[0] = fRec942[0];
			float fRec941 = (fRec944 + fTemp457);
			float fTemp461 = (fConst148 * (((0.0618486144f * fTemp10) + (0.0388411246f * fTemp9)) - (0.028516816f * fTemp8)));
			float fTemp462 = (fConst149 * fRec949[1]);
			fRec951[0] = (fTemp461 + (fRec951[1] + fTemp462));
			fRec949[0] = fRec951[0];
			float fRec950 = (fTemp462 + fTemp461);
			float fTemp463 = (fConst137 * ((0.0621710345f * fTemp38) - ((((0.0403146707f * fTemp41) + (0.0201298986f * fTemp39)) + (0.022593135f * fTemp40)) + (0.0293579157f * fTemp42))));
			float fTemp464 = (fConst138 * fRec952[1]);
			float fTemp465 = (fConst139 * fRec955[1]);
			fRec957[0] = (fTemp463 + (fTemp464 + (fRec957[1] + fTemp465)));
			fRec955[0] = fRec957[0];
			float fRec956 = ((fTemp465 + fTemp464) + fTemp463);
			fRec954[0] = (fRec955[0] + fRec954[1]);
			fRec952[0] = fRec954[0];
			float fRec953 = fRec956;
			output30[i] = FAUSTFLOAT((fRec0[0] * (fRec929 + (fRec941 + ((0.0494268872f * fTemp7) + (fRec950 + fRec953))))));
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			fRec7[2] = fRec7[1];
			fRec7[1] = fRec7[0];
			fRec8[2] = fRec8[1];
			fRec8[1] = fRec8[0];
			fRec9[2] = fRec9[1];
			fRec9[1] = fRec9[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			fRec20[2] = fRec20[1];
			fRec20[1] = fRec20[0];
			fRec21[2] = fRec21[1];
			fRec21[1] = fRec21[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec18[1] = fRec18[0];
			fRec16[1] = fRec16[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec38[2] = fRec38[1];
			fRec38[1] = fRec38[0];
			fRec39[2] = fRec39[1];
			fRec39[1] = fRec39[0];
			fRec40[2] = fRec40[1];
			fRec40[1] = fRec40[0];
			fRec41[2] = fRec41[1];
			fRec41[1] = fRec41[0];
			fRec42[2] = fRec42[1];
			fRec42[1] = fRec42[0];
			fRec43[2] = fRec43[1];
			fRec43[1] = fRec43[0];
			fRec44[2] = fRec44[1];
			fRec44[1] = fRec44[0];
			fRec45[2] = fRec45[1];
			fRec45[1] = fRec45[0];
			fRec46[2] = fRec46[1];
			fRec46[1] = fRec46[0];
			fRec37[1] = fRec37[0];
			fRec35[1] = fRec35[0];
			fRec34[1] = fRec34[0];
			fRec32[1] = fRec32[0];
			fRec31[1] = fRec31[0];
			fRec29[1] = fRec29[0];
			fRec28[1] = fRec28[0];
			fRec26[1] = fRec26[0];
			fRec53[2] = fRec53[1];
			fRec53[1] = fRec53[0];
			fRec54[2] = fRec54[1];
			fRec54[1] = fRec54[0];
			fRec55[2] = fRec55[1];
			fRec55[1] = fRec55[0];
			fRec56[2] = fRec56[1];
			fRec56[1] = fRec56[0];
			fRec57[2] = fRec57[1];
			fRec57[1] = fRec57[0];
			fRec52[1] = fRec52[0];
			fRec50[1] = fRec50[0];
			fRec49[1] = fRec49[0];
			fRec47[1] = fRec47[0];
			IOTA = (IOTA + 1);
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec66[1] = fRec66[0];
			fRec64[1] = fRec64[0];
			fRec63[1] = fRec63[0];
			fRec61[1] = fRec61[0];
			fRec60[1] = fRec60[0];
			fRec58[1] = fRec58[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			fRec72[1] = fRec72[0];
			fRec70[1] = fRec70[0];
			fRec81[1] = fRec81[0];
			fRec79[1] = fRec79[0];
			fRec87[1] = fRec87[0];
			fRec85[1] = fRec85[0];
			fRec84[1] = fRec84[0];
			fRec82[1] = fRec82[0];
			fRec99[1] = fRec99[0];
			fRec97[1] = fRec97[0];
			fRec96[1] = fRec96[0];
			fRec94[1] = fRec94[0];
			fRec93[1] = fRec93[0];
			fRec91[1] = fRec91[0];
			fRec90[1] = fRec90[0];
			fRec88[1] = fRec88[0];
			fRec108[1] = fRec108[0];
			fRec106[1] = fRec106[0];
			fRec105[1] = fRec105[0];
			fRec103[1] = fRec103[0];
			fRec102[1] = fRec102[0];
			fRec100[1] = fRec100[0];
			fRec111[1] = fRec111[0];
			fRec109[1] = fRec109[0];
			fRec117[1] = fRec117[0];
			fRec115[1] = fRec115[0];
			fRec114[1] = fRec114[0];
			fRec112[1] = fRec112[0];
			fRec129[1] = fRec129[0];
			fRec127[1] = fRec127[0];
			fRec126[1] = fRec126[0];
			fRec124[1] = fRec124[0];
			fRec123[1] = fRec123[0];
			fRec121[1] = fRec121[0];
			fRec120[1] = fRec120[0];
			fRec118[1] = fRec118[0];
			fRec138[1] = fRec138[0];
			fRec136[1] = fRec136[0];
			fRec135[1] = fRec135[0];
			fRec133[1] = fRec133[0];
			fRec132[1] = fRec132[0];
			fRec130[1] = fRec130[0];
			fRec141[1] = fRec141[0];
			fRec139[1] = fRec139[0];
			fRec147[1] = fRec147[0];
			fRec145[1] = fRec145[0];
			fRec144[1] = fRec144[0];
			fRec142[1] = fRec142[0];
			fRec150[1] = fRec150[0];
			fRec148[1] = fRec148[0];
			fRec156[1] = fRec156[0];
			fRec154[1] = fRec154[0];
			fRec153[1] = fRec153[0];
			fRec151[1] = fRec151[0];
			fRec165[1] = fRec165[0];
			fRec163[1] = fRec163[0];
			fRec162[1] = fRec162[0];
			fRec160[1] = fRec160[0];
			fRec159[1] = fRec159[0];
			fRec157[1] = fRec157[0];
			fRec177[1] = fRec177[0];
			fRec175[1] = fRec175[0];
			fRec174[1] = fRec174[0];
			fRec172[1] = fRec172[0];
			fRec171[1] = fRec171[0];
			fRec169[1] = fRec169[0];
			fRec168[1] = fRec168[0];
			fRec166[1] = fRec166[0];
			fRec180[1] = fRec180[0];
			fRec178[1] = fRec178[0];
			fRec186[1] = fRec186[0];
			fRec184[1] = fRec184[0];
			fRec183[1] = fRec183[0];
			fRec181[1] = fRec181[0];
			fRec195[1] = fRec195[0];
			fRec193[1] = fRec193[0];
			fRec192[1] = fRec192[0];
			fRec190[1] = fRec190[0];
			fRec189[1] = fRec189[0];
			fRec187[1] = fRec187[0];
			fRec207[1] = fRec207[0];
			fRec205[1] = fRec205[0];
			fRec204[1] = fRec204[0];
			fRec202[1] = fRec202[0];
			fRec201[1] = fRec201[0];
			fRec199[1] = fRec199[0];
			fRec198[1] = fRec198[0];
			fRec196[1] = fRec196[0];
			fRec210[1] = fRec210[0];
			fRec208[1] = fRec208[0];
			fRec216[1] = fRec216[0];
			fRec214[1] = fRec214[0];
			fRec213[1] = fRec213[0];
			fRec211[1] = fRec211[0];
			fRec225[1] = fRec225[0];
			fRec223[1] = fRec223[0];
			fRec222[1] = fRec222[0];
			fRec220[1] = fRec220[0];
			fRec219[1] = fRec219[0];
			fRec217[1] = fRec217[0];
			fRec237[1] = fRec237[0];
			fRec235[1] = fRec235[0];
			fRec234[1] = fRec234[0];
			fRec232[1] = fRec232[0];
			fRec231[1] = fRec231[0];
			fRec229[1] = fRec229[0];
			fRec228[1] = fRec228[0];
			fRec226[1] = fRec226[0];
			fRec240[1] = fRec240[0];
			fRec238[1] = fRec238[0];
			fRec246[1] = fRec246[0];
			fRec244[1] = fRec244[0];
			fRec243[1] = fRec243[0];
			fRec241[1] = fRec241[0];
			fRec255[1] = fRec255[0];
			fRec253[1] = fRec253[0];
			fRec252[1] = fRec252[0];
			fRec250[1] = fRec250[0];
			fRec249[1] = fRec249[0];
			fRec247[1] = fRec247[0];
			fRec267[1] = fRec267[0];
			fRec265[1] = fRec265[0];
			fRec264[1] = fRec264[0];
			fRec262[1] = fRec262[0];
			fRec261[1] = fRec261[0];
			fRec259[1] = fRec259[0];
			fRec258[1] = fRec258[0];
			fRec256[1] = fRec256[0];
			fRec273[1] = fRec273[0];
			fRec271[1] = fRec271[0];
			fRec270[1] = fRec270[0];
			fRec268[1] = fRec268[0];
			fRec276[1] = fRec276[0];
			fRec274[1] = fRec274[0];
			fRec285[1] = fRec285[0];
			fRec283[1] = fRec283[0];
			fRec282[1] = fRec282[0];
			fRec280[1] = fRec280[0];
			fRec279[1] = fRec279[0];
			fRec277[1] = fRec277[0];
			fRec297[1] = fRec297[0];
			fRec295[1] = fRec295[0];
			fRec294[1] = fRec294[0];
			fRec292[1] = fRec292[0];
			fRec291[1] = fRec291[0];
			fRec289[1] = fRec289[0];
			fRec288[1] = fRec288[0];
			fRec286[1] = fRec286[0];
			fRec309[1] = fRec309[0];
			fRec307[1] = fRec307[0];
			fRec306[1] = fRec306[0];
			fRec304[1] = fRec304[0];
			fRec303[1] = fRec303[0];
			fRec301[1] = fRec301[0];
			fRec300[1] = fRec300[0];
			fRec298[1] = fRec298[0];
			fRec318[1] = fRec318[0];
			fRec316[1] = fRec316[0];
			fRec315[1] = fRec315[0];
			fRec313[1] = fRec313[0];
			fRec312[1] = fRec312[0];
			fRec310[1] = fRec310[0];
			fRec321[1] = fRec321[0];
			fRec319[1] = fRec319[0];
			fRec327[1] = fRec327[0];
			fRec325[1] = fRec325[0];
			fRec324[1] = fRec324[0];
			fRec322[1] = fRec322[0];
			fRec339[1] = fRec339[0];
			fRec337[1] = fRec337[0];
			fRec336[1] = fRec336[0];
			fRec334[1] = fRec334[0];
			fRec333[1] = fRec333[0];
			fRec331[1] = fRec331[0];
			fRec330[1] = fRec330[0];
			fRec328[1] = fRec328[0];
			fRec348[1] = fRec348[0];
			fRec346[1] = fRec346[0];
			fRec345[1] = fRec345[0];
			fRec343[1] = fRec343[0];
			fRec342[1] = fRec342[0];
			fRec340[1] = fRec340[0];
			fRec351[1] = fRec351[0];
			fRec349[1] = fRec349[0];
			fRec357[1] = fRec357[0];
			fRec355[1] = fRec355[0];
			fRec354[1] = fRec354[0];
			fRec352[1] = fRec352[0];
			fRec366[1] = fRec366[0];
			fRec364[1] = fRec364[0];
			fRec363[1] = fRec363[0];
			fRec361[1] = fRec361[0];
			fRec360[1] = fRec360[0];
			fRec358[1] = fRec358[0];
			fRec372[1] = fRec372[0];
			fRec370[1] = fRec370[0];
			fRec369[1] = fRec369[0];
			fRec367[1] = fRec367[0];
			fRec384[1] = fRec384[0];
			fRec382[1] = fRec382[0];
			fRec381[1] = fRec381[0];
			fRec379[1] = fRec379[0];
			fRec378[1] = fRec378[0];
			fRec376[1] = fRec376[0];
			fRec375[1] = fRec375[0];
			fRec373[1] = fRec373[0];
			fRec387[1] = fRec387[0];
			fRec385[1] = fRec385[0];
			fRec399[1] = fRec399[0];
			fRec397[1] = fRec397[0];
			fRec396[1] = fRec396[0];
			fRec394[1] = fRec394[0];
			fRec393[1] = fRec393[0];
			fRec391[1] = fRec391[0];
			fRec390[1] = fRec390[0];
			fRec388[1] = fRec388[0];
			fRec408[1] = fRec408[0];
			fRec406[1] = fRec406[0];
			fRec405[1] = fRec405[0];
			fRec403[1] = fRec403[0];
			fRec402[1] = fRec402[0];
			fRec400[1] = fRec400[0];
			fRec411[1] = fRec411[0];
			fRec409[1] = fRec409[0];
			fRec417[1] = fRec417[0];
			fRec415[1] = fRec415[0];
			fRec414[1] = fRec414[0];
			fRec412[1] = fRec412[0];
			fRec423[1] = fRec423[0];
			fRec421[1] = fRec421[0];
			fRec420[1] = fRec420[0];
			fRec418[1] = fRec418[0];
			fRec426[1] = fRec426[0];
			fRec424[1] = fRec424[0];
			fRec435[1] = fRec435[0];
			fRec433[1] = fRec433[0];
			fRec432[1] = fRec432[0];
			fRec430[1] = fRec430[0];
			fRec429[1] = fRec429[0];
			fRec427[1] = fRec427[0];
			fRec447[1] = fRec447[0];
			fRec445[1] = fRec445[0];
			fRec444[1] = fRec444[0];
			fRec442[1] = fRec442[0];
			fRec441[1] = fRec441[0];
			fRec439[1] = fRec439[0];
			fRec438[1] = fRec438[0];
			fRec436[1] = fRec436[0];
			fRec459[1] = fRec459[0];
			fRec457[1] = fRec457[0];
			fRec456[1] = fRec456[0];
			fRec454[1] = fRec454[0];
			fRec453[1] = fRec453[0];
			fRec451[1] = fRec451[0];
			fRec450[1] = fRec450[0];
			fRec448[1] = fRec448[0];
			fRec468[1] = fRec468[0];
			fRec466[1] = fRec466[0];
			fRec465[1] = fRec465[0];
			fRec463[1] = fRec463[0];
			fRec462[1] = fRec462[0];
			fRec460[1] = fRec460[0];
			fRec471[1] = fRec471[0];
			fRec469[1] = fRec469[0];
			fRec477[1] = fRec477[0];
			fRec475[1] = fRec475[0];
			fRec474[1] = fRec474[0];
			fRec472[1] = fRec472[0];
			fRec480[1] = fRec480[0];
			fRec478[1] = fRec478[0];
			fRec489[1] = fRec489[0];
			fRec487[1] = fRec487[0];
			fRec486[1] = fRec486[0];
			fRec484[1] = fRec484[0];
			fRec483[1] = fRec483[0];
			fRec481[1] = fRec481[0];
			fRec501[1] = fRec501[0];
			fRec499[1] = fRec499[0];
			fRec498[1] = fRec498[0];
			fRec496[1] = fRec496[0];
			fRec495[1] = fRec495[0];
			fRec493[1] = fRec493[0];
			fRec492[1] = fRec492[0];
			fRec490[1] = fRec490[0];
			fRec507[1] = fRec507[0];
			fRec505[1] = fRec505[0];
			fRec504[1] = fRec504[0];
			fRec502[1] = fRec502[0];
			fRec519[1] = fRec519[0];
			fRec517[1] = fRec517[0];
			fRec516[1] = fRec516[0];
			fRec514[1] = fRec514[0];
			fRec513[1] = fRec513[0];
			fRec511[1] = fRec511[0];
			fRec510[1] = fRec510[0];
			fRec508[1] = fRec508[0];
			fRec528[1] = fRec528[0];
			fRec526[1] = fRec526[0];
			fRec525[1] = fRec525[0];
			fRec523[1] = fRec523[0];
			fRec522[1] = fRec522[0];
			fRec520[1] = fRec520[0];
			fRec531[1] = fRec531[0];
			fRec529[1] = fRec529[0];
			fRec537[1] = fRec537[0];
			fRec535[1] = fRec535[0];
			fRec534[1] = fRec534[0];
			fRec532[1] = fRec532[0];
			fRec549[1] = fRec549[0];
			fRec547[1] = fRec547[0];
			fRec546[1] = fRec546[0];
			fRec544[1] = fRec544[0];
			fRec543[1] = fRec543[0];
			fRec541[1] = fRec541[0];
			fRec540[1] = fRec540[0];
			fRec538[1] = fRec538[0];
			fRec555[1] = fRec555[0];
			fRec553[1] = fRec553[0];
			fRec552[1] = fRec552[0];
			fRec550[1] = fRec550[0];
			fRec564[1] = fRec564[0];
			fRec562[1] = fRec562[0];
			fRec561[1] = fRec561[0];
			fRec559[1] = fRec559[0];
			fRec558[1] = fRec558[0];
			fRec556[1] = fRec556[0];
			fRec567[1] = fRec567[0];
			fRec565[1] = fRec565[0];
			fRec579[1] = fRec579[0];
			fRec577[1] = fRec577[0];
			fRec576[1] = fRec576[0];
			fRec574[1] = fRec574[0];
			fRec573[1] = fRec573[0];
			fRec571[1] = fRec571[0];
			fRec570[1] = fRec570[0];
			fRec568[1] = fRec568[0];
			fRec588[1] = fRec588[0];
			fRec586[1] = fRec586[0];
			fRec585[1] = fRec585[0];
			fRec583[1] = fRec583[0];
			fRec582[1] = fRec582[0];
			fRec580[1] = fRec580[0];
			fRec591[1] = fRec591[0];
			fRec589[1] = fRec589[0];
			fRec597[1] = fRec597[0];
			fRec595[1] = fRec595[0];
			fRec594[1] = fRec594[0];
			fRec592[1] = fRec592[0];
			fRec609[1] = fRec609[0];
			fRec607[1] = fRec607[0];
			fRec606[1] = fRec606[0];
			fRec604[1] = fRec604[0];
			fRec603[1] = fRec603[0];
			fRec601[1] = fRec601[0];
			fRec600[1] = fRec600[0];
			fRec598[1] = fRec598[0];
			fRec618[1] = fRec618[0];
			fRec616[1] = fRec616[0];
			fRec615[1] = fRec615[0];
			fRec613[1] = fRec613[0];
			fRec612[1] = fRec612[0];
			fRec610[1] = fRec610[0];
			fRec621[1] = fRec621[0];
			fRec619[1] = fRec619[0];
			fRec627[1] = fRec627[0];
			fRec625[1] = fRec625[0];
			fRec624[1] = fRec624[0];
			fRec622[1] = fRec622[0];
			fVec19[2] = fVec19[1];
			fVec19[1] = fVec19[0];
			fRec636[1] = fRec636[0];
			fRec634[1] = fRec634[0];
			fRec633[1] = fRec633[0];
			fRec631[1] = fRec631[0];
			fRec630[1] = fRec630[0];
			fRec628[1] = fRec628[0];
			fRec642[1] = fRec642[0];
			fRec640[1] = fRec640[0];
			fRec639[1] = fRec639[0];
			fRec637[1] = fRec637[0];
			fRec654[1] = fRec654[0];
			fRec652[1] = fRec652[0];
			fRec651[1] = fRec651[0];
			fRec649[1] = fRec649[0];
			fRec648[1] = fRec648[0];
			fRec646[1] = fRec646[0];
			fRec645[1] = fRec645[0];
			fRec643[1] = fRec643[0];
			fRec657[1] = fRec657[0];
			fRec655[1] = fRec655[0];
			fRec666[1] = fRec666[0];
			fRec664[1] = fRec664[0];
			fRec663[1] = fRec663[0];
			fRec661[1] = fRec661[0];
			fRec660[1] = fRec660[0];
			fRec658[1] = fRec658[0];
			fRec669[1] = fRec669[0];
			fRec667[1] = fRec667[0];
			fRec675[1] = fRec675[0];
			fRec673[1] = fRec673[0];
			fRec672[1] = fRec672[0];
			fRec670[1] = fRec670[0];
			fRec687[1] = fRec687[0];
			fRec685[1] = fRec685[0];
			fRec684[1] = fRec684[0];
			fRec682[1] = fRec682[0];
			fRec681[1] = fRec681[0];
			fRec679[1] = fRec679[0];
			fRec678[1] = fRec678[0];
			fRec676[1] = fRec676[0];
			fRec699[1] = fRec699[0];
			fRec697[1] = fRec697[0];
			fRec696[1] = fRec696[0];
			fRec694[1] = fRec694[0];
			fRec693[1] = fRec693[0];
			fRec691[1] = fRec691[0];
			fRec690[1] = fRec690[0];
			fRec688[1] = fRec688[0];
			fRec708[1] = fRec708[0];
			fRec706[1] = fRec706[0];
			fRec705[1] = fRec705[0];
			fRec703[1] = fRec703[0];
			fRec702[1] = fRec702[0];
			fRec700[1] = fRec700[0];
			fRec711[1] = fRec711[0];
			fRec709[1] = fRec709[0];
			fRec717[1] = fRec717[0];
			fRec715[1] = fRec715[0];
			fRec714[1] = fRec714[0];
			fRec712[1] = fRec712[0];
			fVec20[2] = fVec20[1];
			fVec20[1] = fVec20[0];
			fRec729[1] = fRec729[0];
			fRec727[1] = fRec727[0];
			fRec726[1] = fRec726[0];
			fRec724[1] = fRec724[0];
			fRec723[1] = fRec723[0];
			fRec721[1] = fRec721[0];
			fRec720[1] = fRec720[0];
			fRec718[1] = fRec718[0];
			fRec732[1] = fRec732[0];
			fRec730[1] = fRec730[0];
			fRec738[1] = fRec738[0];
			fRec736[1] = fRec736[0];
			fRec735[1] = fRec735[0];
			fRec733[1] = fRec733[0];
			fRec747[1] = fRec747[0];
			fRec745[1] = fRec745[0];
			fRec744[1] = fRec744[0];
			fRec742[1] = fRec742[0];
			fRec741[1] = fRec741[0];
			fRec739[1] = fRec739[0];
			fRec753[1] = fRec753[0];
			fRec751[1] = fRec751[0];
			fRec750[1] = fRec750[0];
			fRec748[1] = fRec748[0];
			fRec756[1] = fRec756[0];
			fRec754[1] = fRec754[0];
			fRec765[1] = fRec765[0];
			fRec763[1] = fRec763[0];
			fRec762[1] = fRec762[0];
			fRec760[1] = fRec760[0];
			fRec759[1] = fRec759[0];
			fRec757[1] = fRec757[0];
			fRec777[1] = fRec777[0];
			fRec775[1] = fRec775[0];
			fRec774[1] = fRec774[0];
			fRec772[1] = fRec772[0];
			fRec771[1] = fRec771[0];
			fRec769[1] = fRec769[0];
			fRec768[1] = fRec768[0];
			fRec766[1] = fRec766[0];
			fRec789[1] = fRec789[0];
			fRec787[1] = fRec787[0];
			fRec786[1] = fRec786[0];
			fRec784[1] = fRec784[0];
			fRec783[1] = fRec783[0];
			fRec781[1] = fRec781[0];
			fRec780[1] = fRec780[0];
			fRec778[1] = fRec778[0];
			fRec798[1] = fRec798[0];
			fRec796[1] = fRec796[0];
			fRec795[1] = fRec795[0];
			fRec793[1] = fRec793[0];
			fRec792[1] = fRec792[0];
			fRec790[1] = fRec790[0];
			fRec801[1] = fRec801[0];
			fRec799[1] = fRec799[0];
			fRec807[1] = fRec807[0];
			fRec805[1] = fRec805[0];
			fRec804[1] = fRec804[0];
			fRec802[1] = fRec802[0];
			fVec21[2] = fVec21[1];
			fVec21[1] = fVec21[0];
			fRec819[1] = fRec819[0];
			fRec817[1] = fRec817[0];
			fRec816[1] = fRec816[0];
			fRec814[1] = fRec814[0];
			fRec813[1] = fRec813[0];
			fRec811[1] = fRec811[0];
			fRec810[1] = fRec810[0];
			fRec808[1] = fRec808[0];
			fRec828[1] = fRec828[0];
			fRec826[1] = fRec826[0];
			fRec825[1] = fRec825[0];
			fRec823[1] = fRec823[0];
			fRec822[1] = fRec822[0];
			fRec820[1] = fRec820[0];
			fRec831[1] = fRec831[0];
			fRec829[1] = fRec829[0];
			fRec837[1] = fRec837[0];
			fRec835[1] = fRec835[0];
			fRec834[1] = fRec834[0];
			fRec832[1] = fRec832[0];
			fRec849[1] = fRec849[0];
			fRec847[1] = fRec847[0];
			fRec846[1] = fRec846[0];
			fRec844[1] = fRec844[0];
			fRec843[1] = fRec843[0];
			fRec841[1] = fRec841[0];
			fRec840[1] = fRec840[0];
			fRec838[1] = fRec838[0];
			fRec858[1] = fRec858[0];
			fRec856[1] = fRec856[0];
			fRec855[1] = fRec855[0];
			fRec853[1] = fRec853[0];
			fRec852[1] = fRec852[0];
			fRec850[1] = fRec850[0];
			fRec861[1] = fRec861[0];
			fRec859[1] = fRec859[0];
			fRec867[1] = fRec867[0];
			fRec865[1] = fRec865[0];
			fRec864[1] = fRec864[0];
			fRec862[1] = fRec862[0];
			fRec879[1] = fRec879[0];
			fRec877[1] = fRec877[0];
			fRec876[1] = fRec876[0];
			fRec874[1] = fRec874[0];
			fRec873[1] = fRec873[0];
			fRec871[1] = fRec871[0];
			fRec870[1] = fRec870[0];
			fRec868[1] = fRec868[0];
			fRec888[1] = fRec888[0];
			fRec886[1] = fRec886[0];
			fRec885[1] = fRec885[0];
			fRec883[1] = fRec883[0];
			fRec882[1] = fRec882[0];
			fRec880[1] = fRec880[0];
			fRec891[1] = fRec891[0];
			fRec889[1] = fRec889[0];
			fRec897[1] = fRec897[0];
			fRec895[1] = fRec895[0];
			fRec894[1] = fRec894[0];
			fRec892[1] = fRec892[0];
			fVec22[2] = fVec22[1];
			fVec22[1] = fVec22[0];
			fRec909[1] = fRec909[0];
			fRec907[1] = fRec907[0];
			fRec906[1] = fRec906[0];
			fRec904[1] = fRec904[0];
			fRec903[1] = fRec903[0];
			fRec901[1] = fRec901[0];
			fRec900[1] = fRec900[0];
			fRec898[1] = fRec898[0];
			fRec918[1] = fRec918[0];
			fRec916[1] = fRec916[0];
			fRec915[1] = fRec915[0];
			fRec913[1] = fRec913[0];
			fRec912[1] = fRec912[0];
			fRec910[1] = fRec910[0];
			fRec921[1] = fRec921[0];
			fRec919[1] = fRec919[0];
			fRec927[1] = fRec927[0];
			fRec925[1] = fRec925[0];
			fRec924[1] = fRec924[0];
			fRec922[1] = fRec922[0];
			fRec939[1] = fRec939[0];
			fRec937[1] = fRec937[0];
			fRec936[1] = fRec936[0];
			fRec934[1] = fRec934[0];
			fRec933[1] = fRec933[0];
			fRec931[1] = fRec931[0];
			fRec930[1] = fRec930[0];
			fRec928[1] = fRec928[0];
			fRec948[1] = fRec948[0];
			fRec946[1] = fRec946[0];
			fRec945[1] = fRec945[0];
			fRec943[1] = fRec943[0];
			fRec942[1] = fRec942[0];
			fRec940[1] = fRec940[0];
			fRec951[1] = fRec951[0];
			fRec949[1] = fRec949[0];
			fRec957[1] = fRec957[0];
			fRec955[1] = fRec955[0];
			fRec954[1] = fRec954[0];
			fRec952[1] = fRec952[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
