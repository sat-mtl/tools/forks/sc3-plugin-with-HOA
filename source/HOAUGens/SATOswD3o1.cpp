/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOswD3o1"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fHslider0;
	FAUSTFLOAT fCheckbox0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	float fConst2;
	float fConst3;
	float fConst4;
	float fRec4[2];
	float fRec2[2];
	float fConst5;
	FAUSTFLOAT fHslider1;
	float fRec5[2];
	float fRec1[3];
	FAUSTFLOAT fHslider2;
	float fRec6[2];
	float fRec7[3];
	float fRec11[2];
	float fRec9[2];
	float fRec8[3];
	float fRec15[2];
	float fRec13[2];
	float fRec12[3];
	float fVec0[12];
	int iConst6;
	float fVec1[13];
	int iConst7;
	float fVec2[12];
	float fVec3[12];
	float fVec4[13];
	float fVec5[12];
	int IOTA;
	float fVec6[32];
	int iConst8;
	float fVec7[32];
	int iConst9;
	float fVec8[32];
	float fVec9[32];
	float fVec10[32];
	float fVec11[32];
	float fVec12[32];
	float fVec13[32];
	float fVec14[32];
	float fVec15[32];
	float fVec16[32];
	float fVec17[32];
	float fVec18[64];
	int iConst10;
	float fVec19[64];
	int iConst11;
	float fVec20[64];
	float fVec21[64];
	float fVec22[64];
	float fVec23[64];
	float fVec24[64];
	float fVec25[64];
	float fVec26[64];
	float fVec27[64];
	float fVec28[64];
	float fVec29[64];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOswD3o1");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 4;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = float(iConst0);
		fConst2 = ((19.133934f / fConst1) + 1.0f);
		fConst3 = (1.0f / fConst2);
		fConst4 = (0.0f - (38.267868f / (fConst1 * fConst2)));
		fConst5 = (3.14159274f / float(iConst0));
		iConst6 = int(((5.24453171e-05f * float(iConst0)) + 0.5f));
		iConst7 = int(((5.8272577e-05f * float(iConst0)) + 0.5f));
		iConst8 = int(((0.000145681435f * float(iConst0)) + 0.5f));
		iConst9 = int(((0.000134026923f * float(iConst0)) + 0.5f));
		iConst10 = int(((0.000262226589f * float(iConst0)) + 0.5f));
		iConst11 = int(((0.000256399333f * float(iConst0)) + 0.5f));
		
	}
	
	virtual void instanceResetUserInterface() {
		fHslider0 = FAUSTFLOAT(-10.0f);
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec4[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 2); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec5[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec1[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 2); l5 = (l5 + 1)) {
			fRec6[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec7[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 2); l7 = (l7 + 1)) {
			fRec11[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 2); l8 = (l8 + 1)) {
			fRec9[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec8[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 2); l10 = (l10 + 1)) {
			fRec15[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 2); l11 = (l11 + 1)) {
			fRec13[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec12[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 12); l13 = (l13 + 1)) {
			fVec0[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 13); l14 = (l14 + 1)) {
			fVec1[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 12); l15 = (l15 + 1)) {
			fVec2[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 12); l16 = (l16 + 1)) {
			fVec3[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 13); l17 = (l17 + 1)) {
			fVec4[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 12); l18 = (l18 + 1)) {
			fVec5[l18] = 0.0f;
			
		}
		IOTA = 0;
		for (int l19 = 0; (l19 < 32); l19 = (l19 + 1)) {
			fVec6[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 32); l20 = (l20 + 1)) {
			fVec7[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 32); l21 = (l21 + 1)) {
			fVec8[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 32); l22 = (l22 + 1)) {
			fVec9[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 32); l23 = (l23 + 1)) {
			fVec10[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 32); l24 = (l24 + 1)) {
			fVec11[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 32); l25 = (l25 + 1)) {
			fVec12[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 32); l26 = (l26 + 1)) {
			fVec13[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 32); l27 = (l27 + 1)) {
			fVec14[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 32); l28 = (l28 + 1)) {
			fVec15[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 32); l29 = (l29 + 1)) {
			fVec16[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 32); l30 = (l30 + 1)) {
			fVec17[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 64); l31 = (l31 + 1)) {
			fVec18[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 64); l32 = (l32 + 1)) {
			fVec19[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 64); l33 = (l33 + 1)) {
			fVec20[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 64); l34 = (l34 + 1)) {
			fVec21[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 64); l35 = (l35 + 1)) {
			fVec22[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 64); l36 = (l36 + 1)) {
			fVec23[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 64); l37 = (l37 + 1)) {
			fVec24[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 64); l38 = (l38 + 1)) {
			fVec25[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 64); l39 = (l39 + 1)) {
			fVec26[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 64); l40 = (l40 + 1)) {
			fVec27[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 64); l41 = (l41 + 1)) {
			fVec28[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 64); l42 = (l42 + 1)) {
			fVec29[l42] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOswD3o1");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (powf(10.0f, (0.0500000007f * float(fHslider0))) * float((float(fCheckbox0) < 0.5f))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			float fTemp0 = (fConst3 * float(input3[i]));
			float fTemp1 = (fConst4 * fRec2[1]);
			fRec4[0] = (fTemp0 + (fRec4[1] + fTemp1));
			fRec2[0] = fRec4[0];
			float fRec3 = (fTemp1 + fTemp0);
			fRec5[0] = (fSlow1 + (0.999000013f * fRec5[1]));
			float fTemp2 = tanf((fConst5 * fRec5[0]));
			float fTemp3 = ((fTemp2 * (fTemp2 + -2.0f)) + 1.0f);
			float fTemp4 = mydsp_faustpower2_f(fTemp2);
			float fTemp5 = (fTemp4 + -1.0f);
			float fTemp6 = ((fTemp2 * (fTemp2 + 2.0f)) + 1.0f);
			fRec1[0] = (fRec3 - (((fRec1[2] * fTemp3) + (2.0f * (fRec1[1] * fTemp5))) / fTemp6));
			fRec6[0] = (fSlow2 + (0.999000013f * fRec6[1]));
			float fTemp7 = (fRec6[0] * fTemp6);
			float fTemp8 = (0.0f - (2.0f / fTemp6));
			float fTemp9 = ((((fRec1[2] + (fRec1[0] + (2.0f * fRec1[1]))) * fTemp4) / fTemp7) + (0.577350259f * (fRec6[0] * (0.0f - ((fRec1[1] * fTemp8) + ((fRec1[0] + fRec1[2]) / fTemp6))))));
			fRec7[0] = (float(input0[i]) - (((fRec7[2] * fTemp3) + (2.0f * (fRec7[1] * fTemp5))) / fTemp6));
			float fTemp10 = (((fTemp4 * (fRec7[2] + (fRec7[0] + (2.0f * fRec7[1])))) / fTemp7) + (fRec6[0] * (0.0f - ((fRec7[1] * fTemp8) + ((fRec7[0] + fRec7[2]) / fTemp6)))));
			float fTemp11 = (fConst3 * float(input2[i]));
			float fTemp12 = (fConst4 * fRec9[1]);
			fRec11[0] = (fTemp11 + (fRec11[1] + fTemp12));
			fRec9[0] = fRec11[0];
			float fRec10 = (fTemp12 + fTemp11);
			fRec8[0] = (fRec10 - (((fTemp3 * fRec8[2]) + (2.0f * (fTemp5 * fRec8[1]))) / fTemp6));
			float fTemp13 = (((fTemp4 * (fRec8[2] + (fRec8[0] + (2.0f * fRec8[1])))) / fTemp7) + (0.577350259f * (fRec6[0] * (0.0f - ((fTemp8 * fRec8[1]) + ((fRec8[0] + fRec8[2]) / fTemp6))))));
			float fTemp14 = (fConst3 * float(input1[i]));
			float fTemp15 = (fConst4 * fRec13[1]);
			fRec15[0] = (fTemp14 + (fRec15[1] + fTemp15));
			fRec13[0] = fRec15[0];
			float fRec14 = (fTemp15 + fTemp14);
			fRec12[0] = (fRec14 - (((fTemp3 * fRec12[2]) + (2.0f * (fTemp5 * fRec12[1]))) / fTemp6));
			float fTemp16 = (((fTemp4 * (fRec12[2] + (fRec12[0] + (2.0f * fRec12[1])))) / fTemp7) + (0.577350259f * (fRec6[0] * (0.0f - ((fTemp8 * fRec12[1]) + ((fRec12[0] + fRec12[2]) / fTemp6))))));
			output0[i] = FAUSTFLOAT((fRec0[0] * ((((2.45000001e-06f * fTemp9) + (0.0473226868f * fTemp10)) + (0.0769467279f * fTemp13)) - (2.71919998e-06f * fTemp16))));
			fVec0[0] = ((0.0463922769f * fTemp13) + ((0.0301853716f * fTemp16) + ((0.0341214538f * fTemp10) + (0.0108339777f * fTemp9))));
			output1[i] = FAUSTFLOAT((0.998005569f * (fRec0[0] * fVec0[iConst6])));
			fVec1[0] = (((0.0510143228f * fTemp13) + ((0.038172856f * fTemp10) + (0.0257401336f * fTemp16))) - (0.0257381666f * fTemp9));
			output2[i] = FAUSTFLOAT((0.997783959f * (fRec0[0] * fVec1[iConst7])));
			fVec2[0] = (((0.0341186747f * fTemp10) + (0.0463907383f * fTemp13)) - ((0.0108332634f * fTemp16) + (0.0301783718f * fTemp9)));
			output3[i] = FAUSTFLOAT((0.998005569f * (fRec0[0] * fVec2[iConst6])));
			fVec3[0] = (((0.0300636496f * fTemp10) + (0.0417693704f * fTemp13)) - ((0.0265663434f * fTemp16) + (0.0071219611f * fTemp9)));
			output4[i] = FAUSTFLOAT((0.998005569f * (fRec0[0] * fVec3[iConst6])));
			fVec4[0] = (((0.0510256663f * fTemp13) + ((0.0381806493f * fTemp10) + (0.0257430729f * fTemp9))) - (0.0257410146f * fTemp16));
			output5[i] = FAUSTFLOAT((0.997783959f * (fRec0[0] * fVec4[iConst7])));
			fVec5[0] = ((0.0417632945f * fTemp13) + ((0.00712388381f * fTemp16) + ((0.0300600771f * fTemp10) + (0.0265659001f * fTemp9))));
			output6[i] = FAUSTFLOAT((0.998005569f * (fVec5[iConst6] * fRec0[0])));
			fVec6[(IOTA & 31)] = (((0.0200215131f * fTemp13) + ((0.0235311687f * fTemp10) + (0.0338993929f * fTemp16))) - (0.00145658397f * fTemp9));
			output7[i] = FAUSTFLOAT((0.994459808f * (fVec6[((IOTA - iConst8) & 31)] * fRec0[0])));
			fVec7[(IOTA & 31)] = (((0.0179317892f * fTemp13) + ((0.0306961462f * fTemp10) + (0.0410687849f * fTemp16))) - (0.0242131073f * fTemp9));
			output8[i] = FAUSTFLOAT((0.994903028f * (fVec7[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec8[(IOTA & 31)] = (((0.0157211255f * fTemp13) + ((0.0242891777f * fTemp10) + (0.0165882278f * fTemp16))) - (0.0335474797f * fTemp9));
			output9[i] = FAUSTFLOAT((0.994903028f * (fVec8[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec9[(IOTA & 31)] = (((0.0200138334f * fTemp13) + ((0.0235146843f * fTemp10) + (0.00145928503f * fTemp16))) - (0.0338744335f * fTemp9));
			output10[i] = FAUSTFLOAT((0.994459808f * (fVec9[((IOTA - iConst8) & 31)] * fRec0[0])));
			fVec10[(IOTA & 31)] = (((0.0242880136f * fTemp10) + (0.0157179944f * fTemp13)) - ((0.0158460233f * fTemp16) + (0.0339761786f * fTemp9)));
			output11[i] = FAUSTFLOAT((0.994903028f * (fVec10[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec11[(IOTA & 31)] = (((0.0363172293f * fTemp10) + (0.0244163014f * fTemp13)) - ((0.0464762598f * fTemp16) + (0.0285261497f * fTemp9)));
			output12[i] = FAUSTFLOAT((0.994903028f * (fVec11[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec12[(IOTA & 31)] = (((0.0200154874f * fTemp13) + ((0.023518106f * fTemp10) + (0.00145378441f * fTemp9))) - (0.033879105f * fTemp16));
			output13[i] = FAUSTFLOAT((0.994459808f * (fVec12[((IOTA - iConst8) & 31)] * fRec0[0])));
			fVec13[(IOTA & 31)] = (((0.0179308224f * fTemp13) + ((0.0307002682f * fTemp10) + (0.0242112409f * fTemp9))) - (0.0410771593f * fTemp16));
			output14[i] = FAUSTFLOAT((0.994903028f * (fVec13[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec14[(IOTA & 31)] = (((0.015737446f * fTemp13) + ((0.0243119132f * fTemp10) + (0.0335719511f * fTemp9))) - (0.0166117605f * fTemp16));
			output15[i] = FAUSTFLOAT((0.994903028f * (fVec14[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec15[(IOTA & 31)] = (((0.0200174712f * fTemp13) + ((0.0235254578f * fTemp10) + (0.0338919684f * fTemp9))) - (0.00146376574f * fTemp16));
			output16[i] = FAUSTFLOAT((0.994459808f * (fVec15[((IOTA - iConst8) & 31)] * fRec0[0])));
			fVec16[(IOTA & 31)] = ((0.0244102012f * fTemp13) + ((0.0285257287f * fTemp16) + ((0.0363168567f * fTemp10) + (0.0464785807f * fTemp9))));
			output17[i] = FAUSTFLOAT((0.994903028f * (fVec16[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec17[(IOTA & 31)] = ((0.0157361906f * fTemp13) + ((0.0339871384f * fTemp16) + ((0.0242996197f * fTemp10) + (0.015851954f * fTemp9))));
			output18[i] = FAUSTFLOAT((0.994903028f * (fVec17[((IOTA - iConst9) & 31)] * fRec0[0])));
			fVec18[(IOTA & 63)] = (((0.0588501506f * fTemp10) + (0.0887992382f * fTemp16)) - ((0.0229201801f * fTemp13) + (1.02769e-05f * fTemp9)));
			output19[i] = FAUSTFLOAT((0.990027726f * (fVec18[((IOTA - iConst10) & 63)] * fRec0[0])));
			fVec19[(IOTA & 63)] = (((0.0475860611f * fTemp10) + (0.061254438f * fTemp16)) - ((0.0270938333f * fTemp13) + (0.0353607573f * fTemp9)));
			output20[i] = FAUSTFLOAT((0.990249336f * (fVec19[((IOTA - iConst11) & 63)] * fRec0[0])));
			fVec20[(IOTA & 63)] = (((0.0532584041f * fTemp10) + (0.042079974f * fTemp16)) - ((0.0250215139f * fTemp13) + (0.067867741f * fTemp9)));
			output21[i] = FAUSTFLOAT((0.990249336f * (fVec20[((IOTA - iConst11) & 63)] * fRec0[0])));
			fVec21[(IOTA & 63)] = (((0.0588359646f * fTemp10) + (6.81699987e-07f * fTemp16)) - ((0.0229164064f * fTemp13) + (0.0887830555f * fTemp9)));
			output22[i] = FAUSTFLOAT((0.990027726f * (fVec21[((IOTA - iConst10) & 63)] * fRec0[0])));
			fVec22[(IOTA & 63)] = ((0.0532494672f * fTemp10) - ((0.0250031725f * fTemp13) + ((0.0420738682f * fTemp16) + (0.0678718835f * fTemp9))));
			output23[i] = FAUSTFLOAT((0.990249336f * (fVec22[((IOTA - iConst11) & 63)] * fRec0[0])));
			fVec23[(IOTA & 63)] = ((0.0476111248f * fTemp10) - ((0.0271065626f * fTemp13) + ((0.0612792261f * fTemp16) + (0.0353708342f * fTemp9))));
			output24[i] = FAUSTFLOAT((0.990249336f * (fVec23[((IOTA - iConst11) & 63)] * fRec0[0])));
			fVec24[(IOTA & 63)] = (((0.0588502251f * fTemp10) + (4.36999983e-08f * fTemp9)) - ((0.0888105035f * fTemp16) + (0.0229036063f * fTemp13)));
			output25[i] = FAUSTFLOAT((0.990027726f * (fVec24[((IOTA - iConst10) & 63)] * fRec0[0])));
			fVec25[(IOTA & 63)] = (((0.0476081558f * fTemp10) + (0.0353744999f * fTemp9)) - ((0.0612672567f * fTemp16) + (0.0271157604f * fTemp13)));
			output26[i] = FAUSTFLOAT((0.990249336f * (fVec25[((IOTA - iConst11) & 63)] * fRec0[0])));
			fVec26[(IOTA & 63)] = (((0.0532465205f * fTemp10) + (0.0678689703f * fTemp9)) - ((0.04206644f * fTemp16) + (0.0250075795f * fTemp13)));
			output27[i] = FAUSTFLOAT((0.990249336f * (fVec26[((IOTA - iConst11) & 63)] * fRec0[0])));
			fVec27[(IOTA & 63)] = (((1.94500004e-07f * fTemp16) + ((0.0588410161f * fTemp10) + (0.0887977406f * fTemp9))) - (0.0228981767f * fTemp13));
			output28[i] = FAUSTFLOAT((0.990027726f * (fVec27[((IOTA - iConst10) & 63)] * fRec0[0])));
			fVec28[(IOTA & 63)] = (((0.0353668891f * fTemp16) + ((0.0476072319f * fTemp10) + (0.0612718165f * fTemp9))) - (0.0271103531f * fTemp13));
			output29[i] = FAUSTFLOAT((0.990249336f * (fVec28[((IOTA - iConst11) & 63)] * fRec0[0])));
			fVec29[(IOTA & 63)] = (((0.0678741783f * fTemp16) + ((0.0532519892f * fTemp10) + (0.042072583f * fTemp9))) - (0.025004046f * fTemp13));
			output30[i] = FAUSTFLOAT((0.990249336f * (fVec29[((IOTA - iConst11) & 63)] * fRec0[0])));
			fRec0[1] = fRec0[0];
			fRec4[1] = fRec4[0];
			fRec2[1] = fRec2[0];
			fRec5[1] = fRec5[0];
			fRec1[2] = fRec1[1];
			fRec1[1] = fRec1[0];
			fRec6[1] = fRec6[0];
			fRec7[2] = fRec7[1];
			fRec7[1] = fRec7[0];
			fRec11[1] = fRec11[0];
			fRec9[1] = fRec9[0];
			fRec8[2] = fRec8[1];
			fRec8[1] = fRec8[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[2] = fRec12[1];
			fRec12[1] = fRec12[0];
			for (int j0 = 11; (j0 > 0); j0 = (j0 - 1)) {
				fVec0[j0] = fVec0[(j0 - 1)];
				
			}
			for (int j1 = 12; (j1 > 0); j1 = (j1 - 1)) {
				fVec1[j1] = fVec1[(j1 - 1)];
				
			}
			for (int j2 = 11; (j2 > 0); j2 = (j2 - 1)) {
				fVec2[j2] = fVec2[(j2 - 1)];
				
			}
			for (int j3 = 11; (j3 > 0); j3 = (j3 - 1)) {
				fVec3[j3] = fVec3[(j3 - 1)];
				
			}
			for (int j4 = 12; (j4 > 0); j4 = (j4 - 1)) {
				fVec4[j4] = fVec4[(j4 - 1)];
				
			}
			for (int j5 = 11; (j5 > 0); j5 = (j5 - 1)) {
				fVec5[j5] = fVec5[(j5 - 1)];
				
			}
			IOTA = (IOTA + 1);
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
