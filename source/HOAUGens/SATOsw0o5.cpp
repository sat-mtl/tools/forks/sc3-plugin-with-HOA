/* ------------------------------------------------------------
author: "AmbisonicDecoderToolkit"
copyright: "(c) Aaron J. Heller 2013"
license: "BSD 3-Clause License"
name: "SATOsw0o5"
version: "1.2"
Code generated with Faust 2.5.12 (https://faust.grame.fr)
Compilation options: cpp, -scal -ftz 0
------------------------------------------------------------ */

#ifndef  __mydsp_H__
#define  __mydsp_H__

//-------------------------------------------------------------------
// FAUST architecture file for SuperCollider.
// Copyright (C) 2005-2012 Stefan Kersten.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License as
// published by the Free Software Foundation; either version 2 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
// 02111-1307 USA
//-------------------------------------------------------------------

// If other than 'faust2sc --prefix Faust' is used, sed this as well:
#if !defined(SC_FAUST_PREFIX)
#define SC_FAUST_PREFIX "Faust"
#endif

#include <map>
#include <string>
#include <string.h>
#include <SC_PlugIn.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __dsp__
#define __dsp__

#include <string>

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

class UI;
struct Meta;

/**
 * DSP memory manager.
 */

struct dsp_memory_manager {
    
    virtual ~dsp_memory_manager() {}
    
    virtual void* allocate(size_t size) = 0;
    virtual void destroy(void* ptr) = 0;
    
};

/**
* Signal processor definition.
*/

class dsp {

    public:

        dsp() {}
        virtual ~dsp() {}

        /* Return instance number of audio inputs */
        virtual int getNumInputs() = 0;
    
        /* Return instance number of audio outputs */
        virtual int getNumOutputs() = 0;
    
        /**
         * Trigger the ui_interface parameter with instance specific calls
         * to 'addBtton', 'addVerticalSlider'... in order to build the UI.
         *
         * @param ui_interface - the user interface builder
         */
        virtual void buildUserInterface(UI* ui_interface) = 0;
    
        /* Returns the sample rate currently used by the instance */
        virtual int getSampleRate() = 0;
    
        /**
         * Global init, calls the following methods:
         * - static class 'classInit': static tables initialization
         * - 'instanceInit': constants and instance state initialization
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void init(int samplingRate) = 0;

        /**
         * Init instance state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceInit(int samplingRate) = 0;

        /**
         * Init instance constant state
         *
         * @param samplingRate - the sampling rate in Hertz
         */
        virtual void instanceConstants(int samplingRate) = 0;
    
        /* Init default control parameters values */
        virtual void instanceResetUserInterface() = 0;
    
        /* Init instance state (delay lines...) */
        virtual void instanceClear() = 0;
 
        /**
         * Return a clone of the instance.
         *
         * @return a copy of the instance on success, otherwise a null pointer.
         */
        virtual dsp* clone() = 0;
    
        /**
         * Trigger the Meta* parameter with instance specific calls to 'declare' (key, value) metadata.
         *
         * @param m - the Meta* meta user
         */
        virtual void metadata(Meta* m) = 0;
    
        /**
         * DSP instance computation, to be called with successive in/out audio buffers.
         *
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) = 0;
    
        /**
         * DSP instance computation: alternative method to be used by subclasses.
         *
         * @param date_usec - the timestamp in microsec given by audio driver.
         * @param count - the number of frames to compute
         * @param inputs - the input audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         * @param outputs - the output audio buffers as an array of non-interleaved FAUSTFLOAT samples (eiher float, double or quad)
         *
         */
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { compute(count, inputs, outputs); }
       
};

/**
 * Generic DSP decorator.
 */

class decorator_dsp : public dsp {

    protected:

        dsp* fDSP;

    public:

        decorator_dsp(dsp* dsp = 0):fDSP(dsp) {}
        virtual ~decorator_dsp() { delete fDSP; }

        virtual int getNumInputs() { return fDSP->getNumInputs(); }
        virtual int getNumOutputs() { return fDSP->getNumOutputs(); }
        virtual void buildUserInterface(UI* ui_interface) { fDSP->buildUserInterface(ui_interface); }
        virtual int getSampleRate() { return fDSP->getSampleRate(); }
        virtual void init(int samplingRate) { fDSP->init(samplingRate); }
        virtual void instanceInit(int samplingRate) { fDSP->instanceInit(samplingRate); }
        virtual void instanceConstants(int samplingRate) { fDSP->instanceConstants(samplingRate); }
        virtual void instanceResetUserInterface() { fDSP->instanceResetUserInterface(); }
        virtual void instanceClear() { fDSP->instanceClear(); }
        virtual decorator_dsp* clone() { return new decorator_dsp(fDSP->clone()); }
        virtual void metadata(Meta* m) { fDSP->metadata(m); }
        // Beware: subclasses usually have to overload the two 'compute' methods
        virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(count, inputs, outputs); }
        virtual void compute(double date_usec, int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) { fDSP->compute(date_usec, count, inputs, outputs); }
    
};

/**
 * DSP factory class.
 */

class dsp_factory {
    
    protected:
    
        // So that to force sub-classes to use deleteDSPFactory(dsp_factory* factory);
        virtual ~dsp_factory() {}
    
    public:
    
        virtual std::string getName() = 0;
        virtual std::string getSHAKey() = 0;
        virtual std::string getDSPCode() = 0;
    
        virtual dsp* createDSPInstance() = 0;
    
        virtual void setMemoryManager(dsp_memory_manager* manager) = 0;
        virtual dsp_memory_manager* getMemoryManager() = 0;
    
};

/**
 * On Intel set FZ (Flush to Zero) and DAZ (Denormals Are Zero)
 * flags to avoid costly denormals.
 */

#ifdef __SSE__
    #include <xmmintrin.h>
    #ifdef __SSE2__
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8040)
    #else
        #define AVOIDDENORMALS _mm_setcsr(_mm_getcsr() | 0x8000)
    #endif
#else
    #define AVOIDDENORMALS
#endif

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __UI_H__
#define __UI_H__

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif

/*******************************************************************************
 * UI : Faust DSP User Interface
 * User Interface as expected by the buildUserInterface() method of a DSP.
 * This abstract class contains only the method that the Faust compiler can
 * generate to describe a DSP user interface.
 ******************************************************************************/

struct Soundfile;

class UI
{

    public:

        UI() {}

        virtual ~UI() {}

        // -- widget's layouts

        virtual void openTabBox(const char* label) = 0;
        virtual void openHorizontalBox(const char* label) = 0;
        virtual void openVerticalBox(const char* label) = 0;
        virtual void closeBox() = 0;

        // -- active widgets

        virtual void addButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addCheckButton(const char* label, FAUSTFLOAT* zone) = 0;
        virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;
        virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step) = 0;

        // -- passive widgets

        virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
        virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) = 0;
    
        // -- soundfiles
    
        virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) = 0;

        // -- metadata declarations

        virtual void declare(FAUSTFLOAT*, const char*, const char*) {}
};

#endif
/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/
 
#ifndef __misc__
#define __misc__

#include <algorithm>
#include <map>
#include <string.h>
#include <stdlib.h>

/************************************************************************
 FAUST Architecture File
 Copyright (C) 2003-2017 GRAME, Centre National de Creation Musicale
 ---------------------------------------------------------------------
 This Architecture section is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public License
 as published by the Free Software Foundation; either version 3 of
 the License, or (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program; If not, see <http://www.gnu.org/licenses/>.
 
 EXCEPTION : As a special exception, you may create a larger work
 that contains this FAUST architecture section and distribute
 that work under terms of your choice, so long as this FAUST
 architecture section is not modified.
 ************************************************************************/

#ifndef __meta__
#define __meta__

struct Meta
{
    virtual void declare(const char* key, const char* value) = 0;
    virtual ~Meta() {};
};

#endif

using std::max;
using std::min;

struct XXXX_Meta : std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

struct MY_Meta : Meta, std::map<const char*, const char*>
{
    void declare(const char* key, const char* value) { (*this)[key]=value; }
};

inline int lsr(int x, int n)	{ return int(((unsigned int)x) >> n); }

inline int int2pow2(int x)		{ int r = 0; while ((1<<r) < x) r++; return r; }

inline long lopt(char* argv[], const char* name, long def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return atoi(argv[i+1]);
	return def;
}

inline bool isopt(char* argv[], const char* name)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return true;
	return false;
}

inline const char* lopts(char* argv[], const char* name, const char* def)
{
	int	i;
	for (i = 0; argv[i]; i++) if (!strcmp(argv[i], name)) return argv[i+1];
	return def;
}

#endif


using namespace std;

#if defined(__GNUC__) && __GNUC__ >= 4
    #define FAUST_EXPORT __attribute__((visibility("default")))
#else
    #define FAUST_EXPORT  SC_API_EXPORT
#endif

#ifdef WIN32
    #define STRDUP _strdup
#else
    #define STRDUP strdup
#endif

//----------------------------------------------------------------------------
// Vector intrinsics
//----------------------------------------------------------------------------


//----------------------------------------------------------------------------
// Metadata
//----------------------------------------------------------------------------

class MetaData : public Meta
               , public std::map<std::string, std::string>
{
public:
    void declare(const char* key, const char* value)
    {
        (*this)[key] = value;
    }
};

//----------------------------------------------------------------------------
// Control counter
//----------------------------------------------------------------------------

class ControlCounter : public UI
{
public:
    ControlCounter()
        : mNumControlInputs(0),
          mNumControlOutputs(0)
    {}

    size_t getNumControls() const { return getNumControlInputs(); }
    size_t getNumControlInputs() const { return mNumControlInputs; }
    size_t getNumControlOutputs() const { return mNumControlOutputs; }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addControlInput(); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addControlInput(); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max)
    { addControlOutput(); }
    
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

protected:
    void addControlInput() { mNumControlInputs++; }
    void addControlOutput() { mNumControlOutputs++; }

private:
    size_t mNumControlInputs;
    size_t mNumControlOutputs;
};

//----------------------------------------------------------------------------
// UI control
//----------------------------------------------------------------------------

struct Control
{
    typedef void (*UpdateFunction)(Control* self, FAUSTFLOAT value);

    UpdateFunction updateFunction;
    FAUSTFLOAT* zone;
    FAUSTFLOAT min, max;

    inline void update(FAUSTFLOAT value)
    {
        (*updateFunction)(this, value);
    }

    static void simpleUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = value;
    }
    static void boundedUpdate(Control* self, FAUSTFLOAT value)
    {
        *self->zone = sc_clip(value, self->min, self->max);
    }
};

//----------------------------------------------------------------------------
// Control allocator
//----------------------------------------------------------------------------

class ControlAllocator : public UI
{
public:
    ControlAllocator(Control* controls)
        : mControls(controls)
    { }

    // Layout widgets
    virtual void openTabBox(const char* label) { }
    virtual void openHorizontalBox(const char* label) { }
    virtual void openVerticalBox(const char* label) { }
    virtual void closeBox() { }

    // Active widgets
    virtual void addButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addCheckButton(const char* label, FAUSTFLOAT* zone)
    { addSimpleControl(zone); }
    virtual void addVerticalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addHorizontalSlider(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }
    virtual void addNumEntry(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT init, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    { addBoundedControl(zone, min, max, step); }

    // Passive widgets
    virtual void addHorizontalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addVerticalBargraph(const char* label, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max) {}
    virtual void addSoundfile(const char* label, const char* filename, Soundfile** sf_zone) {}

private:
    void addControl(Control::UpdateFunction updateFunction, FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT /* step */)
    {
        Control* ctrl        = mControls++;
        ctrl->updateFunction = updateFunction;
        ctrl->zone           = zone;
        ctrl->min            = min;
        ctrl->max            = max;
    }
    void addSimpleControl(FAUSTFLOAT* zone)
    {
        addControl(Control::simpleUpdate, zone, 0.f, 0.f, 0.f);
    }
    void addBoundedControl(FAUSTFLOAT* zone, FAUSTFLOAT min, FAUSTFLOAT max, FAUSTFLOAT step)
    {
        addControl(Control::boundedUpdate, zone, min, max, step);
    }

private:
    Control* mControls;
};

//----------------------------------------------------------------------------
// FAUST generated code
//----------------------------------------------------------------------------

#ifndef FAUSTFLOAT
#define FAUSTFLOAT float
#endif 

#include <cmath>
#include <math.h>

float mydsp_faustpower2_f(float value) {
	return (value * value);
	
}

#ifndef FAUSTCLASS 
#define FAUSTCLASS mydsp
#endif
#ifdef __APPLE__ 
#define exp10f __exp10f
#define exp10 __exp10
#endif

class mydsp : public dsp {
	
 private:
	
	FAUSTFLOAT fCheckbox0;
	FAUSTFLOAT fHslider0;
	float fRec0[2];
	int fSamplingFreq;
	int iConst0;
	float fConst1;
	FAUSTFLOAT fHslider1;
	float fRec1[2];
	float fRec2[3];
	FAUSTFLOAT fHslider2;
	float fRec3[2];
	float fConst2;
	float fConst3;
	float fConst4;
	float fConst5;
	float fConst6;
	float fConst7;
	float fConst8;
	float fConst9;
	float fConst10;
	float fRec19[3];
	float fRec20[3];
	float fRec21[3];
	float fRec22[3];
	float fRec23[3];
	float fRec24[3];
	float fRec25[3];
	float fRec26[3];
	float fRec27[3];
	float fRec28[3];
	float fRec29[3];
	float fConst11;
	float fConst12;
	float fRec18[2];
	float fRec16[2];
	float fRec15[2];
	float fRec13[2];
	float fRec12[2];
	float fRec10[2];
	float fRec9[2];
	float fRec7[2];
	float fRec6[2];
	float fRec4[2];
	float fConst13;
	float fConst14;
	float fConst15;
	float fConst16;
	float fConst17;
	float fRec42[3];
	float fRec43[3];
	float fRec44[3];
	float fRec45[3];
	float fRec46[3];
	float fRec47[3];
	float fRec48[3];
	float fRec49[3];
	float fRec50[3];
	float fConst18;
	float fConst19;
	float fRec41[2];
	float fRec39[2];
	float fRec38[2];
	float fRec36[2];
	float fRec35[2];
	float fRec33[2];
	float fRec32[2];
	float fRec30[2];
	float fConst20;
	float fConst21;
	float fConst22;
	float fConst23;
	float fRec60[3];
	float fRec61[3];
	float fRec62[3];
	float fRec63[3];
	float fRec64[3];
	float fRec65[3];
	float fRec66[3];
	float fConst24;
	float fConst25;
	float fRec59[2];
	float fRec57[2];
	float fRec56[2];
	float fRec54[2];
	float fRec53[2];
	float fRec51[2];
	float fConst26;
	float fConst27;
	float fRec70[3];
	float fRec71[3];
	float fRec72[3];
	float fConst28;
	float fRec69[2];
	float fRec67[2];
	float fConst29;
	float fConst30;
	float fRec79[3];
	float fRec80[3];
	float fRec81[3];
	float fRec82[3];
	float fRec83[3];
	float fConst31;
	float fConst32;
	float fRec78[2];
	float fRec76[2];
	float fRec75[2];
	float fRec73[2];
	int IOTA;
	float fVec0[2048];
	int iConst33;
	float fConst34;
	float fConst35;
	float fConst36;
	float fConst37;
	float fConst38;
	float fConst39;
	float fConst40;
	float fConst41;
	float fConst42;
	float fRec98[2];
	float fRec96[2];
	float fRec95[2];
	float fRec93[2];
	float fRec92[2];
	float fRec90[2];
	float fRec89[2];
	float fRec87[2];
	float fRec86[2];
	float fRec84[2];
	float fConst43;
	float fConst44;
	float fConst45;
	float fConst46;
	float fConst47;
	float fConst48;
	float fConst49;
	float fRec110[2];
	float fRec108[2];
	float fRec107[2];
	float fRec105[2];
	float fRec104[2];
	float fRec102[2];
	float fRec101[2];
	float fRec99[2];
	float fConst50;
	float fConst51;
	float fConst52;
	float fConst53;
	float fConst54;
	float fConst55;
	float fRec119[2];
	float fRec117[2];
	float fRec116[2];
	float fRec114[2];
	float fRec113[2];
	float fRec111[2];
	float fConst56;
	float fConst57;
	float fConst58;
	float fRec122[2];
	float fRec120[2];
	float fConst59;
	float fConst60;
	float fConst61;
	float fConst62;
	float fRec128[2];
	float fRec126[2];
	float fRec125[2];
	float fRec123[2];
	float fVec1[2048];
	int iConst63;
	float fConst64;
	float fConst65;
	float fConst66;
	float fConst67;
	float fConst68;
	float fConst69;
	float fConst70;
	float fConst71;
	float fConst72;
	float fRec143[2];
	float fRec141[2];
	float fRec140[2];
	float fRec138[2];
	float fRec137[2];
	float fRec135[2];
	float fRec134[2];
	float fRec132[2];
	float fRec131[2];
	float fRec129[2];
	float fConst73;
	float fConst74;
	float fConst75;
	float fConst76;
	float fConst77;
	float fConst78;
	float fConst79;
	float fRec155[2];
	float fRec153[2];
	float fRec152[2];
	float fRec150[2];
	float fRec149[2];
	float fRec147[2];
	float fRec146[2];
	float fRec144[2];
	float fConst80;
	float fConst81;
	float fConst82;
	float fConst83;
	float fConst84;
	float fConst85;
	float fRec164[2];
	float fRec162[2];
	float fRec161[2];
	float fRec159[2];
	float fRec158[2];
	float fRec156[2];
	float fConst86;
	float fConst87;
	float fConst88;
	float fRec167[2];
	float fRec165[2];
	float fConst89;
	float fConst90;
	float fConst91;
	float fConst92;
	float fRec173[2];
	float fRec171[2];
	float fRec170[2];
	float fRec168[2];
	float fVec2[2048];
	int iConst93;
	float fRec188[2];
	float fRec186[2];
	float fRec185[2];
	float fRec183[2];
	float fRec182[2];
	float fRec180[2];
	float fRec179[2];
	float fRec177[2];
	float fRec176[2];
	float fRec174[2];
	float fRec200[2];
	float fRec198[2];
	float fRec197[2];
	float fRec195[2];
	float fRec194[2];
	float fRec192[2];
	float fRec191[2];
	float fRec189[2];
	float fRec209[2];
	float fRec207[2];
	float fRec206[2];
	float fRec204[2];
	float fRec203[2];
	float fRec201[2];
	float fRec212[2];
	float fRec210[2];
	float fRec218[2];
	float fRec216[2];
	float fRec215[2];
	float fRec213[2];
	float fVec3[2048];
	float fRec233[2];
	float fRec231[2];
	float fRec230[2];
	float fRec228[2];
	float fRec227[2];
	float fRec225[2];
	float fRec224[2];
	float fRec222[2];
	float fRec221[2];
	float fRec219[2];
	float fRec245[2];
	float fRec243[2];
	float fRec242[2];
	float fRec240[2];
	float fRec239[2];
	float fRec237[2];
	float fRec236[2];
	float fRec234[2];
	float fRec254[2];
	float fRec252[2];
	float fRec251[2];
	float fRec249[2];
	float fRec248[2];
	float fRec246[2];
	float fRec257[2];
	float fRec255[2];
	float fRec263[2];
	float fRec261[2];
	float fRec260[2];
	float fRec258[2];
	float fVec4[2048];
	float fRec278[2];
	float fRec276[2];
	float fRec275[2];
	float fRec273[2];
	float fRec272[2];
	float fRec270[2];
	float fRec269[2];
	float fRec267[2];
	float fRec266[2];
	float fRec264[2];
	float fRec290[2];
	float fRec288[2];
	float fRec287[2];
	float fRec285[2];
	float fRec284[2];
	float fRec282[2];
	float fRec281[2];
	float fRec279[2];
	float fRec299[2];
	float fRec297[2];
	float fRec296[2];
	float fRec294[2];
	float fRec293[2];
	float fRec291[2];
	float fRec302[2];
	float fRec300[2];
	float fRec308[2];
	float fRec306[2];
	float fRec305[2];
	float fRec303[2];
	float fVec5[2048];
	float fRec323[2];
	float fRec321[2];
	float fRec320[2];
	float fRec318[2];
	float fRec317[2];
	float fRec315[2];
	float fRec314[2];
	float fRec312[2];
	float fRec311[2];
	float fRec309[2];
	float fRec335[2];
	float fRec333[2];
	float fRec332[2];
	float fRec330[2];
	float fRec329[2];
	float fRec327[2];
	float fRec326[2];
	float fRec324[2];
	float fRec344[2];
	float fRec342[2];
	float fRec341[2];
	float fRec339[2];
	float fRec338[2];
	float fRec336[2];
	float fRec347[2];
	float fRec345[2];
	float fRec353[2];
	float fRec351[2];
	float fRec350[2];
	float fRec348[2];
	float fVec6[2048];
	float fConst94;
	float fConst95;
	float fConst96;
	float fConst97;
	float fConst98;
	float fConst99;
	float fConst100;
	float fConst101;
	float fConst102;
	float fRec368[2];
	float fRec366[2];
	float fRec365[2];
	float fRec363[2];
	float fRec362[2];
	float fRec360[2];
	float fRec359[2];
	float fRec357[2];
	float fRec356[2];
	float fRec354[2];
	float fConst103;
	float fConst104;
	float fConst105;
	float fConst106;
	float fConst107;
	float fConst108;
	float fConst109;
	float fRec380[2];
	float fRec378[2];
	float fRec377[2];
	float fRec375[2];
	float fRec374[2];
	float fRec372[2];
	float fRec371[2];
	float fRec369[2];
	float fConst110;
	float fConst111;
	float fConst112;
	float fConst113;
	float fConst114;
	float fConst115;
	float fRec389[2];
	float fRec387[2];
	float fRec386[2];
	float fRec384[2];
	float fRec383[2];
	float fRec381[2];
	float fConst116;
	float fConst117;
	float fConst118;
	float fRec392[2];
	float fRec390[2];
	float fConst119;
	float fConst120;
	float fConst121;
	float fConst122;
	float fRec398[2];
	float fRec396[2];
	float fRec395[2];
	float fRec393[2];
	float fVec7[1024];
	int iConst123;
	float fConst124;
	float fConst125;
	float fConst126;
	float fConst127;
	float fConst128;
	float fConst129;
	float fConst130;
	float fRec410[2];
	float fRec408[2];
	float fRec407[2];
	float fRec405[2];
	float fRec404[2];
	float fRec402[2];
	float fRec401[2];
	float fRec399[2];
	float fConst131;
	float fConst132;
	float fConst133;
	float fConst134;
	float fConst135;
	float fConst136;
	float fRec419[2];
	float fRec417[2];
	float fRec416[2];
	float fRec414[2];
	float fRec413[2];
	float fRec411[2];
	float fConst137;
	float fConst138;
	float fConst139;
	float fConst140;
	float fRec425[2];
	float fRec423[2];
	float fRec422[2];
	float fRec420[2];
	float fConst141;
	float fConst142;
	float fConst143;
	float fConst144;
	float fConst145;
	float fConst146;
	float fConst147;
	float fConst148;
	float fConst149;
	float fRec440[2];
	float fRec438[2];
	float fRec437[2];
	float fRec435[2];
	float fRec434[2];
	float fRec432[2];
	float fRec431[2];
	float fRec429[2];
	float fRec428[2];
	float fRec426[2];
	float fConst150;
	float fConst151;
	float fConst152;
	float fRec443[2];
	float fRec441[2];
	float fVec8[1024];
	int iConst153;
	float fRec458[2];
	float fRec456[2];
	float fRec455[2];
	float fRec453[2];
	float fRec452[2];
	float fRec450[2];
	float fRec449[2];
	float fRec447[2];
	float fRec446[2];
	float fRec444[2];
	float fRec470[2];
	float fRec468[2];
	float fRec467[2];
	float fRec465[2];
	float fRec464[2];
	float fRec462[2];
	float fRec461[2];
	float fRec459[2];
	float fRec479[2];
	float fRec477[2];
	float fRec476[2];
	float fRec474[2];
	float fRec473[2];
	float fRec471[2];
	float fRec482[2];
	float fRec480[2];
	float fRec488[2];
	float fRec486[2];
	float fRec485[2];
	float fRec483[2];
	float fVec9[1024];
	float fRec503[2];
	float fRec501[2];
	float fRec500[2];
	float fRec498[2];
	float fRec497[2];
	float fRec495[2];
	float fRec494[2];
	float fRec492[2];
	float fRec491[2];
	float fRec489[2];
	float fRec515[2];
	float fRec513[2];
	float fRec512[2];
	float fRec510[2];
	float fRec509[2];
	float fRec507[2];
	float fRec506[2];
	float fRec504[2];
	float fRec524[2];
	float fRec522[2];
	float fRec521[2];
	float fRec519[2];
	float fRec518[2];
	float fRec516[2];
	float fRec527[2];
	float fRec525[2];
	float fRec533[2];
	float fRec531[2];
	float fRec530[2];
	float fRec528[2];
	float fVec10[1024];
	float fRec548[2];
	float fRec546[2];
	float fRec545[2];
	float fRec543[2];
	float fRec542[2];
	float fRec540[2];
	float fRec539[2];
	float fRec537[2];
	float fRec536[2];
	float fRec534[2];
	float fRec560[2];
	float fRec558[2];
	float fRec557[2];
	float fRec555[2];
	float fRec554[2];
	float fRec552[2];
	float fRec551[2];
	float fRec549[2];
	float fRec569[2];
	float fRec567[2];
	float fRec566[2];
	float fRec564[2];
	float fRec563[2];
	float fRec561[2];
	float fRec572[2];
	float fRec570[2];
	float fRec578[2];
	float fRec576[2];
	float fRec575[2];
	float fRec573[2];
	float fVec11[1024];
	float fRec593[2];
	float fRec591[2];
	float fRec590[2];
	float fRec588[2];
	float fRec587[2];
	float fRec585[2];
	float fRec584[2];
	float fRec582[2];
	float fRec581[2];
	float fRec579[2];
	float fRec605[2];
	float fRec603[2];
	float fRec602[2];
	float fRec600[2];
	float fRec599[2];
	float fRec597[2];
	float fRec596[2];
	float fRec594[2];
	float fRec614[2];
	float fRec612[2];
	float fRec611[2];
	float fRec609[2];
	float fRec608[2];
	float fRec606[2];
	float fRec617[2];
	float fRec615[2];
	float fRec623[2];
	float fRec621[2];
	float fRec620[2];
	float fRec618[2];
	float fVec12[1024];
	float fRec638[2];
	float fRec636[2];
	float fRec635[2];
	float fRec633[2];
	float fRec632[2];
	float fRec630[2];
	float fRec629[2];
	float fRec627[2];
	float fRec626[2];
	float fRec624[2];
	float fRec650[2];
	float fRec648[2];
	float fRec647[2];
	float fRec645[2];
	float fRec644[2];
	float fRec642[2];
	float fRec641[2];
	float fRec639[2];
	float fRec659[2];
	float fRec657[2];
	float fRec656[2];
	float fRec654[2];
	float fRec653[2];
	float fRec651[2];
	float fRec662[2];
	float fRec660[2];
	float fRec668[2];
	float fRec666[2];
	float fRec665[2];
	float fRec663[2];
	float fVec13[1024];
	float fRec683[2];
	float fRec681[2];
	float fRec680[2];
	float fRec678[2];
	float fRec677[2];
	float fRec675[2];
	float fRec674[2];
	float fRec672[2];
	float fRec671[2];
	float fRec669[2];
	float fRec695[2];
	float fRec693[2];
	float fRec692[2];
	float fRec690[2];
	float fRec689[2];
	float fRec687[2];
	float fRec686[2];
	float fRec684[2];
	float fRec704[2];
	float fRec702[2];
	float fRec701[2];
	float fRec699[2];
	float fRec698[2];
	float fRec696[2];
	float fRec707[2];
	float fRec705[2];
	float fRec713[2];
	float fRec711[2];
	float fRec710[2];
	float fRec708[2];
	float fVec14[1024];
	float fRec728[2];
	float fRec726[2];
	float fRec725[2];
	float fRec723[2];
	float fRec722[2];
	float fRec720[2];
	float fRec719[2];
	float fRec717[2];
	float fRec716[2];
	float fRec714[2];
	float fRec740[2];
	float fRec738[2];
	float fRec737[2];
	float fRec735[2];
	float fRec734[2];
	float fRec732[2];
	float fRec731[2];
	float fRec729[2];
	float fRec749[2];
	float fRec747[2];
	float fRec746[2];
	float fRec744[2];
	float fRec743[2];
	float fRec741[2];
	float fRec752[2];
	float fRec750[2];
	float fRec758[2];
	float fRec756[2];
	float fRec755[2];
	float fRec753[2];
	float fVec15[1024];
	float fRec773[2];
	float fRec771[2];
	float fRec770[2];
	float fRec768[2];
	float fRec767[2];
	float fRec765[2];
	float fRec764[2];
	float fRec762[2];
	float fRec761[2];
	float fRec759[2];
	float fRec785[2];
	float fRec783[2];
	float fRec782[2];
	float fRec780[2];
	float fRec779[2];
	float fRec777[2];
	float fRec776[2];
	float fRec774[2];
	float fRec794[2];
	float fRec792[2];
	float fRec791[2];
	float fRec789[2];
	float fRec788[2];
	float fRec786[2];
	float fRec800[2];
	float fRec798[2];
	float fRec797[2];
	float fRec795[2];
	float fRec803[2];
	float fRec801[2];
	float fVec16[1024];
	float fRec812[2];
	float fRec810[2];
	float fRec809[2];
	float fRec807[2];
	float fRec806[2];
	float fRec804[2];
	float fRec818[2];
	float fRec816[2];
	float fRec815[2];
	float fRec813[2];
	float fRec821[2];
	float fRec819[2];
	float fRec833[2];
	float fRec831[2];
	float fRec830[2];
	float fRec828[2];
	float fRec827[2];
	float fRec825[2];
	float fRec824[2];
	float fRec822[2];
	float fRec848[2];
	float fRec846[2];
	float fRec845[2];
	float fRec843[2];
	float fRec842[2];
	float fRec840[2];
	float fRec839[2];
	float fRec837[2];
	float fRec836[2];
	float fRec834[2];
	float fVec17[1024];
	float fRec863[2];
	float fRec861[2];
	float fRec860[2];
	float fRec858[2];
	float fRec857[2];
	float fRec855[2];
	float fRec854[2];
	float fRec852[2];
	float fRec851[2];
	float fRec849[2];
	float fRec875[2];
	float fRec873[2];
	float fRec872[2];
	float fRec870[2];
	float fRec869[2];
	float fRec867[2];
	float fRec866[2];
	float fRec864[2];
	float fRec884[2];
	float fRec882[2];
	float fRec881[2];
	float fRec879[2];
	float fRec878[2];
	float fRec876[2];
	float fRec887[2];
	float fRec885[2];
	float fRec893[2];
	float fRec891[2];
	float fRec890[2];
	float fRec888[2];
	float fVec18[1024];
	float fConst154;
	float fConst155;
	float fConst156;
	float fConst157;
	float fConst158;
	float fConst159;
	float fConst160;
	float fConst161;
	float fConst162;
	float fRec908[2];
	float fRec906[2];
	float fRec905[2];
	float fRec903[2];
	float fRec902[2];
	float fRec900[2];
	float fRec899[2];
	float fRec897[2];
	float fRec896[2];
	float fRec894[2];
	float fConst163;
	float fConst164;
	float fConst165;
	float fConst166;
	float fConst167;
	float fConst168;
	float fConst169;
	float fRec920[2];
	float fRec918[2];
	float fRec917[2];
	float fRec915[2];
	float fRec914[2];
	float fRec912[2];
	float fRec911[2];
	float fRec909[2];
	float fConst170;
	float fConst171;
	float fConst172;
	float fConst173;
	float fConst174;
	float fConst175;
	float fRec929[2];
	float fRec927[2];
	float fRec926[2];
	float fRec924[2];
	float fRec923[2];
	float fRec921[2];
	float fConst176;
	float fConst177;
	float fConst178;
	float fConst179;
	float fRec935[2];
	float fRec933[2];
	float fRec932[2];
	float fRec930[2];
	float fConst180;
	float fConst181;
	float fConst182;
	float fRec938[2];
	float fRec936[2];
	float fVec19[3];
	int iConst183;
	float fConst184;
	float fConst185;
	float fConst186;
	float fConst187;
	float fConst188;
	float fConst189;
	float fRec947[2];
	float fRec945[2];
	float fRec944[2];
	float fRec942[2];
	float fRec941[2];
	float fRec939[2];
	float fConst190;
	float fConst191;
	float fConst192;
	float fConst193;
	float fRec953[2];
	float fRec951[2];
	float fRec950[2];
	float fRec948[2];
	float fConst194;
	float fConst195;
	float fConst196;
	float fRec956[2];
	float fRec954[2];
	float fConst197;
	float fConst198;
	float fConst199;
	float fConst200;
	float fConst201;
	float fConst202;
	float fConst203;
	float fConst204;
	float fConst205;
	float fRec971[2];
	float fRec969[2];
	float fRec968[2];
	float fRec966[2];
	float fRec965[2];
	float fRec963[2];
	float fRec962[2];
	float fRec960[2];
	float fRec959[2];
	float fRec957[2];
	float fConst206;
	float fConst207;
	float fConst208;
	float fConst209;
	float fConst210;
	float fConst211;
	float fConst212;
	float fRec983[2];
	float fRec981[2];
	float fRec980[2];
	float fRec978[2];
	float fRec977[2];
	float fRec975[2];
	float fRec974[2];
	float fRec972[2];
	float fRec998[2];
	float fRec996[2];
	float fRec995[2];
	float fRec993[2];
	float fRec992[2];
	float fRec990[2];
	float fRec989[2];
	float fRec987[2];
	float fRec986[2];
	float fRec984[2];
	float fRec1010[2];
	float fRec1008[2];
	float fRec1007[2];
	float fRec1005[2];
	float fRec1004[2];
	float fRec1002[2];
	float fRec1001[2];
	float fRec999[2];
	float fRec1019[2];
	float fRec1017[2];
	float fRec1016[2];
	float fRec1014[2];
	float fRec1013[2];
	float fRec1011[2];
	float fRec1025[2];
	float fRec1023[2];
	float fRec1022[2];
	float fRec1020[2];
	float fRec1028[2];
	float fRec1026[2];
	float fRec1043[2];
	float fRec1041[2];
	float fRec1040[2];
	float fRec1038[2];
	float fRec1037[2];
	float fRec1035[2];
	float fRec1034[2];
	float fRec1032[2];
	float fRec1031[2];
	float fRec1029[2];
	float fRec1055[2];
	float fRec1053[2];
	float fRec1052[2];
	float fRec1050[2];
	float fRec1049[2];
	float fRec1047[2];
	float fRec1046[2];
	float fRec1044[2];
	float fRec1064[2];
	float fRec1062[2];
	float fRec1061[2];
	float fRec1059[2];
	float fRec1058[2];
	float fRec1056[2];
	float fRec1070[2];
	float fRec1068[2];
	float fRec1067[2];
	float fRec1065[2];
	float fRec1073[2];
	float fRec1071[2];
	float fVec20[3];
	float fRec1088[2];
	float fRec1086[2];
	float fRec1085[2];
	float fRec1083[2];
	float fRec1082[2];
	float fRec1080[2];
	float fRec1079[2];
	float fRec1077[2];
	float fRec1076[2];
	float fRec1074[2];
	float fRec1100[2];
	float fRec1098[2];
	float fRec1097[2];
	float fRec1095[2];
	float fRec1094[2];
	float fRec1092[2];
	float fRec1091[2];
	float fRec1089[2];
	float fRec1109[2];
	float fRec1107[2];
	float fRec1106[2];
	float fRec1104[2];
	float fRec1103[2];
	float fRec1101[2];
	float fRec1115[2];
	float fRec1113[2];
	float fRec1112[2];
	float fRec1110[2];
	float fRec1118[2];
	float fRec1116[2];
	float fRec1133[2];
	float fRec1131[2];
	float fRec1130[2];
	float fRec1128[2];
	float fRec1127[2];
	float fRec1125[2];
	float fRec1124[2];
	float fRec1122[2];
	float fRec1121[2];
	float fRec1119[2];
	float fRec1145[2];
	float fRec1143[2];
	float fRec1142[2];
	float fRec1140[2];
	float fRec1139[2];
	float fRec1137[2];
	float fRec1136[2];
	float fRec1134[2];
	float fRec1154[2];
	float fRec1152[2];
	float fRec1151[2];
	float fRec1149[2];
	float fRec1148[2];
	float fRec1146[2];
	float fRec1160[2];
	float fRec1158[2];
	float fRec1157[2];
	float fRec1155[2];
	float fRec1163[2];
	float fRec1161[2];
	float fRec1178[2];
	float fRec1176[2];
	float fRec1175[2];
	float fRec1173[2];
	float fRec1172[2];
	float fRec1170[2];
	float fRec1169[2];
	float fRec1167[2];
	float fRec1166[2];
	float fRec1164[2];
	float fRec1190[2];
	float fRec1188[2];
	float fRec1187[2];
	float fRec1185[2];
	float fRec1184[2];
	float fRec1182[2];
	float fRec1181[2];
	float fRec1179[2];
	float fRec1199[2];
	float fRec1197[2];
	float fRec1196[2];
	float fRec1194[2];
	float fRec1193[2];
	float fRec1191[2];
	float fRec1205[2];
	float fRec1203[2];
	float fRec1202[2];
	float fRec1200[2];
	float fRec1208[2];
	float fRec1206[2];
	float fVec21[3];
	float fRec1217[2];
	float fRec1215[2];
	float fRec1214[2];
	float fRec1212[2];
	float fRec1211[2];
	float fRec1209[2];
	float fRec1223[2];
	float fRec1221[2];
	float fRec1220[2];
	float fRec1218[2];
	float fRec1226[2];
	float fRec1224[2];
	float fRec1241[2];
	float fRec1239[2];
	float fRec1238[2];
	float fRec1236[2];
	float fRec1235[2];
	float fRec1233[2];
	float fRec1232[2];
	float fRec1230[2];
	float fRec1229[2];
	float fRec1227[2];
	float fRec1253[2];
	float fRec1251[2];
	float fRec1250[2];
	float fRec1248[2];
	float fRec1247[2];
	float fRec1245[2];
	float fRec1244[2];
	float fRec1242[2];
	float fRec1268[2];
	float fRec1266[2];
	float fRec1265[2];
	float fRec1263[2];
	float fRec1262[2];
	float fRec1260[2];
	float fRec1259[2];
	float fRec1257[2];
	float fRec1256[2];
	float fRec1254[2];
	float fRec1280[2];
	float fRec1278[2];
	float fRec1277[2];
	float fRec1275[2];
	float fRec1274[2];
	float fRec1272[2];
	float fRec1271[2];
	float fRec1269[2];
	float fRec1289[2];
	float fRec1287[2];
	float fRec1286[2];
	float fRec1284[2];
	float fRec1283[2];
	float fRec1281[2];
	float fRec1295[2];
	float fRec1293[2];
	float fRec1292[2];
	float fRec1290[2];
	float fRec1298[2];
	float fRec1296[2];
	float fRec1313[2];
	float fRec1311[2];
	float fRec1310[2];
	float fRec1308[2];
	float fRec1307[2];
	float fRec1305[2];
	float fRec1304[2];
	float fRec1302[2];
	float fRec1301[2];
	float fRec1299[2];
	float fRec1325[2];
	float fRec1323[2];
	float fRec1322[2];
	float fRec1320[2];
	float fRec1319[2];
	float fRec1317[2];
	float fRec1316[2];
	float fRec1314[2];
	float fRec1334[2];
	float fRec1332[2];
	float fRec1331[2];
	float fRec1329[2];
	float fRec1328[2];
	float fRec1326[2];
	float fRec1340[2];
	float fRec1338[2];
	float fRec1337[2];
	float fRec1335[2];
	float fRec1343[2];
	float fRec1341[2];
	float fVec22[3];
	float fRec1358[2];
	float fRec1356[2];
	float fRec1355[2];
	float fRec1353[2];
	float fRec1352[2];
	float fRec1350[2];
	float fRec1349[2];
	float fRec1347[2];
	float fRec1346[2];
	float fRec1344[2];
	float fRec1370[2];
	float fRec1368[2];
	float fRec1367[2];
	float fRec1365[2];
	float fRec1364[2];
	float fRec1362[2];
	float fRec1361[2];
	float fRec1359[2];
	float fRec1379[2];
	float fRec1377[2];
	float fRec1376[2];
	float fRec1374[2];
	float fRec1373[2];
	float fRec1371[2];
	float fRec1385[2];
	float fRec1383[2];
	float fRec1382[2];
	float fRec1380[2];
	float fRec1388[2];
	float fRec1386[2];
	float fRec1391[2];
	float fRec1389[2];
	float fRec1406[2];
	float fRec1404[2];
	float fRec1403[2];
	float fRec1401[2];
	float fRec1400[2];
	float fRec1398[2];
	float fRec1397[2];
	float fRec1395[2];
	float fRec1394[2];
	float fRec1392[2];
	float fRec1418[2];
	float fRec1416[2];
	float fRec1415[2];
	float fRec1413[2];
	float fRec1412[2];
	float fRec1410[2];
	float fRec1409[2];
	float fRec1407[2];
	float fRec1424[2];
	float fRec1422[2];
	float fRec1421[2];
	float fRec1419[2];
	float fRec1433[2];
	float fRec1431[2];
	float fRec1430[2];
	float fRec1428[2];
	float fRec1427[2];
	float fRec1425[2];
	
 public:
	
	void metadata(Meta* m) { 
		m->declare("author", "AmbisonicDecoderToolkit");
		m->declare("copyright", "(c) Aaron J. Heller 2013");
		m->declare("license", "BSD 3-Clause License");
		m->declare("name", "SATOsw0o5");
		m->declare("version", "1.2");
	}

	virtual int getNumInputs() {
		return 36;
		
	}
	virtual int getNumOutputs() {
		return 31;
		
	}
	virtual int getInputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			case 31: {
				rate = 1;
				break;
			}
			case 32: {
				rate = 1;
				break;
			}
			case 33: {
				rate = 1;
				break;
			}
			case 34: {
				rate = 1;
				break;
			}
			case 35: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	virtual int getOutputRate(int channel) {
		int rate;
		switch (channel) {
			case 0: {
				rate = 1;
				break;
			}
			case 1: {
				rate = 1;
				break;
			}
			case 2: {
				rate = 1;
				break;
			}
			case 3: {
				rate = 1;
				break;
			}
			case 4: {
				rate = 1;
				break;
			}
			case 5: {
				rate = 1;
				break;
			}
			case 6: {
				rate = 1;
				break;
			}
			case 7: {
				rate = 1;
				break;
			}
			case 8: {
				rate = 1;
				break;
			}
			case 9: {
				rate = 1;
				break;
			}
			case 10: {
				rate = 1;
				break;
			}
			case 11: {
				rate = 1;
				break;
			}
			case 12: {
				rate = 1;
				break;
			}
			case 13: {
				rate = 1;
				break;
			}
			case 14: {
				rate = 1;
				break;
			}
			case 15: {
				rate = 1;
				break;
			}
			case 16: {
				rate = 1;
				break;
			}
			case 17: {
				rate = 1;
				break;
			}
			case 18: {
				rate = 1;
				break;
			}
			case 19: {
				rate = 1;
				break;
			}
			case 20: {
				rate = 1;
				break;
			}
			case 21: {
				rate = 1;
				break;
			}
			case 22: {
				rate = 1;
				break;
			}
			case 23: {
				rate = 1;
				break;
			}
			case 24: {
				rate = 1;
				break;
			}
			case 25: {
				rate = 1;
				break;
			}
			case 26: {
				rate = 1;
				break;
			}
			case 27: {
				rate = 1;
				break;
			}
			case 28: {
				rate = 1;
				break;
			}
			case 29: {
				rate = 1;
				break;
			}
			case 30: {
				rate = 1;
				break;
			}
			default: {
				rate = -1;
				break;
			}
			
		}
		return rate;
		
	}
	
	static void classInit(int samplingFreq) {
		
	}
	
	virtual void instanceConstants(int samplingFreq) {
		fSamplingFreq = samplingFreq;
		iConst0 = min(192000, max(1, fSamplingFreq));
		fConst1 = (3.14159274f / float(iConst0));
		fConst2 = float(iConst0);
		fConst3 = ((103.868385f / fConst2) + 1.0f);
		fConst4 = (0.0f - (207.736771f / (fConst2 * fConst3)));
		fConst5 = mydsp_faustpower2_f(fConst2);
		fConst6 = ((((11578.623f / fConst2) + 190.944473f) / fConst2) + 1.0f);
		fConst7 = (0.0f - (46314.4922f / (fConst5 * fConst6)));
		fConst8 = (0.0f - (((46314.4922f / fConst2) + 381.888947f) / (fConst2 * fConst6)));
		fConst9 = ((((14729.4043f / fConst2) + 132.425262f) / fConst2) + 1.0f);
		fConst10 = (1.0f / ((fConst3 * fConst6) * fConst9));
		fConst11 = (0.0f - (58917.6172f / (fConst5 * fConst9)));
		fConst12 = (0.0f - (((58917.6172f / fConst2) + 264.850525f) / (fConst2 * fConst9)));
		fConst13 = ((((7414.97852f / fConst2) + 164.98288f) / fConst2) + 1.0f);
		fConst14 = (0.0f - (29659.9141f / (fConst5 * fConst13)));
		fConst15 = (0.0f - (((29659.9141f / fConst2) + 329.965759f) / (fConst2 * fConst13)));
		fConst16 = ((((9319.53711f / fConst2) + 119.842537f) / fConst2) + 1.0f);
		fConst17 = (1.0f / (fConst13 * fConst16));
		fConst18 = (0.0f - (37278.1484f / (fConst5 * fConst16)));
		fConst19 = (0.0f - (((37278.1484f / fConst2) + 239.685074f) / (fConst2 * fConst16)));
		fConst20 = ((66.1417389f / fConst2) + 1.0f);
		fConst21 = (0.0f - (132.283478f / (fConst2 * fConst20)));
		fConst22 = ((((5240.24805f / fConst2) + 104.75351f) / fConst2) + 1.0f);
		fConst23 = (1.0f / (fConst20 * fConst22));
		fConst24 = (0.0f - (20960.9922f / (fConst5 * fConst22)));
		fConst25 = (0.0f - (((20960.9922f / fConst2) + 209.507019f) / (fConst2 * fConst22)));
		fConst26 = ((28.482542f / fConst2) + 1.0f);
		fConst27 = (1.0f / fConst26);
		fConst28 = (0.0f - (56.9650841f / (fConst2 * fConst26)));
		fConst29 = ((((2433.76538f / fConst2) + 85.4476242f) / fConst2) + 1.0f);
		fConst30 = (1.0f / fConst29);
		fConst31 = (0.0f - (9735.06152f / (fConst5 * fConst29)));
		fConst32 = (0.0f - (((9735.06152f / fConst2) + 170.895248f) / (fConst2 * fConst29)));
		iConst33 = int(((0.0106405718f * float(iConst0)) + 0.5f));
		fConst34 = ((90.1869125f / fConst2) + 1.0f);
		fConst35 = (0.0f - (180.373825f / (fConst2 * fConst34)));
		fConst36 = ((((8729.25684f / fConst2) + 165.793411f) / fConst2) + 1.0f);
		fConst37 = (0.0f - (34917.0273f / (fConst5 * fConst36)));
		fConst38 = (0.0f - (((34917.0273f / fConst2) + 331.586823f) / (fConst2 * fConst36)));
		fConst39 = ((((11104.666f / fConst2) + 114.982307f) / fConst2) + 1.0f);
		fConst40 = (1.0f / ((fConst34 * fConst36) * fConst39));
		fConst41 = (0.0f - (44418.6641f / (fConst5 * fConst39)));
		fConst42 = (0.0f - (((44418.6641f / fConst2) + 229.964615f) / (fConst2 * fConst39)));
		fConst43 = ((((5590.23682f / fConst2) + 143.25145f) / fConst2) + 1.0f);
		fConst44 = (0.0f - (22360.9473f / (fConst5 * fConst43)));
		fConst45 = (0.0f - (((22360.9473f / fConst2) + 286.502899f) / (fConst2 * fConst43)));
		fConst46 = ((((7026.10596f / fConst2) + 104.056969f) / fConst2) + 1.0f);
		fConst47 = (1.0f / (fConst43 * fConst46));
		fConst48 = (0.0f - (28104.4238f / (fConst5 * fConst46)));
		fConst49 = (0.0f - (((28104.4238f / fConst2) + 208.113937f) / (fConst2 * fConst46)));
		fConst50 = ((57.4295998f / fConst2) + 1.0f);
		fConst51 = (0.0f - (114.8592f / (fConst2 * fConst50)));
		fConst52 = ((((3950.68286f / fConst2) + 90.955452f) / fConst2) + 1.0f);
		fConst53 = (1.0f / (fConst50 * fConst52));
		fConst54 = (0.0f - (15802.7314f / (fConst5 * fConst52)));
		fConst55 = (0.0f - (((15802.7314f / fConst2) + 181.910904f) / (fConst2 * fConst52)));
		fConst56 = ((24.7308426f / fConst2) + 1.0f);
		fConst57 = (1.0f / fConst56);
		fConst58 = (0.0f - (49.4616852f / (fConst2 * fConst56)));
		fConst59 = ((((1834.84363f / fConst2) + 74.1925278f) / fConst2) + 1.0f);
		fConst60 = (1.0f / fConst59);
		fConst61 = (0.0f - (7339.37451f / (fConst5 * fConst59)));
		fConst62 = (0.0f - (((7339.37451f / fConst2) + 148.385056f) / (fConst2 * fConst59)));
		iConst63 = int(((0.00797751546f * float(iConst0)) + 0.5f));
		fConst64 = ((90.2129135f / fConst2) + 1.0f);
		fConst65 = (0.0f - (180.425827f / (fConst2 * fConst64)));
		fConst66 = ((((8734.29102f / fConst2) + 165.841202f) / fConst2) + 1.0f);
		fConst67 = (0.0f - (34937.1641f / (fConst5 * fConst66)));
		fConst68 = (0.0f - (((34937.1641f / fConst2) + 331.682404f) / (fConst2 * fConst66)));
		fConst69 = ((((11111.0703f / fConst2) + 115.015457f) / fConst2) + 1.0f);
		fConst70 = (1.0f / ((fConst64 * fConst66) * fConst69));
		fConst71 = (0.0f - (44444.2812f / (fConst5 * fConst69)));
		fConst72 = (0.0f - (((44444.2812f / fConst2) + 230.030914f) / (fConst2 * fConst69)));
		fConst73 = ((((5593.46094f / fConst2) + 143.292755f) / fConst2) + 1.0f);
		fConst74 = (0.0f - (22373.8438f / (fConst5 * fConst73)));
		fConst75 = (0.0f - (((22373.8438f / fConst2) + 286.58551f) / (fConst2 * fConst73)));
		fConst76 = ((((7030.15771f / fConst2) + 104.086967f) / fConst2) + 1.0f);
		fConst77 = (1.0f / (fConst73 * fConst76));
		fConst78 = (0.0f - (28120.6309f / (fConst5 * fConst76)));
		fConst79 = (0.0f - (((28120.6309f / fConst2) + 208.173935f) / (fConst2 * fConst76)));
		fConst80 = ((57.4461555f / fConst2) + 1.0f);
		fConst81 = (0.0f - (114.892311f / (fConst2 * fConst80)));
		fConst82 = ((((3952.96118f / fConst2) + 90.9816742f) / fConst2) + 1.0f);
		fConst83 = (1.0f / (fConst80 * fConst82));
		fConst84 = (0.0f - (15811.8447f / (fConst5 * fConst82)));
		fConst85 = (0.0f - (((15811.8447f / fConst2) + 181.963348f) / (fConst2 * fConst82)));
		fConst86 = ((24.7379723f / fConst2) + 1.0f);
		fConst87 = (1.0f / fConst86);
		fConst88 = (0.0f - (49.4759445f / (fConst2 * fConst86)));
		fConst89 = ((((1835.90173f / fConst2) + 74.213913f) / fConst2) + 1.0f);
		fConst90 = (1.0f / fConst89);
		fConst91 = (0.0f - (7343.60693f / (fConst5 * fConst89)));
		fConst92 = (0.0f - (((7343.60693f / fConst2) + 148.427826f) / (fConst2 * fConst89)));
		iConst93 = int(((0.00798334274f * float(iConst0)) + 0.5f));
		fConst94 = ((78.3630142f / fConst2) + 1.0f);
		fConst95 = (0.0f - (156.726028f / (fConst2 * fConst94)));
		fConst96 = ((((6590.40967f / fConst2) + 144.057159f) / fConst2) + 1.0f);
		fConst97 = (0.0f - (26361.6387f / (fConst5 * fConst96)));
		fConst98 = (0.0f - (((26361.6387f / fConst2) + 288.114319f) / (fConst2 * fConst96)));
		fConst99 = ((((8383.7959f / fConst2) + 99.9076157f) / fConst2) + 1.0f);
		fConst100 = (1.0f / ((fConst94 * fConst96) * fConst99));
		fConst101 = (0.0f - (33535.1836f / (fConst5 * fConst99)));
		fConst102 = (0.0f - (((33535.1836f / fConst2) + 199.815231f) / (fConst2 * fConst99)));
		fConst103 = ((((4220.51416f / fConst2) + 124.470551f) / fConst2) + 1.0f);
		fConst104 = (0.0f - (16882.0566f / (fConst5 * fConst103)));
		fConst105 = (0.0f - (((16882.0566f / fConst2) + 248.941101f) / (fConst2 * fConst103)));
		fConst106 = ((((5304.56592f / fConst2) + 90.4146347f) / fConst2) + 1.0f);
		fConst107 = (1.0f / (fConst103 * fConst106));
		fConst108 = (0.0f - (21218.2637f / (fConst5 * fConst106)));
		fConst109 = (0.0f - (((21218.2637f / fConst2) + 180.829269f) / (fConst2 * fConst106)));
		fConst110 = ((49.900322f / fConst2) + 1.0f);
		fConst111 = (0.0f - (99.8006439f / (fConst2 * fConst110)));
		fConst112 = ((((2982.68457f / fConst2) + 79.0307922f) / fConst2) + 1.0f);
		fConst113 = (1.0f / (fConst110 * fConst112));
		fConst114 = (0.0f - (11930.7383f / (fConst5 * fConst112)));
		fConst115 = (0.0f - (((11930.7383f / fConst2) + 158.061584f) / (fConst2 * fConst112)));
		fConst116 = ((21.4885197f / fConst2) + 1.0f);
		fConst117 = (1.0f / fConst116);
		fConst118 = (0.0f - (42.9770393f / (fConst2 * fConst116)));
		fConst119 = ((((1385.26929f / fConst2) + 64.4655533f) / fConst2) + 1.0f);
		fConst120 = (1.0f / fConst119);
		fConst121 = (0.0f - (5541.07715f / (fConst5 * fConst119)));
		fConst122 = (0.0f - (((5541.07715f / fConst2) + 128.931107f) / (fConst2 * fConst119)));
		iConst123 = int(((0.00492694648f * float(iConst0)) + 0.5f));
		fConst124 = ((((4215.23438f / fConst2) + 124.39267f) / fConst2) + 1.0f);
		fConst125 = (0.0f - (16860.9375f / (fConst5 * fConst124)));
		fConst126 = (0.0f - (((16860.9375f / fConst2) + 248.785339f) / (fConst2 * fConst124)));
		fConst127 = ((((5297.92969f / fConst2) + 90.3580627f) / fConst2) + 1.0f);
		fConst128 = (1.0f / (fConst124 * fConst127));
		fConst129 = (0.0f - (21191.7188f / (fConst5 * fConst127)));
		fConst130 = (0.0f - (((21191.7188f / fConst2) + 180.716125f) / (fConst2 * fConst127)));
		fConst131 = ((49.8691025f / fConst2) + 1.0f);
		fConst132 = (0.0f - (99.738205f / (fConst2 * fConst131)));
		fConst133 = ((((2978.95337f / fConst2) + 78.9813385f) / fConst2) + 1.0f);
		fConst134 = (1.0f / (fConst131 * fConst133));
		fConst135 = (0.0f - (11915.8135f / (fConst5 * fConst133)));
		fConst136 = (0.0f - (((11915.8135f / fConst2) + 157.962677f) / (fConst2 * fConst133)));
		fConst137 = ((((1383.53638f / fConst2) + 64.4252167f) / fConst2) + 1.0f);
		fConst138 = (1.0f / fConst137);
		fConst139 = (0.0f - (5534.14551f / (fConst137 * fConst5)));
		fConst140 = (0.0f - (((5534.14551f / fConst2) + 128.850433f) / (fConst137 * fConst2)));
		fConst141 = ((78.3139801f / fConst2) + 1.0f);
		fConst142 = (0.0f - (156.62796f / (fConst2 * fConst141)));
		fConst143 = ((((6582.16504f / fConst2) + 143.967026f) / fConst2) + 1.0f);
		fConst144 = (0.0f - (26328.6602f / (fConst5 * fConst143)));
		fConst145 = (0.0f - (((26328.6602f / fConst2) + 287.934052f) / (fConst2 * fConst143)));
		fConst146 = ((((8373.30762f / fConst2) + 99.8451004f) / fConst2) + 1.0f);
		fConst147 = (1.0f / ((fConst141 * fConst143) * fConst146));
		fConst148 = (0.0f - (33493.2305f / (fConst5 * fConst146)));
		fConst149 = (0.0f - (((33493.2305f / fConst2) + 199.690201f) / (fConst2 * fConst146)));
		fConst150 = ((21.4750729f / fConst2) + 1.0f);
		fConst151 = (1.0f / fConst150);
		fConst152 = (0.0f - (42.9501457f / (fConst2 * fConst150)));
		iConst153 = int(((0.00491237827f * float(iConst0)) + 0.5f));
		fConst154 = ((64.6828918f / fConst2) + 1.0f);
		fConst155 = (0.0f - (129.365784f / (fConst2 * fConst154)));
		fConst156 = ((((4490.23535f / fConst2) + 118.908577f) / fConst2) + 1.0f);
		fConst157 = (0.0f - (17960.9414f / (fConst5 * fConst156)));
		fConst158 = (0.0f - (((17960.9414f / fConst2) + 237.817154f) / (fConst2 * fConst156)));
		fConst159 = ((((5712.12012f / fConst2) + 82.4663773f) / fConst2) + 1.0f);
		fConst160 = (1.0f / ((fConst154 * fConst156) * fConst159));
		fConst161 = (0.0f - (22848.4805f / (fConst5 * fConst159)));
		fConst162 = (0.0f - (((22848.4805f / fConst2) + 164.932755f) / (fConst2 * fConst159)));
		fConst163 = ((((2875.55737f / fConst2) + 102.741272f) / fConst2) + 1.0f);
		fConst164 = (0.0f - (11502.2295f / (fConst5 * fConst163)));
		fConst165 = (0.0f - (((11502.2295f / fConst2) + 205.482544f) / (fConst2 * fConst163)));
		fConst166 = ((((3614.15283f / fConst2) + 74.6306229f) / fConst2) + 1.0f);
		fConst167 = (1.0f / (fConst163 * fConst166));
		fConst168 = (0.0f - (14456.6113f / (fConst5 * fConst166)));
		fConst169 = (0.0f - (((14456.6113f / fConst2) + 149.261246f) / (fConst2 * fConst166)));
		fConst170 = ((41.1890411f / fConst2) + 1.0f);
		fConst171 = (0.0f - (82.3780823f / (fConst2 * fConst170)));
		fConst172 = ((((2032.1886f / fConst2) + 65.2340927f) / fConst2) + 1.0f);
		fConst173 = (1.0f / (fConst170 * fConst172));
		fConst174 = (0.0f - (8128.75439f / (fConst5 * fConst172)));
		fConst175 = (0.0f - (((8128.75439f / fConst2) + 130.468185f) / (fConst2 * fConst172)));
		fConst176 = ((((943.82373f / fConst2) + 53.2115707f) / fConst2) + 1.0f);
		fConst177 = (1.0f / fConst176);
		fConst178 = (0.0f - (3775.29492f / (fConst5 * fConst176)));
		fConst179 = (0.0f - (((3775.29492f / fConst2) + 106.423141f) / (fConst2 * fConst176)));
		fConst180 = ((17.7371902f / fConst2) + 1.0f);
		fConst181 = (1.0f / fConst180);
		fConst182 = (0.0f - (35.4743805f / (fConst2 * fConst180)));
		iConst183 = int(((5.82725761e-06f * float(iConst0)) + 0.5f));
		fConst184 = ((41.1805305f / fConst2) + 1.0f);
		fConst185 = (0.0f - (82.3610611f / (fConst2 * fConst184)));
		fConst186 = ((((2031.34863f / fConst2) + 65.2206116f) / fConst2) + 1.0f);
		fConst187 = (1.0f / (fConst184 * fConst186));
		fConst188 = (0.0f - (8125.39453f / (fConst5 * fConst186)));
		fConst189 = (0.0f - (((8125.39453f / fConst2) + 130.441223f) / (fConst2 * fConst186)));
		fConst190 = ((((943.433594f / fConst2) + 53.200573f) / fConst2) + 1.0f);
		fConst191 = (1.0f / fConst190);
		fConst192 = (0.0f - (3773.73438f / (fConst5 * fConst190)));
		fConst193 = (0.0f - (((3773.73438f / fConst2) + 106.401146f) / (fConst2 * fConst190)));
		fConst194 = ((17.7335243f / fConst2) + 1.0f);
		fConst195 = (1.0f / fConst194);
		fConst196 = (0.0f - (35.4670486f / (fConst2 * fConst194)));
		fConst197 = ((64.6695251f / fConst2) + 1.0f);
		fConst198 = (0.0f - (129.33905f / (fConst2 * fConst197)));
		fConst199 = ((((4488.37939f / fConst2) + 118.883995f) / fConst2) + 1.0f);
		fConst200 = (0.0f - (17953.5176f / (fConst5 * fConst199)));
		fConst201 = (0.0f - (((17953.5176f / fConst2) + 237.76799f) / (fConst2 * fConst199)));
		fConst202 = ((((5709.75928f / fConst2) + 82.4493332f) / fConst2) + 1.0f);
		fConst203 = (1.0f / ((fConst197 * fConst199) * fConst202));
		fConst204 = (0.0f - (22839.0371f / (fConst5 * fConst202)));
		fConst205 = (0.0f - (((22839.0371f / fConst2) + 164.898666f) / (fConst2 * fConst202)));
		fConst206 = ((((2874.3689f / fConst2) + 102.720039f) / fConst2) + 1.0f);
		fConst207 = (0.0f - (11497.4756f / (fConst5 * fConst206)));
		fConst208 = (0.0f - (((11497.4756f / fConst2) + 205.440079f) / (fConst2 * fConst206)));
		fConst209 = ((((3612.65894f / fConst2) + 74.6151962f) / fConst2) + 1.0f);
		fConst210 = (1.0f / (fConst206 * fConst209));
		fConst211 = (0.0f - (14450.6357f / (fConst5 * fConst209)));
		fConst212 = (0.0f - (((14450.6357f / fConst2) + 149.230392f) / (fConst2 * fConst209)));
		
	}
	
	virtual void instanceResetUserInterface() {
		fCheckbox0 = FAUSTFLOAT(0.0f);
		fHslider0 = FAUSTFLOAT(-10.0f);
		fHslider1 = FAUSTFLOAT(400.0f);
		fHslider2 = FAUSTFLOAT(0.0f);
		
	}
	
	virtual void instanceClear() {
		for (int l0 = 0; (l0 < 2); l0 = (l0 + 1)) {
			fRec0[l0] = 0.0f;
			
		}
		for (int l1 = 0; (l1 < 2); l1 = (l1 + 1)) {
			fRec1[l1] = 0.0f;
			
		}
		for (int l2 = 0; (l2 < 3); l2 = (l2 + 1)) {
			fRec2[l2] = 0.0f;
			
		}
		for (int l3 = 0; (l3 < 2); l3 = (l3 + 1)) {
			fRec3[l3] = 0.0f;
			
		}
		for (int l4 = 0; (l4 < 3); l4 = (l4 + 1)) {
			fRec19[l4] = 0.0f;
			
		}
		for (int l5 = 0; (l5 < 3); l5 = (l5 + 1)) {
			fRec20[l5] = 0.0f;
			
		}
		for (int l6 = 0; (l6 < 3); l6 = (l6 + 1)) {
			fRec21[l6] = 0.0f;
			
		}
		for (int l7 = 0; (l7 < 3); l7 = (l7 + 1)) {
			fRec22[l7] = 0.0f;
			
		}
		for (int l8 = 0; (l8 < 3); l8 = (l8 + 1)) {
			fRec23[l8] = 0.0f;
			
		}
		for (int l9 = 0; (l9 < 3); l9 = (l9 + 1)) {
			fRec24[l9] = 0.0f;
			
		}
		for (int l10 = 0; (l10 < 3); l10 = (l10 + 1)) {
			fRec25[l10] = 0.0f;
			
		}
		for (int l11 = 0; (l11 < 3); l11 = (l11 + 1)) {
			fRec26[l11] = 0.0f;
			
		}
		for (int l12 = 0; (l12 < 3); l12 = (l12 + 1)) {
			fRec27[l12] = 0.0f;
			
		}
		for (int l13 = 0; (l13 < 3); l13 = (l13 + 1)) {
			fRec28[l13] = 0.0f;
			
		}
		for (int l14 = 0; (l14 < 3); l14 = (l14 + 1)) {
			fRec29[l14] = 0.0f;
			
		}
		for (int l15 = 0; (l15 < 2); l15 = (l15 + 1)) {
			fRec18[l15] = 0.0f;
			
		}
		for (int l16 = 0; (l16 < 2); l16 = (l16 + 1)) {
			fRec16[l16] = 0.0f;
			
		}
		for (int l17 = 0; (l17 < 2); l17 = (l17 + 1)) {
			fRec15[l17] = 0.0f;
			
		}
		for (int l18 = 0; (l18 < 2); l18 = (l18 + 1)) {
			fRec13[l18] = 0.0f;
			
		}
		for (int l19 = 0; (l19 < 2); l19 = (l19 + 1)) {
			fRec12[l19] = 0.0f;
			
		}
		for (int l20 = 0; (l20 < 2); l20 = (l20 + 1)) {
			fRec10[l20] = 0.0f;
			
		}
		for (int l21 = 0; (l21 < 2); l21 = (l21 + 1)) {
			fRec9[l21] = 0.0f;
			
		}
		for (int l22 = 0; (l22 < 2); l22 = (l22 + 1)) {
			fRec7[l22] = 0.0f;
			
		}
		for (int l23 = 0; (l23 < 2); l23 = (l23 + 1)) {
			fRec6[l23] = 0.0f;
			
		}
		for (int l24 = 0; (l24 < 2); l24 = (l24 + 1)) {
			fRec4[l24] = 0.0f;
			
		}
		for (int l25 = 0; (l25 < 3); l25 = (l25 + 1)) {
			fRec42[l25] = 0.0f;
			
		}
		for (int l26 = 0; (l26 < 3); l26 = (l26 + 1)) {
			fRec43[l26] = 0.0f;
			
		}
		for (int l27 = 0; (l27 < 3); l27 = (l27 + 1)) {
			fRec44[l27] = 0.0f;
			
		}
		for (int l28 = 0; (l28 < 3); l28 = (l28 + 1)) {
			fRec45[l28] = 0.0f;
			
		}
		for (int l29 = 0; (l29 < 3); l29 = (l29 + 1)) {
			fRec46[l29] = 0.0f;
			
		}
		for (int l30 = 0; (l30 < 3); l30 = (l30 + 1)) {
			fRec47[l30] = 0.0f;
			
		}
		for (int l31 = 0; (l31 < 3); l31 = (l31 + 1)) {
			fRec48[l31] = 0.0f;
			
		}
		for (int l32 = 0; (l32 < 3); l32 = (l32 + 1)) {
			fRec49[l32] = 0.0f;
			
		}
		for (int l33 = 0; (l33 < 3); l33 = (l33 + 1)) {
			fRec50[l33] = 0.0f;
			
		}
		for (int l34 = 0; (l34 < 2); l34 = (l34 + 1)) {
			fRec41[l34] = 0.0f;
			
		}
		for (int l35 = 0; (l35 < 2); l35 = (l35 + 1)) {
			fRec39[l35] = 0.0f;
			
		}
		for (int l36 = 0; (l36 < 2); l36 = (l36 + 1)) {
			fRec38[l36] = 0.0f;
			
		}
		for (int l37 = 0; (l37 < 2); l37 = (l37 + 1)) {
			fRec36[l37] = 0.0f;
			
		}
		for (int l38 = 0; (l38 < 2); l38 = (l38 + 1)) {
			fRec35[l38] = 0.0f;
			
		}
		for (int l39 = 0; (l39 < 2); l39 = (l39 + 1)) {
			fRec33[l39] = 0.0f;
			
		}
		for (int l40 = 0; (l40 < 2); l40 = (l40 + 1)) {
			fRec32[l40] = 0.0f;
			
		}
		for (int l41 = 0; (l41 < 2); l41 = (l41 + 1)) {
			fRec30[l41] = 0.0f;
			
		}
		for (int l42 = 0; (l42 < 3); l42 = (l42 + 1)) {
			fRec60[l42] = 0.0f;
			
		}
		for (int l43 = 0; (l43 < 3); l43 = (l43 + 1)) {
			fRec61[l43] = 0.0f;
			
		}
		for (int l44 = 0; (l44 < 3); l44 = (l44 + 1)) {
			fRec62[l44] = 0.0f;
			
		}
		for (int l45 = 0; (l45 < 3); l45 = (l45 + 1)) {
			fRec63[l45] = 0.0f;
			
		}
		for (int l46 = 0; (l46 < 3); l46 = (l46 + 1)) {
			fRec64[l46] = 0.0f;
			
		}
		for (int l47 = 0; (l47 < 3); l47 = (l47 + 1)) {
			fRec65[l47] = 0.0f;
			
		}
		for (int l48 = 0; (l48 < 3); l48 = (l48 + 1)) {
			fRec66[l48] = 0.0f;
			
		}
		for (int l49 = 0; (l49 < 2); l49 = (l49 + 1)) {
			fRec59[l49] = 0.0f;
			
		}
		for (int l50 = 0; (l50 < 2); l50 = (l50 + 1)) {
			fRec57[l50] = 0.0f;
			
		}
		for (int l51 = 0; (l51 < 2); l51 = (l51 + 1)) {
			fRec56[l51] = 0.0f;
			
		}
		for (int l52 = 0; (l52 < 2); l52 = (l52 + 1)) {
			fRec54[l52] = 0.0f;
			
		}
		for (int l53 = 0; (l53 < 2); l53 = (l53 + 1)) {
			fRec53[l53] = 0.0f;
			
		}
		for (int l54 = 0; (l54 < 2); l54 = (l54 + 1)) {
			fRec51[l54] = 0.0f;
			
		}
		for (int l55 = 0; (l55 < 3); l55 = (l55 + 1)) {
			fRec70[l55] = 0.0f;
			
		}
		for (int l56 = 0; (l56 < 3); l56 = (l56 + 1)) {
			fRec71[l56] = 0.0f;
			
		}
		for (int l57 = 0; (l57 < 3); l57 = (l57 + 1)) {
			fRec72[l57] = 0.0f;
			
		}
		for (int l58 = 0; (l58 < 2); l58 = (l58 + 1)) {
			fRec69[l58] = 0.0f;
			
		}
		for (int l59 = 0; (l59 < 2); l59 = (l59 + 1)) {
			fRec67[l59] = 0.0f;
			
		}
		for (int l60 = 0; (l60 < 3); l60 = (l60 + 1)) {
			fRec79[l60] = 0.0f;
			
		}
		for (int l61 = 0; (l61 < 3); l61 = (l61 + 1)) {
			fRec80[l61] = 0.0f;
			
		}
		for (int l62 = 0; (l62 < 3); l62 = (l62 + 1)) {
			fRec81[l62] = 0.0f;
			
		}
		for (int l63 = 0; (l63 < 3); l63 = (l63 + 1)) {
			fRec82[l63] = 0.0f;
			
		}
		for (int l64 = 0; (l64 < 3); l64 = (l64 + 1)) {
			fRec83[l64] = 0.0f;
			
		}
		for (int l65 = 0; (l65 < 2); l65 = (l65 + 1)) {
			fRec78[l65] = 0.0f;
			
		}
		for (int l66 = 0; (l66 < 2); l66 = (l66 + 1)) {
			fRec76[l66] = 0.0f;
			
		}
		for (int l67 = 0; (l67 < 2); l67 = (l67 + 1)) {
			fRec75[l67] = 0.0f;
			
		}
		for (int l68 = 0; (l68 < 2); l68 = (l68 + 1)) {
			fRec73[l68] = 0.0f;
			
		}
		IOTA = 0;
		for (int l69 = 0; (l69 < 2048); l69 = (l69 + 1)) {
			fVec0[l69] = 0.0f;
			
		}
		for (int l70 = 0; (l70 < 2); l70 = (l70 + 1)) {
			fRec98[l70] = 0.0f;
			
		}
		for (int l71 = 0; (l71 < 2); l71 = (l71 + 1)) {
			fRec96[l71] = 0.0f;
			
		}
		for (int l72 = 0; (l72 < 2); l72 = (l72 + 1)) {
			fRec95[l72] = 0.0f;
			
		}
		for (int l73 = 0; (l73 < 2); l73 = (l73 + 1)) {
			fRec93[l73] = 0.0f;
			
		}
		for (int l74 = 0; (l74 < 2); l74 = (l74 + 1)) {
			fRec92[l74] = 0.0f;
			
		}
		for (int l75 = 0; (l75 < 2); l75 = (l75 + 1)) {
			fRec90[l75] = 0.0f;
			
		}
		for (int l76 = 0; (l76 < 2); l76 = (l76 + 1)) {
			fRec89[l76] = 0.0f;
			
		}
		for (int l77 = 0; (l77 < 2); l77 = (l77 + 1)) {
			fRec87[l77] = 0.0f;
			
		}
		for (int l78 = 0; (l78 < 2); l78 = (l78 + 1)) {
			fRec86[l78] = 0.0f;
			
		}
		for (int l79 = 0; (l79 < 2); l79 = (l79 + 1)) {
			fRec84[l79] = 0.0f;
			
		}
		for (int l80 = 0; (l80 < 2); l80 = (l80 + 1)) {
			fRec110[l80] = 0.0f;
			
		}
		for (int l81 = 0; (l81 < 2); l81 = (l81 + 1)) {
			fRec108[l81] = 0.0f;
			
		}
		for (int l82 = 0; (l82 < 2); l82 = (l82 + 1)) {
			fRec107[l82] = 0.0f;
			
		}
		for (int l83 = 0; (l83 < 2); l83 = (l83 + 1)) {
			fRec105[l83] = 0.0f;
			
		}
		for (int l84 = 0; (l84 < 2); l84 = (l84 + 1)) {
			fRec104[l84] = 0.0f;
			
		}
		for (int l85 = 0; (l85 < 2); l85 = (l85 + 1)) {
			fRec102[l85] = 0.0f;
			
		}
		for (int l86 = 0; (l86 < 2); l86 = (l86 + 1)) {
			fRec101[l86] = 0.0f;
			
		}
		for (int l87 = 0; (l87 < 2); l87 = (l87 + 1)) {
			fRec99[l87] = 0.0f;
			
		}
		for (int l88 = 0; (l88 < 2); l88 = (l88 + 1)) {
			fRec119[l88] = 0.0f;
			
		}
		for (int l89 = 0; (l89 < 2); l89 = (l89 + 1)) {
			fRec117[l89] = 0.0f;
			
		}
		for (int l90 = 0; (l90 < 2); l90 = (l90 + 1)) {
			fRec116[l90] = 0.0f;
			
		}
		for (int l91 = 0; (l91 < 2); l91 = (l91 + 1)) {
			fRec114[l91] = 0.0f;
			
		}
		for (int l92 = 0; (l92 < 2); l92 = (l92 + 1)) {
			fRec113[l92] = 0.0f;
			
		}
		for (int l93 = 0; (l93 < 2); l93 = (l93 + 1)) {
			fRec111[l93] = 0.0f;
			
		}
		for (int l94 = 0; (l94 < 2); l94 = (l94 + 1)) {
			fRec122[l94] = 0.0f;
			
		}
		for (int l95 = 0; (l95 < 2); l95 = (l95 + 1)) {
			fRec120[l95] = 0.0f;
			
		}
		for (int l96 = 0; (l96 < 2); l96 = (l96 + 1)) {
			fRec128[l96] = 0.0f;
			
		}
		for (int l97 = 0; (l97 < 2); l97 = (l97 + 1)) {
			fRec126[l97] = 0.0f;
			
		}
		for (int l98 = 0; (l98 < 2); l98 = (l98 + 1)) {
			fRec125[l98] = 0.0f;
			
		}
		for (int l99 = 0; (l99 < 2); l99 = (l99 + 1)) {
			fRec123[l99] = 0.0f;
			
		}
		for (int l100 = 0; (l100 < 2048); l100 = (l100 + 1)) {
			fVec1[l100] = 0.0f;
			
		}
		for (int l101 = 0; (l101 < 2); l101 = (l101 + 1)) {
			fRec143[l101] = 0.0f;
			
		}
		for (int l102 = 0; (l102 < 2); l102 = (l102 + 1)) {
			fRec141[l102] = 0.0f;
			
		}
		for (int l103 = 0; (l103 < 2); l103 = (l103 + 1)) {
			fRec140[l103] = 0.0f;
			
		}
		for (int l104 = 0; (l104 < 2); l104 = (l104 + 1)) {
			fRec138[l104] = 0.0f;
			
		}
		for (int l105 = 0; (l105 < 2); l105 = (l105 + 1)) {
			fRec137[l105] = 0.0f;
			
		}
		for (int l106 = 0; (l106 < 2); l106 = (l106 + 1)) {
			fRec135[l106] = 0.0f;
			
		}
		for (int l107 = 0; (l107 < 2); l107 = (l107 + 1)) {
			fRec134[l107] = 0.0f;
			
		}
		for (int l108 = 0; (l108 < 2); l108 = (l108 + 1)) {
			fRec132[l108] = 0.0f;
			
		}
		for (int l109 = 0; (l109 < 2); l109 = (l109 + 1)) {
			fRec131[l109] = 0.0f;
			
		}
		for (int l110 = 0; (l110 < 2); l110 = (l110 + 1)) {
			fRec129[l110] = 0.0f;
			
		}
		for (int l111 = 0; (l111 < 2); l111 = (l111 + 1)) {
			fRec155[l111] = 0.0f;
			
		}
		for (int l112 = 0; (l112 < 2); l112 = (l112 + 1)) {
			fRec153[l112] = 0.0f;
			
		}
		for (int l113 = 0; (l113 < 2); l113 = (l113 + 1)) {
			fRec152[l113] = 0.0f;
			
		}
		for (int l114 = 0; (l114 < 2); l114 = (l114 + 1)) {
			fRec150[l114] = 0.0f;
			
		}
		for (int l115 = 0; (l115 < 2); l115 = (l115 + 1)) {
			fRec149[l115] = 0.0f;
			
		}
		for (int l116 = 0; (l116 < 2); l116 = (l116 + 1)) {
			fRec147[l116] = 0.0f;
			
		}
		for (int l117 = 0; (l117 < 2); l117 = (l117 + 1)) {
			fRec146[l117] = 0.0f;
			
		}
		for (int l118 = 0; (l118 < 2); l118 = (l118 + 1)) {
			fRec144[l118] = 0.0f;
			
		}
		for (int l119 = 0; (l119 < 2); l119 = (l119 + 1)) {
			fRec164[l119] = 0.0f;
			
		}
		for (int l120 = 0; (l120 < 2); l120 = (l120 + 1)) {
			fRec162[l120] = 0.0f;
			
		}
		for (int l121 = 0; (l121 < 2); l121 = (l121 + 1)) {
			fRec161[l121] = 0.0f;
			
		}
		for (int l122 = 0; (l122 < 2); l122 = (l122 + 1)) {
			fRec159[l122] = 0.0f;
			
		}
		for (int l123 = 0; (l123 < 2); l123 = (l123 + 1)) {
			fRec158[l123] = 0.0f;
			
		}
		for (int l124 = 0; (l124 < 2); l124 = (l124 + 1)) {
			fRec156[l124] = 0.0f;
			
		}
		for (int l125 = 0; (l125 < 2); l125 = (l125 + 1)) {
			fRec167[l125] = 0.0f;
			
		}
		for (int l126 = 0; (l126 < 2); l126 = (l126 + 1)) {
			fRec165[l126] = 0.0f;
			
		}
		for (int l127 = 0; (l127 < 2); l127 = (l127 + 1)) {
			fRec173[l127] = 0.0f;
			
		}
		for (int l128 = 0; (l128 < 2); l128 = (l128 + 1)) {
			fRec171[l128] = 0.0f;
			
		}
		for (int l129 = 0; (l129 < 2); l129 = (l129 + 1)) {
			fRec170[l129] = 0.0f;
			
		}
		for (int l130 = 0; (l130 < 2); l130 = (l130 + 1)) {
			fRec168[l130] = 0.0f;
			
		}
		for (int l131 = 0; (l131 < 2048); l131 = (l131 + 1)) {
			fVec2[l131] = 0.0f;
			
		}
		for (int l132 = 0; (l132 < 2); l132 = (l132 + 1)) {
			fRec188[l132] = 0.0f;
			
		}
		for (int l133 = 0; (l133 < 2); l133 = (l133 + 1)) {
			fRec186[l133] = 0.0f;
			
		}
		for (int l134 = 0; (l134 < 2); l134 = (l134 + 1)) {
			fRec185[l134] = 0.0f;
			
		}
		for (int l135 = 0; (l135 < 2); l135 = (l135 + 1)) {
			fRec183[l135] = 0.0f;
			
		}
		for (int l136 = 0; (l136 < 2); l136 = (l136 + 1)) {
			fRec182[l136] = 0.0f;
			
		}
		for (int l137 = 0; (l137 < 2); l137 = (l137 + 1)) {
			fRec180[l137] = 0.0f;
			
		}
		for (int l138 = 0; (l138 < 2); l138 = (l138 + 1)) {
			fRec179[l138] = 0.0f;
			
		}
		for (int l139 = 0; (l139 < 2); l139 = (l139 + 1)) {
			fRec177[l139] = 0.0f;
			
		}
		for (int l140 = 0; (l140 < 2); l140 = (l140 + 1)) {
			fRec176[l140] = 0.0f;
			
		}
		for (int l141 = 0; (l141 < 2); l141 = (l141 + 1)) {
			fRec174[l141] = 0.0f;
			
		}
		for (int l142 = 0; (l142 < 2); l142 = (l142 + 1)) {
			fRec200[l142] = 0.0f;
			
		}
		for (int l143 = 0; (l143 < 2); l143 = (l143 + 1)) {
			fRec198[l143] = 0.0f;
			
		}
		for (int l144 = 0; (l144 < 2); l144 = (l144 + 1)) {
			fRec197[l144] = 0.0f;
			
		}
		for (int l145 = 0; (l145 < 2); l145 = (l145 + 1)) {
			fRec195[l145] = 0.0f;
			
		}
		for (int l146 = 0; (l146 < 2); l146 = (l146 + 1)) {
			fRec194[l146] = 0.0f;
			
		}
		for (int l147 = 0; (l147 < 2); l147 = (l147 + 1)) {
			fRec192[l147] = 0.0f;
			
		}
		for (int l148 = 0; (l148 < 2); l148 = (l148 + 1)) {
			fRec191[l148] = 0.0f;
			
		}
		for (int l149 = 0; (l149 < 2); l149 = (l149 + 1)) {
			fRec189[l149] = 0.0f;
			
		}
		for (int l150 = 0; (l150 < 2); l150 = (l150 + 1)) {
			fRec209[l150] = 0.0f;
			
		}
		for (int l151 = 0; (l151 < 2); l151 = (l151 + 1)) {
			fRec207[l151] = 0.0f;
			
		}
		for (int l152 = 0; (l152 < 2); l152 = (l152 + 1)) {
			fRec206[l152] = 0.0f;
			
		}
		for (int l153 = 0; (l153 < 2); l153 = (l153 + 1)) {
			fRec204[l153] = 0.0f;
			
		}
		for (int l154 = 0; (l154 < 2); l154 = (l154 + 1)) {
			fRec203[l154] = 0.0f;
			
		}
		for (int l155 = 0; (l155 < 2); l155 = (l155 + 1)) {
			fRec201[l155] = 0.0f;
			
		}
		for (int l156 = 0; (l156 < 2); l156 = (l156 + 1)) {
			fRec212[l156] = 0.0f;
			
		}
		for (int l157 = 0; (l157 < 2); l157 = (l157 + 1)) {
			fRec210[l157] = 0.0f;
			
		}
		for (int l158 = 0; (l158 < 2); l158 = (l158 + 1)) {
			fRec218[l158] = 0.0f;
			
		}
		for (int l159 = 0; (l159 < 2); l159 = (l159 + 1)) {
			fRec216[l159] = 0.0f;
			
		}
		for (int l160 = 0; (l160 < 2); l160 = (l160 + 1)) {
			fRec215[l160] = 0.0f;
			
		}
		for (int l161 = 0; (l161 < 2); l161 = (l161 + 1)) {
			fRec213[l161] = 0.0f;
			
		}
		for (int l162 = 0; (l162 < 2048); l162 = (l162 + 1)) {
			fVec3[l162] = 0.0f;
			
		}
		for (int l163 = 0; (l163 < 2); l163 = (l163 + 1)) {
			fRec233[l163] = 0.0f;
			
		}
		for (int l164 = 0; (l164 < 2); l164 = (l164 + 1)) {
			fRec231[l164] = 0.0f;
			
		}
		for (int l165 = 0; (l165 < 2); l165 = (l165 + 1)) {
			fRec230[l165] = 0.0f;
			
		}
		for (int l166 = 0; (l166 < 2); l166 = (l166 + 1)) {
			fRec228[l166] = 0.0f;
			
		}
		for (int l167 = 0; (l167 < 2); l167 = (l167 + 1)) {
			fRec227[l167] = 0.0f;
			
		}
		for (int l168 = 0; (l168 < 2); l168 = (l168 + 1)) {
			fRec225[l168] = 0.0f;
			
		}
		for (int l169 = 0; (l169 < 2); l169 = (l169 + 1)) {
			fRec224[l169] = 0.0f;
			
		}
		for (int l170 = 0; (l170 < 2); l170 = (l170 + 1)) {
			fRec222[l170] = 0.0f;
			
		}
		for (int l171 = 0; (l171 < 2); l171 = (l171 + 1)) {
			fRec221[l171] = 0.0f;
			
		}
		for (int l172 = 0; (l172 < 2); l172 = (l172 + 1)) {
			fRec219[l172] = 0.0f;
			
		}
		for (int l173 = 0; (l173 < 2); l173 = (l173 + 1)) {
			fRec245[l173] = 0.0f;
			
		}
		for (int l174 = 0; (l174 < 2); l174 = (l174 + 1)) {
			fRec243[l174] = 0.0f;
			
		}
		for (int l175 = 0; (l175 < 2); l175 = (l175 + 1)) {
			fRec242[l175] = 0.0f;
			
		}
		for (int l176 = 0; (l176 < 2); l176 = (l176 + 1)) {
			fRec240[l176] = 0.0f;
			
		}
		for (int l177 = 0; (l177 < 2); l177 = (l177 + 1)) {
			fRec239[l177] = 0.0f;
			
		}
		for (int l178 = 0; (l178 < 2); l178 = (l178 + 1)) {
			fRec237[l178] = 0.0f;
			
		}
		for (int l179 = 0; (l179 < 2); l179 = (l179 + 1)) {
			fRec236[l179] = 0.0f;
			
		}
		for (int l180 = 0; (l180 < 2); l180 = (l180 + 1)) {
			fRec234[l180] = 0.0f;
			
		}
		for (int l181 = 0; (l181 < 2); l181 = (l181 + 1)) {
			fRec254[l181] = 0.0f;
			
		}
		for (int l182 = 0; (l182 < 2); l182 = (l182 + 1)) {
			fRec252[l182] = 0.0f;
			
		}
		for (int l183 = 0; (l183 < 2); l183 = (l183 + 1)) {
			fRec251[l183] = 0.0f;
			
		}
		for (int l184 = 0; (l184 < 2); l184 = (l184 + 1)) {
			fRec249[l184] = 0.0f;
			
		}
		for (int l185 = 0; (l185 < 2); l185 = (l185 + 1)) {
			fRec248[l185] = 0.0f;
			
		}
		for (int l186 = 0; (l186 < 2); l186 = (l186 + 1)) {
			fRec246[l186] = 0.0f;
			
		}
		for (int l187 = 0; (l187 < 2); l187 = (l187 + 1)) {
			fRec257[l187] = 0.0f;
			
		}
		for (int l188 = 0; (l188 < 2); l188 = (l188 + 1)) {
			fRec255[l188] = 0.0f;
			
		}
		for (int l189 = 0; (l189 < 2); l189 = (l189 + 1)) {
			fRec263[l189] = 0.0f;
			
		}
		for (int l190 = 0; (l190 < 2); l190 = (l190 + 1)) {
			fRec261[l190] = 0.0f;
			
		}
		for (int l191 = 0; (l191 < 2); l191 = (l191 + 1)) {
			fRec260[l191] = 0.0f;
			
		}
		for (int l192 = 0; (l192 < 2); l192 = (l192 + 1)) {
			fRec258[l192] = 0.0f;
			
		}
		for (int l193 = 0; (l193 < 2048); l193 = (l193 + 1)) {
			fVec4[l193] = 0.0f;
			
		}
		for (int l194 = 0; (l194 < 2); l194 = (l194 + 1)) {
			fRec278[l194] = 0.0f;
			
		}
		for (int l195 = 0; (l195 < 2); l195 = (l195 + 1)) {
			fRec276[l195] = 0.0f;
			
		}
		for (int l196 = 0; (l196 < 2); l196 = (l196 + 1)) {
			fRec275[l196] = 0.0f;
			
		}
		for (int l197 = 0; (l197 < 2); l197 = (l197 + 1)) {
			fRec273[l197] = 0.0f;
			
		}
		for (int l198 = 0; (l198 < 2); l198 = (l198 + 1)) {
			fRec272[l198] = 0.0f;
			
		}
		for (int l199 = 0; (l199 < 2); l199 = (l199 + 1)) {
			fRec270[l199] = 0.0f;
			
		}
		for (int l200 = 0; (l200 < 2); l200 = (l200 + 1)) {
			fRec269[l200] = 0.0f;
			
		}
		for (int l201 = 0; (l201 < 2); l201 = (l201 + 1)) {
			fRec267[l201] = 0.0f;
			
		}
		for (int l202 = 0; (l202 < 2); l202 = (l202 + 1)) {
			fRec266[l202] = 0.0f;
			
		}
		for (int l203 = 0; (l203 < 2); l203 = (l203 + 1)) {
			fRec264[l203] = 0.0f;
			
		}
		for (int l204 = 0; (l204 < 2); l204 = (l204 + 1)) {
			fRec290[l204] = 0.0f;
			
		}
		for (int l205 = 0; (l205 < 2); l205 = (l205 + 1)) {
			fRec288[l205] = 0.0f;
			
		}
		for (int l206 = 0; (l206 < 2); l206 = (l206 + 1)) {
			fRec287[l206] = 0.0f;
			
		}
		for (int l207 = 0; (l207 < 2); l207 = (l207 + 1)) {
			fRec285[l207] = 0.0f;
			
		}
		for (int l208 = 0; (l208 < 2); l208 = (l208 + 1)) {
			fRec284[l208] = 0.0f;
			
		}
		for (int l209 = 0; (l209 < 2); l209 = (l209 + 1)) {
			fRec282[l209] = 0.0f;
			
		}
		for (int l210 = 0; (l210 < 2); l210 = (l210 + 1)) {
			fRec281[l210] = 0.0f;
			
		}
		for (int l211 = 0; (l211 < 2); l211 = (l211 + 1)) {
			fRec279[l211] = 0.0f;
			
		}
		for (int l212 = 0; (l212 < 2); l212 = (l212 + 1)) {
			fRec299[l212] = 0.0f;
			
		}
		for (int l213 = 0; (l213 < 2); l213 = (l213 + 1)) {
			fRec297[l213] = 0.0f;
			
		}
		for (int l214 = 0; (l214 < 2); l214 = (l214 + 1)) {
			fRec296[l214] = 0.0f;
			
		}
		for (int l215 = 0; (l215 < 2); l215 = (l215 + 1)) {
			fRec294[l215] = 0.0f;
			
		}
		for (int l216 = 0; (l216 < 2); l216 = (l216 + 1)) {
			fRec293[l216] = 0.0f;
			
		}
		for (int l217 = 0; (l217 < 2); l217 = (l217 + 1)) {
			fRec291[l217] = 0.0f;
			
		}
		for (int l218 = 0; (l218 < 2); l218 = (l218 + 1)) {
			fRec302[l218] = 0.0f;
			
		}
		for (int l219 = 0; (l219 < 2); l219 = (l219 + 1)) {
			fRec300[l219] = 0.0f;
			
		}
		for (int l220 = 0; (l220 < 2); l220 = (l220 + 1)) {
			fRec308[l220] = 0.0f;
			
		}
		for (int l221 = 0; (l221 < 2); l221 = (l221 + 1)) {
			fRec306[l221] = 0.0f;
			
		}
		for (int l222 = 0; (l222 < 2); l222 = (l222 + 1)) {
			fRec305[l222] = 0.0f;
			
		}
		for (int l223 = 0; (l223 < 2); l223 = (l223 + 1)) {
			fRec303[l223] = 0.0f;
			
		}
		for (int l224 = 0; (l224 < 2048); l224 = (l224 + 1)) {
			fVec5[l224] = 0.0f;
			
		}
		for (int l225 = 0; (l225 < 2); l225 = (l225 + 1)) {
			fRec323[l225] = 0.0f;
			
		}
		for (int l226 = 0; (l226 < 2); l226 = (l226 + 1)) {
			fRec321[l226] = 0.0f;
			
		}
		for (int l227 = 0; (l227 < 2); l227 = (l227 + 1)) {
			fRec320[l227] = 0.0f;
			
		}
		for (int l228 = 0; (l228 < 2); l228 = (l228 + 1)) {
			fRec318[l228] = 0.0f;
			
		}
		for (int l229 = 0; (l229 < 2); l229 = (l229 + 1)) {
			fRec317[l229] = 0.0f;
			
		}
		for (int l230 = 0; (l230 < 2); l230 = (l230 + 1)) {
			fRec315[l230] = 0.0f;
			
		}
		for (int l231 = 0; (l231 < 2); l231 = (l231 + 1)) {
			fRec314[l231] = 0.0f;
			
		}
		for (int l232 = 0; (l232 < 2); l232 = (l232 + 1)) {
			fRec312[l232] = 0.0f;
			
		}
		for (int l233 = 0; (l233 < 2); l233 = (l233 + 1)) {
			fRec311[l233] = 0.0f;
			
		}
		for (int l234 = 0; (l234 < 2); l234 = (l234 + 1)) {
			fRec309[l234] = 0.0f;
			
		}
		for (int l235 = 0; (l235 < 2); l235 = (l235 + 1)) {
			fRec335[l235] = 0.0f;
			
		}
		for (int l236 = 0; (l236 < 2); l236 = (l236 + 1)) {
			fRec333[l236] = 0.0f;
			
		}
		for (int l237 = 0; (l237 < 2); l237 = (l237 + 1)) {
			fRec332[l237] = 0.0f;
			
		}
		for (int l238 = 0; (l238 < 2); l238 = (l238 + 1)) {
			fRec330[l238] = 0.0f;
			
		}
		for (int l239 = 0; (l239 < 2); l239 = (l239 + 1)) {
			fRec329[l239] = 0.0f;
			
		}
		for (int l240 = 0; (l240 < 2); l240 = (l240 + 1)) {
			fRec327[l240] = 0.0f;
			
		}
		for (int l241 = 0; (l241 < 2); l241 = (l241 + 1)) {
			fRec326[l241] = 0.0f;
			
		}
		for (int l242 = 0; (l242 < 2); l242 = (l242 + 1)) {
			fRec324[l242] = 0.0f;
			
		}
		for (int l243 = 0; (l243 < 2); l243 = (l243 + 1)) {
			fRec344[l243] = 0.0f;
			
		}
		for (int l244 = 0; (l244 < 2); l244 = (l244 + 1)) {
			fRec342[l244] = 0.0f;
			
		}
		for (int l245 = 0; (l245 < 2); l245 = (l245 + 1)) {
			fRec341[l245] = 0.0f;
			
		}
		for (int l246 = 0; (l246 < 2); l246 = (l246 + 1)) {
			fRec339[l246] = 0.0f;
			
		}
		for (int l247 = 0; (l247 < 2); l247 = (l247 + 1)) {
			fRec338[l247] = 0.0f;
			
		}
		for (int l248 = 0; (l248 < 2); l248 = (l248 + 1)) {
			fRec336[l248] = 0.0f;
			
		}
		for (int l249 = 0; (l249 < 2); l249 = (l249 + 1)) {
			fRec347[l249] = 0.0f;
			
		}
		for (int l250 = 0; (l250 < 2); l250 = (l250 + 1)) {
			fRec345[l250] = 0.0f;
			
		}
		for (int l251 = 0; (l251 < 2); l251 = (l251 + 1)) {
			fRec353[l251] = 0.0f;
			
		}
		for (int l252 = 0; (l252 < 2); l252 = (l252 + 1)) {
			fRec351[l252] = 0.0f;
			
		}
		for (int l253 = 0; (l253 < 2); l253 = (l253 + 1)) {
			fRec350[l253] = 0.0f;
			
		}
		for (int l254 = 0; (l254 < 2); l254 = (l254 + 1)) {
			fRec348[l254] = 0.0f;
			
		}
		for (int l255 = 0; (l255 < 2048); l255 = (l255 + 1)) {
			fVec6[l255] = 0.0f;
			
		}
		for (int l256 = 0; (l256 < 2); l256 = (l256 + 1)) {
			fRec368[l256] = 0.0f;
			
		}
		for (int l257 = 0; (l257 < 2); l257 = (l257 + 1)) {
			fRec366[l257] = 0.0f;
			
		}
		for (int l258 = 0; (l258 < 2); l258 = (l258 + 1)) {
			fRec365[l258] = 0.0f;
			
		}
		for (int l259 = 0; (l259 < 2); l259 = (l259 + 1)) {
			fRec363[l259] = 0.0f;
			
		}
		for (int l260 = 0; (l260 < 2); l260 = (l260 + 1)) {
			fRec362[l260] = 0.0f;
			
		}
		for (int l261 = 0; (l261 < 2); l261 = (l261 + 1)) {
			fRec360[l261] = 0.0f;
			
		}
		for (int l262 = 0; (l262 < 2); l262 = (l262 + 1)) {
			fRec359[l262] = 0.0f;
			
		}
		for (int l263 = 0; (l263 < 2); l263 = (l263 + 1)) {
			fRec357[l263] = 0.0f;
			
		}
		for (int l264 = 0; (l264 < 2); l264 = (l264 + 1)) {
			fRec356[l264] = 0.0f;
			
		}
		for (int l265 = 0; (l265 < 2); l265 = (l265 + 1)) {
			fRec354[l265] = 0.0f;
			
		}
		for (int l266 = 0; (l266 < 2); l266 = (l266 + 1)) {
			fRec380[l266] = 0.0f;
			
		}
		for (int l267 = 0; (l267 < 2); l267 = (l267 + 1)) {
			fRec378[l267] = 0.0f;
			
		}
		for (int l268 = 0; (l268 < 2); l268 = (l268 + 1)) {
			fRec377[l268] = 0.0f;
			
		}
		for (int l269 = 0; (l269 < 2); l269 = (l269 + 1)) {
			fRec375[l269] = 0.0f;
			
		}
		for (int l270 = 0; (l270 < 2); l270 = (l270 + 1)) {
			fRec374[l270] = 0.0f;
			
		}
		for (int l271 = 0; (l271 < 2); l271 = (l271 + 1)) {
			fRec372[l271] = 0.0f;
			
		}
		for (int l272 = 0; (l272 < 2); l272 = (l272 + 1)) {
			fRec371[l272] = 0.0f;
			
		}
		for (int l273 = 0; (l273 < 2); l273 = (l273 + 1)) {
			fRec369[l273] = 0.0f;
			
		}
		for (int l274 = 0; (l274 < 2); l274 = (l274 + 1)) {
			fRec389[l274] = 0.0f;
			
		}
		for (int l275 = 0; (l275 < 2); l275 = (l275 + 1)) {
			fRec387[l275] = 0.0f;
			
		}
		for (int l276 = 0; (l276 < 2); l276 = (l276 + 1)) {
			fRec386[l276] = 0.0f;
			
		}
		for (int l277 = 0; (l277 < 2); l277 = (l277 + 1)) {
			fRec384[l277] = 0.0f;
			
		}
		for (int l278 = 0; (l278 < 2); l278 = (l278 + 1)) {
			fRec383[l278] = 0.0f;
			
		}
		for (int l279 = 0; (l279 < 2); l279 = (l279 + 1)) {
			fRec381[l279] = 0.0f;
			
		}
		for (int l280 = 0; (l280 < 2); l280 = (l280 + 1)) {
			fRec392[l280] = 0.0f;
			
		}
		for (int l281 = 0; (l281 < 2); l281 = (l281 + 1)) {
			fRec390[l281] = 0.0f;
			
		}
		for (int l282 = 0; (l282 < 2); l282 = (l282 + 1)) {
			fRec398[l282] = 0.0f;
			
		}
		for (int l283 = 0; (l283 < 2); l283 = (l283 + 1)) {
			fRec396[l283] = 0.0f;
			
		}
		for (int l284 = 0; (l284 < 2); l284 = (l284 + 1)) {
			fRec395[l284] = 0.0f;
			
		}
		for (int l285 = 0; (l285 < 2); l285 = (l285 + 1)) {
			fRec393[l285] = 0.0f;
			
		}
		for (int l286 = 0; (l286 < 1024); l286 = (l286 + 1)) {
			fVec7[l286] = 0.0f;
			
		}
		for (int l287 = 0; (l287 < 2); l287 = (l287 + 1)) {
			fRec410[l287] = 0.0f;
			
		}
		for (int l288 = 0; (l288 < 2); l288 = (l288 + 1)) {
			fRec408[l288] = 0.0f;
			
		}
		for (int l289 = 0; (l289 < 2); l289 = (l289 + 1)) {
			fRec407[l289] = 0.0f;
			
		}
		for (int l290 = 0; (l290 < 2); l290 = (l290 + 1)) {
			fRec405[l290] = 0.0f;
			
		}
		for (int l291 = 0; (l291 < 2); l291 = (l291 + 1)) {
			fRec404[l291] = 0.0f;
			
		}
		for (int l292 = 0; (l292 < 2); l292 = (l292 + 1)) {
			fRec402[l292] = 0.0f;
			
		}
		for (int l293 = 0; (l293 < 2); l293 = (l293 + 1)) {
			fRec401[l293] = 0.0f;
			
		}
		for (int l294 = 0; (l294 < 2); l294 = (l294 + 1)) {
			fRec399[l294] = 0.0f;
			
		}
		for (int l295 = 0; (l295 < 2); l295 = (l295 + 1)) {
			fRec419[l295] = 0.0f;
			
		}
		for (int l296 = 0; (l296 < 2); l296 = (l296 + 1)) {
			fRec417[l296] = 0.0f;
			
		}
		for (int l297 = 0; (l297 < 2); l297 = (l297 + 1)) {
			fRec416[l297] = 0.0f;
			
		}
		for (int l298 = 0; (l298 < 2); l298 = (l298 + 1)) {
			fRec414[l298] = 0.0f;
			
		}
		for (int l299 = 0; (l299 < 2); l299 = (l299 + 1)) {
			fRec413[l299] = 0.0f;
			
		}
		for (int l300 = 0; (l300 < 2); l300 = (l300 + 1)) {
			fRec411[l300] = 0.0f;
			
		}
		for (int l301 = 0; (l301 < 2); l301 = (l301 + 1)) {
			fRec425[l301] = 0.0f;
			
		}
		for (int l302 = 0; (l302 < 2); l302 = (l302 + 1)) {
			fRec423[l302] = 0.0f;
			
		}
		for (int l303 = 0; (l303 < 2); l303 = (l303 + 1)) {
			fRec422[l303] = 0.0f;
			
		}
		for (int l304 = 0; (l304 < 2); l304 = (l304 + 1)) {
			fRec420[l304] = 0.0f;
			
		}
		for (int l305 = 0; (l305 < 2); l305 = (l305 + 1)) {
			fRec440[l305] = 0.0f;
			
		}
		for (int l306 = 0; (l306 < 2); l306 = (l306 + 1)) {
			fRec438[l306] = 0.0f;
			
		}
		for (int l307 = 0; (l307 < 2); l307 = (l307 + 1)) {
			fRec437[l307] = 0.0f;
			
		}
		for (int l308 = 0; (l308 < 2); l308 = (l308 + 1)) {
			fRec435[l308] = 0.0f;
			
		}
		for (int l309 = 0; (l309 < 2); l309 = (l309 + 1)) {
			fRec434[l309] = 0.0f;
			
		}
		for (int l310 = 0; (l310 < 2); l310 = (l310 + 1)) {
			fRec432[l310] = 0.0f;
			
		}
		for (int l311 = 0; (l311 < 2); l311 = (l311 + 1)) {
			fRec431[l311] = 0.0f;
			
		}
		for (int l312 = 0; (l312 < 2); l312 = (l312 + 1)) {
			fRec429[l312] = 0.0f;
			
		}
		for (int l313 = 0; (l313 < 2); l313 = (l313 + 1)) {
			fRec428[l313] = 0.0f;
			
		}
		for (int l314 = 0; (l314 < 2); l314 = (l314 + 1)) {
			fRec426[l314] = 0.0f;
			
		}
		for (int l315 = 0; (l315 < 2); l315 = (l315 + 1)) {
			fRec443[l315] = 0.0f;
			
		}
		for (int l316 = 0; (l316 < 2); l316 = (l316 + 1)) {
			fRec441[l316] = 0.0f;
			
		}
		for (int l317 = 0; (l317 < 1024); l317 = (l317 + 1)) {
			fVec8[l317] = 0.0f;
			
		}
		for (int l318 = 0; (l318 < 2); l318 = (l318 + 1)) {
			fRec458[l318] = 0.0f;
			
		}
		for (int l319 = 0; (l319 < 2); l319 = (l319 + 1)) {
			fRec456[l319] = 0.0f;
			
		}
		for (int l320 = 0; (l320 < 2); l320 = (l320 + 1)) {
			fRec455[l320] = 0.0f;
			
		}
		for (int l321 = 0; (l321 < 2); l321 = (l321 + 1)) {
			fRec453[l321] = 0.0f;
			
		}
		for (int l322 = 0; (l322 < 2); l322 = (l322 + 1)) {
			fRec452[l322] = 0.0f;
			
		}
		for (int l323 = 0; (l323 < 2); l323 = (l323 + 1)) {
			fRec450[l323] = 0.0f;
			
		}
		for (int l324 = 0; (l324 < 2); l324 = (l324 + 1)) {
			fRec449[l324] = 0.0f;
			
		}
		for (int l325 = 0; (l325 < 2); l325 = (l325 + 1)) {
			fRec447[l325] = 0.0f;
			
		}
		for (int l326 = 0; (l326 < 2); l326 = (l326 + 1)) {
			fRec446[l326] = 0.0f;
			
		}
		for (int l327 = 0; (l327 < 2); l327 = (l327 + 1)) {
			fRec444[l327] = 0.0f;
			
		}
		for (int l328 = 0; (l328 < 2); l328 = (l328 + 1)) {
			fRec470[l328] = 0.0f;
			
		}
		for (int l329 = 0; (l329 < 2); l329 = (l329 + 1)) {
			fRec468[l329] = 0.0f;
			
		}
		for (int l330 = 0; (l330 < 2); l330 = (l330 + 1)) {
			fRec467[l330] = 0.0f;
			
		}
		for (int l331 = 0; (l331 < 2); l331 = (l331 + 1)) {
			fRec465[l331] = 0.0f;
			
		}
		for (int l332 = 0; (l332 < 2); l332 = (l332 + 1)) {
			fRec464[l332] = 0.0f;
			
		}
		for (int l333 = 0; (l333 < 2); l333 = (l333 + 1)) {
			fRec462[l333] = 0.0f;
			
		}
		for (int l334 = 0; (l334 < 2); l334 = (l334 + 1)) {
			fRec461[l334] = 0.0f;
			
		}
		for (int l335 = 0; (l335 < 2); l335 = (l335 + 1)) {
			fRec459[l335] = 0.0f;
			
		}
		for (int l336 = 0; (l336 < 2); l336 = (l336 + 1)) {
			fRec479[l336] = 0.0f;
			
		}
		for (int l337 = 0; (l337 < 2); l337 = (l337 + 1)) {
			fRec477[l337] = 0.0f;
			
		}
		for (int l338 = 0; (l338 < 2); l338 = (l338 + 1)) {
			fRec476[l338] = 0.0f;
			
		}
		for (int l339 = 0; (l339 < 2); l339 = (l339 + 1)) {
			fRec474[l339] = 0.0f;
			
		}
		for (int l340 = 0; (l340 < 2); l340 = (l340 + 1)) {
			fRec473[l340] = 0.0f;
			
		}
		for (int l341 = 0; (l341 < 2); l341 = (l341 + 1)) {
			fRec471[l341] = 0.0f;
			
		}
		for (int l342 = 0; (l342 < 2); l342 = (l342 + 1)) {
			fRec482[l342] = 0.0f;
			
		}
		for (int l343 = 0; (l343 < 2); l343 = (l343 + 1)) {
			fRec480[l343] = 0.0f;
			
		}
		for (int l344 = 0; (l344 < 2); l344 = (l344 + 1)) {
			fRec488[l344] = 0.0f;
			
		}
		for (int l345 = 0; (l345 < 2); l345 = (l345 + 1)) {
			fRec486[l345] = 0.0f;
			
		}
		for (int l346 = 0; (l346 < 2); l346 = (l346 + 1)) {
			fRec485[l346] = 0.0f;
			
		}
		for (int l347 = 0; (l347 < 2); l347 = (l347 + 1)) {
			fRec483[l347] = 0.0f;
			
		}
		for (int l348 = 0; (l348 < 1024); l348 = (l348 + 1)) {
			fVec9[l348] = 0.0f;
			
		}
		for (int l349 = 0; (l349 < 2); l349 = (l349 + 1)) {
			fRec503[l349] = 0.0f;
			
		}
		for (int l350 = 0; (l350 < 2); l350 = (l350 + 1)) {
			fRec501[l350] = 0.0f;
			
		}
		for (int l351 = 0; (l351 < 2); l351 = (l351 + 1)) {
			fRec500[l351] = 0.0f;
			
		}
		for (int l352 = 0; (l352 < 2); l352 = (l352 + 1)) {
			fRec498[l352] = 0.0f;
			
		}
		for (int l353 = 0; (l353 < 2); l353 = (l353 + 1)) {
			fRec497[l353] = 0.0f;
			
		}
		for (int l354 = 0; (l354 < 2); l354 = (l354 + 1)) {
			fRec495[l354] = 0.0f;
			
		}
		for (int l355 = 0; (l355 < 2); l355 = (l355 + 1)) {
			fRec494[l355] = 0.0f;
			
		}
		for (int l356 = 0; (l356 < 2); l356 = (l356 + 1)) {
			fRec492[l356] = 0.0f;
			
		}
		for (int l357 = 0; (l357 < 2); l357 = (l357 + 1)) {
			fRec491[l357] = 0.0f;
			
		}
		for (int l358 = 0; (l358 < 2); l358 = (l358 + 1)) {
			fRec489[l358] = 0.0f;
			
		}
		for (int l359 = 0; (l359 < 2); l359 = (l359 + 1)) {
			fRec515[l359] = 0.0f;
			
		}
		for (int l360 = 0; (l360 < 2); l360 = (l360 + 1)) {
			fRec513[l360] = 0.0f;
			
		}
		for (int l361 = 0; (l361 < 2); l361 = (l361 + 1)) {
			fRec512[l361] = 0.0f;
			
		}
		for (int l362 = 0; (l362 < 2); l362 = (l362 + 1)) {
			fRec510[l362] = 0.0f;
			
		}
		for (int l363 = 0; (l363 < 2); l363 = (l363 + 1)) {
			fRec509[l363] = 0.0f;
			
		}
		for (int l364 = 0; (l364 < 2); l364 = (l364 + 1)) {
			fRec507[l364] = 0.0f;
			
		}
		for (int l365 = 0; (l365 < 2); l365 = (l365 + 1)) {
			fRec506[l365] = 0.0f;
			
		}
		for (int l366 = 0; (l366 < 2); l366 = (l366 + 1)) {
			fRec504[l366] = 0.0f;
			
		}
		for (int l367 = 0; (l367 < 2); l367 = (l367 + 1)) {
			fRec524[l367] = 0.0f;
			
		}
		for (int l368 = 0; (l368 < 2); l368 = (l368 + 1)) {
			fRec522[l368] = 0.0f;
			
		}
		for (int l369 = 0; (l369 < 2); l369 = (l369 + 1)) {
			fRec521[l369] = 0.0f;
			
		}
		for (int l370 = 0; (l370 < 2); l370 = (l370 + 1)) {
			fRec519[l370] = 0.0f;
			
		}
		for (int l371 = 0; (l371 < 2); l371 = (l371 + 1)) {
			fRec518[l371] = 0.0f;
			
		}
		for (int l372 = 0; (l372 < 2); l372 = (l372 + 1)) {
			fRec516[l372] = 0.0f;
			
		}
		for (int l373 = 0; (l373 < 2); l373 = (l373 + 1)) {
			fRec527[l373] = 0.0f;
			
		}
		for (int l374 = 0; (l374 < 2); l374 = (l374 + 1)) {
			fRec525[l374] = 0.0f;
			
		}
		for (int l375 = 0; (l375 < 2); l375 = (l375 + 1)) {
			fRec533[l375] = 0.0f;
			
		}
		for (int l376 = 0; (l376 < 2); l376 = (l376 + 1)) {
			fRec531[l376] = 0.0f;
			
		}
		for (int l377 = 0; (l377 < 2); l377 = (l377 + 1)) {
			fRec530[l377] = 0.0f;
			
		}
		for (int l378 = 0; (l378 < 2); l378 = (l378 + 1)) {
			fRec528[l378] = 0.0f;
			
		}
		for (int l379 = 0; (l379 < 1024); l379 = (l379 + 1)) {
			fVec10[l379] = 0.0f;
			
		}
		for (int l380 = 0; (l380 < 2); l380 = (l380 + 1)) {
			fRec548[l380] = 0.0f;
			
		}
		for (int l381 = 0; (l381 < 2); l381 = (l381 + 1)) {
			fRec546[l381] = 0.0f;
			
		}
		for (int l382 = 0; (l382 < 2); l382 = (l382 + 1)) {
			fRec545[l382] = 0.0f;
			
		}
		for (int l383 = 0; (l383 < 2); l383 = (l383 + 1)) {
			fRec543[l383] = 0.0f;
			
		}
		for (int l384 = 0; (l384 < 2); l384 = (l384 + 1)) {
			fRec542[l384] = 0.0f;
			
		}
		for (int l385 = 0; (l385 < 2); l385 = (l385 + 1)) {
			fRec540[l385] = 0.0f;
			
		}
		for (int l386 = 0; (l386 < 2); l386 = (l386 + 1)) {
			fRec539[l386] = 0.0f;
			
		}
		for (int l387 = 0; (l387 < 2); l387 = (l387 + 1)) {
			fRec537[l387] = 0.0f;
			
		}
		for (int l388 = 0; (l388 < 2); l388 = (l388 + 1)) {
			fRec536[l388] = 0.0f;
			
		}
		for (int l389 = 0; (l389 < 2); l389 = (l389 + 1)) {
			fRec534[l389] = 0.0f;
			
		}
		for (int l390 = 0; (l390 < 2); l390 = (l390 + 1)) {
			fRec560[l390] = 0.0f;
			
		}
		for (int l391 = 0; (l391 < 2); l391 = (l391 + 1)) {
			fRec558[l391] = 0.0f;
			
		}
		for (int l392 = 0; (l392 < 2); l392 = (l392 + 1)) {
			fRec557[l392] = 0.0f;
			
		}
		for (int l393 = 0; (l393 < 2); l393 = (l393 + 1)) {
			fRec555[l393] = 0.0f;
			
		}
		for (int l394 = 0; (l394 < 2); l394 = (l394 + 1)) {
			fRec554[l394] = 0.0f;
			
		}
		for (int l395 = 0; (l395 < 2); l395 = (l395 + 1)) {
			fRec552[l395] = 0.0f;
			
		}
		for (int l396 = 0; (l396 < 2); l396 = (l396 + 1)) {
			fRec551[l396] = 0.0f;
			
		}
		for (int l397 = 0; (l397 < 2); l397 = (l397 + 1)) {
			fRec549[l397] = 0.0f;
			
		}
		for (int l398 = 0; (l398 < 2); l398 = (l398 + 1)) {
			fRec569[l398] = 0.0f;
			
		}
		for (int l399 = 0; (l399 < 2); l399 = (l399 + 1)) {
			fRec567[l399] = 0.0f;
			
		}
		for (int l400 = 0; (l400 < 2); l400 = (l400 + 1)) {
			fRec566[l400] = 0.0f;
			
		}
		for (int l401 = 0; (l401 < 2); l401 = (l401 + 1)) {
			fRec564[l401] = 0.0f;
			
		}
		for (int l402 = 0; (l402 < 2); l402 = (l402 + 1)) {
			fRec563[l402] = 0.0f;
			
		}
		for (int l403 = 0; (l403 < 2); l403 = (l403 + 1)) {
			fRec561[l403] = 0.0f;
			
		}
		for (int l404 = 0; (l404 < 2); l404 = (l404 + 1)) {
			fRec572[l404] = 0.0f;
			
		}
		for (int l405 = 0; (l405 < 2); l405 = (l405 + 1)) {
			fRec570[l405] = 0.0f;
			
		}
		for (int l406 = 0; (l406 < 2); l406 = (l406 + 1)) {
			fRec578[l406] = 0.0f;
			
		}
		for (int l407 = 0; (l407 < 2); l407 = (l407 + 1)) {
			fRec576[l407] = 0.0f;
			
		}
		for (int l408 = 0; (l408 < 2); l408 = (l408 + 1)) {
			fRec575[l408] = 0.0f;
			
		}
		for (int l409 = 0; (l409 < 2); l409 = (l409 + 1)) {
			fRec573[l409] = 0.0f;
			
		}
		for (int l410 = 0; (l410 < 1024); l410 = (l410 + 1)) {
			fVec11[l410] = 0.0f;
			
		}
		for (int l411 = 0; (l411 < 2); l411 = (l411 + 1)) {
			fRec593[l411] = 0.0f;
			
		}
		for (int l412 = 0; (l412 < 2); l412 = (l412 + 1)) {
			fRec591[l412] = 0.0f;
			
		}
		for (int l413 = 0; (l413 < 2); l413 = (l413 + 1)) {
			fRec590[l413] = 0.0f;
			
		}
		for (int l414 = 0; (l414 < 2); l414 = (l414 + 1)) {
			fRec588[l414] = 0.0f;
			
		}
		for (int l415 = 0; (l415 < 2); l415 = (l415 + 1)) {
			fRec587[l415] = 0.0f;
			
		}
		for (int l416 = 0; (l416 < 2); l416 = (l416 + 1)) {
			fRec585[l416] = 0.0f;
			
		}
		for (int l417 = 0; (l417 < 2); l417 = (l417 + 1)) {
			fRec584[l417] = 0.0f;
			
		}
		for (int l418 = 0; (l418 < 2); l418 = (l418 + 1)) {
			fRec582[l418] = 0.0f;
			
		}
		for (int l419 = 0; (l419 < 2); l419 = (l419 + 1)) {
			fRec581[l419] = 0.0f;
			
		}
		for (int l420 = 0; (l420 < 2); l420 = (l420 + 1)) {
			fRec579[l420] = 0.0f;
			
		}
		for (int l421 = 0; (l421 < 2); l421 = (l421 + 1)) {
			fRec605[l421] = 0.0f;
			
		}
		for (int l422 = 0; (l422 < 2); l422 = (l422 + 1)) {
			fRec603[l422] = 0.0f;
			
		}
		for (int l423 = 0; (l423 < 2); l423 = (l423 + 1)) {
			fRec602[l423] = 0.0f;
			
		}
		for (int l424 = 0; (l424 < 2); l424 = (l424 + 1)) {
			fRec600[l424] = 0.0f;
			
		}
		for (int l425 = 0; (l425 < 2); l425 = (l425 + 1)) {
			fRec599[l425] = 0.0f;
			
		}
		for (int l426 = 0; (l426 < 2); l426 = (l426 + 1)) {
			fRec597[l426] = 0.0f;
			
		}
		for (int l427 = 0; (l427 < 2); l427 = (l427 + 1)) {
			fRec596[l427] = 0.0f;
			
		}
		for (int l428 = 0; (l428 < 2); l428 = (l428 + 1)) {
			fRec594[l428] = 0.0f;
			
		}
		for (int l429 = 0; (l429 < 2); l429 = (l429 + 1)) {
			fRec614[l429] = 0.0f;
			
		}
		for (int l430 = 0; (l430 < 2); l430 = (l430 + 1)) {
			fRec612[l430] = 0.0f;
			
		}
		for (int l431 = 0; (l431 < 2); l431 = (l431 + 1)) {
			fRec611[l431] = 0.0f;
			
		}
		for (int l432 = 0; (l432 < 2); l432 = (l432 + 1)) {
			fRec609[l432] = 0.0f;
			
		}
		for (int l433 = 0; (l433 < 2); l433 = (l433 + 1)) {
			fRec608[l433] = 0.0f;
			
		}
		for (int l434 = 0; (l434 < 2); l434 = (l434 + 1)) {
			fRec606[l434] = 0.0f;
			
		}
		for (int l435 = 0; (l435 < 2); l435 = (l435 + 1)) {
			fRec617[l435] = 0.0f;
			
		}
		for (int l436 = 0; (l436 < 2); l436 = (l436 + 1)) {
			fRec615[l436] = 0.0f;
			
		}
		for (int l437 = 0; (l437 < 2); l437 = (l437 + 1)) {
			fRec623[l437] = 0.0f;
			
		}
		for (int l438 = 0; (l438 < 2); l438 = (l438 + 1)) {
			fRec621[l438] = 0.0f;
			
		}
		for (int l439 = 0; (l439 < 2); l439 = (l439 + 1)) {
			fRec620[l439] = 0.0f;
			
		}
		for (int l440 = 0; (l440 < 2); l440 = (l440 + 1)) {
			fRec618[l440] = 0.0f;
			
		}
		for (int l441 = 0; (l441 < 1024); l441 = (l441 + 1)) {
			fVec12[l441] = 0.0f;
			
		}
		for (int l442 = 0; (l442 < 2); l442 = (l442 + 1)) {
			fRec638[l442] = 0.0f;
			
		}
		for (int l443 = 0; (l443 < 2); l443 = (l443 + 1)) {
			fRec636[l443] = 0.0f;
			
		}
		for (int l444 = 0; (l444 < 2); l444 = (l444 + 1)) {
			fRec635[l444] = 0.0f;
			
		}
		for (int l445 = 0; (l445 < 2); l445 = (l445 + 1)) {
			fRec633[l445] = 0.0f;
			
		}
		for (int l446 = 0; (l446 < 2); l446 = (l446 + 1)) {
			fRec632[l446] = 0.0f;
			
		}
		for (int l447 = 0; (l447 < 2); l447 = (l447 + 1)) {
			fRec630[l447] = 0.0f;
			
		}
		for (int l448 = 0; (l448 < 2); l448 = (l448 + 1)) {
			fRec629[l448] = 0.0f;
			
		}
		for (int l449 = 0; (l449 < 2); l449 = (l449 + 1)) {
			fRec627[l449] = 0.0f;
			
		}
		for (int l450 = 0; (l450 < 2); l450 = (l450 + 1)) {
			fRec626[l450] = 0.0f;
			
		}
		for (int l451 = 0; (l451 < 2); l451 = (l451 + 1)) {
			fRec624[l451] = 0.0f;
			
		}
		for (int l452 = 0; (l452 < 2); l452 = (l452 + 1)) {
			fRec650[l452] = 0.0f;
			
		}
		for (int l453 = 0; (l453 < 2); l453 = (l453 + 1)) {
			fRec648[l453] = 0.0f;
			
		}
		for (int l454 = 0; (l454 < 2); l454 = (l454 + 1)) {
			fRec647[l454] = 0.0f;
			
		}
		for (int l455 = 0; (l455 < 2); l455 = (l455 + 1)) {
			fRec645[l455] = 0.0f;
			
		}
		for (int l456 = 0; (l456 < 2); l456 = (l456 + 1)) {
			fRec644[l456] = 0.0f;
			
		}
		for (int l457 = 0; (l457 < 2); l457 = (l457 + 1)) {
			fRec642[l457] = 0.0f;
			
		}
		for (int l458 = 0; (l458 < 2); l458 = (l458 + 1)) {
			fRec641[l458] = 0.0f;
			
		}
		for (int l459 = 0; (l459 < 2); l459 = (l459 + 1)) {
			fRec639[l459] = 0.0f;
			
		}
		for (int l460 = 0; (l460 < 2); l460 = (l460 + 1)) {
			fRec659[l460] = 0.0f;
			
		}
		for (int l461 = 0; (l461 < 2); l461 = (l461 + 1)) {
			fRec657[l461] = 0.0f;
			
		}
		for (int l462 = 0; (l462 < 2); l462 = (l462 + 1)) {
			fRec656[l462] = 0.0f;
			
		}
		for (int l463 = 0; (l463 < 2); l463 = (l463 + 1)) {
			fRec654[l463] = 0.0f;
			
		}
		for (int l464 = 0; (l464 < 2); l464 = (l464 + 1)) {
			fRec653[l464] = 0.0f;
			
		}
		for (int l465 = 0; (l465 < 2); l465 = (l465 + 1)) {
			fRec651[l465] = 0.0f;
			
		}
		for (int l466 = 0; (l466 < 2); l466 = (l466 + 1)) {
			fRec662[l466] = 0.0f;
			
		}
		for (int l467 = 0; (l467 < 2); l467 = (l467 + 1)) {
			fRec660[l467] = 0.0f;
			
		}
		for (int l468 = 0; (l468 < 2); l468 = (l468 + 1)) {
			fRec668[l468] = 0.0f;
			
		}
		for (int l469 = 0; (l469 < 2); l469 = (l469 + 1)) {
			fRec666[l469] = 0.0f;
			
		}
		for (int l470 = 0; (l470 < 2); l470 = (l470 + 1)) {
			fRec665[l470] = 0.0f;
			
		}
		for (int l471 = 0; (l471 < 2); l471 = (l471 + 1)) {
			fRec663[l471] = 0.0f;
			
		}
		for (int l472 = 0; (l472 < 1024); l472 = (l472 + 1)) {
			fVec13[l472] = 0.0f;
			
		}
		for (int l473 = 0; (l473 < 2); l473 = (l473 + 1)) {
			fRec683[l473] = 0.0f;
			
		}
		for (int l474 = 0; (l474 < 2); l474 = (l474 + 1)) {
			fRec681[l474] = 0.0f;
			
		}
		for (int l475 = 0; (l475 < 2); l475 = (l475 + 1)) {
			fRec680[l475] = 0.0f;
			
		}
		for (int l476 = 0; (l476 < 2); l476 = (l476 + 1)) {
			fRec678[l476] = 0.0f;
			
		}
		for (int l477 = 0; (l477 < 2); l477 = (l477 + 1)) {
			fRec677[l477] = 0.0f;
			
		}
		for (int l478 = 0; (l478 < 2); l478 = (l478 + 1)) {
			fRec675[l478] = 0.0f;
			
		}
		for (int l479 = 0; (l479 < 2); l479 = (l479 + 1)) {
			fRec674[l479] = 0.0f;
			
		}
		for (int l480 = 0; (l480 < 2); l480 = (l480 + 1)) {
			fRec672[l480] = 0.0f;
			
		}
		for (int l481 = 0; (l481 < 2); l481 = (l481 + 1)) {
			fRec671[l481] = 0.0f;
			
		}
		for (int l482 = 0; (l482 < 2); l482 = (l482 + 1)) {
			fRec669[l482] = 0.0f;
			
		}
		for (int l483 = 0; (l483 < 2); l483 = (l483 + 1)) {
			fRec695[l483] = 0.0f;
			
		}
		for (int l484 = 0; (l484 < 2); l484 = (l484 + 1)) {
			fRec693[l484] = 0.0f;
			
		}
		for (int l485 = 0; (l485 < 2); l485 = (l485 + 1)) {
			fRec692[l485] = 0.0f;
			
		}
		for (int l486 = 0; (l486 < 2); l486 = (l486 + 1)) {
			fRec690[l486] = 0.0f;
			
		}
		for (int l487 = 0; (l487 < 2); l487 = (l487 + 1)) {
			fRec689[l487] = 0.0f;
			
		}
		for (int l488 = 0; (l488 < 2); l488 = (l488 + 1)) {
			fRec687[l488] = 0.0f;
			
		}
		for (int l489 = 0; (l489 < 2); l489 = (l489 + 1)) {
			fRec686[l489] = 0.0f;
			
		}
		for (int l490 = 0; (l490 < 2); l490 = (l490 + 1)) {
			fRec684[l490] = 0.0f;
			
		}
		for (int l491 = 0; (l491 < 2); l491 = (l491 + 1)) {
			fRec704[l491] = 0.0f;
			
		}
		for (int l492 = 0; (l492 < 2); l492 = (l492 + 1)) {
			fRec702[l492] = 0.0f;
			
		}
		for (int l493 = 0; (l493 < 2); l493 = (l493 + 1)) {
			fRec701[l493] = 0.0f;
			
		}
		for (int l494 = 0; (l494 < 2); l494 = (l494 + 1)) {
			fRec699[l494] = 0.0f;
			
		}
		for (int l495 = 0; (l495 < 2); l495 = (l495 + 1)) {
			fRec698[l495] = 0.0f;
			
		}
		for (int l496 = 0; (l496 < 2); l496 = (l496 + 1)) {
			fRec696[l496] = 0.0f;
			
		}
		for (int l497 = 0; (l497 < 2); l497 = (l497 + 1)) {
			fRec707[l497] = 0.0f;
			
		}
		for (int l498 = 0; (l498 < 2); l498 = (l498 + 1)) {
			fRec705[l498] = 0.0f;
			
		}
		for (int l499 = 0; (l499 < 2); l499 = (l499 + 1)) {
			fRec713[l499] = 0.0f;
			
		}
		for (int l500 = 0; (l500 < 2); l500 = (l500 + 1)) {
			fRec711[l500] = 0.0f;
			
		}
		for (int l501 = 0; (l501 < 2); l501 = (l501 + 1)) {
			fRec710[l501] = 0.0f;
			
		}
		for (int l502 = 0; (l502 < 2); l502 = (l502 + 1)) {
			fRec708[l502] = 0.0f;
			
		}
		for (int l503 = 0; (l503 < 1024); l503 = (l503 + 1)) {
			fVec14[l503] = 0.0f;
			
		}
		for (int l504 = 0; (l504 < 2); l504 = (l504 + 1)) {
			fRec728[l504] = 0.0f;
			
		}
		for (int l505 = 0; (l505 < 2); l505 = (l505 + 1)) {
			fRec726[l505] = 0.0f;
			
		}
		for (int l506 = 0; (l506 < 2); l506 = (l506 + 1)) {
			fRec725[l506] = 0.0f;
			
		}
		for (int l507 = 0; (l507 < 2); l507 = (l507 + 1)) {
			fRec723[l507] = 0.0f;
			
		}
		for (int l508 = 0; (l508 < 2); l508 = (l508 + 1)) {
			fRec722[l508] = 0.0f;
			
		}
		for (int l509 = 0; (l509 < 2); l509 = (l509 + 1)) {
			fRec720[l509] = 0.0f;
			
		}
		for (int l510 = 0; (l510 < 2); l510 = (l510 + 1)) {
			fRec719[l510] = 0.0f;
			
		}
		for (int l511 = 0; (l511 < 2); l511 = (l511 + 1)) {
			fRec717[l511] = 0.0f;
			
		}
		for (int l512 = 0; (l512 < 2); l512 = (l512 + 1)) {
			fRec716[l512] = 0.0f;
			
		}
		for (int l513 = 0; (l513 < 2); l513 = (l513 + 1)) {
			fRec714[l513] = 0.0f;
			
		}
		for (int l514 = 0; (l514 < 2); l514 = (l514 + 1)) {
			fRec740[l514] = 0.0f;
			
		}
		for (int l515 = 0; (l515 < 2); l515 = (l515 + 1)) {
			fRec738[l515] = 0.0f;
			
		}
		for (int l516 = 0; (l516 < 2); l516 = (l516 + 1)) {
			fRec737[l516] = 0.0f;
			
		}
		for (int l517 = 0; (l517 < 2); l517 = (l517 + 1)) {
			fRec735[l517] = 0.0f;
			
		}
		for (int l518 = 0; (l518 < 2); l518 = (l518 + 1)) {
			fRec734[l518] = 0.0f;
			
		}
		for (int l519 = 0; (l519 < 2); l519 = (l519 + 1)) {
			fRec732[l519] = 0.0f;
			
		}
		for (int l520 = 0; (l520 < 2); l520 = (l520 + 1)) {
			fRec731[l520] = 0.0f;
			
		}
		for (int l521 = 0; (l521 < 2); l521 = (l521 + 1)) {
			fRec729[l521] = 0.0f;
			
		}
		for (int l522 = 0; (l522 < 2); l522 = (l522 + 1)) {
			fRec749[l522] = 0.0f;
			
		}
		for (int l523 = 0; (l523 < 2); l523 = (l523 + 1)) {
			fRec747[l523] = 0.0f;
			
		}
		for (int l524 = 0; (l524 < 2); l524 = (l524 + 1)) {
			fRec746[l524] = 0.0f;
			
		}
		for (int l525 = 0; (l525 < 2); l525 = (l525 + 1)) {
			fRec744[l525] = 0.0f;
			
		}
		for (int l526 = 0; (l526 < 2); l526 = (l526 + 1)) {
			fRec743[l526] = 0.0f;
			
		}
		for (int l527 = 0; (l527 < 2); l527 = (l527 + 1)) {
			fRec741[l527] = 0.0f;
			
		}
		for (int l528 = 0; (l528 < 2); l528 = (l528 + 1)) {
			fRec752[l528] = 0.0f;
			
		}
		for (int l529 = 0; (l529 < 2); l529 = (l529 + 1)) {
			fRec750[l529] = 0.0f;
			
		}
		for (int l530 = 0; (l530 < 2); l530 = (l530 + 1)) {
			fRec758[l530] = 0.0f;
			
		}
		for (int l531 = 0; (l531 < 2); l531 = (l531 + 1)) {
			fRec756[l531] = 0.0f;
			
		}
		for (int l532 = 0; (l532 < 2); l532 = (l532 + 1)) {
			fRec755[l532] = 0.0f;
			
		}
		for (int l533 = 0; (l533 < 2); l533 = (l533 + 1)) {
			fRec753[l533] = 0.0f;
			
		}
		for (int l534 = 0; (l534 < 1024); l534 = (l534 + 1)) {
			fVec15[l534] = 0.0f;
			
		}
		for (int l535 = 0; (l535 < 2); l535 = (l535 + 1)) {
			fRec773[l535] = 0.0f;
			
		}
		for (int l536 = 0; (l536 < 2); l536 = (l536 + 1)) {
			fRec771[l536] = 0.0f;
			
		}
		for (int l537 = 0; (l537 < 2); l537 = (l537 + 1)) {
			fRec770[l537] = 0.0f;
			
		}
		for (int l538 = 0; (l538 < 2); l538 = (l538 + 1)) {
			fRec768[l538] = 0.0f;
			
		}
		for (int l539 = 0; (l539 < 2); l539 = (l539 + 1)) {
			fRec767[l539] = 0.0f;
			
		}
		for (int l540 = 0; (l540 < 2); l540 = (l540 + 1)) {
			fRec765[l540] = 0.0f;
			
		}
		for (int l541 = 0; (l541 < 2); l541 = (l541 + 1)) {
			fRec764[l541] = 0.0f;
			
		}
		for (int l542 = 0; (l542 < 2); l542 = (l542 + 1)) {
			fRec762[l542] = 0.0f;
			
		}
		for (int l543 = 0; (l543 < 2); l543 = (l543 + 1)) {
			fRec761[l543] = 0.0f;
			
		}
		for (int l544 = 0; (l544 < 2); l544 = (l544 + 1)) {
			fRec759[l544] = 0.0f;
			
		}
		for (int l545 = 0; (l545 < 2); l545 = (l545 + 1)) {
			fRec785[l545] = 0.0f;
			
		}
		for (int l546 = 0; (l546 < 2); l546 = (l546 + 1)) {
			fRec783[l546] = 0.0f;
			
		}
		for (int l547 = 0; (l547 < 2); l547 = (l547 + 1)) {
			fRec782[l547] = 0.0f;
			
		}
		for (int l548 = 0; (l548 < 2); l548 = (l548 + 1)) {
			fRec780[l548] = 0.0f;
			
		}
		for (int l549 = 0; (l549 < 2); l549 = (l549 + 1)) {
			fRec779[l549] = 0.0f;
			
		}
		for (int l550 = 0; (l550 < 2); l550 = (l550 + 1)) {
			fRec777[l550] = 0.0f;
			
		}
		for (int l551 = 0; (l551 < 2); l551 = (l551 + 1)) {
			fRec776[l551] = 0.0f;
			
		}
		for (int l552 = 0; (l552 < 2); l552 = (l552 + 1)) {
			fRec774[l552] = 0.0f;
			
		}
		for (int l553 = 0; (l553 < 2); l553 = (l553 + 1)) {
			fRec794[l553] = 0.0f;
			
		}
		for (int l554 = 0; (l554 < 2); l554 = (l554 + 1)) {
			fRec792[l554] = 0.0f;
			
		}
		for (int l555 = 0; (l555 < 2); l555 = (l555 + 1)) {
			fRec791[l555] = 0.0f;
			
		}
		for (int l556 = 0; (l556 < 2); l556 = (l556 + 1)) {
			fRec789[l556] = 0.0f;
			
		}
		for (int l557 = 0; (l557 < 2); l557 = (l557 + 1)) {
			fRec788[l557] = 0.0f;
			
		}
		for (int l558 = 0; (l558 < 2); l558 = (l558 + 1)) {
			fRec786[l558] = 0.0f;
			
		}
		for (int l559 = 0; (l559 < 2); l559 = (l559 + 1)) {
			fRec800[l559] = 0.0f;
			
		}
		for (int l560 = 0; (l560 < 2); l560 = (l560 + 1)) {
			fRec798[l560] = 0.0f;
			
		}
		for (int l561 = 0; (l561 < 2); l561 = (l561 + 1)) {
			fRec797[l561] = 0.0f;
			
		}
		for (int l562 = 0; (l562 < 2); l562 = (l562 + 1)) {
			fRec795[l562] = 0.0f;
			
		}
		for (int l563 = 0; (l563 < 2); l563 = (l563 + 1)) {
			fRec803[l563] = 0.0f;
			
		}
		for (int l564 = 0; (l564 < 2); l564 = (l564 + 1)) {
			fRec801[l564] = 0.0f;
			
		}
		for (int l565 = 0; (l565 < 1024); l565 = (l565 + 1)) {
			fVec16[l565] = 0.0f;
			
		}
		for (int l566 = 0; (l566 < 2); l566 = (l566 + 1)) {
			fRec812[l566] = 0.0f;
			
		}
		for (int l567 = 0; (l567 < 2); l567 = (l567 + 1)) {
			fRec810[l567] = 0.0f;
			
		}
		for (int l568 = 0; (l568 < 2); l568 = (l568 + 1)) {
			fRec809[l568] = 0.0f;
			
		}
		for (int l569 = 0; (l569 < 2); l569 = (l569 + 1)) {
			fRec807[l569] = 0.0f;
			
		}
		for (int l570 = 0; (l570 < 2); l570 = (l570 + 1)) {
			fRec806[l570] = 0.0f;
			
		}
		for (int l571 = 0; (l571 < 2); l571 = (l571 + 1)) {
			fRec804[l571] = 0.0f;
			
		}
		for (int l572 = 0; (l572 < 2); l572 = (l572 + 1)) {
			fRec818[l572] = 0.0f;
			
		}
		for (int l573 = 0; (l573 < 2); l573 = (l573 + 1)) {
			fRec816[l573] = 0.0f;
			
		}
		for (int l574 = 0; (l574 < 2); l574 = (l574 + 1)) {
			fRec815[l574] = 0.0f;
			
		}
		for (int l575 = 0; (l575 < 2); l575 = (l575 + 1)) {
			fRec813[l575] = 0.0f;
			
		}
		for (int l576 = 0; (l576 < 2); l576 = (l576 + 1)) {
			fRec821[l576] = 0.0f;
			
		}
		for (int l577 = 0; (l577 < 2); l577 = (l577 + 1)) {
			fRec819[l577] = 0.0f;
			
		}
		for (int l578 = 0; (l578 < 2); l578 = (l578 + 1)) {
			fRec833[l578] = 0.0f;
			
		}
		for (int l579 = 0; (l579 < 2); l579 = (l579 + 1)) {
			fRec831[l579] = 0.0f;
			
		}
		for (int l580 = 0; (l580 < 2); l580 = (l580 + 1)) {
			fRec830[l580] = 0.0f;
			
		}
		for (int l581 = 0; (l581 < 2); l581 = (l581 + 1)) {
			fRec828[l581] = 0.0f;
			
		}
		for (int l582 = 0; (l582 < 2); l582 = (l582 + 1)) {
			fRec827[l582] = 0.0f;
			
		}
		for (int l583 = 0; (l583 < 2); l583 = (l583 + 1)) {
			fRec825[l583] = 0.0f;
			
		}
		for (int l584 = 0; (l584 < 2); l584 = (l584 + 1)) {
			fRec824[l584] = 0.0f;
			
		}
		for (int l585 = 0; (l585 < 2); l585 = (l585 + 1)) {
			fRec822[l585] = 0.0f;
			
		}
		for (int l586 = 0; (l586 < 2); l586 = (l586 + 1)) {
			fRec848[l586] = 0.0f;
			
		}
		for (int l587 = 0; (l587 < 2); l587 = (l587 + 1)) {
			fRec846[l587] = 0.0f;
			
		}
		for (int l588 = 0; (l588 < 2); l588 = (l588 + 1)) {
			fRec845[l588] = 0.0f;
			
		}
		for (int l589 = 0; (l589 < 2); l589 = (l589 + 1)) {
			fRec843[l589] = 0.0f;
			
		}
		for (int l590 = 0; (l590 < 2); l590 = (l590 + 1)) {
			fRec842[l590] = 0.0f;
			
		}
		for (int l591 = 0; (l591 < 2); l591 = (l591 + 1)) {
			fRec840[l591] = 0.0f;
			
		}
		for (int l592 = 0; (l592 < 2); l592 = (l592 + 1)) {
			fRec839[l592] = 0.0f;
			
		}
		for (int l593 = 0; (l593 < 2); l593 = (l593 + 1)) {
			fRec837[l593] = 0.0f;
			
		}
		for (int l594 = 0; (l594 < 2); l594 = (l594 + 1)) {
			fRec836[l594] = 0.0f;
			
		}
		for (int l595 = 0; (l595 < 2); l595 = (l595 + 1)) {
			fRec834[l595] = 0.0f;
			
		}
		for (int l596 = 0; (l596 < 1024); l596 = (l596 + 1)) {
			fVec17[l596] = 0.0f;
			
		}
		for (int l597 = 0; (l597 < 2); l597 = (l597 + 1)) {
			fRec863[l597] = 0.0f;
			
		}
		for (int l598 = 0; (l598 < 2); l598 = (l598 + 1)) {
			fRec861[l598] = 0.0f;
			
		}
		for (int l599 = 0; (l599 < 2); l599 = (l599 + 1)) {
			fRec860[l599] = 0.0f;
			
		}
		for (int l600 = 0; (l600 < 2); l600 = (l600 + 1)) {
			fRec858[l600] = 0.0f;
			
		}
		for (int l601 = 0; (l601 < 2); l601 = (l601 + 1)) {
			fRec857[l601] = 0.0f;
			
		}
		for (int l602 = 0; (l602 < 2); l602 = (l602 + 1)) {
			fRec855[l602] = 0.0f;
			
		}
		for (int l603 = 0; (l603 < 2); l603 = (l603 + 1)) {
			fRec854[l603] = 0.0f;
			
		}
		for (int l604 = 0; (l604 < 2); l604 = (l604 + 1)) {
			fRec852[l604] = 0.0f;
			
		}
		for (int l605 = 0; (l605 < 2); l605 = (l605 + 1)) {
			fRec851[l605] = 0.0f;
			
		}
		for (int l606 = 0; (l606 < 2); l606 = (l606 + 1)) {
			fRec849[l606] = 0.0f;
			
		}
		for (int l607 = 0; (l607 < 2); l607 = (l607 + 1)) {
			fRec875[l607] = 0.0f;
			
		}
		for (int l608 = 0; (l608 < 2); l608 = (l608 + 1)) {
			fRec873[l608] = 0.0f;
			
		}
		for (int l609 = 0; (l609 < 2); l609 = (l609 + 1)) {
			fRec872[l609] = 0.0f;
			
		}
		for (int l610 = 0; (l610 < 2); l610 = (l610 + 1)) {
			fRec870[l610] = 0.0f;
			
		}
		for (int l611 = 0; (l611 < 2); l611 = (l611 + 1)) {
			fRec869[l611] = 0.0f;
			
		}
		for (int l612 = 0; (l612 < 2); l612 = (l612 + 1)) {
			fRec867[l612] = 0.0f;
			
		}
		for (int l613 = 0; (l613 < 2); l613 = (l613 + 1)) {
			fRec866[l613] = 0.0f;
			
		}
		for (int l614 = 0; (l614 < 2); l614 = (l614 + 1)) {
			fRec864[l614] = 0.0f;
			
		}
		for (int l615 = 0; (l615 < 2); l615 = (l615 + 1)) {
			fRec884[l615] = 0.0f;
			
		}
		for (int l616 = 0; (l616 < 2); l616 = (l616 + 1)) {
			fRec882[l616] = 0.0f;
			
		}
		for (int l617 = 0; (l617 < 2); l617 = (l617 + 1)) {
			fRec881[l617] = 0.0f;
			
		}
		for (int l618 = 0; (l618 < 2); l618 = (l618 + 1)) {
			fRec879[l618] = 0.0f;
			
		}
		for (int l619 = 0; (l619 < 2); l619 = (l619 + 1)) {
			fRec878[l619] = 0.0f;
			
		}
		for (int l620 = 0; (l620 < 2); l620 = (l620 + 1)) {
			fRec876[l620] = 0.0f;
			
		}
		for (int l621 = 0; (l621 < 2); l621 = (l621 + 1)) {
			fRec887[l621] = 0.0f;
			
		}
		for (int l622 = 0; (l622 < 2); l622 = (l622 + 1)) {
			fRec885[l622] = 0.0f;
			
		}
		for (int l623 = 0; (l623 < 2); l623 = (l623 + 1)) {
			fRec893[l623] = 0.0f;
			
		}
		for (int l624 = 0; (l624 < 2); l624 = (l624 + 1)) {
			fRec891[l624] = 0.0f;
			
		}
		for (int l625 = 0; (l625 < 2); l625 = (l625 + 1)) {
			fRec890[l625] = 0.0f;
			
		}
		for (int l626 = 0; (l626 < 2); l626 = (l626 + 1)) {
			fRec888[l626] = 0.0f;
			
		}
		for (int l627 = 0; (l627 < 1024); l627 = (l627 + 1)) {
			fVec18[l627] = 0.0f;
			
		}
		for (int l628 = 0; (l628 < 2); l628 = (l628 + 1)) {
			fRec908[l628] = 0.0f;
			
		}
		for (int l629 = 0; (l629 < 2); l629 = (l629 + 1)) {
			fRec906[l629] = 0.0f;
			
		}
		for (int l630 = 0; (l630 < 2); l630 = (l630 + 1)) {
			fRec905[l630] = 0.0f;
			
		}
		for (int l631 = 0; (l631 < 2); l631 = (l631 + 1)) {
			fRec903[l631] = 0.0f;
			
		}
		for (int l632 = 0; (l632 < 2); l632 = (l632 + 1)) {
			fRec902[l632] = 0.0f;
			
		}
		for (int l633 = 0; (l633 < 2); l633 = (l633 + 1)) {
			fRec900[l633] = 0.0f;
			
		}
		for (int l634 = 0; (l634 < 2); l634 = (l634 + 1)) {
			fRec899[l634] = 0.0f;
			
		}
		for (int l635 = 0; (l635 < 2); l635 = (l635 + 1)) {
			fRec897[l635] = 0.0f;
			
		}
		for (int l636 = 0; (l636 < 2); l636 = (l636 + 1)) {
			fRec896[l636] = 0.0f;
			
		}
		for (int l637 = 0; (l637 < 2); l637 = (l637 + 1)) {
			fRec894[l637] = 0.0f;
			
		}
		for (int l638 = 0; (l638 < 2); l638 = (l638 + 1)) {
			fRec920[l638] = 0.0f;
			
		}
		for (int l639 = 0; (l639 < 2); l639 = (l639 + 1)) {
			fRec918[l639] = 0.0f;
			
		}
		for (int l640 = 0; (l640 < 2); l640 = (l640 + 1)) {
			fRec917[l640] = 0.0f;
			
		}
		for (int l641 = 0; (l641 < 2); l641 = (l641 + 1)) {
			fRec915[l641] = 0.0f;
			
		}
		for (int l642 = 0; (l642 < 2); l642 = (l642 + 1)) {
			fRec914[l642] = 0.0f;
			
		}
		for (int l643 = 0; (l643 < 2); l643 = (l643 + 1)) {
			fRec912[l643] = 0.0f;
			
		}
		for (int l644 = 0; (l644 < 2); l644 = (l644 + 1)) {
			fRec911[l644] = 0.0f;
			
		}
		for (int l645 = 0; (l645 < 2); l645 = (l645 + 1)) {
			fRec909[l645] = 0.0f;
			
		}
		for (int l646 = 0; (l646 < 2); l646 = (l646 + 1)) {
			fRec929[l646] = 0.0f;
			
		}
		for (int l647 = 0; (l647 < 2); l647 = (l647 + 1)) {
			fRec927[l647] = 0.0f;
			
		}
		for (int l648 = 0; (l648 < 2); l648 = (l648 + 1)) {
			fRec926[l648] = 0.0f;
			
		}
		for (int l649 = 0; (l649 < 2); l649 = (l649 + 1)) {
			fRec924[l649] = 0.0f;
			
		}
		for (int l650 = 0; (l650 < 2); l650 = (l650 + 1)) {
			fRec923[l650] = 0.0f;
			
		}
		for (int l651 = 0; (l651 < 2); l651 = (l651 + 1)) {
			fRec921[l651] = 0.0f;
			
		}
		for (int l652 = 0; (l652 < 2); l652 = (l652 + 1)) {
			fRec935[l652] = 0.0f;
			
		}
		for (int l653 = 0; (l653 < 2); l653 = (l653 + 1)) {
			fRec933[l653] = 0.0f;
			
		}
		for (int l654 = 0; (l654 < 2); l654 = (l654 + 1)) {
			fRec932[l654] = 0.0f;
			
		}
		for (int l655 = 0; (l655 < 2); l655 = (l655 + 1)) {
			fRec930[l655] = 0.0f;
			
		}
		for (int l656 = 0; (l656 < 2); l656 = (l656 + 1)) {
			fRec938[l656] = 0.0f;
			
		}
		for (int l657 = 0; (l657 < 2); l657 = (l657 + 1)) {
			fRec936[l657] = 0.0f;
			
		}
		for (int l658 = 0; (l658 < 3); l658 = (l658 + 1)) {
			fVec19[l658] = 0.0f;
			
		}
		for (int l659 = 0; (l659 < 2); l659 = (l659 + 1)) {
			fRec947[l659] = 0.0f;
			
		}
		for (int l660 = 0; (l660 < 2); l660 = (l660 + 1)) {
			fRec945[l660] = 0.0f;
			
		}
		for (int l661 = 0; (l661 < 2); l661 = (l661 + 1)) {
			fRec944[l661] = 0.0f;
			
		}
		for (int l662 = 0; (l662 < 2); l662 = (l662 + 1)) {
			fRec942[l662] = 0.0f;
			
		}
		for (int l663 = 0; (l663 < 2); l663 = (l663 + 1)) {
			fRec941[l663] = 0.0f;
			
		}
		for (int l664 = 0; (l664 < 2); l664 = (l664 + 1)) {
			fRec939[l664] = 0.0f;
			
		}
		for (int l665 = 0; (l665 < 2); l665 = (l665 + 1)) {
			fRec953[l665] = 0.0f;
			
		}
		for (int l666 = 0; (l666 < 2); l666 = (l666 + 1)) {
			fRec951[l666] = 0.0f;
			
		}
		for (int l667 = 0; (l667 < 2); l667 = (l667 + 1)) {
			fRec950[l667] = 0.0f;
			
		}
		for (int l668 = 0; (l668 < 2); l668 = (l668 + 1)) {
			fRec948[l668] = 0.0f;
			
		}
		for (int l669 = 0; (l669 < 2); l669 = (l669 + 1)) {
			fRec956[l669] = 0.0f;
			
		}
		for (int l670 = 0; (l670 < 2); l670 = (l670 + 1)) {
			fRec954[l670] = 0.0f;
			
		}
		for (int l671 = 0; (l671 < 2); l671 = (l671 + 1)) {
			fRec971[l671] = 0.0f;
			
		}
		for (int l672 = 0; (l672 < 2); l672 = (l672 + 1)) {
			fRec969[l672] = 0.0f;
			
		}
		for (int l673 = 0; (l673 < 2); l673 = (l673 + 1)) {
			fRec968[l673] = 0.0f;
			
		}
		for (int l674 = 0; (l674 < 2); l674 = (l674 + 1)) {
			fRec966[l674] = 0.0f;
			
		}
		for (int l675 = 0; (l675 < 2); l675 = (l675 + 1)) {
			fRec965[l675] = 0.0f;
			
		}
		for (int l676 = 0; (l676 < 2); l676 = (l676 + 1)) {
			fRec963[l676] = 0.0f;
			
		}
		for (int l677 = 0; (l677 < 2); l677 = (l677 + 1)) {
			fRec962[l677] = 0.0f;
			
		}
		for (int l678 = 0; (l678 < 2); l678 = (l678 + 1)) {
			fRec960[l678] = 0.0f;
			
		}
		for (int l679 = 0; (l679 < 2); l679 = (l679 + 1)) {
			fRec959[l679] = 0.0f;
			
		}
		for (int l680 = 0; (l680 < 2); l680 = (l680 + 1)) {
			fRec957[l680] = 0.0f;
			
		}
		for (int l681 = 0; (l681 < 2); l681 = (l681 + 1)) {
			fRec983[l681] = 0.0f;
			
		}
		for (int l682 = 0; (l682 < 2); l682 = (l682 + 1)) {
			fRec981[l682] = 0.0f;
			
		}
		for (int l683 = 0; (l683 < 2); l683 = (l683 + 1)) {
			fRec980[l683] = 0.0f;
			
		}
		for (int l684 = 0; (l684 < 2); l684 = (l684 + 1)) {
			fRec978[l684] = 0.0f;
			
		}
		for (int l685 = 0; (l685 < 2); l685 = (l685 + 1)) {
			fRec977[l685] = 0.0f;
			
		}
		for (int l686 = 0; (l686 < 2); l686 = (l686 + 1)) {
			fRec975[l686] = 0.0f;
			
		}
		for (int l687 = 0; (l687 < 2); l687 = (l687 + 1)) {
			fRec974[l687] = 0.0f;
			
		}
		for (int l688 = 0; (l688 < 2); l688 = (l688 + 1)) {
			fRec972[l688] = 0.0f;
			
		}
		for (int l689 = 0; (l689 < 2); l689 = (l689 + 1)) {
			fRec998[l689] = 0.0f;
			
		}
		for (int l690 = 0; (l690 < 2); l690 = (l690 + 1)) {
			fRec996[l690] = 0.0f;
			
		}
		for (int l691 = 0; (l691 < 2); l691 = (l691 + 1)) {
			fRec995[l691] = 0.0f;
			
		}
		for (int l692 = 0; (l692 < 2); l692 = (l692 + 1)) {
			fRec993[l692] = 0.0f;
			
		}
		for (int l693 = 0; (l693 < 2); l693 = (l693 + 1)) {
			fRec992[l693] = 0.0f;
			
		}
		for (int l694 = 0; (l694 < 2); l694 = (l694 + 1)) {
			fRec990[l694] = 0.0f;
			
		}
		for (int l695 = 0; (l695 < 2); l695 = (l695 + 1)) {
			fRec989[l695] = 0.0f;
			
		}
		for (int l696 = 0; (l696 < 2); l696 = (l696 + 1)) {
			fRec987[l696] = 0.0f;
			
		}
		for (int l697 = 0; (l697 < 2); l697 = (l697 + 1)) {
			fRec986[l697] = 0.0f;
			
		}
		for (int l698 = 0; (l698 < 2); l698 = (l698 + 1)) {
			fRec984[l698] = 0.0f;
			
		}
		for (int l699 = 0; (l699 < 2); l699 = (l699 + 1)) {
			fRec1010[l699] = 0.0f;
			
		}
		for (int l700 = 0; (l700 < 2); l700 = (l700 + 1)) {
			fRec1008[l700] = 0.0f;
			
		}
		for (int l701 = 0; (l701 < 2); l701 = (l701 + 1)) {
			fRec1007[l701] = 0.0f;
			
		}
		for (int l702 = 0; (l702 < 2); l702 = (l702 + 1)) {
			fRec1005[l702] = 0.0f;
			
		}
		for (int l703 = 0; (l703 < 2); l703 = (l703 + 1)) {
			fRec1004[l703] = 0.0f;
			
		}
		for (int l704 = 0; (l704 < 2); l704 = (l704 + 1)) {
			fRec1002[l704] = 0.0f;
			
		}
		for (int l705 = 0; (l705 < 2); l705 = (l705 + 1)) {
			fRec1001[l705] = 0.0f;
			
		}
		for (int l706 = 0; (l706 < 2); l706 = (l706 + 1)) {
			fRec999[l706] = 0.0f;
			
		}
		for (int l707 = 0; (l707 < 2); l707 = (l707 + 1)) {
			fRec1019[l707] = 0.0f;
			
		}
		for (int l708 = 0; (l708 < 2); l708 = (l708 + 1)) {
			fRec1017[l708] = 0.0f;
			
		}
		for (int l709 = 0; (l709 < 2); l709 = (l709 + 1)) {
			fRec1016[l709] = 0.0f;
			
		}
		for (int l710 = 0; (l710 < 2); l710 = (l710 + 1)) {
			fRec1014[l710] = 0.0f;
			
		}
		for (int l711 = 0; (l711 < 2); l711 = (l711 + 1)) {
			fRec1013[l711] = 0.0f;
			
		}
		for (int l712 = 0; (l712 < 2); l712 = (l712 + 1)) {
			fRec1011[l712] = 0.0f;
			
		}
		for (int l713 = 0; (l713 < 2); l713 = (l713 + 1)) {
			fRec1025[l713] = 0.0f;
			
		}
		for (int l714 = 0; (l714 < 2); l714 = (l714 + 1)) {
			fRec1023[l714] = 0.0f;
			
		}
		for (int l715 = 0; (l715 < 2); l715 = (l715 + 1)) {
			fRec1022[l715] = 0.0f;
			
		}
		for (int l716 = 0; (l716 < 2); l716 = (l716 + 1)) {
			fRec1020[l716] = 0.0f;
			
		}
		for (int l717 = 0; (l717 < 2); l717 = (l717 + 1)) {
			fRec1028[l717] = 0.0f;
			
		}
		for (int l718 = 0; (l718 < 2); l718 = (l718 + 1)) {
			fRec1026[l718] = 0.0f;
			
		}
		for (int l719 = 0; (l719 < 2); l719 = (l719 + 1)) {
			fRec1043[l719] = 0.0f;
			
		}
		for (int l720 = 0; (l720 < 2); l720 = (l720 + 1)) {
			fRec1041[l720] = 0.0f;
			
		}
		for (int l721 = 0; (l721 < 2); l721 = (l721 + 1)) {
			fRec1040[l721] = 0.0f;
			
		}
		for (int l722 = 0; (l722 < 2); l722 = (l722 + 1)) {
			fRec1038[l722] = 0.0f;
			
		}
		for (int l723 = 0; (l723 < 2); l723 = (l723 + 1)) {
			fRec1037[l723] = 0.0f;
			
		}
		for (int l724 = 0; (l724 < 2); l724 = (l724 + 1)) {
			fRec1035[l724] = 0.0f;
			
		}
		for (int l725 = 0; (l725 < 2); l725 = (l725 + 1)) {
			fRec1034[l725] = 0.0f;
			
		}
		for (int l726 = 0; (l726 < 2); l726 = (l726 + 1)) {
			fRec1032[l726] = 0.0f;
			
		}
		for (int l727 = 0; (l727 < 2); l727 = (l727 + 1)) {
			fRec1031[l727] = 0.0f;
			
		}
		for (int l728 = 0; (l728 < 2); l728 = (l728 + 1)) {
			fRec1029[l728] = 0.0f;
			
		}
		for (int l729 = 0; (l729 < 2); l729 = (l729 + 1)) {
			fRec1055[l729] = 0.0f;
			
		}
		for (int l730 = 0; (l730 < 2); l730 = (l730 + 1)) {
			fRec1053[l730] = 0.0f;
			
		}
		for (int l731 = 0; (l731 < 2); l731 = (l731 + 1)) {
			fRec1052[l731] = 0.0f;
			
		}
		for (int l732 = 0; (l732 < 2); l732 = (l732 + 1)) {
			fRec1050[l732] = 0.0f;
			
		}
		for (int l733 = 0; (l733 < 2); l733 = (l733 + 1)) {
			fRec1049[l733] = 0.0f;
			
		}
		for (int l734 = 0; (l734 < 2); l734 = (l734 + 1)) {
			fRec1047[l734] = 0.0f;
			
		}
		for (int l735 = 0; (l735 < 2); l735 = (l735 + 1)) {
			fRec1046[l735] = 0.0f;
			
		}
		for (int l736 = 0; (l736 < 2); l736 = (l736 + 1)) {
			fRec1044[l736] = 0.0f;
			
		}
		for (int l737 = 0; (l737 < 2); l737 = (l737 + 1)) {
			fRec1064[l737] = 0.0f;
			
		}
		for (int l738 = 0; (l738 < 2); l738 = (l738 + 1)) {
			fRec1062[l738] = 0.0f;
			
		}
		for (int l739 = 0; (l739 < 2); l739 = (l739 + 1)) {
			fRec1061[l739] = 0.0f;
			
		}
		for (int l740 = 0; (l740 < 2); l740 = (l740 + 1)) {
			fRec1059[l740] = 0.0f;
			
		}
		for (int l741 = 0; (l741 < 2); l741 = (l741 + 1)) {
			fRec1058[l741] = 0.0f;
			
		}
		for (int l742 = 0; (l742 < 2); l742 = (l742 + 1)) {
			fRec1056[l742] = 0.0f;
			
		}
		for (int l743 = 0; (l743 < 2); l743 = (l743 + 1)) {
			fRec1070[l743] = 0.0f;
			
		}
		for (int l744 = 0; (l744 < 2); l744 = (l744 + 1)) {
			fRec1068[l744] = 0.0f;
			
		}
		for (int l745 = 0; (l745 < 2); l745 = (l745 + 1)) {
			fRec1067[l745] = 0.0f;
			
		}
		for (int l746 = 0; (l746 < 2); l746 = (l746 + 1)) {
			fRec1065[l746] = 0.0f;
			
		}
		for (int l747 = 0; (l747 < 2); l747 = (l747 + 1)) {
			fRec1073[l747] = 0.0f;
			
		}
		for (int l748 = 0; (l748 < 2); l748 = (l748 + 1)) {
			fRec1071[l748] = 0.0f;
			
		}
		for (int l749 = 0; (l749 < 3); l749 = (l749 + 1)) {
			fVec20[l749] = 0.0f;
			
		}
		for (int l750 = 0; (l750 < 2); l750 = (l750 + 1)) {
			fRec1088[l750] = 0.0f;
			
		}
		for (int l751 = 0; (l751 < 2); l751 = (l751 + 1)) {
			fRec1086[l751] = 0.0f;
			
		}
		for (int l752 = 0; (l752 < 2); l752 = (l752 + 1)) {
			fRec1085[l752] = 0.0f;
			
		}
		for (int l753 = 0; (l753 < 2); l753 = (l753 + 1)) {
			fRec1083[l753] = 0.0f;
			
		}
		for (int l754 = 0; (l754 < 2); l754 = (l754 + 1)) {
			fRec1082[l754] = 0.0f;
			
		}
		for (int l755 = 0; (l755 < 2); l755 = (l755 + 1)) {
			fRec1080[l755] = 0.0f;
			
		}
		for (int l756 = 0; (l756 < 2); l756 = (l756 + 1)) {
			fRec1079[l756] = 0.0f;
			
		}
		for (int l757 = 0; (l757 < 2); l757 = (l757 + 1)) {
			fRec1077[l757] = 0.0f;
			
		}
		for (int l758 = 0; (l758 < 2); l758 = (l758 + 1)) {
			fRec1076[l758] = 0.0f;
			
		}
		for (int l759 = 0; (l759 < 2); l759 = (l759 + 1)) {
			fRec1074[l759] = 0.0f;
			
		}
		for (int l760 = 0; (l760 < 2); l760 = (l760 + 1)) {
			fRec1100[l760] = 0.0f;
			
		}
		for (int l761 = 0; (l761 < 2); l761 = (l761 + 1)) {
			fRec1098[l761] = 0.0f;
			
		}
		for (int l762 = 0; (l762 < 2); l762 = (l762 + 1)) {
			fRec1097[l762] = 0.0f;
			
		}
		for (int l763 = 0; (l763 < 2); l763 = (l763 + 1)) {
			fRec1095[l763] = 0.0f;
			
		}
		for (int l764 = 0; (l764 < 2); l764 = (l764 + 1)) {
			fRec1094[l764] = 0.0f;
			
		}
		for (int l765 = 0; (l765 < 2); l765 = (l765 + 1)) {
			fRec1092[l765] = 0.0f;
			
		}
		for (int l766 = 0; (l766 < 2); l766 = (l766 + 1)) {
			fRec1091[l766] = 0.0f;
			
		}
		for (int l767 = 0; (l767 < 2); l767 = (l767 + 1)) {
			fRec1089[l767] = 0.0f;
			
		}
		for (int l768 = 0; (l768 < 2); l768 = (l768 + 1)) {
			fRec1109[l768] = 0.0f;
			
		}
		for (int l769 = 0; (l769 < 2); l769 = (l769 + 1)) {
			fRec1107[l769] = 0.0f;
			
		}
		for (int l770 = 0; (l770 < 2); l770 = (l770 + 1)) {
			fRec1106[l770] = 0.0f;
			
		}
		for (int l771 = 0; (l771 < 2); l771 = (l771 + 1)) {
			fRec1104[l771] = 0.0f;
			
		}
		for (int l772 = 0; (l772 < 2); l772 = (l772 + 1)) {
			fRec1103[l772] = 0.0f;
			
		}
		for (int l773 = 0; (l773 < 2); l773 = (l773 + 1)) {
			fRec1101[l773] = 0.0f;
			
		}
		for (int l774 = 0; (l774 < 2); l774 = (l774 + 1)) {
			fRec1115[l774] = 0.0f;
			
		}
		for (int l775 = 0; (l775 < 2); l775 = (l775 + 1)) {
			fRec1113[l775] = 0.0f;
			
		}
		for (int l776 = 0; (l776 < 2); l776 = (l776 + 1)) {
			fRec1112[l776] = 0.0f;
			
		}
		for (int l777 = 0; (l777 < 2); l777 = (l777 + 1)) {
			fRec1110[l777] = 0.0f;
			
		}
		for (int l778 = 0; (l778 < 2); l778 = (l778 + 1)) {
			fRec1118[l778] = 0.0f;
			
		}
		for (int l779 = 0; (l779 < 2); l779 = (l779 + 1)) {
			fRec1116[l779] = 0.0f;
			
		}
		for (int l780 = 0; (l780 < 2); l780 = (l780 + 1)) {
			fRec1133[l780] = 0.0f;
			
		}
		for (int l781 = 0; (l781 < 2); l781 = (l781 + 1)) {
			fRec1131[l781] = 0.0f;
			
		}
		for (int l782 = 0; (l782 < 2); l782 = (l782 + 1)) {
			fRec1130[l782] = 0.0f;
			
		}
		for (int l783 = 0; (l783 < 2); l783 = (l783 + 1)) {
			fRec1128[l783] = 0.0f;
			
		}
		for (int l784 = 0; (l784 < 2); l784 = (l784 + 1)) {
			fRec1127[l784] = 0.0f;
			
		}
		for (int l785 = 0; (l785 < 2); l785 = (l785 + 1)) {
			fRec1125[l785] = 0.0f;
			
		}
		for (int l786 = 0; (l786 < 2); l786 = (l786 + 1)) {
			fRec1124[l786] = 0.0f;
			
		}
		for (int l787 = 0; (l787 < 2); l787 = (l787 + 1)) {
			fRec1122[l787] = 0.0f;
			
		}
		for (int l788 = 0; (l788 < 2); l788 = (l788 + 1)) {
			fRec1121[l788] = 0.0f;
			
		}
		for (int l789 = 0; (l789 < 2); l789 = (l789 + 1)) {
			fRec1119[l789] = 0.0f;
			
		}
		for (int l790 = 0; (l790 < 2); l790 = (l790 + 1)) {
			fRec1145[l790] = 0.0f;
			
		}
		for (int l791 = 0; (l791 < 2); l791 = (l791 + 1)) {
			fRec1143[l791] = 0.0f;
			
		}
		for (int l792 = 0; (l792 < 2); l792 = (l792 + 1)) {
			fRec1142[l792] = 0.0f;
			
		}
		for (int l793 = 0; (l793 < 2); l793 = (l793 + 1)) {
			fRec1140[l793] = 0.0f;
			
		}
		for (int l794 = 0; (l794 < 2); l794 = (l794 + 1)) {
			fRec1139[l794] = 0.0f;
			
		}
		for (int l795 = 0; (l795 < 2); l795 = (l795 + 1)) {
			fRec1137[l795] = 0.0f;
			
		}
		for (int l796 = 0; (l796 < 2); l796 = (l796 + 1)) {
			fRec1136[l796] = 0.0f;
			
		}
		for (int l797 = 0; (l797 < 2); l797 = (l797 + 1)) {
			fRec1134[l797] = 0.0f;
			
		}
		for (int l798 = 0; (l798 < 2); l798 = (l798 + 1)) {
			fRec1154[l798] = 0.0f;
			
		}
		for (int l799 = 0; (l799 < 2); l799 = (l799 + 1)) {
			fRec1152[l799] = 0.0f;
			
		}
		for (int l800 = 0; (l800 < 2); l800 = (l800 + 1)) {
			fRec1151[l800] = 0.0f;
			
		}
		for (int l801 = 0; (l801 < 2); l801 = (l801 + 1)) {
			fRec1149[l801] = 0.0f;
			
		}
		for (int l802 = 0; (l802 < 2); l802 = (l802 + 1)) {
			fRec1148[l802] = 0.0f;
			
		}
		for (int l803 = 0; (l803 < 2); l803 = (l803 + 1)) {
			fRec1146[l803] = 0.0f;
			
		}
		for (int l804 = 0; (l804 < 2); l804 = (l804 + 1)) {
			fRec1160[l804] = 0.0f;
			
		}
		for (int l805 = 0; (l805 < 2); l805 = (l805 + 1)) {
			fRec1158[l805] = 0.0f;
			
		}
		for (int l806 = 0; (l806 < 2); l806 = (l806 + 1)) {
			fRec1157[l806] = 0.0f;
			
		}
		for (int l807 = 0; (l807 < 2); l807 = (l807 + 1)) {
			fRec1155[l807] = 0.0f;
			
		}
		for (int l808 = 0; (l808 < 2); l808 = (l808 + 1)) {
			fRec1163[l808] = 0.0f;
			
		}
		for (int l809 = 0; (l809 < 2); l809 = (l809 + 1)) {
			fRec1161[l809] = 0.0f;
			
		}
		for (int l810 = 0; (l810 < 2); l810 = (l810 + 1)) {
			fRec1178[l810] = 0.0f;
			
		}
		for (int l811 = 0; (l811 < 2); l811 = (l811 + 1)) {
			fRec1176[l811] = 0.0f;
			
		}
		for (int l812 = 0; (l812 < 2); l812 = (l812 + 1)) {
			fRec1175[l812] = 0.0f;
			
		}
		for (int l813 = 0; (l813 < 2); l813 = (l813 + 1)) {
			fRec1173[l813] = 0.0f;
			
		}
		for (int l814 = 0; (l814 < 2); l814 = (l814 + 1)) {
			fRec1172[l814] = 0.0f;
			
		}
		for (int l815 = 0; (l815 < 2); l815 = (l815 + 1)) {
			fRec1170[l815] = 0.0f;
			
		}
		for (int l816 = 0; (l816 < 2); l816 = (l816 + 1)) {
			fRec1169[l816] = 0.0f;
			
		}
		for (int l817 = 0; (l817 < 2); l817 = (l817 + 1)) {
			fRec1167[l817] = 0.0f;
			
		}
		for (int l818 = 0; (l818 < 2); l818 = (l818 + 1)) {
			fRec1166[l818] = 0.0f;
			
		}
		for (int l819 = 0; (l819 < 2); l819 = (l819 + 1)) {
			fRec1164[l819] = 0.0f;
			
		}
		for (int l820 = 0; (l820 < 2); l820 = (l820 + 1)) {
			fRec1190[l820] = 0.0f;
			
		}
		for (int l821 = 0; (l821 < 2); l821 = (l821 + 1)) {
			fRec1188[l821] = 0.0f;
			
		}
		for (int l822 = 0; (l822 < 2); l822 = (l822 + 1)) {
			fRec1187[l822] = 0.0f;
			
		}
		for (int l823 = 0; (l823 < 2); l823 = (l823 + 1)) {
			fRec1185[l823] = 0.0f;
			
		}
		for (int l824 = 0; (l824 < 2); l824 = (l824 + 1)) {
			fRec1184[l824] = 0.0f;
			
		}
		for (int l825 = 0; (l825 < 2); l825 = (l825 + 1)) {
			fRec1182[l825] = 0.0f;
			
		}
		for (int l826 = 0; (l826 < 2); l826 = (l826 + 1)) {
			fRec1181[l826] = 0.0f;
			
		}
		for (int l827 = 0; (l827 < 2); l827 = (l827 + 1)) {
			fRec1179[l827] = 0.0f;
			
		}
		for (int l828 = 0; (l828 < 2); l828 = (l828 + 1)) {
			fRec1199[l828] = 0.0f;
			
		}
		for (int l829 = 0; (l829 < 2); l829 = (l829 + 1)) {
			fRec1197[l829] = 0.0f;
			
		}
		for (int l830 = 0; (l830 < 2); l830 = (l830 + 1)) {
			fRec1196[l830] = 0.0f;
			
		}
		for (int l831 = 0; (l831 < 2); l831 = (l831 + 1)) {
			fRec1194[l831] = 0.0f;
			
		}
		for (int l832 = 0; (l832 < 2); l832 = (l832 + 1)) {
			fRec1193[l832] = 0.0f;
			
		}
		for (int l833 = 0; (l833 < 2); l833 = (l833 + 1)) {
			fRec1191[l833] = 0.0f;
			
		}
		for (int l834 = 0; (l834 < 2); l834 = (l834 + 1)) {
			fRec1205[l834] = 0.0f;
			
		}
		for (int l835 = 0; (l835 < 2); l835 = (l835 + 1)) {
			fRec1203[l835] = 0.0f;
			
		}
		for (int l836 = 0; (l836 < 2); l836 = (l836 + 1)) {
			fRec1202[l836] = 0.0f;
			
		}
		for (int l837 = 0; (l837 < 2); l837 = (l837 + 1)) {
			fRec1200[l837] = 0.0f;
			
		}
		for (int l838 = 0; (l838 < 2); l838 = (l838 + 1)) {
			fRec1208[l838] = 0.0f;
			
		}
		for (int l839 = 0; (l839 < 2); l839 = (l839 + 1)) {
			fRec1206[l839] = 0.0f;
			
		}
		for (int l840 = 0; (l840 < 3); l840 = (l840 + 1)) {
			fVec21[l840] = 0.0f;
			
		}
		for (int l841 = 0; (l841 < 2); l841 = (l841 + 1)) {
			fRec1217[l841] = 0.0f;
			
		}
		for (int l842 = 0; (l842 < 2); l842 = (l842 + 1)) {
			fRec1215[l842] = 0.0f;
			
		}
		for (int l843 = 0; (l843 < 2); l843 = (l843 + 1)) {
			fRec1214[l843] = 0.0f;
			
		}
		for (int l844 = 0; (l844 < 2); l844 = (l844 + 1)) {
			fRec1212[l844] = 0.0f;
			
		}
		for (int l845 = 0; (l845 < 2); l845 = (l845 + 1)) {
			fRec1211[l845] = 0.0f;
			
		}
		for (int l846 = 0; (l846 < 2); l846 = (l846 + 1)) {
			fRec1209[l846] = 0.0f;
			
		}
		for (int l847 = 0; (l847 < 2); l847 = (l847 + 1)) {
			fRec1223[l847] = 0.0f;
			
		}
		for (int l848 = 0; (l848 < 2); l848 = (l848 + 1)) {
			fRec1221[l848] = 0.0f;
			
		}
		for (int l849 = 0; (l849 < 2); l849 = (l849 + 1)) {
			fRec1220[l849] = 0.0f;
			
		}
		for (int l850 = 0; (l850 < 2); l850 = (l850 + 1)) {
			fRec1218[l850] = 0.0f;
			
		}
		for (int l851 = 0; (l851 < 2); l851 = (l851 + 1)) {
			fRec1226[l851] = 0.0f;
			
		}
		for (int l852 = 0; (l852 < 2); l852 = (l852 + 1)) {
			fRec1224[l852] = 0.0f;
			
		}
		for (int l853 = 0; (l853 < 2); l853 = (l853 + 1)) {
			fRec1241[l853] = 0.0f;
			
		}
		for (int l854 = 0; (l854 < 2); l854 = (l854 + 1)) {
			fRec1239[l854] = 0.0f;
			
		}
		for (int l855 = 0; (l855 < 2); l855 = (l855 + 1)) {
			fRec1238[l855] = 0.0f;
			
		}
		for (int l856 = 0; (l856 < 2); l856 = (l856 + 1)) {
			fRec1236[l856] = 0.0f;
			
		}
		for (int l857 = 0; (l857 < 2); l857 = (l857 + 1)) {
			fRec1235[l857] = 0.0f;
			
		}
		for (int l858 = 0; (l858 < 2); l858 = (l858 + 1)) {
			fRec1233[l858] = 0.0f;
			
		}
		for (int l859 = 0; (l859 < 2); l859 = (l859 + 1)) {
			fRec1232[l859] = 0.0f;
			
		}
		for (int l860 = 0; (l860 < 2); l860 = (l860 + 1)) {
			fRec1230[l860] = 0.0f;
			
		}
		for (int l861 = 0; (l861 < 2); l861 = (l861 + 1)) {
			fRec1229[l861] = 0.0f;
			
		}
		for (int l862 = 0; (l862 < 2); l862 = (l862 + 1)) {
			fRec1227[l862] = 0.0f;
			
		}
		for (int l863 = 0; (l863 < 2); l863 = (l863 + 1)) {
			fRec1253[l863] = 0.0f;
			
		}
		for (int l864 = 0; (l864 < 2); l864 = (l864 + 1)) {
			fRec1251[l864] = 0.0f;
			
		}
		for (int l865 = 0; (l865 < 2); l865 = (l865 + 1)) {
			fRec1250[l865] = 0.0f;
			
		}
		for (int l866 = 0; (l866 < 2); l866 = (l866 + 1)) {
			fRec1248[l866] = 0.0f;
			
		}
		for (int l867 = 0; (l867 < 2); l867 = (l867 + 1)) {
			fRec1247[l867] = 0.0f;
			
		}
		for (int l868 = 0; (l868 < 2); l868 = (l868 + 1)) {
			fRec1245[l868] = 0.0f;
			
		}
		for (int l869 = 0; (l869 < 2); l869 = (l869 + 1)) {
			fRec1244[l869] = 0.0f;
			
		}
		for (int l870 = 0; (l870 < 2); l870 = (l870 + 1)) {
			fRec1242[l870] = 0.0f;
			
		}
		for (int l871 = 0; (l871 < 2); l871 = (l871 + 1)) {
			fRec1268[l871] = 0.0f;
			
		}
		for (int l872 = 0; (l872 < 2); l872 = (l872 + 1)) {
			fRec1266[l872] = 0.0f;
			
		}
		for (int l873 = 0; (l873 < 2); l873 = (l873 + 1)) {
			fRec1265[l873] = 0.0f;
			
		}
		for (int l874 = 0; (l874 < 2); l874 = (l874 + 1)) {
			fRec1263[l874] = 0.0f;
			
		}
		for (int l875 = 0; (l875 < 2); l875 = (l875 + 1)) {
			fRec1262[l875] = 0.0f;
			
		}
		for (int l876 = 0; (l876 < 2); l876 = (l876 + 1)) {
			fRec1260[l876] = 0.0f;
			
		}
		for (int l877 = 0; (l877 < 2); l877 = (l877 + 1)) {
			fRec1259[l877] = 0.0f;
			
		}
		for (int l878 = 0; (l878 < 2); l878 = (l878 + 1)) {
			fRec1257[l878] = 0.0f;
			
		}
		for (int l879 = 0; (l879 < 2); l879 = (l879 + 1)) {
			fRec1256[l879] = 0.0f;
			
		}
		for (int l880 = 0; (l880 < 2); l880 = (l880 + 1)) {
			fRec1254[l880] = 0.0f;
			
		}
		for (int l881 = 0; (l881 < 2); l881 = (l881 + 1)) {
			fRec1280[l881] = 0.0f;
			
		}
		for (int l882 = 0; (l882 < 2); l882 = (l882 + 1)) {
			fRec1278[l882] = 0.0f;
			
		}
		for (int l883 = 0; (l883 < 2); l883 = (l883 + 1)) {
			fRec1277[l883] = 0.0f;
			
		}
		for (int l884 = 0; (l884 < 2); l884 = (l884 + 1)) {
			fRec1275[l884] = 0.0f;
			
		}
		for (int l885 = 0; (l885 < 2); l885 = (l885 + 1)) {
			fRec1274[l885] = 0.0f;
			
		}
		for (int l886 = 0; (l886 < 2); l886 = (l886 + 1)) {
			fRec1272[l886] = 0.0f;
			
		}
		for (int l887 = 0; (l887 < 2); l887 = (l887 + 1)) {
			fRec1271[l887] = 0.0f;
			
		}
		for (int l888 = 0; (l888 < 2); l888 = (l888 + 1)) {
			fRec1269[l888] = 0.0f;
			
		}
		for (int l889 = 0; (l889 < 2); l889 = (l889 + 1)) {
			fRec1289[l889] = 0.0f;
			
		}
		for (int l890 = 0; (l890 < 2); l890 = (l890 + 1)) {
			fRec1287[l890] = 0.0f;
			
		}
		for (int l891 = 0; (l891 < 2); l891 = (l891 + 1)) {
			fRec1286[l891] = 0.0f;
			
		}
		for (int l892 = 0; (l892 < 2); l892 = (l892 + 1)) {
			fRec1284[l892] = 0.0f;
			
		}
		for (int l893 = 0; (l893 < 2); l893 = (l893 + 1)) {
			fRec1283[l893] = 0.0f;
			
		}
		for (int l894 = 0; (l894 < 2); l894 = (l894 + 1)) {
			fRec1281[l894] = 0.0f;
			
		}
		for (int l895 = 0; (l895 < 2); l895 = (l895 + 1)) {
			fRec1295[l895] = 0.0f;
			
		}
		for (int l896 = 0; (l896 < 2); l896 = (l896 + 1)) {
			fRec1293[l896] = 0.0f;
			
		}
		for (int l897 = 0; (l897 < 2); l897 = (l897 + 1)) {
			fRec1292[l897] = 0.0f;
			
		}
		for (int l898 = 0; (l898 < 2); l898 = (l898 + 1)) {
			fRec1290[l898] = 0.0f;
			
		}
		for (int l899 = 0; (l899 < 2); l899 = (l899 + 1)) {
			fRec1298[l899] = 0.0f;
			
		}
		for (int l900 = 0; (l900 < 2); l900 = (l900 + 1)) {
			fRec1296[l900] = 0.0f;
			
		}
		for (int l901 = 0; (l901 < 2); l901 = (l901 + 1)) {
			fRec1313[l901] = 0.0f;
			
		}
		for (int l902 = 0; (l902 < 2); l902 = (l902 + 1)) {
			fRec1311[l902] = 0.0f;
			
		}
		for (int l903 = 0; (l903 < 2); l903 = (l903 + 1)) {
			fRec1310[l903] = 0.0f;
			
		}
		for (int l904 = 0; (l904 < 2); l904 = (l904 + 1)) {
			fRec1308[l904] = 0.0f;
			
		}
		for (int l905 = 0; (l905 < 2); l905 = (l905 + 1)) {
			fRec1307[l905] = 0.0f;
			
		}
		for (int l906 = 0; (l906 < 2); l906 = (l906 + 1)) {
			fRec1305[l906] = 0.0f;
			
		}
		for (int l907 = 0; (l907 < 2); l907 = (l907 + 1)) {
			fRec1304[l907] = 0.0f;
			
		}
		for (int l908 = 0; (l908 < 2); l908 = (l908 + 1)) {
			fRec1302[l908] = 0.0f;
			
		}
		for (int l909 = 0; (l909 < 2); l909 = (l909 + 1)) {
			fRec1301[l909] = 0.0f;
			
		}
		for (int l910 = 0; (l910 < 2); l910 = (l910 + 1)) {
			fRec1299[l910] = 0.0f;
			
		}
		for (int l911 = 0; (l911 < 2); l911 = (l911 + 1)) {
			fRec1325[l911] = 0.0f;
			
		}
		for (int l912 = 0; (l912 < 2); l912 = (l912 + 1)) {
			fRec1323[l912] = 0.0f;
			
		}
		for (int l913 = 0; (l913 < 2); l913 = (l913 + 1)) {
			fRec1322[l913] = 0.0f;
			
		}
		for (int l914 = 0; (l914 < 2); l914 = (l914 + 1)) {
			fRec1320[l914] = 0.0f;
			
		}
		for (int l915 = 0; (l915 < 2); l915 = (l915 + 1)) {
			fRec1319[l915] = 0.0f;
			
		}
		for (int l916 = 0; (l916 < 2); l916 = (l916 + 1)) {
			fRec1317[l916] = 0.0f;
			
		}
		for (int l917 = 0; (l917 < 2); l917 = (l917 + 1)) {
			fRec1316[l917] = 0.0f;
			
		}
		for (int l918 = 0; (l918 < 2); l918 = (l918 + 1)) {
			fRec1314[l918] = 0.0f;
			
		}
		for (int l919 = 0; (l919 < 2); l919 = (l919 + 1)) {
			fRec1334[l919] = 0.0f;
			
		}
		for (int l920 = 0; (l920 < 2); l920 = (l920 + 1)) {
			fRec1332[l920] = 0.0f;
			
		}
		for (int l921 = 0; (l921 < 2); l921 = (l921 + 1)) {
			fRec1331[l921] = 0.0f;
			
		}
		for (int l922 = 0; (l922 < 2); l922 = (l922 + 1)) {
			fRec1329[l922] = 0.0f;
			
		}
		for (int l923 = 0; (l923 < 2); l923 = (l923 + 1)) {
			fRec1328[l923] = 0.0f;
			
		}
		for (int l924 = 0; (l924 < 2); l924 = (l924 + 1)) {
			fRec1326[l924] = 0.0f;
			
		}
		for (int l925 = 0; (l925 < 2); l925 = (l925 + 1)) {
			fRec1340[l925] = 0.0f;
			
		}
		for (int l926 = 0; (l926 < 2); l926 = (l926 + 1)) {
			fRec1338[l926] = 0.0f;
			
		}
		for (int l927 = 0; (l927 < 2); l927 = (l927 + 1)) {
			fRec1337[l927] = 0.0f;
			
		}
		for (int l928 = 0; (l928 < 2); l928 = (l928 + 1)) {
			fRec1335[l928] = 0.0f;
			
		}
		for (int l929 = 0; (l929 < 2); l929 = (l929 + 1)) {
			fRec1343[l929] = 0.0f;
			
		}
		for (int l930 = 0; (l930 < 2); l930 = (l930 + 1)) {
			fRec1341[l930] = 0.0f;
			
		}
		for (int l931 = 0; (l931 < 3); l931 = (l931 + 1)) {
			fVec22[l931] = 0.0f;
			
		}
		for (int l932 = 0; (l932 < 2); l932 = (l932 + 1)) {
			fRec1358[l932] = 0.0f;
			
		}
		for (int l933 = 0; (l933 < 2); l933 = (l933 + 1)) {
			fRec1356[l933] = 0.0f;
			
		}
		for (int l934 = 0; (l934 < 2); l934 = (l934 + 1)) {
			fRec1355[l934] = 0.0f;
			
		}
		for (int l935 = 0; (l935 < 2); l935 = (l935 + 1)) {
			fRec1353[l935] = 0.0f;
			
		}
		for (int l936 = 0; (l936 < 2); l936 = (l936 + 1)) {
			fRec1352[l936] = 0.0f;
			
		}
		for (int l937 = 0; (l937 < 2); l937 = (l937 + 1)) {
			fRec1350[l937] = 0.0f;
			
		}
		for (int l938 = 0; (l938 < 2); l938 = (l938 + 1)) {
			fRec1349[l938] = 0.0f;
			
		}
		for (int l939 = 0; (l939 < 2); l939 = (l939 + 1)) {
			fRec1347[l939] = 0.0f;
			
		}
		for (int l940 = 0; (l940 < 2); l940 = (l940 + 1)) {
			fRec1346[l940] = 0.0f;
			
		}
		for (int l941 = 0; (l941 < 2); l941 = (l941 + 1)) {
			fRec1344[l941] = 0.0f;
			
		}
		for (int l942 = 0; (l942 < 2); l942 = (l942 + 1)) {
			fRec1370[l942] = 0.0f;
			
		}
		for (int l943 = 0; (l943 < 2); l943 = (l943 + 1)) {
			fRec1368[l943] = 0.0f;
			
		}
		for (int l944 = 0; (l944 < 2); l944 = (l944 + 1)) {
			fRec1367[l944] = 0.0f;
			
		}
		for (int l945 = 0; (l945 < 2); l945 = (l945 + 1)) {
			fRec1365[l945] = 0.0f;
			
		}
		for (int l946 = 0; (l946 < 2); l946 = (l946 + 1)) {
			fRec1364[l946] = 0.0f;
			
		}
		for (int l947 = 0; (l947 < 2); l947 = (l947 + 1)) {
			fRec1362[l947] = 0.0f;
			
		}
		for (int l948 = 0; (l948 < 2); l948 = (l948 + 1)) {
			fRec1361[l948] = 0.0f;
			
		}
		for (int l949 = 0; (l949 < 2); l949 = (l949 + 1)) {
			fRec1359[l949] = 0.0f;
			
		}
		for (int l950 = 0; (l950 < 2); l950 = (l950 + 1)) {
			fRec1379[l950] = 0.0f;
			
		}
		for (int l951 = 0; (l951 < 2); l951 = (l951 + 1)) {
			fRec1377[l951] = 0.0f;
			
		}
		for (int l952 = 0; (l952 < 2); l952 = (l952 + 1)) {
			fRec1376[l952] = 0.0f;
			
		}
		for (int l953 = 0; (l953 < 2); l953 = (l953 + 1)) {
			fRec1374[l953] = 0.0f;
			
		}
		for (int l954 = 0; (l954 < 2); l954 = (l954 + 1)) {
			fRec1373[l954] = 0.0f;
			
		}
		for (int l955 = 0; (l955 < 2); l955 = (l955 + 1)) {
			fRec1371[l955] = 0.0f;
			
		}
		for (int l956 = 0; (l956 < 2); l956 = (l956 + 1)) {
			fRec1385[l956] = 0.0f;
			
		}
		for (int l957 = 0; (l957 < 2); l957 = (l957 + 1)) {
			fRec1383[l957] = 0.0f;
			
		}
		for (int l958 = 0; (l958 < 2); l958 = (l958 + 1)) {
			fRec1382[l958] = 0.0f;
			
		}
		for (int l959 = 0; (l959 < 2); l959 = (l959 + 1)) {
			fRec1380[l959] = 0.0f;
			
		}
		for (int l960 = 0; (l960 < 2); l960 = (l960 + 1)) {
			fRec1388[l960] = 0.0f;
			
		}
		for (int l961 = 0; (l961 < 2); l961 = (l961 + 1)) {
			fRec1386[l961] = 0.0f;
			
		}
		for (int l962 = 0; (l962 < 2); l962 = (l962 + 1)) {
			fRec1391[l962] = 0.0f;
			
		}
		for (int l963 = 0; (l963 < 2); l963 = (l963 + 1)) {
			fRec1389[l963] = 0.0f;
			
		}
		for (int l964 = 0; (l964 < 2); l964 = (l964 + 1)) {
			fRec1406[l964] = 0.0f;
			
		}
		for (int l965 = 0; (l965 < 2); l965 = (l965 + 1)) {
			fRec1404[l965] = 0.0f;
			
		}
		for (int l966 = 0; (l966 < 2); l966 = (l966 + 1)) {
			fRec1403[l966] = 0.0f;
			
		}
		for (int l967 = 0; (l967 < 2); l967 = (l967 + 1)) {
			fRec1401[l967] = 0.0f;
			
		}
		for (int l968 = 0; (l968 < 2); l968 = (l968 + 1)) {
			fRec1400[l968] = 0.0f;
			
		}
		for (int l969 = 0; (l969 < 2); l969 = (l969 + 1)) {
			fRec1398[l969] = 0.0f;
			
		}
		for (int l970 = 0; (l970 < 2); l970 = (l970 + 1)) {
			fRec1397[l970] = 0.0f;
			
		}
		for (int l971 = 0; (l971 < 2); l971 = (l971 + 1)) {
			fRec1395[l971] = 0.0f;
			
		}
		for (int l972 = 0; (l972 < 2); l972 = (l972 + 1)) {
			fRec1394[l972] = 0.0f;
			
		}
		for (int l973 = 0; (l973 < 2); l973 = (l973 + 1)) {
			fRec1392[l973] = 0.0f;
			
		}
		for (int l974 = 0; (l974 < 2); l974 = (l974 + 1)) {
			fRec1418[l974] = 0.0f;
			
		}
		for (int l975 = 0; (l975 < 2); l975 = (l975 + 1)) {
			fRec1416[l975] = 0.0f;
			
		}
		for (int l976 = 0; (l976 < 2); l976 = (l976 + 1)) {
			fRec1415[l976] = 0.0f;
			
		}
		for (int l977 = 0; (l977 < 2); l977 = (l977 + 1)) {
			fRec1413[l977] = 0.0f;
			
		}
		for (int l978 = 0; (l978 < 2); l978 = (l978 + 1)) {
			fRec1412[l978] = 0.0f;
			
		}
		for (int l979 = 0; (l979 < 2); l979 = (l979 + 1)) {
			fRec1410[l979] = 0.0f;
			
		}
		for (int l980 = 0; (l980 < 2); l980 = (l980 + 1)) {
			fRec1409[l980] = 0.0f;
			
		}
		for (int l981 = 0; (l981 < 2); l981 = (l981 + 1)) {
			fRec1407[l981] = 0.0f;
			
		}
		for (int l982 = 0; (l982 < 2); l982 = (l982 + 1)) {
			fRec1424[l982] = 0.0f;
			
		}
		for (int l983 = 0; (l983 < 2); l983 = (l983 + 1)) {
			fRec1422[l983] = 0.0f;
			
		}
		for (int l984 = 0; (l984 < 2); l984 = (l984 + 1)) {
			fRec1421[l984] = 0.0f;
			
		}
		for (int l985 = 0; (l985 < 2); l985 = (l985 + 1)) {
			fRec1419[l985] = 0.0f;
			
		}
		for (int l986 = 0; (l986 < 2); l986 = (l986 + 1)) {
			fRec1433[l986] = 0.0f;
			
		}
		for (int l987 = 0; (l987 < 2); l987 = (l987 + 1)) {
			fRec1431[l987] = 0.0f;
			
		}
		for (int l988 = 0; (l988 < 2); l988 = (l988 + 1)) {
			fRec1430[l988] = 0.0f;
			
		}
		for (int l989 = 0; (l989 < 2); l989 = (l989 + 1)) {
			fRec1428[l989] = 0.0f;
			
		}
		for (int l990 = 0; (l990 < 2); l990 = (l990 + 1)) {
			fRec1427[l990] = 0.0f;
			
		}
		for (int l991 = 0; (l991 < 2); l991 = (l991 + 1)) {
			fRec1425[l991] = 0.0f;
			
		}
		
	}
	
	virtual void init(int samplingFreq) {
		classInit(samplingFreq);
		instanceInit(samplingFreq);
	}
	virtual void instanceInit(int samplingFreq) {
		instanceConstants(samplingFreq);
		instanceResetUserInterface();
		instanceClear();
	}
	
	virtual mydsp* clone() {
		return new mydsp();
	}
	virtual int getSampleRate() {
		return fSamplingFreq;
		
	}
	
	virtual void buildUserInterface(UI* ui_interface) {
		ui_interface->openVerticalBox("SATOsw0o5");
		ui_interface->declare(&fHslider0, "unit", "dB");
		ui_interface->addHorizontalSlider("gain", &fHslider0, -10.0f, -30.0f, 10.0f, 1.0f);
		ui_interface->declare(&fHslider2, "unit", "dB");
		ui_interface->addHorizontalSlider("lf/hf", &fHslider2, 0.0f, -3.0f, 3.0f, 0.100000001f);
		ui_interface->addCheckButton("mute", &fCheckbox0);
		ui_interface->declare(&fHslider1, "unit", "Hz");
		ui_interface->addHorizontalSlider("xover", &fHslider1, 400.0f, 200.0f, 800.0f, 20.0f);
		ui_interface->closeBox();
		
	}
	
	virtual void compute(int count, FAUSTFLOAT** inputs, FAUSTFLOAT** outputs) {
		FAUSTFLOAT* input0 = inputs[0];
		FAUSTFLOAT* input1 = inputs[1];
		FAUSTFLOAT* input2 = inputs[2];
		FAUSTFLOAT* input3 = inputs[3];
		FAUSTFLOAT* input4 = inputs[4];
		FAUSTFLOAT* input5 = inputs[5];
		FAUSTFLOAT* input6 = inputs[6];
		FAUSTFLOAT* input7 = inputs[7];
		FAUSTFLOAT* input8 = inputs[8];
		FAUSTFLOAT* input9 = inputs[9];
		FAUSTFLOAT* input10 = inputs[10];
		FAUSTFLOAT* input11 = inputs[11];
		FAUSTFLOAT* input12 = inputs[12];
		FAUSTFLOAT* input13 = inputs[13];
		FAUSTFLOAT* input14 = inputs[14];
		FAUSTFLOAT* input15 = inputs[15];
		FAUSTFLOAT* input16 = inputs[16];
		FAUSTFLOAT* input17 = inputs[17];
		FAUSTFLOAT* input18 = inputs[18];
		FAUSTFLOAT* input19 = inputs[19];
		FAUSTFLOAT* input20 = inputs[20];
		FAUSTFLOAT* input21 = inputs[21];
		FAUSTFLOAT* input22 = inputs[22];
		FAUSTFLOAT* input23 = inputs[23];
		FAUSTFLOAT* input24 = inputs[24];
		FAUSTFLOAT* input25 = inputs[25];
		FAUSTFLOAT* input26 = inputs[26];
		FAUSTFLOAT* input27 = inputs[27];
		FAUSTFLOAT* input28 = inputs[28];
		FAUSTFLOAT* input29 = inputs[29];
		FAUSTFLOAT* input30 = inputs[30];
		FAUSTFLOAT* input31 = inputs[31];
		FAUSTFLOAT* input32 = inputs[32];
		FAUSTFLOAT* input33 = inputs[33];
		FAUSTFLOAT* input34 = inputs[34];
		FAUSTFLOAT* input35 = inputs[35];
		FAUSTFLOAT* output0 = outputs[0];
		FAUSTFLOAT* output1 = outputs[1];
		FAUSTFLOAT* output2 = outputs[2];
		FAUSTFLOAT* output3 = outputs[3];
		FAUSTFLOAT* output4 = outputs[4];
		FAUSTFLOAT* output5 = outputs[5];
		FAUSTFLOAT* output6 = outputs[6];
		FAUSTFLOAT* output7 = outputs[7];
		FAUSTFLOAT* output8 = outputs[8];
		FAUSTFLOAT* output9 = outputs[9];
		FAUSTFLOAT* output10 = outputs[10];
		FAUSTFLOAT* output11 = outputs[11];
		FAUSTFLOAT* output12 = outputs[12];
		FAUSTFLOAT* output13 = outputs[13];
		FAUSTFLOAT* output14 = outputs[14];
		FAUSTFLOAT* output15 = outputs[15];
		FAUSTFLOAT* output16 = outputs[16];
		FAUSTFLOAT* output17 = outputs[17];
		FAUSTFLOAT* output18 = outputs[18];
		FAUSTFLOAT* output19 = outputs[19];
		FAUSTFLOAT* output20 = outputs[20];
		FAUSTFLOAT* output21 = outputs[21];
		FAUSTFLOAT* output22 = outputs[22];
		FAUSTFLOAT* output23 = outputs[23];
		FAUSTFLOAT* output24 = outputs[24];
		FAUSTFLOAT* output25 = outputs[25];
		FAUSTFLOAT* output26 = outputs[26];
		FAUSTFLOAT* output27 = outputs[27];
		FAUSTFLOAT* output28 = outputs[28];
		FAUSTFLOAT* output29 = outputs[29];
		FAUSTFLOAT* output30 = outputs[30];
		float fSlow0 = (0.00100000005f * (float((float(fCheckbox0) < 0.5f)) * powf(10.0f, (0.0500000007f * float(fHslider0)))));
		float fSlow1 = (0.00100000005f * float(fHslider1));
		float fSlow2 = (0.00100000005f * powf(10.0f, (0.0500000007f * float(fHslider2))));
		for (int i = 0; (i < count); i = (i + 1)) {
			fRec0[0] = (fSlow0 + (0.999000013f * fRec0[1]));
			fRec1[0] = (fSlow1 + (0.999000013f * fRec1[1]));
			float fTemp0 = tanf((fConst1 * fRec1[0]));
			float fTemp1 = mydsp_faustpower2_f(fTemp0);
			float fTemp2 = ((fTemp0 * (fTemp0 + -2.0f)) + 1.0f);
			float fTemp3 = (fTemp1 + -1.0f);
			float fTemp4 = ((fTemp0 * (fTemp0 + 2.0f)) + 1.0f);
			fRec2[0] = (float(input0[i]) - (((fRec2[2] * fTemp2) + (2.0f * (fRec2[1] * fTemp3))) / fTemp4));
			fRec3[0] = (fSlow2 + (0.999000013f * fRec3[1]));
			float fTemp5 = (fRec3[0] * fTemp4);
			float fTemp6 = (0.0f - (2.0f / fTemp4));
			float fTemp7 = (((fTemp1 * (fRec2[2] + (fRec2[0] + (2.0f * fRec2[1])))) / fTemp5) + (fRec3[0] * (0.0f - ((fRec2[1] * fTemp6) + ((fRec2[0] + fRec2[2]) / fTemp4)))));
			float fTemp8 = (fConst4 * fRec4[1]);
			float fTemp9 = (fConst7 * fRec7[1]);
			float fTemp10 = (fConst8 * fRec10[1]);
			fRec19[0] = (float(input25[i]) - (((fTemp2 * fRec19[2]) + (2.0f * (fTemp3 * fRec19[1]))) / fTemp4));
			float fTemp11 = (((fTemp1 * (fRec19[2] + (fRec19[0] + (2.0f * fRec19[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec19[1]) + ((fRec19[0] + fRec19[2]) / fTemp4))))));
			fRec20[0] = (float(input26[i]) - (((fTemp2 * fRec20[2]) + (2.0f * (fTemp3 * fRec20[1]))) / fTemp4));
			float fTemp12 = (((fTemp1 * (fRec20[2] + (fRec20[0] + (2.0f * fRec20[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec20[1]) + ((fRec20[0] + fRec20[2]) / fTemp4))))));
			fRec21[0] = (float(input28[i]) - (((fTemp2 * fRec21[2]) + (2.0f * (fTemp3 * fRec21[1]))) / fTemp4));
			float fTemp13 = (((fTemp1 * (fRec21[2] + (fRec21[0] + (2.0f * fRec21[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec21[1]) + ((fRec21[0] + fRec21[2]) / fTemp4))))));
			fRec22[0] = (float(input29[i]) - (((fTemp2 * fRec22[2]) + (2.0f * (fTemp3 * fRec22[1]))) / fTemp4));
			float fTemp14 = (((fTemp1 * (fRec22[2] + (fRec22[0] + (2.0f * fRec22[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec22[1]) + ((fRec22[0] + fRec22[2]) / fTemp4))))));
			fRec23[0] = (float(input30[i]) - (((fTemp2 * fRec23[2]) + (2.0f * (fTemp3 * fRec23[1]))) / fTemp4));
			float fTemp15 = (((fTemp1 * (fRec23[2] + (fRec23[0] + (2.0f * fRec23[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec23[1]) + ((fRec23[0] + fRec23[2]) / fTemp4))))));
			fRec24[0] = (float(input32[i]) - (((fTemp2 * fRec24[2]) + (2.0f * (fTemp3 * fRec24[1]))) / fTemp4));
			float fTemp16 = (((fTemp1 * (fRec24[2] + (fRec24[0] + (2.0f * fRec24[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec24[1]) + ((fRec24[0] + fRec24[2]) / fTemp4))))));
			fRec25[0] = (float(input27[i]) - (((fTemp2 * fRec25[2]) + (2.0f * (fTemp3 * fRec25[1]))) / fTemp4));
			float fTemp17 = (((fTemp1 * (fRec25[2] + (fRec25[0] + (2.0f * fRec25[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec25[1]) + ((fRec25[0] + fRec25[2]) / fTemp4))))));
			fRec26[0] = (float(input31[i]) - (((fTemp2 * fRec26[2]) + (2.0f * (fTemp3 * fRec26[1]))) / fTemp4));
			float fTemp18 = (((fTemp1 * (fRec26[2] + (fRec26[0] + (2.0f * fRec26[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec26[1]) + ((fRec26[0] + fRec26[2]) / fTemp4))))));
			fRec27[0] = (float(input33[i]) - (((fTemp2 * fRec27[2]) + (2.0f * (fTemp3 * fRec27[1]))) / fTemp4));
			float fTemp19 = (((fTemp1 * (fRec27[2] + (fRec27[0] + (2.0f * fRec27[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec27[1]) + ((fRec27[0] + fRec27[2]) / fTemp4))))));
			fRec28[0] = (float(input34[i]) - (((fTemp2 * fRec28[2]) + (2.0f * (fTemp3 * fRec28[1]))) / fTemp4));
			float fTemp20 = (((fTemp1 * (fRec28[2] + (fRec28[0] + (2.0f * fRec28[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec28[1]) + ((fRec28[0] + fRec28[2]) / fTemp4))))));
			fRec29[0] = (float(input35[i]) - (((fTemp2 * fRec29[2]) + (2.0f * (fTemp3 * fRec29[1]))) / fTemp4));
			float fTemp21 = (((fTemp1 * (fRec29[2] + (fRec29[0] + (2.0f * fRec29[1])))) / fTemp5) + (0.205712318f * (fRec3[0] * (0.0f - ((fTemp6 * fRec29[1]) + ((fRec29[0] + fRec29[2]) / fTemp4))))));
			float fTemp22 = (fConst10 * (((((((5.56650002e-06f * fTemp11) + (3.50100004e-06f * fTemp12)) + (3.17544e-05f * fTemp13)) + (2.38069993e-06f * fTemp14)) + (0.0133424634f * fTemp15)) + (5.73399973e-07f * fTemp16)) - (((((9.66859989e-06f * fTemp17) + (4.51110009e-06f * fTemp18)) + (1.19765e-05f * fTemp19)) + (9.55959968e-06f * fTemp20)) + (3.03900009e-07f * fTemp21))));
			float fTemp23 = (fConst11 * fRec13[1]);
			float fTemp24 = (fConst12 * fRec16[1]);
			fRec18[0] = (fTemp22 + (fTemp23 + (fRec18[1] + fTemp24)));
			fRec16[0] = fRec18[0];
			float fRec17 = ((fTemp24 + fTemp23) + fTemp22);
			fRec15[0] = (fRec16[0] + fRec15[1]);
			fRec13[0] = fRec15[0];
			float fRec14 = fRec17;
			fRec12[0] = (fTemp9 + (fTemp10 + (fRec14 + fRec12[1])));
			fRec10[0] = fRec12[0];
			float fRec11 = (fTemp9 + (fRec14 + fTemp10));
			fRec9[0] = (fRec10[0] + fRec9[1]);
			fRec7[0] = fRec9[0];
			float fRec8 = fRec11;
			fRec6[0] = (fTemp8 + (fRec8 + fRec6[1]));
			fRec4[0] = fRec6[0];
			float fRec5 = (fRec8 + fTemp8);
			float fTemp25 = (fConst14 * fRec30[1]);
			float fTemp26 = (fConst15 * fRec33[1]);
			fRec42[0] = (float(input16[i]) - (((fTemp2 * fRec42[2]) + (2.0f * (fTemp3 * fRec42[1]))) / fTemp4));
			float fTemp27 = (((fTemp1 * (fRec42[2] + (fRec42[0] + (2.0f * fRec42[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec42[1]) + ((fRec42[0] + fRec42[2]) / fTemp4))))));
			fRec43[0] = (float(input18[i]) - (((fTemp2 * fRec43[2]) + (2.0f * (fTemp3 * fRec43[1]))) / fTemp4));
			float fTemp28 = (((fTemp1 * (fRec43[2] + (fRec43[0] + (2.0f * fRec43[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec43[1]) + ((fRec43[0] + fRec43[2]) / fTemp4))))));
			fRec44[0] = (float(input20[i]) - (((fTemp2 * fRec44[2]) + (2.0f * (fTemp3 * fRec44[1]))) / fTemp4));
			float fTemp29 = (((fTemp1 * (fRec44[2] + (fRec44[0] + (2.0f * fRec44[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec44[1]) + ((fRec44[0] + fRec44[2]) / fTemp4))))));
			fRec45[0] = (float(input22[i]) - (((fTemp2 * fRec45[2]) + (2.0f * (fTemp3 * fRec45[1]))) / fTemp4));
			float fTemp30 = (((fTemp1 * (fRec45[2] + (fRec45[0] + (2.0f * fRec45[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec45[1]) + ((fRec45[0] + fRec45[2]) / fTemp4))))));
			fRec46[0] = (float(input17[i]) - (((fTemp2 * fRec46[2]) + (2.0f * (fTemp3 * fRec46[1]))) / fTemp4));
			float fTemp31 = (((fTemp1 * (fRec46[2] + (fRec46[0] + (2.0f * fRec46[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec46[1]) + ((fRec46[0] + fRec46[2]) / fTemp4))))));
			fRec47[0] = (float(input19[i]) - (((fTemp2 * fRec47[2]) + (2.0f * (fTemp3 * fRec47[1]))) / fTemp4));
			float fTemp32 = (((fTemp1 * (fRec47[2] + (fRec47[0] + (2.0f * fRec47[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec47[1]) + ((fRec47[0] + fRec47[2]) / fTemp4))))));
			fRec48[0] = (float(input21[i]) - (((fTemp2 * fRec48[2]) + (2.0f * (fTemp3 * fRec48[1]))) / fTemp4));
			float fTemp33 = (((fTemp1 * (fRec48[2] + (fRec48[0] + (2.0f * fRec48[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec48[1]) + ((fRec48[0] + fRec48[2]) / fTemp4))))));
			fRec49[0] = (float(input23[i]) - (((fTemp2 * fRec49[2]) + (2.0f * (fTemp3 * fRec49[1]))) / fTemp4));
			float fTemp34 = (((fTemp1 * (fRec49[2] + (fRec49[0] + (2.0f * fRec49[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec49[1]) + ((fRec49[0] + fRec49[2]) / fTemp4))))));
			fRec50[0] = (float(input24[i]) - (((fTemp2 * fRec50[2]) + (2.0f * (fTemp3 * fRec50[1]))) / fTemp4));
			float fTemp35 = (((fTemp1 * (fRec50[2] + (fRec50[0] + (2.0f * fRec50[1])))) / fTemp5) + (0.422004998f * (fRec3[0] * (0.0f - ((fTemp6 * fRec50[1]) + ((fRec50[0] + fRec50[2]) / fTemp4))))));
			float fTemp36 = (fConst17 * (((((1.73160004e-06f * fTemp27) + (4.22958001e-05f * fTemp28)) + (0.0567824654f * fTemp29)) + (1.84860005e-06f * fTemp30)) - (((((8.83719986e-06f * fTemp31) + (1.39949998e-06f * fTemp32)) + (4.73799986e-07f * fTemp33)) + (1.07545002e-05f * fTemp34)) + (4.48160017e-06f * fTemp35))));
			float fTemp37 = (fConst18 * fRec36[1]);
			float fTemp38 = (fConst19 * fRec39[1]);
			fRec41[0] = (fTemp36 + (fTemp37 + (fRec41[1] + fTemp38)));
			fRec39[0] = fRec41[0];
			float fRec40 = ((fTemp38 + fTemp37) + fTemp36);
			fRec38[0] = (fRec39[0] + fRec38[1]);
			fRec36[0] = fRec38[0];
			float fRec37 = fRec40;
			fRec35[0] = (fTemp25 + (fTemp26 + (fRec37 + fRec35[1])));
			fRec33[0] = fRec35[0];
			float fRec34 = (fTemp25 + (fRec37 + fTemp26));
			fRec32[0] = (fRec33[0] + fRec32[1]);
			fRec30[0] = fRec32[0];
			float fRec31 = fRec34;
			float fTemp39 = (fConst21 * fRec51[1]);
			fRec60[0] = (float(input10[i]) - (((fTemp2 * fRec60[2]) + (2.0f * (fTemp3 * fRec60[1]))) / fTemp4));
			float fTemp40 = (((fTemp1 * (fRec60[2] + (fRec60[0] + (2.0f * fRec60[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec60[1]) + ((fRec60[0] + fRec60[2]) / fTemp4))))));
			fRec61[0] = (float(input12[i]) - (((fTemp2 * fRec61[2]) + (2.0f * (fTemp3 * fRec61[1]))) / fTemp4));
			float fTemp41 = (((fTemp1 * (fRec61[2] + (fRec61[0] + (2.0f * fRec61[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec61[1]) + ((fRec61[0] + fRec61[2]) / fTemp4))))));
			fRec62[0] = (float(input13[i]) - (((fTemp2 * fRec62[2]) + (2.0f * (fTemp3 * fRec62[1]))) / fTemp4));
			float fTemp42 = (((fTemp1 * (fRec62[2] + (fRec62[0] + (2.0f * fRec62[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec62[1]) + ((fRec62[0] + fRec62[2]) / fTemp4))))));
			fRec63[0] = (float(input14[i]) - (((fTemp2 * fRec63[2]) + (2.0f * (fTemp3 * fRec63[1]))) / fTemp4));
			float fTemp43 = (((fTemp1 * (fRec63[2] + (fRec63[0] + (2.0f * fRec63[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec63[1]) + ((fRec63[0] + fRec63[2]) / fTemp4))))));
			fRec64[0] = (float(input9[i]) - (((fTemp2 * fRec64[2]) + (2.0f * (fTemp3 * fRec64[1]))) / fTemp4));
			float fTemp44 = (((fTemp1 * (fRec64[2] + (fRec64[0] + (2.0f * fRec64[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec64[1]) + ((fRec64[0] + fRec64[2]) / fTemp4))))));
			fRec65[0] = (float(input11[i]) - (((fTemp2 * fRec65[2]) + (2.0f * (fTemp3 * fRec65[1]))) / fTemp4));
			float fTemp45 = (((fTemp1 * (fRec65[2] + (fRec65[0] + (2.0f * fRec65[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec65[1]) + ((fRec65[0] + fRec65[2]) / fTemp4))))));
			fRec66[0] = (float(input15[i]) - (((fTemp2 * fRec66[2]) + (2.0f * (fTemp3 * fRec66[1]))) / fTemp4));
			float fTemp46 = (((fTemp1 * (fRec66[2] + (fRec66[0] + (2.0f * fRec66[1])))) / fTemp5) + (0.628249943f * (fRec3[0] * (0.0f - ((fTemp6 * fRec66[1]) + ((fRec66[0] + fRec66[2]) / fTemp4))))));
			float fTemp47 = (fConst23 * (((((3.67071007e-05f * fTemp40) + (0.105663024f * fTemp41)) + (4.44899979e-06f * fTemp42)) + (1.9603001e-06f * fTemp43)) - (((4.7333001e-06f * fTemp44) + (5.63999993e-06f * fTemp45)) + (5.70290013e-06f * fTemp46))));
			float fTemp48 = (fConst24 * fRec54[1]);
			float fTemp49 = (fConst25 * fRec57[1]);
			fRec59[0] = (fTemp47 + (fTemp48 + (fRec59[1] + fTemp49)));
			fRec57[0] = fRec59[0];
			float fRec58 = ((fTemp49 + fTemp48) + fTemp47);
			fRec56[0] = (fRec57[0] + fRec56[1]);
			fRec54[0] = fRec56[0];
			float fRec55 = fRec58;
			fRec53[0] = (fTemp39 + (fRec55 + fRec53[1]));
			fRec51[0] = fRec53[0];
			float fRec52 = (fRec55 + fTemp39);
			fRec70[0] = (float(input2[i]) - (((fTemp2 * fRec70[2]) + (2.0f * (fTemp3 * fRec70[1]))) / fTemp4));
			float fTemp50 = (((fTemp1 * (fRec70[2] + (fRec70[0] + (2.0f * fRec70[1])))) / fTemp5) + (0.932469487f * (fRec3[0] * (0.0f - ((fTemp6 * fRec70[1]) + ((fRec70[0] + fRec70[2]) / fTemp4))))));
			fRec71[0] = (float(input3[i]) - (((fTemp2 * fRec71[2]) + (2.0f * (fTemp3 * fRec71[1]))) / fTemp4));
			float fTemp51 = (((fTemp1 * (fRec71[2] + (fRec71[0] + (2.0f * fRec71[1])))) / fTemp5) + (0.932469487f * (fRec3[0] * (0.0f - ((fTemp6 * fRec71[1]) + ((fRec71[0] + fRec71[2]) / fTemp4))))));
			fRec72[0] = (float(input1[i]) - (((fTemp2 * fRec72[2]) + (2.0f * (fTemp3 * fRec72[1]))) / fTemp4));
			float fTemp52 = (((fTemp1 * (fRec72[2] + (fRec72[0] + (2.0f * fRec72[1])))) / fTemp5) + (0.932469487f * (fRec3[0] * (0.0f - ((fTemp6 * fRec72[1]) + ((fRec72[0] + fRec72[2]) / fTemp4))))));
			float fTemp53 = (fConst27 * (((0.144552425f * fTemp50) + (4.70970008e-06f * fTemp51)) - (4.80300014e-06f * fTemp52)));
			float fTemp54 = (fConst28 * fRec67[1]);
			fRec69[0] = (fTemp53 + (fRec69[1] + fTemp54));
			fRec67[0] = fRec69[0];
			float fRec68 = (fTemp54 + fTemp53);
			fRec79[0] = (float(input4[i]) - (((fTemp2 * fRec79[2]) + (2.0f * (fTemp3 * fRec79[1]))) / fTemp4));
			float fTemp55 = (((fTemp1 * (fRec79[2] + (fRec79[0] + (2.0f * fRec79[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec79[1]) + ((fRec79[0] + fRec79[2]) / fTemp4))))));
			fRec80[0] = (float(input6[i]) - (((fTemp2 * fRec80[2]) + (2.0f * (fTemp3 * fRec80[1]))) / fTemp4));
			float fTemp56 = (((fTemp1 * (fRec80[2] + (fRec80[0] + (2.0f * fRec80[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec80[1]) + ((fRec80[0] + fRec80[2]) / fTemp4))))));
			fRec81[0] = (float(input7[i]) - (((fTemp2 * fRec81[2]) + (2.0f * (fTemp3 * fRec81[1]))) / fTemp4));
			float fTemp57 = (((fTemp1 * (fRec81[2] + (fRec81[0] + (2.0f * fRec81[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec81[1]) + ((fRec81[0] + fRec81[2]) / fTemp4))))));
			fRec82[0] = (float(input8[i]) - (((fTemp2 * fRec82[2]) + (2.0f * (fTemp3 * fRec82[1]))) / fTemp4));
			float fTemp58 = (((fTemp1 * (fRec82[2] + (fRec82[0] + (2.0f * fRec82[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec82[1]) + ((fRec82[0] + fRec82[2]) / fTemp4))))));
			fRec83[0] = (float(input5[i]) - (((fTemp2 * fRec83[2]) + (2.0f * (fTemp3 * fRec83[1]))) / fTemp4));
			float fTemp59 = (((fTemp1 * (fRec83[2] + (fRec83[0] + (2.0f * fRec83[1])))) / fTemp5) + (0.804249108f * (fRec3[0] * (0.0f - ((fTemp6 * fRec83[1]) + ((fRec83[0] + fRec83[2]) / fTemp4))))));
			float fTemp60 = (fConst30 * (((((1.98604994e-05f * fTemp55) + (0.141460821f * fTemp56)) + (6.64250001e-06f * fTemp57)) + (1.14950001e-06f * fTemp58)) - (7.13349982e-06f * fTemp59)));
			float fTemp61 = (fConst31 * fRec73[1]);
			float fTemp62 = (fConst32 * fRec76[1]);
			fRec78[0] = (fTemp60 + (fTemp61 + (fRec78[1] + fTemp62)));
			fRec76[0] = fRec78[0];
			float fRec77 = ((fTemp62 + fTemp61) + fTemp60);
			fRec75[0] = (fRec76[0] + fRec75[1]);
			fRec73[0] = fRec75[0];
			float fRec74 = fRec77;
			fVec0[(IOTA & 2047)] = ((0.0951942503f * fTemp7) + (fRec5 + (fRec31 + (fRec52 + (fRec68 + fRec74)))));
			output0[i] = FAUSTFLOAT((0.622610331f * (fRec0[0] * fVec0[((IOTA - iConst33) & 2047)])));
			float fTemp63 = (fConst35 * fRec84[1]);
			float fTemp64 = (fConst37 * fRec87[1]);
			float fTemp65 = (fConst38 * fRec90[1]);
			float fTemp66 = (fConst40 * (((0.00286798226f * fTemp21) + ((0.0033318114f * fTemp13) + (0.00164187339f * fTemp20))) - ((0.0232847333f * fTemp19) + ((0.0170458779f * fTemp16) + ((0.00817899685f * fTemp18) + (((((0.00195021275f * fTemp11) + (0.0165203959f * fTemp12)) + (0.0199282635f * fTemp17)) + (0.0210142974f * fTemp14)) + (0.0140326442f * fTemp15)))))));
			float fTemp67 = (fConst41 * fRec93[1]);
			float fTemp68 = (fConst42 * fRec96[1]);
			fRec98[0] = (fTemp66 + (fTemp67 + (fRec98[1] + fTemp68)));
			fRec96[0] = fRec98[0];
			float fRec97 = ((fTemp68 + fTemp67) + fTemp66);
			fRec95[0] = (fRec96[0] + fRec95[1]);
			fRec93[0] = fRec95[0];
			float fRec94 = fRec97;
			fRec92[0] = (fTemp64 + (fTemp65 + (fRec94 + fRec92[1])));
			fRec90[0] = fRec92[0];
			float fRec91 = (fTemp64 + (fRec94 + fTemp65));
			fRec89[0] = (fRec90[0] + fRec89[1]);
			fRec87[0] = fRec89[0];
			float fRec88 = fRec91;
			fRec86[0] = (fTemp63 + (fRec88 + fRec86[1]));
			fRec84[0] = fRec86[0];
			float fRec85 = (fRec88 + fTemp63);
			float fTemp69 = (fConst44 * fRec99[1]);
			float fTemp70 = (fConst45 * fRec102[1]);
			float fTemp71 = (fConst47 * ((((0.025524132f * fTemp28) + (0.00431362074f * fTemp32)) + (0.0002112191f * fTemp35)) - ((0.0322232991f * fTemp34) + ((0.0426399745f * fTemp30) + ((((0.0116217826f * fTemp27) + (0.0200883429f * fTemp31)) + (0.0332100652f * fTemp29)) + (0.00292050908f * fTemp33))))));
			float fTemp72 = (fConst48 * fRec105[1]);
			float fTemp73 = (fConst49 * fRec108[1]);
			fRec110[0] = (fTemp71 + (fTemp72 + (fRec110[1] + fTemp73)));
			fRec108[0] = fRec110[0];
			float fRec109 = ((fTemp73 + fTemp72) + fTemp71);
			fRec107[0] = (fRec108[0] + fRec107[1]);
			fRec105[0] = fRec107[0];
			float fRec106 = fRec109;
			fRec104[0] = (fTemp69 + (fTemp70 + (fRec106 + fRec104[1])));
			fRec102[0] = fRec104[0];
			float fRec103 = (fTemp69 + (fRec106 + fTemp70));
			fRec101[0] = (fRec102[0] + fRec101[1]);
			fRec99[0] = fRec101[0];
			float fRec100 = fRec103;
			float fTemp74 = (fConst51 * fRec111[1]);
			float fTemp75 = (fConst53 * ((((0.0387602337f * fTemp40) + (0.0532056242f * fTemp45)) + (0.0135681545f * fTemp42)) - ((0.0226701032f * fTemp46) + (((0.012015922f * fTemp44) + (0.0242027957f * fTemp41)) + (0.0523547642f * fTemp43)))));
			float fTemp76 = (fConst54 * fRec114[1]);
			float fTemp77 = (fConst55 * fRec117[1]);
			fRec119[0] = (fTemp75 + (fTemp76 + (fRec119[1] + fTemp77)));
			fRec117[0] = fRec119[0];
			float fRec118 = ((fTemp77 + fTemp76) + fTemp75);
			fRec116[0] = (fRec117[0] + fRec116[1]);
			fRec114[0] = fRec116[0];
			float fRec115 = fRec118;
			fRec113[0] = (fTemp74 + (fRec115 + fRec113[1]));
			fRec111[0] = fRec113[0];
			float fRec112 = (fRec115 + fTemp74);
			float fTemp78 = (fConst57 * (((0.0640498549f * fTemp52) + (0.0632267669f * fTemp50)) + (0.0215868391f * fTemp51)));
			float fTemp79 = (fConst58 * fRec120[1]);
			fRec122[0] = (fTemp78 + (fRec122[1] + fTemp79));
			fRec120[0] = fRec122[0];
			float fRec121 = (fTemp79 + fTemp78);
			float fTemp80 = (fConst60 * (((((0.0286792312f * fTemp55) + (0.0842565075f * fTemp59)) + (0.0191042144f * fTemp56)) + (0.0264974907f * fTemp57)) - (0.0353156179f * fTemp58)));
			float fTemp81 = (fConst61 * fRec123[1]);
			float fTemp82 = (fConst62 * fRec126[1]);
			fRec128[0] = (fTemp80 + (fTemp81 + (fRec128[1] + fTemp82)));
			fRec126[0] = fRec128[0];
			float fRec127 = ((fTemp82 + fTemp81) + fTemp80);
			fRec125[0] = (fRec126[0] + fRec125[1]);
			fRec123[0] = fRec125[0];
			float fRec124 = fRec127;
			fVec1[(IOTA & 2047)] = ((0.0579515621f * fTemp7) + (fRec85 + (fRec100 + (fRec112 + (fRec121 + fRec124)))));
			output1[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec1[((IOTA - iConst63) & 2047)])));
			float fTemp83 = (fConst65 * fRec129[1]);
			float fTemp84 = (fConst67 * fRec132[1]);
			float fTemp85 = (fConst68 * fRec135[1]);
			float fTemp86 = (fConst70 * (((0.0212461799f * fTemp19) + ((1.20615996e-05f * fTemp16) + ((((0.0019208095f * fTemp11) + (1.18089001e-05f * fTemp12)) + (0.0212516431f * fTemp17)) + (0.0185893457f * fTemp18)))) - ((0.00193675631f * fTemp21) + ((((0.0132310288f * fTemp13) + (0.0185862146f * fTemp14)) + (0.010032909f * fTemp15)) + (0.0105215553f * fTemp20)))));
			float fTemp87 = (fConst71 * fRec138[1]);
			float fTemp88 = (fConst72 * fRec141[1]);
			fRec143[0] = (fTemp86 + (fTemp87 + (fRec143[1] + fTemp88)));
			fRec141[0] = fRec143[0];
			float fRec142 = ((fTemp88 + fTemp87) + fTemp86);
			fRec140[0] = (fRec141[0] + fRec140[1]);
			fRec138[0] = fRec140[0];
			float fRec139 = fRec142;
			fRec137[0] = (fTemp84 + (fTemp85 + (fRec139 + fRec137[1])));
			fRec135[0] = fRec137[0];
			float fRec136 = (fTemp84 + (fRec139 + fTemp85));
			fRec134[0] = (fRec135[0] + fRec134[1]);
			fRec132[0] = fRec134[0];
			float fRec133 = fRec136;
			fRec131[0] = (fTemp83 + (fRec133 + fRec131[1]));
			fRec129[0] = fRec131[0];
			float fRec130 = (fRec133 + fTemp83);
			float fTemp89 = (fConst74 * fRec144[1]);
			float fTemp90 = (fConst75 * fRec147[1]);
			float fTemp91 = (fConst77 * (((0.0258102696f * fTemp34) + ((1.35194996e-05f * fTemp30) + (((1.16617002e-05f * fTemp27) + (0.0258032493f * fTemp31)) + (0.00226937444f * fTemp33)))) - ((((0.0498330481f * fTemp28) + (0.00227920571f * fTemp32)) + (0.0348369852f * fTemp29)) + (0.00663256412f * fTemp35))));
			float fTemp92 = (fConst78 * fRec150[1]);
			float fTemp93 = (fConst79 * fRec153[1]);
			fRec155[0] = (fTemp91 + (fTemp92 + (fRec155[1] + fTemp93)));
			fRec153[0] = fRec155[0];
			float fRec154 = ((fTemp93 + fTemp92) + fTemp91);
			fRec152[0] = (fRec153[0] + fRec152[1]);
			fRec150[0] = fRec152[0];
			float fRec151 = fRec154;
			fRec149[0] = (fTemp89 + (fTemp90 + (fRec151 + fRec149[1])));
			fRec147[0] = fRec149[0];
			float fRec148 = (fTemp89 + (fRec151 + fTemp90));
			fRec146[0] = (fRec147[0] + fRec146[1]);
			fRec144[0] = fRec146[0];
			float fRec145 = fRec148;
			float fTemp94 = (fConst81 * fRec156[1]);
			float fTemp95 = (fConst83 * (((0.017173158f * fTemp46) + (((0.0171615649f * fTemp44) + (0.0378993526f * fTemp45)) + (5.23010021e-06f * fTemp43))) - (((0.0685905889f * fTemp40) + (0.0300105829f * fTemp41)) + (0.0379156061f * fTemp42))));
			float fTemp96 = (fConst84 * fRec159[1]);
			float fTemp97 = (fConst85 * fRec162[1]);
			fRec164[0] = (fTemp95 + (fTemp96 + (fRec164[1] + fTemp97)));
			fRec162[0] = fRec164[0];
			float fRec163 = ((fTemp97 + fTemp96) + fTemp95);
			fRec161[0] = (fRec162[0] + fRec161[1]);
			fRec159[0] = fRec161[0];
			float fRec160 = fRec163;
			fRec158[0] = (fTemp94 + (fRec160 + fRec158[1]));
			fRec156[0] = fRec158[0];
			float fRec157 = (fRec160 + fTemp94);
			float fTemp98 = (fConst87 * (((0.0527678393f * fTemp52) + (0.0669136494f * fTemp50)) - (0.0527719259f * fTemp51)));
			float fTemp99 = (fConst88 * fRec165[1]);
			fRec167[0] = (fTemp98 + (fRec167[1] + fTemp99));
			fRec165[0] = fRec167[0];
			float fRec166 = (fTemp99 + fTemp98);
			float fTemp100 = (fConst90 * (((0.066809155f * fTemp59) + (0.0161149334f * fTemp56)) - (((0.0488329753f * fTemp55) + (0.0668209344f * fTemp57)) + (1.61180003e-06f * fTemp58))));
			float fTemp101 = (fConst91 * fRec168[1]);
			float fTemp102 = (fConst92 * fRec171[1]);
			fRec173[0] = (fTemp100 + (fTemp101 + (fRec173[1] + fTemp102)));
			fRec171[0] = fRec173[0];
			float fRec172 = ((fTemp102 + fTemp101) + fTemp100);
			fRec170[0] = (fRec171[0] + fRec170[1]);
			fRec168[0] = fRec170[0];
			float fRec169 = fRec172;
			fVec2[(IOTA & 2047)] = ((0.0633188263f * fTemp7) + (fRec130 + (fRec145 + (fRec157 + (fRec166 + fRec169)))));
			output2[i] = FAUSTFLOAT((0.716854393f * (fRec0[0] * fVec2[((IOTA - iConst93) & 2047)])));
			float fTemp103 = (fConst35 * fRec174[1]);
			float fTemp104 = (fConst37 * fRec177[1]);
			float fTemp105 = (fConst38 * fRec180[1]);
			float fTemp106 = (fConst40 * ((((0.0169985574f * fTemp16) + ((((0.0171084143f * fTemp12) + (0.00980887469f * fTemp13)) + (0.00479532266f * fTemp14)) + (0.0178821273f * fTemp18))) + (0.00987346098f * fTemp20)) - (((((0.00703606103f * fTemp11) + (0.0219569765f * fTemp17)) + (0.0180491135f * fTemp15)) + (0.0219419375f * fTemp19)) + (0.00189362315f * fTemp21))));
			float fTemp107 = (fConst41 * fRec183[1]);
			float fTemp108 = (fConst42 * fRec186[1]);
			fRec188[0] = (fTemp106 + (fTemp107 + (fRec188[1] + fTemp108)));
			fRec186[0] = fRec188[0];
			float fRec187 = ((fTemp108 + fTemp107) + fTemp106);
			fRec185[0] = (fRec186[0] + fRec185[1]);
			fRec183[0] = fRec185[0];
			float fRec184 = fRec187;
			fRec182[0] = (fTemp104 + (fTemp105 + (fRec184 + fRec182[1])));
			fRec180[0] = fRec182[0];
			float fRec181 = (fTemp104 + (fRec184 + fTemp105));
			fRec179[0] = (fRec180[0] + fRec179[1]);
			fRec177[0] = fRec179[0];
			float fRec178 = fRec181;
			fRec176[0] = (fTemp103 + (fRec178 + fRec176[1]));
			fRec174[0] = fRec176[0];
			float fRec175 = (fRec178 + fTemp103);
			float fTemp109 = (fConst44 * fRec189[1]);
			float fTemp110 = (fConst45 * fRec192[1]);
			float fTemp111 = (fConst47 * (((((0.0118943388f * fTemp27) + (0.0247731693f * fTemp28)) + (0.0428953283f * fTemp30)) + (0.00686916383f * fTemp35)) - (((((0.0265180059f * fTemp31) + (0.00259988382f * fTemp32)) + (0.03157847f * fTemp29)) + (0.0097140763f * fTemp33)) + (0.0265038051f * fTemp34))));
			float fTemp112 = (fConst48 * fRec195[1]);
			float fTemp113 = (fConst49 * fRec198[1]);
			fRec200[0] = (fTemp111 + (fTemp112 + (fRec200[1] + fTemp113)));
			fRec198[0] = fRec200[0];
			float fRec199 = ((fTemp113 + fTemp112) + fTemp111);
			fRec197[0] = (fRec198[0] + fRec197[1]);
			fRec195[0] = fRec197[0];
			float fRec196 = fRec199;
			fRec194[0] = (fTemp109 + (fTemp110 + (fRec196 + fRec194[1])));
			fRec192[0] = fRec194[0];
			float fRec193 = (fTemp109 + (fRec196 + fTemp110));
			fRec191[0] = (fRec192[0] + fRec191[1]);
			fRec189[0] = fRec191[0];
			float fRec190 = fRec193;
			float fTemp114 = (fConst51 * fRec201[1]);
			float fTemp115 = (fConst53 * (((0.0304364227f * fTemp40) + (0.0526964627f * fTemp43)) - (((((0.0175366681f * fTemp44) + (0.0145489927f * fTemp45)) + (0.018388411f * fTemp41)) + (0.0542868599f * fTemp42)) + (0.0175308287f * fTemp46))));
			float fTemp116 = (fConst54 * fRec204[1]);
			float fTemp117 = (fConst55 * fRec207[1]);
			fRec209[0] = (fTemp115 + (fTemp116 + (fRec209[1] + fTemp117)));
			fRec207[0] = fRec209[0];
			float fRec208 = ((fTemp117 + fTemp116) + fTemp115);
			fRec206[0] = (fRec207[0] + fRec206[1]);
			fRec204[0] = fRec206[0];
			float fRec205 = fRec208;
			fRec203[0] = (fTemp114 + (fRec205 + fRec203[1]));
			fRec201[0] = fRec203[0];
			float fRec202 = (fRec205 + fTemp114);
			float fTemp118 = (fConst57 * ((0.0595454238f * fTemp50) - ((0.0156096201f * fTemp52) + (0.0582364872f * fTemp51))));
			float fTemp119 = (fConst58 * fRec210[1]);
			fRec212[0] = (fTemp118 + (fRec212[1] + fTemp119));
			fRec210[0] = fRec212[0];
			float fRec211 = (fTemp119 + fTemp118);
			float fTemp120 = (fConst60 * ((((0.02052035f * fTemp55) + (0.022095751f * fTemp56)) + (0.0355290733f * fTemp58)) - ((0.0212301388f * fTemp59) + (0.0792069137f * fTemp57))));
			float fTemp121 = (fConst61 * fRec213[1]);
			float fTemp122 = (fConst62 * fRec216[1]);
			fRec218[0] = (fTemp120 + (fTemp121 + (fRec218[1] + fTemp122)));
			fRec216[0] = fRec218[0];
			float fRec217 = ((fTemp122 + fTemp121) + fTemp120);
			fRec215[0] = (fRec216[0] + fRec215[1]);
			fRec213[0] = fRec215[0];
			float fRec214 = fRec217;
			fVec3[(IOTA & 2047)] = ((0.0525917113f * fTemp7) + (fRec175 + (fRec190 + (fRec202 + (fRec211 + fRec214)))));
			output3[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec3[((IOTA - iConst63) & 2047)])));
			float fTemp123 = (fConst35 * fRec219[1]);
			float fTemp124 = (fConst37 * fRec222[1]);
			float fTemp125 = (fConst38 * fRec225[1]);
			float fTemp126 = (fConst40 * (((0.00165238965f * fTemp20) + ((((((0.00196849438f * fTemp11) + (0.0199413281f * fTemp17)) + (0.00333455321f * fTemp13)) + (0.0210022964f * fTemp14)) + (0.00818891358f * fTemp18)) + (0.0232942775f * fTemp19))) - ((((0.0165365506f * fTemp12) + (0.0140201868f * fTemp15)) + (0.0170337372f * fTemp16)) + (0.00286887912f * fTemp21))));
			float fTemp127 = (fConst41 * fRec228[1]);
			float fTemp128 = (fConst42 * fRec231[1]);
			fRec233[0] = (fTemp126 + (fTemp127 + (fRec233[1] + fTemp128)));
			fRec231[0] = fRec233[0];
			float fRec232 = ((fTemp128 + fTemp127) + fTemp126);
			fRec230[0] = (fRec231[0] + fRec230[1]);
			fRec228[0] = fRec230[0];
			float fRec229 = fRec232;
			fRec227[0] = (fTemp124 + (fTemp125 + (fRec229 + fRec227[1])));
			fRec225[0] = fRec227[0];
			float fRec226 = (fTemp124 + (fRec229 + fTemp125));
			fRec224[0] = (fRec225[0] + fRec224[1]);
			fRec222[0] = fRec224[0];
			float fRec223 = fRec226;
			fRec221[0] = (fTemp123 + (fRec223 + fRec221[1]));
			fRec219[0] = fRec221[0];
			float fRec220 = (fRec223 + fTemp123);
			float fTemp129 = (fConst44 * fRec234[1]);
			float fTemp130 = (fConst45 * fRec237[1]);
			float fTemp131 = (fConst47 * (((0.0002056183f * fTemp35) + ((((0.0201007035f * fTemp31) + (0.0255371984f * fTemp28)) + (0.00292492588f * fTemp33)) + (0.0322504453f * fTemp34))) - ((((0.0116404695f * fTemp27) + (0.00430144789f * fTemp32)) + (0.0331932008f * fTemp29)) + (0.0426293314f * fTemp30))));
			float fTemp132 = (fConst48 * fRec240[1]);
			float fTemp133 = (fConst49 * fRec243[1]);
			fRec245[0] = (fTemp131 + (fTemp132 + (fRec245[1] + fTemp133)));
			fRec243[0] = fRec245[0];
			float fRec244 = ((fTemp133 + fTemp132) + fTemp131);
			fRec242[0] = (fRec243[0] + fRec242[1]);
			fRec240[0] = fRec242[0];
			float fRec241 = fRec244;
			fRec239[0] = (fTemp129 + (fTemp130 + (fRec241 + fRec239[1])));
			fRec237[0] = fRec239[0];
			float fRec238 = (fTemp129 + (fRec241 + fTemp130));
			fRec236[0] = (fRec237[0] + fRec236[1]);
			fRec234[0] = fRec236[0];
			float fRec235 = fRec238;
			float fTemp134 = (fConst51 * fRec246[1]);
			float fTemp135 = (fConst53 * ((((0.0120212426f * fTemp44) + (0.0387873575f * fTemp40)) + (0.0226971731f * fTemp46)) - ((((0.0531799942f * fTemp45) + (0.0242017414f * fTemp41)) + (0.0135731231f * fTemp42)) + (0.0523548201f * fTemp43))));
			float fTemp136 = (fConst54 * fRec249[1]);
			float fTemp137 = (fConst55 * fRec252[1]);
			fRec254[0] = (fTemp135 + (fTemp136 + (fRec254[1] + fTemp137)));
			fRec252[0] = fRec254[0];
			float fRec253 = ((fTemp137 + fTemp136) + fTemp135);
			fRec251[0] = (fRec252[0] + fRec251[1]);
			fRec249[0] = fRec251[0];
			float fRec250 = fRec253;
			fRec248[0] = (fTemp134 + (fRec250 + fRec248[1]));
			fRec246[0] = fRec248[0];
			float fRec247 = (fRec250 + fTemp134);
			float fTemp138 = (fConst57 * ((0.063209936f * fTemp50) - ((0.0640460402f * fTemp52) + (0.0216016546f * fTemp51))));
			float fTemp139 = (fConst58 * fRec255[1]);
			fRec257[0] = (fTemp138 + (fRec257[1] + fTemp139));
			fRec255[0] = fRec257[0];
			float fRec256 = (fTemp139 + fTemp138);
			float fTemp140 = (fConst60 * (((0.0287043639f * fTemp55) + (0.0190891717f * fTemp56)) - (((0.0842392817f * fTemp59) + (0.0265128072f * fTemp57)) + (0.0353211984f * fTemp58))));
			float fTemp141 = (fConst61 * fRec258[1]);
			float fTemp142 = (fConst62 * fRec261[1]);
			fRec263[0] = (fTemp140 + (fTemp141 + (fRec263[1] + fTemp142)));
			fRec261[0] = fRec263[0];
			float fRec262 = ((fTemp142 + fTemp141) + fTemp140);
			fRec260[0] = (fRec261[0] + fRec260[1]);
			fRec258[0] = fRec260[0];
			float fRec259 = fRec262;
			fVec4[(IOTA & 2047)] = ((0.0579424426f * fTemp7) + (fRec220 + (fRec235 + (fRec247 + (fRec256 + fRec259)))));
			output4[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec4[((IOTA - iConst63) & 2047)])));
			float fTemp143 = (fConst65 * fRec264[1]);
			float fTemp144 = (fConst67 * fRec267[1]);
			float fTemp145 = (fConst68 * fRec270[1]);
			float fTemp146 = (fConst70 * (((0.0185845122f * fTemp14) + (0.00190813199f * fTemp21)) - ((0.0105418498f * fTemp20) + ((0.0212450754f * fTemp19) + ((1.29088003e-05f * fTemp16) + ((0.0185854603f * fTemp18) + (((((0.00191086321f * fTemp11) + (5.05419985e-06f * fTemp12)) + (0.0212375224f * fTemp17)) + (0.0132289501f * fTemp13)) + (0.0100453161f * fTemp15))))))));
			float fTemp147 = (fConst71 * fRec273[1]);
			float fTemp148 = (fConst72 * fRec276[1]);
			fRec278[0] = (fTemp146 + (fTemp147 + (fRec278[1] + fTemp148)));
			fRec276[0] = fRec278[0];
			float fRec277 = ((fTemp148 + fTemp147) + fTemp146);
			fRec275[0] = (fRec276[0] + fRec275[1]);
			fRec273[0] = fRec275[0];
			float fRec274 = fRec277;
			fRec272[0] = (fTemp144 + (fTemp145 + (fRec274 + fRec272[1])));
			fRec270[0] = fRec272[0];
			float fRec271 = (fTemp144 + (fRec274 + fTemp145));
			fRec269[0] = (fRec270[0] + fRec269[1]);
			fRec267[0] = fRec269[0];
			float fRec268 = fRec271;
			fRec266[0] = (fTemp143 + (fRec268 + fRec266[1]));
			fRec264[0] = fRec266[0];
			float fRec265 = (fRec268 + fTemp143);
			float fTemp149 = (fConst74 * fRec279[1]);
			float fTemp150 = (fConst75 * fRec282[1]);
			float fTemp151 = (fConst77 * ((0.00225335499f * fTemp32) - ((0.00666434877f * fTemp35) + ((0.0258102771f * fTemp34) + ((8.92810021e-06f * fTemp30) + (((((6.97999985e-06f * fTemp27) + (0.0258209221f * fTemp31)) + (0.0498308949f * fTemp28)) + (0.0348385312f * fTemp29)) + (0.00226649758f * fTemp33)))))));
			float fTemp152 = (fConst78 * fRec285[1]);
			float fTemp153 = (fConst79 * fRec288[1]);
			fRec290[0] = (fTemp151 + (fTemp152 + (fRec290[1] + fTemp153)));
			fRec288[0] = fRec290[0];
			float fRec289 = ((fTemp153 + fTemp152) + fTemp151);
			fRec287[0] = (fRec288[0] + fRec287[1]);
			fRec285[0] = fRec287[0];
			float fRec286 = fRec289;
			fRec284[0] = (fTemp149 + (fTemp150 + (fRec286 + fRec284[1])));
			fRec282[0] = fRec284[0];
			float fRec283 = (fTemp149 + (fRec286 + fTemp150));
			fRec281[0] = (fRec282[0] + fRec281[1]);
			fRec279[0] = fRec281[0];
			float fRec280 = fRec283;
			float fTemp154 = (fConst81 * fRec291[1]);
			float fTemp155 = (fConst83 * (((0.0379130989f * fTemp42) + (5.81179984e-06f * fTemp43)) - (((((0.0171904024f * fTemp44) + (0.0685903355f * fTemp40)) + (0.0379246771f * fTemp45)) + (0.0299918056f * fTemp41)) + (0.0171739701f * fTemp46))));
			float fTemp156 = (fConst84 * fRec294[1]);
			float fTemp157 = (fConst85 * fRec297[1]);
			fRec299[0] = (fTemp155 + (fTemp156 + (fRec299[1] + fTemp157)));
			fRec297[0] = fRec299[0];
			float fRec298 = ((fTemp157 + fTemp156) + fTemp155);
			fRec296[0] = (fRec297[0] + fRec296[1]);
			fRec294[0] = fRec296[0];
			float fRec295 = fRec298;
			fRec293[0] = (fTemp154 + (fRec295 + fRec293[1]));
			fRec291[0] = fRec293[0];
			float fRec292 = (fRec295 + fTemp154);
			float fTemp158 = (fConst87 * (((0.0669162646f * fTemp50) + (0.0527649f * fTemp51)) - (0.0527561419f * fTemp52)));
			float fTemp159 = (fConst88 * fRec300[1]);
			fRec302[0] = (fTemp158 + (fRec302[1] + fTemp159));
			fRec300[0] = fRec302[0];
			float fRec301 = (fTemp159 + fTemp158);
			float fTemp160 = (fConst90 * ((((0.0161357336f * fTemp56) + (0.0668131635f * fTemp57)) + (1.16084002e-05f * fTemp58)) - ((0.0488342457f * fTemp55) + (0.0668111816f * fTemp59))));
			float fTemp161 = (fConst91 * fRec303[1]);
			float fTemp162 = (fConst92 * fRec306[1]);
			fRec308[0] = (fTemp160 + (fTemp161 + (fRec308[1] + fTemp162)));
			fRec306[0] = fRec308[0];
			float fRec307 = ((fTemp162 + fTemp161) + fTemp160);
			fRec305[0] = (fRec306[0] + fRec305[1]);
			fRec303[0] = fRec305[0];
			float fRec304 = fRec307;
			fVec5[(IOTA & 2047)] = ((0.06331072f * fTemp7) + (fRec265 + (fRec280 + (fRec292 + (fRec301 + fRec304)))));
			output5[i] = FAUSTFLOAT((0.716854393f * (fRec0[0] * fVec5[((IOTA - iConst93) & 2047)])));
			float fTemp163 = (fConst35 * fRec309[1]);
			float fTemp164 = (fConst37 * fRec312[1]);
			float fTemp165 = (fConst38 * fRec315[1]);
			float fTemp166 = (fConst40 * (((0.00187531218f * fTemp21) + ((0.00986265857f * fTemp20) + ((0.0219628084f * fTemp19) + (((((0.00701580709f * fTemp11) + (0.0170988087f * fTemp12)) + (0.0219666809f * fTemp17)) + (0.00980995316f * fTemp13)) + (0.0170027632f * fTemp16))))) - ((0.0178936273f * fTemp18) + ((0.00480109733f * fTemp14) + (0.0180332512f * fTemp15)))));
			float fTemp167 = (fConst41 * fRec318[1]);
			float fTemp168 = (fConst42 * fRec321[1]);
			fRec323[0] = (fTemp166 + (fTemp167 + (fRec323[1] + fTemp168)));
			fRec321[0] = fRec323[0];
			float fRec322 = ((fTemp168 + fTemp167) + fTemp166);
			fRec320[0] = (fRec321[0] + fRec320[1]);
			fRec318[0] = fRec320[0];
			float fRec319 = fRec322;
			fRec317[0] = (fTemp164 + (fTemp165 + (fRec319 + fRec317[1])));
			fRec315[0] = fRec317[0];
			float fRec316 = (fTemp164 + (fRec319 + fTemp165));
			fRec314[0] = (fRec315[0] + fRec314[1]);
			fRec312[0] = fRec314[0];
			float fRec313 = fRec316;
			fRec311[0] = (fTemp163 + (fRec313 + fRec311[1]));
			fRec309[0] = fRec311[0];
			float fRec310 = (fRec313 + fTemp163);
			float fTemp169 = (fConst44 * fRec324[1]);
			float fTemp170 = (fConst45 * fRec327[1]);
			float fTemp171 = (fConst47 * (((0.00685405731f * fTemp35) + ((0.0265102237f * fTemp34) + ((0.0429091901f * fTemp30) + (((((0.0118854297f * fTemp27) + (0.0265230574f * fTemp31)) + (0.0247748103f * fTemp28)) + (0.00259165652f * fTemp32)) + (0.00969489478f * fTemp33))))) - (0.0315765627f * fTemp29)));
			float fTemp172 = (fConst48 * fRec330[1]);
			float fTemp173 = (fConst49 * fRec333[1]);
			fRec335[0] = (fTemp171 + (fTemp172 + (fRec335[1] + fTemp173)));
			fRec333[0] = fRec335[0];
			float fRec334 = ((fTemp173 + fTemp172) + fTemp171);
			fRec332[0] = (fRec333[0] + fRec332[1]);
			fRec330[0] = fRec332[0];
			float fRec331 = fRec334;
			fRec329[0] = (fTemp169 + (fTemp170 + (fRec331 + fRec329[1])));
			fRec327[0] = fRec329[0];
			float fRec328 = (fTemp169 + (fRec331 + fTemp170));
			fRec326[0] = (fRec327[0] + fRec326[1]);
			fRec324[0] = fRec326[0];
			float fRec325 = fRec328;
			float fTemp174 = (fConst51 * fRec336[1]);
			float fTemp175 = (fConst53 * (((0.0175272785f * fTemp46) + ((0.0527098142f * fTemp43) + ((((0.0175386928f * fTemp44) + (0.0304406285f * fTemp40)) + (0.0145412851f * fTemp45)) + (0.0542752631f * fTemp42)))) - (0.0184011627f * fTemp41)));
			float fTemp176 = (fConst54 * fRec339[1]);
			float fTemp177 = (fConst55 * fRec342[1]);
			fRec344[0] = (fTemp175 + (fTemp176 + (fRec344[1] + fTemp177)));
			fRec342[0] = fRec344[0];
			float fRec343 = ((fTemp177 + fTemp176) + fTemp175);
			fRec341[0] = (fRec342[0] + fRec341[1]);
			fRec339[0] = fRec341[0];
			float fRec340 = fRec343;
			fRec338[0] = (fTemp174 + (fRec340 + fRec338[1]));
			fRec336[0] = fRec338[0];
			float fRec337 = (fRec340 + fTemp174);
			float fTemp178 = (fConst57 * (((0.0156095503f * fTemp52) + (0.0595348626f * fTemp50)) + (0.0582405664f * fTemp51)));
			float fTemp179 = (fConst58 * fRec345[1]);
			fRec347[0] = (fTemp178 + (fRec347[1] + fTemp179));
			fRec345[0] = fRec347[0];
			float fRec346 = (fTemp179 + fTemp178);
			float fTemp180 = (fConst60 * (((((0.0205246415f * fTemp55) + (0.0212266501f * fTemp59)) + (0.0220794007f * fTemp56)) + (0.0792068467f * fTemp57)) + (0.035535343f * fTemp58)));
			float fTemp181 = (fConst61 * fRec348[1]);
			float fTemp182 = (fConst62 * fRec351[1]);
			fRec353[0] = (fTemp180 + (fTemp181 + (fRec353[1] + fTemp182)));
			fRec351[0] = fRec353[0];
			float fRec352 = ((fTemp182 + fTemp181) + fTemp180);
			fRec350[0] = (fRec351[0] + fRec350[1]);
			fRec348[0] = fRec350[0];
			float fRec349 = fRec352;
			fVec6[(IOTA & 2047)] = ((0.052587714f * fTemp7) + (fRec310 + (fRec325 + (fRec337 + (fRec346 + fRec349)))));
			output6[i] = FAUSTFLOAT((0.717061102f * (fRec0[0] * fVec6[((IOTA - iConst63) & 2047)])));
			float fTemp183 = (fConst95 * fRec354[1]);
			float fTemp184 = (fConst97 * fRec357[1]);
			float fTemp185 = (fConst98 * fRec360[1]);
			float fTemp186 = (fConst100 * (((((((((((0.0308601614f * fTemp11) + (0.00827050302f * fTemp12)) + (0.015319285f * fTemp17)) + (0.00373257068f * fTemp13)) + (0.00778385112f * fTemp14)) + (0.0169985518f * fTemp15)) + (0.00177371479f * fTemp18)) + (0.023355348f * fTemp16)) + (0.00273729791f * fTemp19)) + (0.0186589882f * fTemp20)) - (0.0054692463f * fTemp21)));
			float fTemp187 = (fConst101 * fRec363[1]);
			float fTemp188 = (fConst102 * fRec366[1]);
			fRec368[0] = (fTemp186 + (fTemp187 + (fRec368[1] + fTemp188)));
			fRec366[0] = fRec368[0];
			float fRec367 = ((fTemp188 + fTemp187) + fTemp186);
			fRec365[0] = (fRec366[0] + fRec365[1]);
			fRec363[0] = fRec365[0];
			float fRec364 = fRec367;
			fRec362[0] = (fTemp184 + (fTemp185 + (fRec364 + fRec362[1])));
			fRec360[0] = fRec362[0];
			float fRec361 = (fTemp184 + (fRec364 + fTemp185));
			fRec359[0] = (fRec360[0] + fRec359[1]);
			fRec357[0] = fRec359[0];
			float fRec358 = fRec361;
			fRec356[0] = (fTemp183 + (fRec358 + fRec356[1]));
			fRec354[0] = fRec356[0];
			float fRec355 = (fRec358 + fTemp183);
			float fTemp189 = (fConst104 * fRec369[1]);
			float fTemp190 = (fConst105 * fRec372[1]);
			float fTemp191 = (fConst107 * (((0.0377700105f * fTemp35) + ((0.00749708479f * fTemp34) + ((0.0205384661f * fTemp30) + ((0.00247007096f * fTemp33) + ((0.00634144247f * fTemp27) + (0.0111316806f * fTemp29)))))) - (((0.0225200728f * fTemp31) + (0.000971430622f * fTemp28)) + (0.0267060734f * fTemp32))));
			float fTemp192 = (fConst108 * fRec375[1]);
			float fTemp193 = (fConst109 * fRec378[1]);
			fRec380[0] = (fTemp191 + (fTemp192 + (fRec380[1] + fTemp193)));
			fRec378[0] = fRec380[0];
			float fRec379 = ((fTemp193 + fTemp192) + fTemp191);
			fRec377[0] = (fRec378[0] + fRec377[1]);
			fRec375[0] = fRec377[0];
			float fRec376 = fRec379;
			fRec374[0] = (fTemp189 + (fTemp190 + (fRec376 + fRec374[1])));
			fRec372[0] = fRec374[0];
			float fRec373 = (fTemp189 + (fRec376 + fTemp190));
			fRec371[0] = (fRec372[0] + fRec371[1]);
			fRec369[0] = fRec371[0];
			float fRec370 = fRec373;
			float fTemp194 = (fConst111 * fRec381[1]);
			float fTemp195 = (fConst113 * (((0.000259017193f * fTemp42) + (0.00624397956f * fTemp46)) - (((((0.0437610447f * fTemp44) + (0.00537092704f * fTemp40)) + (0.0273077898f * fTemp45)) + (0.0189718362f * fTemp41)) + (0.0239363015f * fTemp43))));
			float fTemp196 = (fConst114 * fRec384[1]);
			float fTemp197 = (fConst115 * fRec387[1]);
			fRec389[0] = (fTemp195 + (fTemp196 + (fRec389[1] + fTemp197)));
			fRec387[0] = fRec389[0];
			float fRec388 = ((fTemp197 + fTemp196) + fTemp195);
			fRec386[0] = (fRec387[0] + fRec386[1]);
			fRec384[0] = fRec386[0];
			float fRec385 = fRec388;
			fRec383[0] = (fTemp194 + (fRec385 + fRec383[1]));
			fRec381[0] = fRec383[0];
			float fRec382 = (fRec385 + fTemp194);
			float fTemp198 = (fConst117 * (((0.0479171462f * fTemp52) + (0.0110148918f * fTemp50)) - (0.00271321135f * fTemp51)));
			float fTemp199 = (fConst118 * fRec390[1]);
			fRec392[0] = (fTemp198 + (fRec392[1] + fTemp199));
			fRec390[0] = fRec392[0];
			float fRec391 = (fTemp199 + fTemp198);
			float fTemp200 = (fConst120 * ((0.0218717307f * fTemp59) - ((((0.0049932464f * fTemp55) + (0.0249028355f * fTemp56)) + (0.0025010386f * fTemp57)) + (0.047650516f * fTemp58))));
			float fTemp201 = (fConst121 * fRec393[1]);
			float fTemp202 = (fConst122 * fRec396[1]);
			fRec398[0] = (fTemp200 + (fTemp201 + (fRec398[1] + fTemp202)));
			fRec396[0] = fRec398[0];
			float fRec397 = ((fTemp202 + fTemp201) + fTemp200);
			fRec395[0] = (fRec396[0] + fRec395[1]);
			fRec393[0] = fRec395[0];
			float fRec394 = fRec397;
			fVec7[(IOTA & 1023)] = ((0.0296740122f * fTemp7) + (fRec355 + (fRec370 + (fRec382 + (fRec391 + fRec394)))));
			output7[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec7[((IOTA - iConst123) & 1023)])));
			float fTemp203 = (fConst125 * fRec399[1]);
			float fTemp204 = (fConst126 * fRec402[1]);
			float fTemp205 = (fConst128 * (((0.00713762827f * fTemp34) + ((0.01698534f * fTemp30) + ((0.00396325951f * fTemp33) + ((((0.0358571522f * fTemp27) + (0.00241139205f * fTemp31)) + (0.0298297461f * fTemp28)) + (0.0233165901f * fTemp29))))) - ((0.00528474478f * fTemp32) + (0.0239895768f * fTemp35))));
			float fTemp206 = (fConst129 * fRec405[1]);
			float fTemp207 = (fConst130 * fRec408[1]);
			fRec410[0] = (fTemp205 + (fTemp206 + (fRec410[1] + fTemp207)));
			fRec408[0] = fRec410[0];
			float fRec409 = ((fTemp207 + fTemp206) + fTemp205);
			fRec407[0] = (fRec408[0] + fRec407[1]);
			fRec405[0] = fRec407[0];
			float fRec406 = fRec409;
			fRec404[0] = (fTemp203 + (fTemp204 + (fRec406 + fRec404[1])));
			fRec402[0] = fRec404[0];
			float fRec403 = (fTemp203 + (fRec406 + fTemp204));
			fRec401[0] = (fRec402[0] + fRec401[1]);
			fRec399[0] = fRec401[0];
			float fRec400 = fRec403;
			float fTemp208 = (fConst132 * fRec411[1]);
			float fTemp209 = (fConst134 * ((((0.00235916674f * fTemp44) + (0.0203794725f * fTemp42)) + (0.050384786f * fTemp46)) - ((((0.00613554753f * fTemp40) + (0.0347099751f * fTemp45)) + (0.00433786446f * fTemp41)) + (0.00180905988f * fTemp43))));
			float fTemp210 = (fConst135 * fRec414[1]);
			float fTemp211 = (fConst136 * fRec417[1]);
			fRec419[0] = (fTemp209 + (fTemp210 + (fRec419[1] + fTemp211)));
			fRec417[0] = fRec419[0];
			float fRec418 = ((fTemp211 + fTemp210) + fTemp209);
			fRec416[0] = (fRec417[0] + fRec416[1]);
			fRec414[0] = fRec416[0];
			float fRec415 = fRec418;
			fRec413[0] = (fTemp208 + (fRec415 + fRec413[1]));
			fRec411[0] = fRec413[0];
			float fRec412 = (fRec415 + fTemp208);
			float fTemp212 = (fConst138 * ((0.00402585231f * fTemp59) - ((((0.047910817f * fTemp55) + (0.0318393186f * fTemp56)) + (0.00305585004f * fTemp57)) + (0.0257446915f * fTemp58))));
			float fTemp213 = (fConst139 * fRec420[1]);
			float fTemp214 = (fConst140 * fRec423[1]);
			fRec425[0] = (fTemp212 + (fTemp213 + (fRec425[1] + fTemp214)));
			fRec423[0] = fRec425[0];
			float fRec424 = ((fTemp214 + fTemp213) + fTemp212);
			fRec422[0] = (fRec423[0] + fRec422[1]);
			fRec420[0] = fRec422[0];
			float fRec421 = fRec424;
			float fTemp215 = (fConst142 * fRec426[1]);
			float fTemp216 = (fConst144 * fRec429[1]);
			float fTemp217 = (fConst145 * fRec432[1]);
			float fTemp218 = (fConst147 * ((((0.00425409852f * fTemp15) + ((((0.00514439307f * fTemp12) + (0.000187427795f * fTemp17)) + (0.00647400273f * fTemp13)) + (0.0226849411f * fTemp14))) + (0.00203823159f * fTemp16)) - ((0.0141532524f * fTemp21) + ((0.00668181432f * fTemp20) + (((0.0306970719f * fTemp11) + (0.0127971442f * fTemp18)) + (0.027412409f * fTemp19))))));
			float fTemp219 = (fConst148 * fRec435[1]);
			float fTemp220 = (fConst149 * fRec438[1]);
			fRec440[0] = (fTemp218 + (fTemp219 + (fRec440[1] + fTemp220)));
			fRec438[0] = fRec440[0];
			float fRec439 = ((fTemp220 + fTemp219) + fTemp218);
			fRec437[0] = (fRec438[0] + fRec437[1]);
			fRec435[0] = fRec437[0];
			float fRec436 = fRec439;
			fRec434[0] = (fTemp216 + (fTemp217 + (fRec436 + fRec434[1])));
			fRec432[0] = fRec434[0];
			float fRec433 = (fTemp216 + (fRec436 + fTemp217));
			fRec431[0] = (fRec432[0] + fRec431[1]);
			fRec429[0] = fRec431[0];
			float fRec430 = fRec433;
			fRec428[0] = (fTemp215 + (fRec430 + fRec428[1]));
			fRec426[0] = fRec428[0];
			float fRec427 = (fRec430 + fTemp215);
			float fTemp221 = (fConst151 * (((0.0460188352f * fTemp52) + (0.00239910278f * fTemp50)) - (0.0275209881f * fTemp51)));
			float fTemp222 = (fConst152 * fRec441[1]);
			fRec443[0] = (fTemp221 + (fRec443[1] + fTemp222));
			fRec441[0] = fRec443[0];
			float fRec442 = (fTemp222 + fTemp221);
			fVec8[(IOTA & 1023)] = ((0.0324214213f * fTemp7) + (fRec400 + (fRec412 + (fRec421 + (fRec427 + fRec442)))));
			output8[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec8[((IOTA - iConst153) & 1023)])));
			float fTemp223 = (fConst142 * fRec444[1]);
			float fTemp224 = (fConst144 * fRec447[1]);
			float fTemp225 = (fConst145 * fRec450[1]);
			float fTemp226 = (fConst147 * (((((0.00760853803f * fTemp15) + (((0.0218213294f * fTemp11) + (0.0111512998f * fTemp13)) + (0.00891583413f * fTemp14))) + (0.00495283073f * fTemp19)) + (0.0236715935f * fTemp21)) - (((0.00212345319f * fTemp16) + (((0.00530337868f * fTemp12) + (0.0230340399f * fTemp17)) + (0.0189385079f * fTemp18))) + (0.0109720742f * fTemp20))));
			float fTemp227 = (fConst148 * fRec453[1]);
			float fTemp228 = (fConst149 * fRec456[1]);
			fRec458[0] = (fTemp226 + (fTemp227 + (fRec458[1] + fTemp228)));
			fRec456[0] = fRec458[0];
			float fRec457 = ((fTemp228 + fTemp227) + fTemp226);
			fRec455[0] = (fRec456[0] + fRec455[1]);
			fRec453[0] = fRec455[0];
			float fRec454 = fRec457;
			fRec452[0] = (fTemp224 + (fTemp225 + (fRec454 + fRec452[1])));
			fRec450[0] = fRec452[0];
			float fRec451 = (fTemp224 + (fRec454 + fTemp225));
			fRec449[0] = (fRec450[0] + fRec449[1]);
			fRec447[0] = fRec449[0];
			float fRec448 = fRec451;
			fRec446[0] = (fTemp223 + (fRec448 + fRec446[1]));
			fRec444[0] = fRec446[0];
			float fRec445 = (fRec448 + fTemp223);
			float fTemp229 = (fConst125 * fRec459[1]);
			float fTemp230 = (fConst126 * fRec462[1]);
			float fTemp231 = (fConst128 * ((((0.00834930222f * fTemp33) + (((0.00999347214f * fTemp31) + (0.0227805395f * fTemp28)) + (0.0188161395f * fTemp29))) + (0.00512609445f * fTemp34)) - ((((0.036219418f * fTemp27) + (0.00697720563f * fTemp32)) + (0.0171614718f * fTemp30)) + (0.0133182807f * fTemp35))));
			float fTemp232 = (fConst129 * fRec465[1]);
			float fTemp233 = (fConst130 * fRec468[1]);
			fRec470[0] = (fTemp231 + (fTemp232 + (fRec470[1] + fTemp233)));
			fRec468[0] = fRec470[0];
			float fRec469 = ((fTemp233 + fTemp232) + fTemp231);
			fRec467[0] = (fRec468[0] + fRec467[1]);
			fRec465[0] = fRec467[0];
			float fRec466 = fRec469;
			fRec464[0] = (fTemp229 + (fTemp230 + (fRec466 + fRec464[1])));
			fRec462[0] = fRec464[0];
			float fRec463 = (fTemp229 + (fRec466 + fTemp230));
			fRec461[0] = (fRec462[0] + fRec461[1]);
			fRec459[0] = fRec461[0];
			float fRec460 = fRec463;
			float fTemp234 = (fConst132 * fRec471[1]);
			float fTemp235 = (fConst134 * (((0.00186196622f * fTemp43) + ((0.0430174358f * fTemp44) + (0.029415274f * fTemp42))) - ((((0.00955280382f * fTemp40) + (0.0149666639f * fTemp45)) + (0.00693474943f * fTemp41)) + (0.00538756512f * fTemp46))));
			float fTemp236 = (fConst135 * fRec474[1]);
			float fTemp237 = (fConst136 * fRec477[1]);
			fRec479[0] = (fTemp235 + (fTemp236 + (fRec479[1] + fTemp237)));
			fRec477[0] = fRec479[0];
			float fRec478 = ((fTemp237 + fTemp236) + fTemp235);
			fRec476[0] = (fRec477[0] + fRec476[1]);
			fRec474[0] = fRec476[0];
			float fRec475 = fRec478;
			fRec473[0] = (fTemp234 + (fRec475 + fRec473[1]));
			fRec471[0] = fRec473[0];
			float fRec472 = (fRec475 + fTemp234);
			float fTemp238 = (fConst151 * (((0.0206746776f * fTemp52) + (0.00365679874f * fTemp50)) - (0.0392839573f * fTemp51)));
			float fTemp239 = (fConst152 * fRec480[1]);
			fRec482[0] = (fTemp238 + (fRec482[1] + fTemp239));
			fRec480[0] = fRec482[0];
			float fRec481 = (fTemp239 + fTemp238);
			float fTemp240 = (fConst138 * (((0.00495913578f * fTemp59) + (0.0259169079f * fTemp58)) - (((0.0375389159f * fTemp55) + (0.0261093918f * fTemp56)) + (0.00595670892f * fTemp57))));
			float fTemp241 = (fConst139 * fRec483[1]);
			float fTemp242 = (fConst140 * fRec486[1]);
			fRec488[0] = (fTemp240 + (fTemp241 + (fRec488[1] + fTemp242)));
			fRec486[0] = fRec488[0];
			float fRec487 = ((fTemp242 + fTemp241) + fTemp240);
			fRec485[0] = (fRec486[0] + fRec485[1]);
			fRec483[0] = fRec485[0];
			float fRec484 = fRec487;
			fVec9[(IOTA & 1023)] = ((0.0267503597f * fTemp7) + (fRec445 + (fRec460 + (fRec472 + (fRec481 + fRec484)))));
			output9[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec9[((IOTA - iConst153) & 1023)])));
			float fTemp243 = (fConst95 * fRec489[1]);
			float fTemp244 = (fConst97 * fRec492[1]);
			float fTemp245 = (fConst98 * fRec495[1]);
			float fTemp246 = (fConst100 * (((0.0186600834f * fTemp20) + (((((0.00549051911f * fTemp11) + (0.00273694703f * fTemp17)) + (0.00373478932f * fTemp13)) + (0.0170119219f * fTemp15)) + (0.0153120505f * fTemp19))) - (((0.0233702082f * fTemp16) + (((0.00828494597f * fTemp12) + (0.00177500106f * fTemp14)) + (0.00776829943f * fTemp18))) + (0.0308452435f * fTemp21))));
			float fTemp247 = (fConst101 * fRec498[1]);
			float fTemp248 = (fConst102 * fRec501[1]);
			fRec503[0] = (fTemp246 + (fTemp247 + (fRec503[1] + fTemp248)));
			fRec501[0] = fRec503[0];
			float fRec502 = ((fTemp248 + fTemp247) + fTemp246);
			fRec500[0] = (fRec501[0] + fRec500[1]);
			fRec498[0] = fRec500[0];
			float fRec499 = fRec502;
			fRec497[0] = (fTemp244 + (fTemp245 + (fRec499 + fRec497[1])));
			fRec495[0] = fRec497[0];
			float fRec496 = (fTemp244 + (fRec499 + fTemp245));
			fRec494[0] = (fRec495[0] + fRec494[1]);
			fRec492[0] = fRec494[0];
			float fRec493 = fRec496;
			fRec491[0] = (fTemp243 + (fRec493 + fRec491[1]));
			fRec489[0] = fRec491[0];
			float fRec490 = (fRec493 + fTemp243);
			float fTemp249 = (fConst104 * fRec504[1]);
			float fTemp250 = (fConst105 * fRec507[1]);
			float fTemp251 = (fConst107 * ((((0.0267242976f * fTemp33) + ((0.0075121671f * fTemp31) + (0.0111189736f * fTemp29))) + (0.0377496704f * fTemp35)) - ((0.0225267373f * fTemp34) + ((((0.00636390084f * fTemp27) + (0.000970638008f * fTemp28)) + (0.00247365818f * fTemp32)) + (0.0205242913f * fTemp30)))));
			float fTemp252 = (fConst108 * fRec510[1]);
			float fTemp253 = (fConst109 * fRec513[1]);
			fRec515[0] = (fTemp251 + (fTemp252 + (fRec515[1] + fTemp253)));
			fRec513[0] = fRec515[0];
			float fRec514 = ((fTemp253 + fTemp252) + fTemp251);
			fRec512[0] = (fRec513[0] + fRec512[1]);
			fRec510[0] = fRec512[0];
			float fRec511 = fRec514;
			fRec509[0] = (fTemp249 + (fTemp250 + (fRec511 + fRec509[1])));
			fRec507[0] = fRec509[0];
			float fRec508 = (fTemp249 + (fRec511 + fTemp250));
			fRec506[0] = (fRec507[0] + fRec506[1]);
			fRec504[0] = fRec506[0];
			float fRec505 = fRec508;
			float fTemp254 = (fConst111 * fRec516[1]);
			float fTemp255 = (fConst113 * (((0.02394839f * fTemp43) + ((0.00626526307f * fTemp44) + (0.0272907466f * fTemp42))) - ((((0.00538331689f * fTemp40) + (0.000260878587f * fTemp45)) + (0.0189852081f * fTemp41)) + (0.0437391959f * fTemp46))));
			float fTemp256 = (fConst114 * fRec519[1]);
			float fTemp257 = (fConst115 * fRec522[1]);
			fRec524[0] = (fTemp255 + (fTemp256 + (fRec524[1] + fTemp257)));
			fRec522[0] = fRec524[0];
			float fRec523 = ((fTemp257 + fTemp256) + fTemp255);
			fRec521[0] = (fRec522[0] + fRec521[1]);
			fRec519[0] = fRec521[0];
			float fRec520 = fRec523;
			fRec518[0] = (fTemp254 + (fRec520 + fRec518[1]));
			fRec516[0] = fRec518[0];
			float fRec517 = (fRec520 + fTemp254);
			float fTemp258 = (fConst117 * (((0.00272243051f * fTemp52) + (0.0110227168f * fTemp50)) - (0.0479010195f * fTemp51)));
			float fTemp259 = (fConst118 * fRec525[1]);
			fRec527[0] = (fTemp258 + (fRec527[1] + fTemp259));
			fRec525[0] = fRec527[0];
			float fRec526 = (fTemp259 + fTemp258);
			float fTemp260 = (fConst120 * (((0.00250748661f * fTemp59) + (0.0476305895f * fTemp58)) - (((0.00501013687f * fTemp55) + (0.0248913877f * fTemp56)) + (0.0218859967f * fTemp57))));
			float fTemp261 = (fConst121 * fRec528[1]);
			float fTemp262 = (fConst122 * fRec531[1]);
			fRec533[0] = (fTemp260 + (fTemp261 + (fRec533[1] + fTemp262)));
			fRec531[0] = fRec533[0];
			float fRec532 = ((fTemp262 + fTemp261) + fTemp260);
			fRec530[0] = (fRec531[0] + fRec530[1]);
			fRec528[0] = fRec530[0];
			float fRec529 = fRec532;
			fVec10[(IOTA & 1023)] = ((0.029665513f * fTemp7) + (fRec490 + (fRec505 + (fRec517 + (fRec526 + fRec529)))));
			output10[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec10[((IOTA - iConst123) & 1023)])));
			float fTemp263 = (fConst142 * fRec534[1]);
			float fTemp264 = (fConst144 * fRec537[1]);
			float fTemp265 = (fConst145 * fRec540[1]);
			float fTemp266 = (fConst147 * ((((((0.0080315778f * fTemp12) + (0.0240798816f * fTemp17)) + (0.0102567486f * fTemp15)) + (0.00273616984f * fTemp19)) + (0.0333046988f * fTemp21)) - (((0.00433081761f * fTemp16) + ((((0.0129251275f * fTemp11) + (0.0149607146f * fTemp13)) + (0.00755689153f * fTemp14)) + (0.016629301f * fTemp18))) + (0.014194631f * fTemp20))));
			float fTemp267 = (fConst148 * fRec543[1]);
			float fTemp268 = (fConst149 * fRec546[1]);
			fRec548[0] = (fTemp266 + (fTemp267 + (fRec548[1] + fTemp268)));
			fRec546[0] = fRec548[0];
			float fRec547 = ((fTemp268 + fTemp267) + fTemp266);
			fRec545[0] = (fRec546[0] + fRec545[1]);
			fRec543[0] = fRec545[0];
			float fRec544 = fRec547;
			fRec542[0] = (fTemp264 + (fTemp265 + (fRec544 + fRec542[1])));
			fRec540[0] = fRec542[0];
			float fRec541 = (fTemp264 + (fRec544 + fTemp265));
			fRec539[0] = (fRec540[0] + fRec539[1]);
			fRec537[0] = fRec539[0];
			float fRec538 = fRec541;
			fRec536[0] = (fTemp263 + (fRec538 + fRec536[1]));
			fRec534[0] = fRec536[0];
			float fRec535 = (fRec538 + fTemp263);
			float fTemp269 = (fConst125 * fRec549[1]);
			float fTemp270 = (fConst126 * fRec552[1]);
			float fTemp271 = (fConst128 * ((((0.0146306194f * fTemp33) + (((0.0382039882f * fTemp27) + (0.0112971468f * fTemp32)) + (0.0201456752f * fTemp29))) + (0.00749935955f * fTemp34)) - ((((0.0167528167f * fTemp31) + (0.0277264602f * fTemp28)) + (0.0171313267f * fTemp30)) + (0.0293669961f * fTemp35))));
			float fTemp272 = (fConst129 * fRec555[1]);
			float fTemp273 = (fConst130 * fRec558[1]);
			fRec560[0] = (fTemp271 + (fTemp272 + (fRec560[1] + fTemp273)));
			fRec558[0] = fRec560[0];
			float fRec559 = ((fTemp273 + fTemp272) + fTemp271);
			fRec557[0] = (fRec558[0] + fRec557[1]);
			fRec555[0] = fRec557[0];
			float fRec556 = fRec559;
			fRec554[0] = (fTemp269 + (fTemp270 + (fRec556 + fRec554[1])));
			fRec552[0] = fRec554[0];
			float fRec553 = (fTemp269 + (fRec556 + fTemp270));
			fRec551[0] = (fRec552[0] + fRec551[1]);
			fRec549[0] = fRec551[0];
			float fRec550 = fRec553;
			float fTemp274 = (fConst132 * fRec561[1]);
			float fTemp275 = (fConst134 * (((0.00623383f * fTemp46) + ((0.00429912098f * fTemp43) + (((0.0181888249f * fTemp40) + (0.0211702045f * fTemp45)) + (0.036154028f * fTemp42)))) - ((0.0584637783f * fTemp44) + (0.013727935f * fTemp41))));
			float fTemp276 = (fConst135 * fRec564[1]);
			float fTemp277 = (fConst136 * fRec567[1]);
			fRec569[0] = (fTemp275 + (fTemp276 + (fRec569[1] + fTemp277)));
			fRec567[0] = fRec569[0];
			float fRec568 = ((fTemp277 + fTemp276) + fTemp275);
			fRec566[0] = (fRec567[0] + fRec566[1]);
			fRec564[0] = fRec566[0];
			float fRec565 = fRec568;
			fRec563[0] = (fTemp274 + (fRec565 + fRec563[1]));
			fRec561[0] = fRec563[0];
			float fRec562 = (fRec565 + fTemp274);
			float fTemp278 = (fConst151 * ((0.00846940465f * fTemp50) - ((0.0355539881f * fTemp52) + (0.0561628602f * fTemp51))));
			float fTemp279 = (fConst152 * fRec570[1]);
			fRec572[0] = (fTemp278 + (fRec572[1] + fTemp279));
			fRec570[0] = fRec572[0];
			float fRec571 = (fTemp279 + fTemp278);
			float fTemp280 = (fConst138 * (((0.0591501631f * fTemp55) + (0.0283936244f * fTemp58)) - (((0.0104484083f * fTemp59) + (0.0363615789f * fTemp56)) + (0.0130950417f * fTemp57))));
			float fTemp281 = (fConst139 * fRec573[1]);
			float fTemp282 = (fConst140 * fRec576[1]);
			fRec578[0] = (fTemp280 + (fTemp281 + (fRec578[1] + fTemp282)));
			fRec576[0] = fRec578[0];
			float fRec577 = ((fTemp282 + fTemp281) + fTemp280);
			fRec575[0] = (fRec576[0] + fRec575[1]);
			fRec573[0] = fRec575[0];
			float fRec574 = fRec577;
			fVec11[(IOTA & 1023)] = ((0.0410089977f * fTemp7) + (fRec535 + (fRec550 + (fRec562 + (fRec571 + fRec574)))));
			output11[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec11[((IOTA - iConst153) & 1023)])));
			float fTemp283 = (fConst142 * fRec579[1]);
			float fTemp284 = (fConst144 * fRec582[1]);
			float fTemp285 = (fConst145 * fRec585[1]);
			float fTemp286 = (fConst147 * ((((0.0205825996f * fTemp11) + (0.00760876201f * fTemp15)) + (0.00466179755f * fTemp16)) - ((0.0271820147f * fTemp21) + ((0.00542996405f * fTemp20) + ((((((0.00853115227f * fTemp12) + (0.00456699077f * fTemp17)) + (0.00969215576f * fTemp13)) + (0.0186727662f * fTemp14)) + (0.009368442f * fTemp18)) + (0.0230235159f * fTemp19))))));
			float fTemp287 = (fConst148 * fRec588[1]);
			float fTemp288 = (fConst149 * fRec591[1]);
			fRec593[0] = (fTemp286 + (fTemp287 + (fRec593[1] + fTemp288)));
			fRec591[0] = fRec593[0];
			float fRec592 = ((fTemp288 + fTemp287) + fTemp286);
			fRec590[0] = (fRec591[0] + fRec590[1]);
			fRec588[0] = fRec590[0];
			float fRec589 = fRec592;
			fRec587[0] = (fTemp284 + (fTemp285 + (fRec589 + fRec587[1])));
			fRec585[0] = fRec587[0];
			float fRec586 = (fTemp284 + (fRec589 + fTemp285));
			fRec584[0] = (fRec585[0] + fRec584[1]);
			fRec582[0] = fRec584[0];
			float fRec583 = fRec586;
			fRec581[0] = (fTemp283 + (fRec583 + fRec581[1]));
			fRec579[0] = fRec581[0];
			float fRec580 = (fRec583 + fTemp283);
			float fTemp289 = (fConst125 * fRec594[1]);
			float fTemp290 = (fConst126 * fRec597[1]);
			float fTemp291 = (fConst128 * (((0.0100131826f * fTemp34) + ((0.0175028779f * fTemp30) + ((0.00562107656f * fTemp33) + ((0.00914157555f * fTemp32) + (0.0188116413f * fTemp29))))) - ((((0.0390624069f * fTemp27) + (0.000321889587f * fTemp31)) + (0.0225733947f * fTemp28)) + (0.00839614682f * fTemp35))));
			float fTemp292 = (fConst129 * fRec600[1]);
			float fTemp293 = (fConst130 * fRec603[1]);
			fRec605[0] = (fTemp291 + (fTemp292 + (fRec605[1] + fTemp293)));
			fRec603[0] = fRec605[0];
			float fRec604 = ((fTemp293 + fTemp292) + fTemp291);
			fRec602[0] = (fRec603[0] + fRec602[1]);
			fRec600[0] = fRec602[0];
			float fRec601 = fRec604;
			fRec599[0] = (fTemp289 + (fTemp290 + (fRec601 + fRec599[1])));
			fRec597[0] = fRec599[0];
			float fRec598 = (fTemp289 + (fRec601 + fTemp290));
			fRec596[0] = (fRec597[0] + fRec596[1]);
			fRec594[0] = fRec596[0];
			float fRec595 = fRec598;
			float fTemp294 = (fConst132 * fRec606[1]);
			float fTemp295 = (fConst134 * ((((((0.010089837f * fTemp44) + (0.00807257369f * fTemp40)) + (0.0296988003f * fTemp45)) + (0.0144589525f * fTemp42)) + (0.0430146195f * fTemp46)) - ((0.00694228988f * fTemp41) + (0.00445707561f * fTemp43))));
			float fTemp296 = (fConst135 * fRec609[1]);
			float fTemp297 = (fConst136 * fRec612[1]);
			fRec614[0] = (fTemp295 + (fTemp296 + (fRec614[1] + fTemp297)));
			fRec612[0] = fRec614[0];
			float fRec613 = ((fTemp297 + fTemp296) + fTemp295);
			fRec611[0] = (fRec612[0] + fRec611[1]);
			fRec609[0] = fRec611[0];
			float fRec610 = fRec613;
			fRec608[0] = (fTemp294 + (fRec610 + fRec608[1]));
			fRec606[0] = fRec608[0];
			float fRec607 = (fRec610 + fTemp294);
			float fTemp298 = (fConst151 * ((0.0036629024f * fTemp50) - ((0.040098194f * fTemp52) + (0.0192500781f * fTemp51))));
			float fTemp299 = (fConst152 * fRec615[1]);
			fRec617[0] = (fTemp298 + (fRec617[1] + fTemp299));
			fRec615[0] = fRec617[0];
			float fRec616 = (fTemp299 + fTemp298);
			float fTemp300 = (fConst138 * ((0.0358814336f * fTemp55) - ((((0.00659731729f * fTemp59) + (0.0261035636f * fTemp56)) + (0.00387390563f * fTemp57)) + (0.0287762228f * fTemp58))));
			float fTemp301 = (fConst139 * fRec618[1]);
			float fTemp302 = (fConst140 * fRec621[1]);
			fRec623[0] = (fTemp300 + (fTemp301 + (fRec623[1] + fTemp302)));
			fRec621[0] = fRec623[0];
			float fRec622 = ((fTemp302 + fTemp301) + fTemp300);
			fRec620[0] = (fRec621[0] + fRec620[1]);
			fRec618[0] = fRec620[0];
			float fRec619 = fRec622;
			fVec12[(IOTA & 1023)] = ((0.0267459098f * fTemp7) + (fRec580 + (fRec595 + (fRec607 + (fRec616 + fRec619)))));
			output12[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec12[((IOTA - iConst153) & 1023)])));
			float fTemp303 = (fConst95 * fRec624[1]);
			float fTemp304 = (fConst97 * fRec627[1]);
			float fTemp305 = (fConst98 * fRec630[1]);
			float fTemp306 = (fConst100 * (((0.00545160519f * fTemp21) + (((((0.00828643702f * fTemp12) + (0.00372886821f * fTemp13)) + (0.0169913433f * fTemp15)) + (0.0233549215f * fTemp16)) + (0.0186516922f * fTemp20))) - (((((0.0308557153f * fTemp11) + (0.0153234908f * fTemp17)) + (0.00776693365f * fTemp14)) + (0.00178584177f * fTemp18)) + (0.00276571722f * fTemp19))));
			float fTemp307 = (fConst101 * fRec633[1]);
			float fTemp308 = (fConst102 * fRec636[1]);
			fRec638[0] = (fTemp306 + (fTemp307 + (fRec638[1] + fTemp308)));
			fRec636[0] = fRec638[0];
			float fRec637 = ((fTemp308 + fTemp307) + fTemp306);
			fRec635[0] = (fRec636[0] + fRec635[1]);
			fRec633[0] = fRec635[0];
			float fRec634 = fRec637;
			fRec632[0] = (fTemp304 + (fTemp305 + (fRec634 + fRec632[1])));
			fRec630[0] = fRec632[0];
			float fRec631 = (fTemp304 + (fRec634 + fTemp305));
			fRec629[0] = (fRec630[0] + fRec629[1]);
			fRec627[0] = fRec629[0];
			float fRec628 = fRec631;
			fRec626[0] = (fTemp303 + (fRec628 + fRec626[1]));
			fRec624[0] = fRec626[0];
			float fRec625 = (fRec628 + fTemp303);
			float fTemp309 = (fConst104 * fRec639[1]);
			float fTemp310 = (fConst105 * fRec642[1]);
			float fTemp311 = (fConst107 * (((((((0.00632829405f * fTemp27) + (0.0225127544f * fTemp31)) + (0.0267035142f * fTemp32)) + (0.011113028f * fTemp29)) + (0.0205247216f * fTemp30)) + (0.0377550609f * fTemp35)) - (((0.000994727248f * fTemp28) + (0.00247249682f * fTemp33)) + (0.00751692522f * fTemp34))));
			float fTemp312 = (fConst108 * fRec645[1]);
			float fTemp313 = (fConst109 * fRec648[1]);
			fRec650[0] = (fTemp311 + (fTemp312 + (fRec650[1] + fTemp313)));
			fRec648[0] = fRec650[0];
			float fRec649 = ((fTemp313 + fTemp312) + fTemp311);
			fRec647[0] = (fRec648[0] + fRec647[1]);
			fRec645[0] = fRec647[0];
			float fRec646 = fRec649;
			fRec644[0] = (fTemp309 + (fTemp310 + (fRec646 + fRec644[1])));
			fRec642[0] = fRec644[0];
			float fRec643 = (fTemp309 + (fRec646 + fTemp310));
			fRec641[0] = (fRec642[0] + fRec641[1]);
			fRec639[0] = fRec641[0];
			float fRec640 = fRec643;
			float fTemp314 = (fConst111 * fRec651[1]);
			float fTemp315 = (fConst113 * (((0.0437380597f * fTemp44) + (0.0272824895f * fTemp45)) - ((0.00623743841f * fTemp46) + ((((0.00538961869f * fTemp40) + (0.0189735387f * fTemp41)) + (0.000248278608f * fTemp42)) + (0.0239343904f * fTemp43)))));
			float fTemp316 = (fConst114 * fRec654[1]);
			float fTemp317 = (fConst115 * fRec657[1]);
			fRec659[0] = (fTemp315 + (fTemp316 + (fRec659[1] + fTemp317)));
			fRec657[0] = fRec659[0];
			float fRec658 = ((fTemp317 + fTemp316) + fTemp315);
			fRec656[0] = (fRec657[0] + fRec656[1]);
			fRec654[0] = fRec656[0];
			float fRec655 = fRec658;
			fRec653[0] = (fTemp314 + (fRec655 + fRec653[1]));
			fRec651[0] = fRec653[0];
			float fRec652 = (fRec655 + fTemp314);
			float fTemp318 = (fConst117 * (((0.0110191144f * fTemp50) + (0.00271522603f * fTemp51)) - (0.0478933044f * fTemp52)));
			float fTemp319 = (fConst118 * fRec660[1]);
			fRec662[0] = (fTemp318 + (fRec662[1] + fTemp319));
			fRec660[0] = fRec662[0];
			float fRec661 = (fTemp319 + fTemp318);
			float fTemp320 = (fConst120 * ((0.0025117835f * fTemp57) - ((((0.00499273418f * fTemp55) + (0.0218761284f * fTemp59)) + (0.0248842426f * fTemp56)) + (0.0476244874f * fTemp58))));
			float fTemp321 = (fConst121 * fRec663[1]);
			float fTemp322 = (fConst122 * fRec666[1]);
			fRec668[0] = (fTemp320 + (fTemp321 + (fRec668[1] + fTemp322)));
			fRec666[0] = fRec668[0];
			float fRec667 = ((fTemp322 + fTemp321) + fTemp320);
			fRec665[0] = (fRec666[0] + fRec665[1]);
			fRec663[0] = fRec665[0];
			float fRec664 = fRec667;
			fVec13[(IOTA & 1023)] = ((0.0296611208f * fTemp7) + (fRec625 + (fRec640 + (fRec652 + (fRec661 + fRec664)))));
			output13[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec13[((IOTA - iConst123) & 1023)])));
			float fTemp323 = (fConst142 * fRec669[1]);
			float fTemp324 = (fConst144 * fRec672[1]);
			float fTemp325 = (fConst145 * fRec675[1]);
			float fTemp326 = (fConst147 * ((((0.0230342075f * fTemp19) + ((0.00215170346f * fTemp16) + ((0.00891430397f * fTemp18) + ((((0.0236739647f * fTemp11) + (0.00534388004f * fTemp12)) + (0.0111709274f * fTemp13)) + (0.00763021363f * fTemp15))))) + (0.0218403898f * fTemp21)) - (((0.004955146f * fTemp17) + (0.0189377908f * fTemp14)) + (0.0109606646f * fTemp20))));
			float fTemp327 = (fConst148 * fRec678[1]);
			float fTemp328 = (fConst149 * fRec681[1]);
			fRec683[0] = (fTemp326 + (fTemp327 + (fRec683[1] + fTemp328)));
			fRec681[0] = fRec683[0];
			float fRec682 = ((fTemp328 + fTemp327) + fTemp326);
			fRec680[0] = (fRec681[0] + fRec680[1]);
			fRec678[0] = fRec680[0];
			float fRec679 = fRec682;
			fRec677[0] = (fTemp324 + (fTemp325 + (fRec679 + fRec677[1])));
			fRec675[0] = fRec677[0];
			float fRec676 = (fTemp324 + (fRec679 + fTemp325));
			fRec674[0] = (fRec675[0] + fRec674[1]);
			fRec672[0] = fRec674[0];
			float fRec673 = fRec676;
			fRec671[0] = (fTemp323 + (fRec673 + fRec671[1]));
			fRec669[0] = fRec671[0];
			float fRec670 = (fRec673 + fTemp323);
			float fTemp329 = (fConst125 * fRec684[1]);
			float fTemp330 = (fConst126 * fRec687[1]);
			float fTemp331 = (fConst128 * ((((((0.036236804f * fTemp27) + (0.0227813944f * fTemp28)) + (0.00838163868f * fTemp32)) + (0.0188177116f * fTemp29)) + (0.0171661526f * fTemp30)) - ((0.0133097423f * fTemp35) + (((0.00510163652f * fTemp31) + (0.00698627252f * fTemp33)) + (0.0100233201f * fTemp34)))));
			float fTemp332 = (fConst129 * fRec690[1]);
			float fTemp333 = (fConst130 * fRec693[1]);
			fRec695[0] = (fTemp331 + (fTemp332 + (fRec695[1] + fTemp333)));
			fRec693[0] = fRec695[0];
			float fRec694 = ((fTemp333 + fTemp332) + fTemp331);
			fRec692[0] = (fRec693[0] + fRec692[1]);
			fRec690[0] = fRec692[0];
			float fRec691 = fRec694;
			fRec689[0] = (fTemp329 + (fTemp330 + (fRec691 + fRec689[1])));
			fRec687[0] = fRec689[0];
			float fRec688 = (fTemp329 + (fRec691 + fTemp330));
			fRec686[0] = (fRec687[0] + fRec686[1]);
			fRec684[0] = fRec686[0];
			float fRec685 = fRec688;
			float fTemp334 = (fConst132 * fRec696[1]);
			float fTemp335 = (fConst134 * (((0.00540215429f * fTemp44) + (0.0294207837f * fTemp45)) - ((0.043026831f * fTemp46) + ((((0.00956894178f * fTemp40) + (0.0069561433f * fTemp41)) + (0.0149665643f * fTemp42)) + (0.00188986782f * fTemp43)))));
			float fTemp336 = (fConst135 * fRec699[1]);
			float fTemp337 = (fConst136 * fRec702[1]);
			fRec704[0] = (fTemp335 + (fTemp336 + (fRec704[1] + fTemp337)));
			fRec702[0] = fRec704[0];
			float fRec703 = ((fTemp337 + fTemp336) + fTemp335);
			fRec701[0] = (fRec702[0] + fRec701[1]);
			fRec699[0] = fRec701[0];
			float fRec700 = fRec703;
			fRec698[0] = (fTemp334 + (fRec700 + fRec698[1]));
			fRec696[0] = fRec698[0];
			float fRec697 = (fRec700 + fTemp334);
			float fTemp338 = (fConst151 * (((0.0036679192f * fTemp50) + (0.0206729658f * fTemp51)) - (0.0392920151f * fTemp52)));
			float fTemp339 = (fConst152 * fRec705[1]);
			fRec707[0] = (fTemp338 + (fRec707[1] + fTemp339));
			fRec705[0] = fRec707[0];
			float fRec706 = (fTemp339 + fTemp338);
			float fTemp340 = (fConst138 * ((0.00496469717f * fTemp57) - ((((0.0375401974f * fTemp55) + (0.00598042877f * fTemp59)) + (0.0261131581f * fTemp56)) + (0.0259301588f * fTemp58))));
			float fTemp341 = (fConst139 * fRec708[1]);
			float fTemp342 = (fConst140 * fRec711[1]);
			fRec713[0] = (fTemp340 + (fTemp341 + (fRec713[1] + fTemp342)));
			fRec711[0] = fRec713[0];
			float fRec712 = ((fTemp342 + fTemp341) + fTemp340);
			fRec710[0] = (fRec711[0] + fRec710[1]);
			fRec708[0] = fRec710[0];
			float fRec709 = fRec712;
			fVec14[(IOTA & 1023)] = ((0.026753651f * fTemp7) + (fRec670 + (fRec685 + (fRec697 + (fRec706 + fRec709)))));
			output14[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec14[((IOTA - iConst153) & 1023)])));
			float fTemp343 = (fConst142 * fRec714[1]);
			float fTemp344 = (fConst144 * fRec717[1]);
			float fTemp345 = (fConst145 * fRec720[1]);
			float fTemp346 = (fConst147 * (((0.0226927008f * fTemp18) + (((0.0274104141f * fTemp17) + (0.0064608003f * fTemp13)) + (0.00424854085f * fTemp15))) - ((0.0306959245f * fTemp21) + ((0.00666117528f * fTemp20) + ((0.0001973529f * fTemp19) + ((((0.014156362f * fTemp11) + (0.00516983354f * fTemp12)) + (0.0127961822f * fTemp14)) + (0.00205374835f * fTemp16)))))));
			float fTemp347 = (fConst148 * fRec723[1]);
			float fTemp348 = (fConst149 * fRec726[1]);
			fRec728[0] = (fTemp346 + (fTemp347 + (fRec728[1] + fTemp348)));
			fRec726[0] = fRec728[0];
			float fRec727 = ((fTemp348 + fTemp347) + fTemp346);
			fRec725[0] = (fRec726[0] + fRec725[1]);
			fRec723[0] = fRec725[0];
			float fRec724 = fRec727;
			fRec722[0] = (fTemp344 + (fTemp345 + (fRec724 + fRec722[1])));
			fRec720[0] = fRec722[0];
			float fRec721 = (fTemp344 + (fRec724 + fTemp345));
			fRec719[0] = (fRec720[0] + fRec719[1]);
			fRec717[0] = fRec719[0];
			float fRec718 = fRec721;
			fRec716[0] = (fTemp343 + (fRec718 + fRec716[1]));
			fRec714[0] = fRec716[0];
			float fRec715 = (fRec718 + fTemp343);
			float fTemp349 = (fConst125 * fRec729[1]);
			float fTemp350 = (fConst126 * fRec732[1]);
			float fTemp351 = (fConst128 * ((((0.0298299026f * fTemp28) + (0.00395053998f * fTemp32)) + (0.0233223774f * fTemp29)) - ((0.0239861924f * fTemp35) + ((0.00238312478f * fTemp34) + ((0.0169941448f * fTemp30) + (((0.0358645692f * fTemp27) + (0.00714203669f * fTemp31)) + (0.00528561603f * fTemp33)))))));
			float fTemp352 = (fConst129 * fRec735[1]);
			float fTemp353 = (fConst130 * fRec738[1]);
			fRec740[0] = (fTemp351 + (fTemp352 + (fRec740[1] + fTemp353)));
			fRec738[0] = fRec740[0];
			float fRec739 = ((fTemp353 + fTemp352) + fTemp351);
			fRec737[0] = (fRec738[0] + fRec737[1]);
			fRec735[0] = fRec737[0];
			float fRec736 = fRec739;
			fRec734[0] = (fTemp349 + (fTemp350 + (fRec736 + fRec734[1])));
			fRec732[0] = fRec734[0];
			float fRec733 = (fTemp349 + (fRec736 + fTemp350));
			fRec731[0] = (fRec732[0] + fRec731[1]);
			fRec729[0] = fRec731[0];
			float fRec730 = fRec733;
			float fTemp354 = (fConst132 * fRec741[1]);
			float fTemp355 = (fConst134 * (((0.0203805249f * fTemp45) + (0.00182607665f * fTemp43)) - (((((0.0503935479f * fTemp44) + (0.0061250506f * fTemp40)) + (0.00433409773f * fTemp41)) + (0.034719348f * fTemp42)) + (0.0023518533f * fTemp46))));
			float fTemp356 = (fConst135 * fRec744[1]);
			float fTemp357 = (fConst136 * fRec747[1]);
			fRec749[0] = (fTemp355 + (fTemp356 + (fRec749[1] + fTemp357)));
			fRec747[0] = fRec749[0];
			float fRec748 = ((fTemp357 + fTemp356) + fTemp355);
			fRec746[0] = (fRec747[0] + fRec746[1]);
			fRec744[0] = fRec746[0];
			float fRec745 = fRec748;
			fRec743[0] = (fTemp354 + (fRec745 + fRec743[1]));
			fRec741[0] = fRec743[0];
			float fRec742 = (fRec745 + fTemp354);
			float fTemp358 = (fConst151 * (((0.00239751418f * fTemp50) + (0.0460310802f * fTemp51)) - (0.0275247861f * fTemp52)));
			float fTemp359 = (fConst152 * fRec750[1]);
			fRec752[0] = (fTemp358 + (fRec752[1] + fTemp359));
			fRec750[0] = fRec752[0];
			float fRec751 = (fTemp359 + fTemp358);
			float fTemp360 = (fConst138 * (((0.00402801251f * fTemp57) + (0.0257554352f * fTemp58)) - (((0.0479180031f * fTemp55) + (0.00304666511f * fTemp59)) + (0.031846758f * fTemp56))));
			float fTemp361 = (fConst139 * fRec753[1]);
			float fTemp362 = (fConst140 * fRec756[1]);
			fRec758[0] = (fTemp360 + (fTemp361 + (fRec758[1] + fTemp362)));
			fRec756[0] = fRec758[0];
			float fRec757 = ((fTemp362 + fTemp361) + fTemp360);
			fRec755[0] = (fRec756[0] + fRec755[1]);
			fRec753[0] = fRec755[0];
			float fRec754 = fRec757;
			fVec15[(IOTA & 1023)] = (fRec715 + (fRec730 + ((0.0324291773f * fTemp7) + (fRec742 + (fRec751 + fRec754)))));
			output15[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec15[((IOTA - iConst153) & 1023)])));
			float fTemp363 = (fConst95 * fRec759[1]);
			float fTemp364 = (fConst97 * fRec762[1]);
			float fTemp365 = (fConst98 * fRec765[1]);
			float fTemp366 = (fConst100 * (((0.0308521893f * fTemp21) + (((0.00777389714f * fTemp18) + ((0.0170025453f * fTemp15) + ((0.00372300576f * fTemp13) + (0.00177117402f * fTemp14)))) + (0.0186599474f * fTemp20))) - ((0.0153168961f * fTemp19) + ((((0.00546546653f * fTemp11) + (0.00825924147f * fTemp12)) + (0.00273303571f * fTemp17)) + (0.023363037f * fTemp16)))));
			float fTemp367 = (fConst101 * fRec768[1]);
			float fTemp368 = (fConst102 * fRec771[1]);
			fRec773[0] = (fTemp366 + (fTemp367 + (fRec773[1] + fTemp368)));
			fRec771[0] = fRec773[0];
			float fRec772 = ((fTemp368 + fTemp367) + fTemp366);
			fRec770[0] = (fRec771[0] + fRec770[1]);
			fRec768[0] = fRec770[0];
			float fRec769 = fRec772;
			fRec767[0] = (fTemp364 + (fTemp365 + (fRec769 + fRec767[1])));
			fRec765[0] = fRec767[0];
			float fRec766 = (fTemp364 + (fRec769 + fTemp365));
			fRec764[0] = (fRec765[0] + fRec764[1]);
			fRec762[0] = fRec764[0];
			float fRec763 = fRec766;
			fRec761[0] = (fTemp363 + (fRec763 + fRec761[1]));
			fRec759[0] = fRec761[0];
			float fRec760 = (fRec763 + fTemp363);
			float fTemp369 = (fConst104 * fRec774[1]);
			float fTemp370 = (fConst105 * fRec777[1]);
			float fTemp371 = (fConst107 * (((0.0377592891f * fTemp35) + (((0.0024657764f * fTemp32) + (0.0111223757f * fTemp29)) + (0.0225220621f * fTemp34))) - ((0.020530086f * fTemp30) + ((((0.00633708993f * fTemp27) + (0.00748936273f * fTemp31)) + (0.000972020498f * fTemp28)) + (0.0267130192f * fTemp33)))));
			float fTemp372 = (fConst108 * fRec780[1]);
			float fTemp373 = (fConst109 * fRec783[1]);
			fRec785[0] = (fTemp371 + (fTemp372 + (fRec785[1] + fTemp373)));
			fRec783[0] = fRec785[0];
			float fRec784 = ((fTemp373 + fTemp372) + fTemp371);
			fRec782[0] = (fRec783[0] + fRec782[1]);
			fRec780[0] = fRec782[0];
			float fRec781 = fRec784;
			fRec779[0] = (fTemp369 + (fTemp370 + (fRec781 + fRec779[1])));
			fRec777[0] = fRec779[0];
			float fRec778 = (fTemp369 + (fRec781 + fTemp370));
			fRec776[0] = (fRec777[0] + fRec776[1]);
			fRec774[0] = fRec776[0];
			float fRec775 = fRec778;
			float fTemp374 = (fConst111 * fRec786[1]);
			float fTemp375 = (fConst113 * (((0.0437487811f * fTemp46) + ((0.0002575533f * fTemp45) + (0.023940945f * fTemp43))) - ((((0.00624047266f * fTemp44) + (0.00536769675f * fTemp40)) + (0.0189775303f * fTemp41)) + (0.027295744f * fTemp42))));
			float fTemp376 = (fConst114 * fRec789[1]);
			float fTemp377 = (fConst115 * fRec792[1]);
			fRec794[0] = (fTemp375 + (fTemp376 + (fRec794[1] + fTemp377)));
			fRec792[0] = fRec794[0];
			float fRec793 = ((fTemp377 + fTemp376) + fTemp375);
			fRec791[0] = (fRec792[0] + fRec791[1]);
			fRec789[0] = fRec791[0];
			float fRec790 = fRec793;
			fRec788[0] = (fTemp374 + (fRec790 + fRec788[1]));
			fRec786[0] = fRec788[0];
			float fRec787 = (fRec790 + fTemp374);
			float fTemp378 = (fConst120 * (((0.0218786057f * fTemp57) + (0.0476387031f * fTemp58)) - (((0.0049914103f * fTemp55) + (0.00250061741f * fTemp59)) + (0.0248947348f * fTemp56))));
			float fTemp379 = (fConst121 * fRec795[1]);
			float fTemp380 = (fConst122 * fRec798[1]);
			fRec800[0] = (fTemp378 + (fTemp379 + (fRec800[1] + fTemp380)));
			fRec798[0] = fRec800[0];
			float fRec799 = ((fTemp380 + fTemp379) + fTemp378);
			fRec797[0] = (fRec798[0] + fRec797[1]);
			fRec795[0] = fRec797[0];
			float fRec796 = fRec799;
			float fTemp381 = (fConst117 * (((0.0110191759f * fTemp50) + (0.0479075424f * fTemp51)) - (0.00271278713f * fTemp52)));
			float fTemp382 = (fConst118 * fRec801[1]);
			fRec803[0] = (fTemp381 + (fRec803[1] + fTemp382));
			fRec801[0] = fRec803[0];
			float fRec802 = (fTemp382 + fTemp381);
			fVec16[(IOTA & 1023)] = (fRec760 + (fRec775 + (fRec787 + (fRec796 + (fRec802 + (0.0296692289f * fTemp7))))));
			output16[i] = FAUSTFLOAT((0.825255752f * (fRec0[0] * fVec16[((IOTA - iConst123) & 1023)])));
			float fTemp383 = (fConst132 * fRec804[1]);
			float fTemp384 = (fConst134 * ((((0.0584710091f * fTemp44) + (0.0181694906f * fTemp40)) + (0.00428620027f * fTemp43)) - ((((0.0211695898f * fTemp45) + (0.0137110623f * fTemp41)) + (0.0361602344f * fTemp42)) + (0.00623232685f * fTemp46))));
			float fTemp385 = (fConst135 * fRec807[1]);
			float fTemp386 = (fConst136 * fRec810[1]);
			fRec812[0] = (fTemp384 + (fTemp385 + (fRec812[1] + fTemp386)));
			fRec810[0] = fRec812[0];
			float fRec811 = ((fTemp386 + fTemp385) + fTemp384);
			fRec809[0] = (fRec810[0] + fRec809[1]);
			fRec807[0] = fRec809[0];
			float fRec808 = fRec811;
			fRec806[0] = (fTemp383 + (fRec808 + fRec806[1]));
			fRec804[0] = fRec806[0];
			float fRec805 = (fRec808 + fTemp383);
			float fTemp387 = (fConst138 * (((((0.05915609f * fTemp55) + (0.0104403626f * fTemp59)) + (0.013077273f * fTemp57)) + (0.028395867f * fTemp58)) - (0.0363653377f * fTemp56)));
			float fTemp388 = (fConst139 * fRec813[1]);
			float fTemp389 = (fConst140 * fRec816[1]);
			fRec818[0] = (fTemp387 + (fTemp388 + (fRec818[1] + fTemp389)));
			fRec816[0] = fRec818[0];
			float fRec817 = ((fTemp389 + fTemp388) + fTemp387);
			fRec815[0] = (fRec816[0] + fRec815[1]);
			fRec813[0] = fRec815[0];
			float fRec814 = fRec817;
			float fTemp390 = (fConst151 * (((0.0355585851f * fTemp52) + (0.00846020691f * fTemp50)) + (0.0561676435f * fTemp51)));
			float fTemp391 = (fConst152 * fRec819[1]);
			fRec821[0] = (fTemp390 + (fRec821[1] + fTemp391));
			fRec819[0] = fRec821[0];
			float fRec820 = (fTemp391 + fTemp390);
			float fTemp392 = (fConst125 * fRec822[1]);
			float fTemp393 = (fConst126 * fRec825[1]);
			float fTemp394 = (fConst128 * ((((0.038214758f * fTemp27) + (0.0167307854f * fTemp31)) + (0.0201459322f * fTemp29)) - ((0.0293682199f * fTemp35) + ((0.00749705313f * fTemp34) + ((0.017142918f * fTemp30) + (((0.0277294759f * fTemp28) + (0.0112831919f * fTemp32)) + (0.0146096526f * fTemp33)))))));
			float fTemp395 = (fConst129 * fRec828[1]);
			float fTemp396 = (fConst130 * fRec831[1]);
			fRec833[0] = (fTemp394 + (fTemp395 + (fRec833[1] + fTemp396)));
			fRec831[0] = fRec833[0];
			float fRec832 = ((fTemp396 + fTemp395) + fTemp394);
			fRec830[0] = (fRec831[0] + fRec830[1]);
			fRec828[0] = fRec830[0];
			float fRec829 = fRec832;
			fRec827[0] = (fTemp392 + (fTemp393 + (fRec829 + fRec827[1])));
			fRec825[0] = fRec827[0];
			float fRec826 = (fTemp392 + (fRec829 + fTemp393));
			fRec824[0] = (fRec825[0] + fRec824[1]);
			fRec822[0] = fRec824[0];
			float fRec823 = fRec826;
			float fTemp397 = (fConst142 * fRec834[1]);
			float fTemp398 = (fConst144 * fRec837[1]);
			float fTemp399 = (fConst145 * fRec840[1]);
			float fTemp400 = (fConst147 * (((0.0166317243f * fTemp18) + ((0.0102377888f * fTemp15) + (((0.0129367337f * fTemp11) + (0.00802294165f * fTemp12)) + (0.00755080581f * fTemp14)))) - ((0.0333135277f * fTemp21) + ((0.0141807692f * fTemp20) + ((0.00274663907f * fTemp19) + (((0.0240940508f * fTemp17) + (0.0149396183f * fTemp13)) + (0.00432667555f * fTemp16)))))));
			float fTemp401 = (fConst148 * fRec843[1]);
			float fTemp402 = (fConst149 * fRec846[1]);
			fRec848[0] = (fTemp400 + (fTemp401 + (fRec848[1] + fTemp402)));
			fRec846[0] = fRec848[0];
			float fRec847 = ((fTemp402 + fTemp401) + fTemp400);
			fRec845[0] = (fRec846[0] + fRec845[1]);
			fRec843[0] = fRec845[0];
			float fRec844 = fRec847;
			fRec842[0] = (fTemp398 + (fTemp399 + (fRec844 + fRec842[1])));
			fRec840[0] = fRec842[0];
			float fRec841 = (fTemp398 + (fRec844 + fTemp399));
			fRec839[0] = (fRec840[0] + fRec839[1]);
			fRec837[0] = fRec839[0];
			float fRec838 = fRec841;
			fRec836[0] = (fTemp397 + (fRec838 + fRec836[1]));
			fRec834[0] = fRec836[0];
			float fRec835 = (fRec838 + fTemp397);
			fVec17[(IOTA & 1023)] = (fRec805 + (fRec814 + (fRec820 + ((0.0410131775f * fTemp7) + (fRec823 + fRec835)))));
			output17[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec17[((IOTA - iConst153) & 1023)])));
			float fTemp403 = (fConst142 * fRec849[1]);
			float fTemp404 = (fConst144 * fRec852[1]);
			float fTemp405 = (fConst145 * fRec855[1]);
			float fTemp406 = (fConst147 * ((((0.0230268147f * fTemp19) + ((0.00467898231f * fTemp16) + ((0.00936584454f * fTemp18) + ((0.00763794174f * fTemp15) + ((0.00457082922f * fTemp17) + (0.0186693426f * fTemp14)))))) + (0.0271751657f * fTemp21)) - ((((0.0205804072f * fTemp11) + (0.00853307825f * fTemp12)) + (0.00972103048f * fTemp13)) + (0.00544337463f * fTemp20))));
			float fTemp407 = (fConst148 * fRec858[1]);
			float fTemp408 = (fConst149 * fRec861[1]);
			fRec863[0] = (fTemp406 + (fTemp407 + (fRec863[1] + fTemp408)));
			fRec861[0] = fRec863[0];
			float fRec862 = ((fTemp408 + fTemp407) + fTemp406);
			fRec860[0] = (fRec861[0] + fRec860[1]);
			fRec858[0] = fRec860[0];
			float fRec859 = fRec862;
			fRec857[0] = (fTemp404 + (fTemp405 + (fRec859 + fRec857[1])));
			fRec855[0] = fRec857[0];
			float fRec856 = (fTemp404 + (fRec859 + fTemp405));
			fRec854[0] = (fRec855[0] + fRec854[1]);
			fRec852[0] = fRec854[0];
			float fRec853 = fRec856;
			fRec851[0] = (fTemp403 + (fRec853 + fRec851[1]));
			fRec849[0] = fRec851[0];
			float fRec850 = (fRec853 + fTemp403);
			float fTemp409 = (fConst125 * fRec864[1]);
			float fTemp410 = (fConst126 * fRec867[1]);
			float fTemp411 = (fConst128 * ((((0.0003267102f * fTemp31) + (0.0188121404f * fTemp29)) + (0.0175062921f * fTemp30)) - ((0.00839922298f * fTemp35) + (((((0.0390599146f * fTemp27) + (0.0225756876f * fTemp28)) + (0.00917239767f * fTemp32)) + (0.00563843688f * fTemp33)) + (0.0100301197f * fTemp34)))));
			float fTemp412 = (fConst129 * fRec870[1]);
			float fTemp413 = (fConst130 * fRec873[1]);
			fRec875[0] = (fTemp411 + (fTemp412 + (fRec875[1] + fTemp413)));
			fRec873[0] = fRec875[0];
			float fRec874 = ((fTemp413 + fTemp412) + fTemp411);
			fRec872[0] = (fRec873[0] + fRec872[1]);
			fRec870[0] = fRec872[0];
			float fRec871 = fRec874;
			fRec869[0] = (fTemp409 + (fTemp410 + (fRec871 + fRec869[1])));
			fRec867[0] = fRec869[0];
			float fRec868 = (fTemp409 + (fRec871 + fTemp410));
			fRec866[0] = (fRec867[0] + fRec866[1]);
			fRec864[0] = fRec866[0];
			float fRec865 = fRec868;
			float fTemp414 = (fConst132 * fRec876[1]);
			float fTemp415 = (fConst134 * ((0.00809177849f * fTemp40) - ((0.0430178829f * fTemp46) + (((((0.0100868177f * fTemp44) + (0.0297035649f * fTemp45)) + (0.00696406513f * fTemp41)) + (0.014461129f * fTemp42)) + (0.00446618395f * fTemp43)))));
			float fTemp416 = (fConst135 * fRec879[1]);
			float fTemp417 = (fConst136 * fRec882[1]);
			fRec884[0] = (fTemp415 + (fTemp416 + (fRec884[1] + fTemp417)));
			fRec882[0] = fRec884[0];
			float fRec883 = ((fTemp417 + fTemp416) + fTemp415);
			fRec881[0] = (fRec882[0] + fRec881[1]);
			fRec879[0] = fRec881[0];
			float fRec880 = fRec883;
			fRec878[0] = (fTemp414 + (fRec880 + fRec878[1]));
			fRec876[0] = fRec878[0];
			float fRec877 = (fRec880 + fTemp414);
			float fTemp418 = (fConst151 * (((0.0401038378f * fTemp52) + (0.00367245008f * fTemp50)) + (0.0192545932f * fTemp51)));
			float fTemp419 = (fConst152 * fRec885[1]);
			fRec887[0] = (fTemp418 + (fRec887[1] + fTemp419));
			fRec885[0] = fRec887[0];
			float fRec886 = (fTemp419 + fTemp418);
			float fTemp420 = (fConst138 * ((((0.0358876549f * fTemp55) + (0.00661438983f * fTemp59)) + (0.00388430688f * fTemp57)) - ((0.0261082873f * fTemp56) + (0.0287773926f * fTemp58))));
			float fTemp421 = (fConst139 * fRec888[1]);
			float fTemp422 = (fConst140 * fRec891[1]);
			fRec893[0] = (fTemp420 + (fTemp421 + (fRec893[1] + fTemp422)));
			fRec891[0] = fRec893[0];
			float fRec892 = ((fTemp422 + fTemp421) + fTemp420);
			fRec890[0] = (fRec891[0] + fRec890[1]);
			fRec888[0] = fRec890[0];
			float fRec889 = fRec892;
			fVec18[(IOTA & 1023)] = ((0.0267503932f * fTemp7) + (fRec850 + (fRec865 + (fRec877 + (fRec886 + fRec889)))));
			output18[i] = FAUSTFLOAT((0.825772464f * (fRec0[0] * fVec18[((IOTA - iConst153) & 1023)])));
			float fTemp423 = (fConst155 * fRec894[1]);
			float fTemp424 = (fConst157 * fRec897[1]);
			float fTemp425 = (fConst158 * fRec900[1]);
			float fTemp426 = (fConst160 * ((((0.0284519251f * fTemp11) + (1.45190998e-05f * fTemp13)) + (3.16740011e-06f * fTemp19)) - ((7.89850037e-06f * fTemp21) + (((0.0096867336f * fTemp16) + ((7.58240003e-06f * fTemp18) + ((((3.01649993e-06f * fTemp12) + (0.0142122796f * fTemp17)) + (0.000523426977f * fTemp14)) + (0.00861447677f * fTemp15)))) + (0.039227441f * fTemp20)))));
			float fTemp427 = (fConst161 * fRec903[1]);
			float fTemp428 = (fConst162 * fRec906[1]);
			fRec908[0] = (fTemp426 + (fTemp427 + (fRec908[1] + fTemp428)));
			fRec906[0] = fRec908[0];
			float fRec907 = ((fTemp428 + fTemp427) + fTemp426);
			fRec905[0] = (fRec906[0] + fRec905[1]);
			fRec903[0] = fRec905[0];
			float fRec904 = fRec907;
			fRec902[0] = (fTemp424 + (fTemp425 + (fRec904 + fRec902[1])));
			fRec900[0] = fRec902[0];
			float fRec901 = (fTemp424 + (fRec904 + fTemp425));
			fRec899[0] = (fRec900[0] + fRec899[1]);
			fRec897[0] = fRec899[0];
			float fRec898 = fRec901;
			fRec896[0] = (fTemp423 + (fRec898 + fRec896[1]));
			fRec894[0] = fRec896[0];
			float fRec895 = (fRec898 + fTemp423);
			float fTemp429 = (fConst164 * fRec909[1]);
			float fTemp430 = (fConst165 * fRec912[1]);
			float fTemp431 = (fConst167 * ((((((1.53417004e-05f * fTemp27) + (0.0477514416f * fTemp31)) + (0.020346595f * fTemp32)) + (8.0187001e-06f * fTemp33)) + (0.040051911f * fTemp35)) - ((6.14549981e-06f * fTemp34) + (((4.3588002e-06f * fTemp28) + (0.00278774789f * fTemp29)) + (0.0099853361f * fTemp30)))));
			float fTemp432 = (fConst168 * fRec915[1]);
			float fTemp433 = (fConst169 * fRec918[1]);
			fRec920[0] = (fTemp431 + (fTemp432 + (fRec920[1] + fTemp433)));
			fRec918[0] = fRec920[0];
			float fRec919 = ((fTemp433 + fTemp432) + fTemp431);
			fRec917[0] = (fRec918[0] + fRec917[1]);
			fRec915[0] = fRec917[0];
			float fRec916 = fRec919;
			fRec914[0] = (fTemp429 + (fTemp430 + (fRec916 + fRec914[1])));
			fRec912[0] = fRec914[0];
			float fRec913 = (fTemp429 + (fRec916 + fTemp430));
			fRec911[0] = (fRec912[0] + fRec911[1]);
			fRec909[0] = fRec911[0];
			float fRec910 = fRec913;
			float fTemp434 = (fConst171 * fRec921[1]);
			float fTemp435 = (fConst173 * (((1.86639008e-05f * fTemp46) + ((((8.20439982e-06f * fTemp40) + (0.000564895803f * fTemp45)) + (0.0250918176f * fTemp41)) + (0.0537228286f * fTemp43))) - ((0.0512112677f * fTemp44) + (3.33759999e-06f * fTemp42))));
			float fTemp436 = (fConst174 * fRec924[1]);
			float fTemp437 = (fConst175 * fRec927[1]);
			fRec929[0] = (fTemp435 + (fTemp436 + (fRec929[1] + fTemp437)));
			fRec927[0] = fRec929[0];
			float fRec928 = ((fTemp437 + fTemp436) + fTemp435);
			fRec926[0] = (fRec927[0] + fRec926[1]);
			fRec924[0] = fRec926[0];
			float fRec925 = fRec928;
			fRec923[0] = (fTemp434 + (fRec925 + fRec923[1]));
			fRec921[0] = fRec923[0];
			float fRec922 = (fRec925 + fTemp434);
			float fTemp438 = (fConst177 * ((6.85509985e-06f * fTemp57) - ((((1.71022002e-05f * fTemp55) + (0.055088263f * fTemp59)) + (0.0130083254f * fTemp56)) + (0.0606702417f * fTemp58))));
			float fTemp439 = (fConst178 * fRec930[1]);
			float fTemp440 = (fConst179 * fRec933[1]);
			fRec935[0] = (fTemp438 + (fTemp439 + (fRec935[1] + fTemp440)));
			fRec933[0] = fRec935[0];
			float fRec934 = ((fTemp440 + fTemp439) + fTemp438);
			fRec932[0] = (fRec933[0] + fRec932[1]);
			fRec930[0] = fRec932[0];
			float fRec931 = fRec934;
			float fTemp441 = (fConst181 * ((0.0668016002f * fTemp52) - ((0.0337675065f * fTemp50) + (1.07285996e-05f * fTemp51))));
			float fTemp442 = (fConst182 * fRec936[1]);
			fRec938[0] = (fTemp441 + (fRec938[1] + fTemp442));
			fRec936[0] = fRec938[0];
			float fRec937 = (fTemp442 + fTemp441);
			fVec19[0] = (fRec895 + (fRec910 + (fRec922 + (fRec931 + (fRec937 + (0.0466528162f * fTemp7))))));
			output19[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec19[iConst183])));
			float fTemp443 = (fConst185 * fRec939[1]);
			float fTemp444 = (fConst187 * (((0.0352554694f * fTemp46) + ((((0.0418010689f * fTemp40) + (0.0143792909f * fTemp45)) + (0.0202015545f * fTemp41)) + (0.0241348408f * fTemp43))) - ((1.2337e-05f * fTemp44) + (0.00830581039f * fTemp42))));
			float fTemp445 = (fConst188 * fRec942[1]);
			float fTemp446 = (fConst189 * fRec945[1]);
			fRec947[0] = (fTemp444 + (fTemp445 + (fRec947[1] + fTemp446)));
			fRec945[0] = fRec947[0];
			float fRec946 = ((fTemp446 + fTemp445) + fTemp444);
			fRec944[0] = (fRec945[0] + fRec944[1]);
			fRec942[0] = fRec944[0];
			float fRec943 = fRec946;
			fRec941[0] = (fTemp443 + (fRec943 + fRec941[1]));
			fRec939[0] = fRec941[0];
			float fRec940 = (fRec943 + fTemp443);
			float fTemp447 = (fConst191 * ((0.0250726212f * fTemp57) - ((((0.0359448791f * fTemp55) + (0.0434271991f * fTemp59)) + (0.000599099381f * fTemp56)) + (0.020761501f * fTemp58))));
			float fTemp448 = (fConst192 * fRec948[1]);
			float fTemp449 = (fConst193 * fRec951[1]);
			fRec953[0] = (fTemp447 + (fTemp448 + (fRec953[1] + fTemp449)));
			fRec951[0] = fRec953[0];
			float fRec952 = ((fTemp449 + fTemp448) + fTemp447);
			fRec950[0] = (fRec951[0] + fRec950[1]);
			fRec948[0] = fRec950[0];
			float fRec949 = fRec952;
			float fTemp450 = (fConst195 * ((0.0406398699f * fTemp52) - ((0.0313559137f * fTemp50) + (0.0234596878f * fTemp51))));
			float fTemp451 = (fConst196 * fRec954[1]);
			fRec956[0] = (fTemp450 + (fRec956[1] + fTemp451));
			fRec954[0] = fRec956[0];
			float fRec955 = (fTemp451 + fTemp450);
			float fTemp452 = (fConst198 * fRec957[1]);
			float fTemp453 = (fConst200 * fRec960[1]);
			float fTemp454 = (fConst201 * fRec963[1]);
			float fTemp455 = (fConst203 * (((0.0179222506f * fTemp20) + (((1.04886003e-05f * fTemp17) + (0.00632963143f * fTemp18)) + (0.0245175455f * fTemp19))) - (((((((0.0201500095f * fTemp11) + (0.0310517792f * fTemp12)) + (0.00189300114f * fTemp13)) + (0.0109545682f * fTemp14)) + (0.00218773494f * fTemp15)) + (0.00109328376f * fTemp16)) + (0.0116588809f * fTemp21))));
			float fTemp456 = (fConst204 * fRec966[1]);
			float fTemp457 = (fConst205 * fRec969[1]);
			fRec971[0] = (fTemp455 + (fTemp456 + (fRec971[1] + fTemp457)));
			fRec969[0] = fRec971[0];
			float fRec970 = ((fTemp457 + fTemp456) + fTemp455);
			fRec968[0] = (fRec969[0] + fRec968[1]);
			fRec966[0] = fRec968[0];
			float fRec967 = fRec970;
			fRec965[0] = (fTemp453 + (fTemp454 + (fRec967 + fRec965[1])));
			fRec963[0] = fRec965[0];
			float fRec964 = (fTemp453 + (fRec967 + fTemp454));
			fRec962[0] = (fRec963[0] + fRec962[1]);
			fRec960[0] = fRec962[0];
			float fRec961 = fRec964;
			fRec959[0] = (fTemp452 + (fRec961 + fRec959[1]));
			fRec957[0] = fRec959[0];
			float fRec958 = (fRec961 + fTemp452);
			float fTemp458 = (fConst207 * fRec972[1]);
			float fTemp459 = (fConst208 * fRec975[1]);
			float fTemp460 = (fConst210 * ((((0.0251543671f * fTemp27) + (2.20640004e-06f * fTemp31)) + (0.0108541893f * fTemp32)) - ((0.0145028802f * fTemp35) + ((0.0428572521f * fTemp34) + ((0.011761561f * fTemp30) + (((0.0203849878f * fTemp28) + (0.0129730217f * fTemp29)) + (0.00626636297f * fTemp33)))))));
			float fTemp461 = (fConst211 * fRec978[1]);
			float fTemp462 = (fConst212 * fRec981[1]);
			fRec983[0] = (fTemp460 + (fTemp461 + (fRec983[1] + fTemp462)));
			fRec981[0] = fRec983[0];
			float fRec982 = ((fTemp462 + fTemp461) + fTemp460);
			fRec980[0] = (fRec981[0] + fRec980[1]);
			fRec978[0] = fRec980[0];
			float fRec979 = fRec982;
			fRec977[0] = (fTemp458 + (fTemp459 + (fRec979 + fRec977[1])));
			fRec975[0] = fRec977[0];
			float fRec976 = (fTemp458 + (fRec979 + fTemp459));
			fRec974[0] = (fRec975[0] + fRec974[1]);
			fRec972[0] = fRec974[0];
			float fRec973 = fRec976;
			output20[i] = FAUSTFLOAT((fRec0[0] * (fRec940 + (fRec949 + (fRec955 + (fRec958 + (fRec973 + (0.0344887897f * fTemp7))))))));
			float fTemp463 = (fConst198 * fRec984[1]);
			float fTemp464 = (fConst200 * fRec987[1]);
			float fTemp465 = (fConst201 * fRec990[1]);
			float fTemp466 = (fConst203 * (((0.0278633516f * fTemp21) + (((0.00087689131f * fTemp16) + ((((0.00345582608f * fTemp11) + (0.0306200888f * fTemp12)) + (0.019354878f * fTemp17)) + (0.0064796987f * fTemp18))) + (0.0220391862f * fTemp20))) - ((((0.00633378886f * fTemp13) + (0.00188656955f * fTemp14)) + (0.00540625351f * fTemp15)) + (0.00513627194f * fTemp19))));
			float fTemp467 = (fConst204 * fRec993[1]);
			float fTemp468 = (fConst205 * fRec996[1]);
			fRec998[0] = (fTemp466 + (fTemp467 + (fRec998[1] + fTemp468)));
			fRec996[0] = fRec998[0];
			float fRec997 = ((fTemp468 + fTemp467) + fTemp466);
			fRec995[0] = (fRec996[0] + fRec995[1]);
			fRec993[0] = fRec995[0];
			float fRec994 = fRec997;
			fRec992[0] = (fTemp464 + (fTemp465 + (fRec994 + fRec992[1])));
			fRec990[0] = fRec992[0];
			float fRec991 = (fTemp464 + (fRec994 + fTemp465));
			fRec989[0] = (fRec990[0] + fRec989[1]);
			fRec987[0] = fRec989[0];
			float fRec988 = fRec991;
			fRec986[0] = (fTemp463 + (fRec988 + fRec986[1]));
			fRec984[0] = fRec986[0];
			float fRec985 = (fRec988 + fTemp463);
			float fTemp469 = (fConst207 * fRec999[1]);
			float fTemp470 = (fConst208 * fRec1002[1]);
			float fTemp471 = (fConst210 * (((0.009207814f * fTemp32) + (0.011788087f * fTemp30)) - ((0.026046088f * fTemp35) + ((((((0.0248568896f * fTemp27) + (0.0453077331f * fTemp31)) + (0.0125348074f * fTemp28)) + (0.00785371754f * fTemp29)) + (0.0136526348f * fTemp33)) + (0.00287043699f * fTemp34)))));
			float fTemp472 = (fConst211 * fRec1005[1]);
			float fTemp473 = (fConst212 * fRec1008[1]);
			fRec1010[0] = (fTemp471 + (fTemp472 + (fRec1010[1] + fTemp473)));
			fRec1008[0] = fRec1010[0];
			float fRec1009 = ((fTemp473 + fTemp472) + fTemp471);
			fRec1007[0] = (fRec1008[0] + fRec1007[1]);
			fRec1005[0] = fRec1007[0];
			float fRec1006 = fRec1009;
			fRec1004[0] = (fTemp469 + (fTemp470 + (fRec1006 + fRec1004[1])));
			fRec1002[0] = fRec1004[0];
			float fRec1003 = (fTemp469 + (fRec1006 + fTemp470));
			fRec1001[0] = (fRec1002[0] + fRec1001[1]);
			fRec999[0] = fRec1001[0];
			float fRec1000 = fRec1003;
			float fTemp474 = (fConst185 * fRec1011[1]);
			float fTemp475 = (fConst187 * ((((((0.0432547703f * fTemp44) + (0.0450634062f * fTemp40)) + (0.00239910092f * fTemp45)) + (0.0226365346f * fTemp41)) + (0.00831752829f * fTemp46)) - ((0.00851497054f * fTemp42) + (0.0239610206f * fTemp43))));
			float fTemp476 = (fConst188 * fRec1014[1]);
			float fTemp477 = (fConst189 * fRec1017[1]);
			fRec1019[0] = (fTemp475 + (fTemp476 + (fRec1019[1] + fTemp477)));
			fRec1017[0] = fRec1019[0];
			float fRec1018 = ((fTemp477 + fTemp476) + fTemp475);
			fRec1016[0] = (fRec1017[0] + fRec1016[1]);
			fRec1014[0] = fRec1016[0];
			float fRec1015 = fRec1018;
			fRec1013[0] = (fTemp474 + (fRec1015 + fRec1013[1]));
			fRec1011[0] = fRec1013[0];
			float fRec1012 = (fRec1015 + fTemp474);
			float fTemp478 = (fConst191 * (((0.0452136435f * fTemp57) + (0.0206034295f * fTemp58)) - (((0.0471394435f * fTemp55) + (0.0269582253f * fTemp59)) + (0.00680943299f * fTemp56))));
			float fTemp479 = (fConst192 * fRec1020[1]);
			float fTemp480 = (fConst193 * fRec1023[1]);
			fRec1025[0] = (fTemp478 + (fTemp479 + (fRec1025[1] + fTemp480)));
			fRec1023[0] = fRec1025[0];
			float fRec1024 = ((fTemp480 + fTemp479) + fTemp478);
			fRec1022[0] = (fRec1023[0] + fRec1022[1]);
			fRec1020[0] = fRec1022[0];
			float fRec1021 = fRec1024;
			float fTemp481 = (fConst195 * ((0.0308367889f * fTemp52) - ((0.0325823613f * fTemp50) + (0.047908619f * fTemp51))));
			float fTemp482 = (fConst196 * fRec1026[1]);
			fRec1028[0] = (fTemp481 + (fRec1028[1] + fTemp482));
			fRec1026[0] = fRec1028[0];
			float fRec1027 = (fTemp482 + fTemp481);
			output21[i] = FAUSTFLOAT((fRec0[0] * (fRec985 + (fRec1000 + (fRec1012 + (fRec1021 + (fRec1027 + (0.0406066403f * fTemp7))))))));
			float fTemp483 = (fConst155 * fRec1029[1]);
			float fTemp484 = (fConst157 * fRec1032[1]);
			float fTemp485 = (fConst158 * fRec1035[1]);
			float fTemp486 = (fConst160 * (((0.00968541205f * fTemp16) + (((2.03910008e-06f * fTemp12) + (5.13370014e-06f * fTemp17)) + (0.000531975005f * fTemp18))) - ((0.0284822471f * fTemp21) + ((0.0392310359f * fTemp20) + (((((5.36049993e-06f * fTemp11) + (2.58280011e-06f * fTemp13)) + (4.81620009e-06f * fTemp14)) + (0.00860646646f * fTemp15)) + (0.0142082181f * fTemp19))))));
			float fTemp487 = (fConst161 * fRec1038[1]);
			float fTemp488 = (fConst162 * fRec1041[1]);
			fRec1043[0] = (fTemp486 + (fTemp487 + (fRec1043[1] + fTemp488)));
			fRec1041[0] = fRec1043[0];
			float fRec1042 = ((fTemp488 + fTemp487) + fTemp486);
			fRec1040[0] = (fRec1041[0] + fRec1040[1]);
			fRec1038[0] = fRec1040[0];
			float fRec1039 = fRec1042;
			fRec1037[0] = (fTemp484 + (fTemp485 + (fRec1039 + fRec1037[1])));
			fRec1035[0] = fRec1037[0];
			float fRec1036 = (fTemp484 + (fRec1039 + fTemp485));
			fRec1034[0] = (fRec1035[0] + fRec1034[1]);
			fRec1032[0] = fRec1034[0];
			float fRec1033 = fRec1036;
			fRec1031[0] = (fTemp483 + (fRec1033 + fRec1031[1]));
			fRec1029[0] = fRec1031[0];
			float fRec1030 = (fRec1033 + fTemp483);
			float fTemp489 = (fConst164 * fRec1044[1]);
			float fTemp490 = (fConst165 * fRec1047[1]);
			float fTemp491 = (fConst167 * (((0.0400748141f * fTemp35) + ((0.0477536209f * fTemp34) + (((4.40240001e-06f * fTemp27) + (2.36560004e-06f * fTemp32)) + (0.00998531654f * fTemp30)))) - ((((1.58520004e-06f * fTemp31) + (2.50020003e-06f * fTemp28)) + (0.00279862061f * fTemp29)) + (0.0203480348f * fTemp33))));
			float fTemp492 = (fConst168 * fRec1050[1]);
			float fTemp493 = (fConst169 * fRec1053[1]);
			fRec1055[0] = (fTemp491 + (fTemp492 + (fRec1055[1] + fTemp493)));
			fRec1053[0] = fRec1055[0];
			float fRec1054 = ((fTemp493 + fTemp492) + fTemp491);
			fRec1052[0] = (fRec1053[0] + fRec1052[1]);
			fRec1050[0] = fRec1052[0];
			float fRec1051 = fRec1054;
			fRec1049[0] = (fTemp489 + (fTemp490 + (fRec1051 + fRec1049[1])));
			fRec1047[0] = fRec1049[0];
			float fRec1048 = (fTemp489 + (fRec1051 + fTemp490));
			fRec1046[0] = (fRec1047[0] + fRec1046[1]);
			fRec1044[0] = fRec1046[0];
			float fRec1045 = fRec1048;
			float fTemp494 = (fConst171 * fRec1056[1]);
			float fTemp495 = (fConst173 * ((((7.28500027e-07f * fTemp40) + (1.21499994e-07f * fTemp45)) + (0.0250976384f * fTemp41)) - ((0.0512235425f * fTemp46) + (((3.07470009e-06f * fTemp44) + (0.000566295581f * fTemp42)) + (0.0537225828f * fTemp43)))));
			float fTemp496 = (fConst174 * fRec1059[1]);
			float fTemp497 = (fConst175 * fRec1062[1]);
			fRec1064[0] = (fTemp495 + (fTemp496 + (fRec1064[1] + fTemp497)));
			fRec1062[0] = fRec1064[0];
			float fRec1063 = ((fTemp497 + fTemp496) + fTemp495);
			fRec1061[0] = (fRec1062[0] + fRec1061[1]);
			fRec1059[0] = fRec1061[0];
			float fRec1060 = fRec1063;
			fRec1058[0] = (fTemp494 + (fRec1060 + fRec1058[1]));
			fRec1056[0] = fRec1058[0];
			float fRec1057 = (fRec1060 + fTemp494);
			float fTemp498 = (fConst177 * (((((1.849e-06f * fTemp55) + (2.55600014e-07f * fTemp59)) + (0.0550848991f * fTemp57)) + (0.0606719367f * fTemp58)) - (0.0130111389f * fTemp56)));
			float fTemp499 = (fConst178 * fRec1065[1]);
			float fTemp500 = (fConst179 * fRec1068[1]);
			fRec1070[0] = (fTemp498 + (fTemp499 + (fRec1070[1] + fTemp500)));
			fRec1068[0] = fRec1070[0];
			float fRec1069 = ((fTemp500 + fTemp499) + fTemp498);
			fRec1067[0] = (fRec1068[0] + fRec1067[1]);
			fRec1065[0] = fRec1067[0];
			float fRec1066 = fRec1069;
			float fTemp501 = (fConst181 * (0.0f - (((9.95899995e-07f * fTemp52) + (0.0337603986f * fTemp50)) + (0.0667955354f * fTemp51))));
			float fTemp502 = (fConst182 * fRec1071[1]);
			fRec1073[0] = (fTemp501 + (fRec1073[1] + fTemp502));
			fRec1071[0] = fRec1073[0];
			float fRec1072 = (fTemp502 + fTemp501);
			fVec20[0] = (fRec1030 + (fRec1045 + (fRec1057 + (fRec1066 + (fRec1072 + (0.0466445908f * fTemp7))))));
			output22[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec20[iConst183])));
			float fTemp503 = (fConst198 * fRec1074[1]);
			float fTemp504 = (fConst200 * fRec1077[1]);
			float fTemp505 = (fConst201 * fRec1080[1]);
			float fTemp506 = (fConst203 * (((0.0201714132f * fTemp21) + (((0.00108145678f * fTemp16) + (((0.00186703668f * fTemp13) + (0.00631730538f * fTemp14)) + (0.010951966f * fTemp18))) + (0.0179166235f * fTemp20))) - (((((0.0116426814f * fTemp11) + (0.0310388543f * fTemp12)) + (0.0245218333f * fTemp17)) + (0.00217805686f * fTemp15)) + (3.20160007e-06f * fTemp19))));
			float fTemp507 = (fConst204 * fRec1083[1]);
			float fTemp508 = (fConst205 * fRec1086[1]);
			fRec1088[0] = (fTemp506 + (fTemp507 + (fRec1088[1] + fTemp508)));
			fRec1086[0] = fRec1088[0];
			float fRec1087 = ((fTemp508 + fTemp507) + fTemp506);
			fRec1085[0] = (fRec1086[0] + fRec1085[1]);
			fRec1083[0] = fRec1085[0];
			float fRec1084 = fRec1087;
			fRec1082[0] = (fTemp504 + (fTemp505 + (fRec1084 + fRec1082[1])));
			fRec1080[0] = fRec1082[0];
			float fRec1081 = (fTemp504 + (fRec1084 + fTemp505));
			fRec1079[0] = (fRec1080[0] + fRec1079[1]);
			fRec1077[0] = fRec1079[0];
			float fRec1078 = fRec1081;
			fRec1076[0] = (fTemp503 + (fRec1078 + fRec1076[1]));
			fRec1074[0] = fRec1076[0];
			float fRec1075 = (fRec1078 + fTemp503);
			float fTemp509 = (fConst207 * fRec1089[1]);
			float fTemp510 = (fConst208 * fRec1092[1]);
			float fTemp511 = (fConst210 * (((3.61189996e-06f * fTemp34) + ((((0.0251501054f * fTemp27) + (0.0428467207f * fTemp31)) + (0.0203895811f * fTemp28)) + (0.0117731281f * fTemp30))) - ((((0.0062495796f * fTemp32) + (0.0129695972f * fTemp29)) + (0.0108326115f * fTemp33)) + (0.0145213213f * fTemp35))));
			float fTemp512 = (fConst211 * fRec1095[1]);
			float fTemp513 = (fConst212 * fRec1098[1]);
			fRec1100[0] = (fTemp511 + (fTemp512 + (fRec1100[1] + fTemp513)));
			fRec1098[0] = fRec1100[0];
			float fRec1099 = ((fTemp513 + fTemp512) + fTemp511);
			fRec1097[0] = (fRec1098[0] + fRec1097[1]);
			fRec1095[0] = fRec1097[0];
			float fRec1096 = fRec1099;
			fRec1094[0] = (fTemp509 + (fTemp510 + (fRec1096 + fRec1094[1])));
			fRec1092[0] = fRec1094[0];
			float fRec1093 = (fTemp509 + (fRec1096 + fTemp510));
			fRec1091[0] = (fRec1092[0] + fRec1091[1]);
			fRec1089[0] = fRec1091[0];
			float fRec1090 = fRec1093;
			float fTemp514 = (fConst185 * fRec1101[1]);
			float fTemp515 = (fConst187 * ((0.0201849788f * fTemp41) - ((6.92699984e-07f * fTemp46) + (((((0.0352582783f * fTemp44) + (0.0417956226f * fTemp40)) + (0.00831092708f * fTemp45)) + (0.0143922055f * fTemp42)) + (0.0241336562f * fTemp43)))));
			float fTemp516 = (fConst188 * fRec1104[1]);
			float fTemp517 = (fConst189 * fRec1107[1]);
			fRec1109[0] = (fTemp515 + (fTemp516 + (fRec1109[1] + fTemp517)));
			fRec1107[0] = fRec1109[0];
			float fRec1108 = ((fTemp517 + fTemp516) + fTemp515);
			fRec1106[0] = (fRec1107[0] + fRec1106[1]);
			fRec1104[0] = fRec1106[0];
			float fRec1105 = fRec1108;
			fRec1103[0] = (fTemp514 + (fRec1105 + fRec1103[1]));
			fRec1101[0] = fRec1103[0];
			float fRec1102 = (fRec1105 + fTemp514);
			float fTemp518 = (fConst191 * (((((0.0359500684f * fTemp55) + (0.0250729267f * fTemp59)) + (0.0434282646f * fTemp57)) + (0.0207570828f * fTemp58)) - (0.000587342074f * fTemp56)));
			float fTemp519 = (fConst192 * fRec1110[1]);
			float fTemp520 = (fConst193 * fRec1113[1]);
			fRec1115[0] = (fTemp518 + (fTemp519 + (fRec1115[1] + fTemp520)));
			fRec1113[0] = fRec1115[0];
			float fRec1114 = ((fTemp520 + fTemp519) + fTemp518);
			fRec1112[0] = (fRec1113[0] + fRec1112[1]);
			fRec1110[0] = fRec1112[0];
			float fRec1111 = fRec1114;
			float fTemp521 = (fConst195 * (0.0f - (((0.0234640706f * fTemp52) + (0.0313628912f * fTemp50)) + (0.0406417958f * fTemp51))));
			float fTemp522 = (fConst196 * fRec1116[1]);
			fRec1118[0] = (fTemp521 + (fRec1118[1] + fTemp522));
			fRec1116[0] = fRec1118[0];
			float fRec1117 = (fTemp522 + fTemp521);
			output23[i] = FAUSTFLOAT((fRec0[0] * (fRec1075 + (fRec1090 + (fRec1102 + (fRec1111 + (fRec1117 + (0.034495037f * fTemp7))))))));
			float fTemp523 = (fConst203 * (((0.0220355149f * fTemp20) + (((((((0.027878603f * fTemp11) + (0.0306277573f * fTemp12)) + (0.00512686651f * fTemp17)) + (0.00634137029f * fTemp13)) + (0.00648505101f * fTemp14)) + (0.00190367468f * fTemp18)) + (0.0193522982f * fTemp19))) - (((0.00540416921f * fTemp15) + (0.000876429316f * fTemp16)) + (0.00347967655f * fTemp21))));
			float fTemp524 = (fConst205 * fRec1131[1]);
			float fTemp525 = (fConst204 * fRec1128[1]);
			fRec1133[0] = (((fTemp523 + fRec1133[1]) + fTemp524) + fTemp525);
			fRec1131[0] = fRec1133[0];
			float fRec1132 = ((fTemp523 + fTemp524) + fTemp525);
			fRec1130[0] = (fRec1131[0] + fRec1130[1]);
			fRec1128[0] = fRec1130[0];
			float fRec1129 = fRec1132;
			float fTemp526 = (fConst201 * fRec1125[1]);
			float fTemp527 = (fConst200 * fRec1122[1]);
			fRec1127[0] = (((fRec1129 + fRec1127[1]) + fTemp526) + fTemp527);
			fRec1125[0] = fRec1127[0];
			float fRec1126 = ((fRec1129 + fTemp526) + fTemp527);
			fRec1124[0] = (fRec1125[0] + fRec1124[1]);
			fRec1122[0] = fRec1124[0];
			float fRec1123 = fRec1126;
			float fTemp528 = (fConst198 * fRec1119[1]);
			fRec1121[0] = ((fRec1123 + fRec1121[1]) + fTemp528);
			fRec1119[0] = fRec1121[0];
			float fRec1120 = (fRec1123 + fTemp528);
			float fTemp529 = (fConst207 * fRec1134[1]);
			float fTemp530 = (fConst208 * fRec1137[1]);
			float fTemp531 = (fConst210 * (((0.00286312122f * fTemp31) + (0.0125351297f * fTemp28)) - ((0.0260353144f * fTemp35) + ((0.0453069843f * fTemp34) + ((0.0117803868f * fTemp30) + ((((0.0248816088f * fTemp27) + (0.0136577887f * fTemp32)) + (0.00786233228f * fTemp29)) + (0.00921548437f * fTemp33)))))));
			float fTemp532 = (fConst211 * fRec1140[1]);
			float fTemp533 = (fConst212 * fRec1143[1]);
			fRec1145[0] = (fTemp531 + (fTemp532 + (fRec1145[1] + fTemp533)));
			fRec1143[0] = fRec1145[0];
			float fRec1144 = ((fTemp533 + fTemp532) + fTemp531);
			fRec1142[0] = (fRec1143[0] + fRec1142[1]);
			fRec1140[0] = fRec1142[0];
			float fRec1141 = fRec1144;
			fRec1139[0] = (fTemp529 + (fTemp530 + (fRec1141 + fRec1139[1])));
			fRec1137[0] = fRec1139[0];
			float fRec1138 = (fTemp529 + (fRec1141 + fTemp530));
			fRec1136[0] = (fRec1137[0] + fRec1136[1]);
			fRec1134[0] = fRec1136[0];
			float fRec1135 = fRec1138;
			float fTemp534 = (fConst185 * fRec1146[1]);
			float fTemp535 = (fConst187 * (((0.0432581492f * fTemp46) + ((0.0226408653f * fTemp41) + (0.0239630714f * fTemp43))) - ((((0.00829337724f * fTemp44) + (0.0450532325f * fTemp40)) + (0.00850997958f * fTemp45)) + (0.0023992199f * fTemp42))));
			float fTemp536 = (fConst188 * fRec1149[1]);
			float fTemp537 = (fConst189 * fRec1152[1]);
			fRec1154[0] = (fTemp535 + (fTemp536 + (fRec1154[1] + fTemp537)));
			fRec1152[0] = fRec1154[0];
			float fRec1153 = ((fTemp537 + fTemp536) + fTemp535);
			fRec1151[0] = (fRec1152[0] + fRec1151[1]);
			fRec1149[0] = fRec1151[0];
			float fRec1150 = fRec1153;
			fRec1148[0] = (fTemp534 + (fRec1150 + fRec1148[1]));
			fRec1146[0] = fRec1148[0];
			float fRec1147 = (fRec1150 + fTemp534);
			float fTemp538 = (fConst191 * ((((0.0471209586f * fTemp55) + (0.0452032685f * fTemp59)) + (0.0269464068f * fTemp57)) - ((0.00681215944f * fTemp56) + (0.0206162035f * fTemp58))));
			float fTemp539 = (fConst192 * fRec1155[1]);
			float fTemp540 = (fConst193 * fRec1158[1]);
			fRec1160[0] = (fTemp538 + (fTemp539 + (fRec1160[1] + fTemp540)));
			fRec1158[0] = fRec1160[0];
			float fRec1159 = ((fTemp540 + fTemp539) + fTemp538);
			fRec1157[0] = (fRec1158[0] + fRec1157[1]);
			fRec1155[0] = fRec1157[0];
			float fRec1156 = fRec1159;
			float fTemp541 = (fConst195 * (0.0f - (((0.0478971489f * fTemp52) + (0.0325675346f * fTemp50)) + (0.0308167655f * fTemp51))));
			float fTemp542 = (fConst196 * fRec1161[1]);
			fRec1163[0] = (fTemp541 + (fRec1163[1] + fTemp542));
			fRec1161[0] = fRec1163[0];
			float fRec1162 = (fTemp542 + fTemp541);
			output24[i] = FAUSTFLOAT((fRec0[0] * (fRec1120 + (fRec1135 + (fRec1147 + (fRec1156 + (fRec1162 + (0.0405882597f * fTemp7))))))));
			float fTemp543 = (fConst155 * fRec1164[1]);
			float fTemp544 = (fConst157 * fRec1167[1]);
			float fTemp545 = (fConst158 * fRec1170[1]);
			float fTemp546 = (fConst160 * ((((4.62000003e-08f * fTemp12) + (0.0142199602f * fTemp17)) + (0.000542933121f * fTemp14)) - ((7.61609999e-06f * fTemp21) + ((0.0392383821f * fTemp20) + ((1.79949996e-06f * fTemp19) + ((0.00969552808f * fTemp16) + ((2.83860004e-06f * fTemp18) + (((0.0284511074f * fTemp11) + (1.19779997e-06f * fTemp13)) + (0.00859902892f * fTemp15)))))))));
			float fTemp547 = (fConst161 * fRec1173[1]);
			float fTemp548 = (fConst162 * fRec1176[1]);
			fRec1178[0] = (fTemp546 + (fTemp547 + (fRec1178[1] + fTemp548)));
			fRec1176[0] = fRec1178[0];
			float fRec1177 = ((fTemp548 + fTemp547) + fTemp546);
			fRec1175[0] = (fRec1176[0] + fRec1175[1]);
			fRec1173[0] = fRec1175[0];
			float fRec1174 = fRec1177;
			fRec1172[0] = (fTemp544 + (fTemp545 + (fRec1174 + fRec1172[1])));
			fRec1170[0] = fRec1172[0];
			float fRec1171 = (fTemp544 + (fRec1174 + fTemp545));
			fRec1169[0] = (fRec1170[0] + fRec1169[1]);
			fRec1167[0] = fRec1169[0];
			float fRec1168 = fRec1171;
			fRec1166[0] = (fTemp543 + (fRec1168 + fRec1166[1]));
			fRec1164[0] = fRec1166[0];
			float fRec1165 = (fRec1168 + fTemp543);
			float fTemp549 = (fConst164 * fRec1179[1]);
			float fTemp550 = (fConst165 * fRec1182[1]);
			float fTemp551 = (fConst167 * (((1.85119995e-06f * fTemp33) + (0.040046867f * fTemp35)) - ((5.11999986e-07f * fTemp34) + ((((((8.20379955e-06f * fTemp27) + (0.0477588959f * fTemp31)) + (1.82190001e-06f * fTemp28)) + (0.0203590505f * fTemp32)) + (0.00281510339f * fTemp29)) + (0.00999389309f * fTemp30)))));
			float fTemp552 = (fConst168 * fRec1185[1]);
			float fTemp553 = (fConst169 * fRec1188[1]);
			fRec1190[0] = (fTemp551 + (fTemp552 + (fRec1190[1] + fTemp553)));
			fRec1188[0] = fRec1190[0];
			float fRec1189 = ((fTemp553 + fTemp552) + fTemp551);
			fRec1187[0] = (fRec1188[0] + fRec1187[1]);
			fRec1185[0] = fRec1187[0];
			float fRec1186 = fRec1189;
			fRec1184[0] = (fTemp549 + (fTemp550 + (fRec1186 + fRec1184[1])));
			fRec1182[0] = fRec1184[0];
			float fRec1183 = (fTemp549 + (fRec1186 + fTemp550));
			fRec1181[0] = (fRec1182[0] + fRec1181[1]);
			fRec1179[0] = fRec1181[0];
			float fRec1180 = fRec1183;
			float fTemp554 = (fConst171 * fRec1191[1]);
			float fTemp555 = (fConst173 * (((7.54509983e-06f * fTemp46) + ((0.0537262298f * fTemp43) + (((0.0512015373f * fTemp44) + (0.0251128487f * fTemp41)) + (3.62999998e-07f * fTemp42)))) - ((7.6409998e-07f * fTemp40) + (0.000570559001f * fTemp45))));
			float fTemp556 = (fConst174 * fRec1194[1]);
			float fTemp557 = (fConst175 * fRec1197[1]);
			fRec1199[0] = (fTemp555 + (fTemp556 + (fRec1199[1] + fTemp557)));
			fRec1197[0] = fRec1199[0];
			float fRec1198 = ((fTemp557 + fTemp556) + fTemp555);
			fRec1196[0] = (fRec1197[0] + fRec1196[1]);
			fRec1194[0] = fRec1196[0];
			float fRec1195 = fRec1198;
			fRec1193[0] = (fTemp554 + (fRec1195 + fRec1193[1]));
			fRec1191[0] = fRec1193[0];
			float fRec1192 = (fRec1195 + fTemp554);
			float fTemp558 = (fConst177 * ((((5.71520013e-06f * fTemp55) + (0.055085998f * fTemp59)) + (1.07580001e-06f * fTemp57)) - ((0.0130164139f * fTemp56) + (0.0606563874f * fTemp58))));
			float fTemp559 = (fConst178 * fRec1200[1]);
			float fTemp560 = (fConst179 * fRec1203[1]);
			fRec1205[0] = (fTemp558 + (fTemp559 + (fRec1205[1] + fTemp560)));
			fRec1203[0] = fRec1205[0];
			float fRec1204 = ((fTemp560 + fTemp559) + fTemp558);
			fRec1202[0] = (fRec1203[0] + fRec1202[1]);
			fRec1200[0] = fRec1202[0];
			float fRec1201 = fRec1204;
			float fTemp561 = (fConst181 * (0.0f - (((0.0667844266f * fTemp52) + (0.0337536409f * fTemp50)) + (3.20599997e-06f * fTemp51))));
			float fTemp562 = (fConst182 * fRec1206[1]);
			fRec1208[0] = (fTemp561 + (fRec1208[1] + fTemp562));
			fRec1206[0] = fRec1208[0];
			float fRec1207 = (fTemp562 + fTemp561);
			fVec21[0] = (fRec1165 + (fRec1180 + (fRec1192 + (fRec1201 + (fRec1207 + (0.0466346815f * fTemp7))))));
			output25[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec21[iConst183])));
			float fTemp563 = (fConst185 * fRec1209[1]);
			float fTemp564 = (fConst187 * (((0.0239670761f * fTemp43) + (((0.0450488664f * fTemp40) + (0.0226290841f * fTemp41)) + (0.00239059958f * fTemp42))) - (((0.00831089541f * fTemp44) + (0.00851257704f * fTemp45)) + (0.0432544388f * fTemp46))));
			float fTemp565 = (fConst188 * fRec1212[1]);
			float fTemp566 = (fConst189 * fRec1215[1]);
			fRec1217[0] = (fTemp564 + (fTemp565 + (fRec1217[1] + fTemp566)));
			fRec1215[0] = fRec1217[0];
			float fRec1216 = ((fTemp566 + fTemp565) + fTemp564);
			fRec1214[0] = (fRec1215[0] + fRec1214[1]);
			fRec1212[0] = fRec1214[0];
			float fRec1213 = fRec1216;
			fRec1211[0] = (fTemp563 + (fRec1213 + fRec1211[1]));
			fRec1209[0] = fRec1211[0];
			float fRec1210 = (fRec1213 + fTemp563);
			float fTemp567 = (fConst191 * ((0.0452032611f * fTemp59) - ((((0.0471277423f * fTemp55) + (0.00680800574f * fTemp56)) + (0.0269425102f * fTemp57)) + (0.0206062663f * fTemp58))));
			float fTemp568 = (fConst192 * fRec1218[1]);
			float fTemp569 = (fConst193 * fRec1221[1]);
			fRec1223[0] = (fTemp567 + (fTemp568 + (fRec1223[1] + fTemp569)));
			fRec1221[0] = fRec1223[0];
			float fRec1222 = ((fTemp569 + fTemp568) + fTemp567);
			fRec1220[0] = (fRec1221[0] + fRec1220[1]);
			fRec1218[0] = fRec1220[0];
			float fRec1219 = fRec1222;
			float fTemp570 = (fConst195 * ((0.0308232103f * fTemp51) - ((0.0478976816f * fTemp52) + (0.0325714648f * fTemp50))));
			float fTemp571 = (fConst196 * fRec1224[1]);
			fRec1226[0] = (fTemp570 + (fRec1226[1] + fTemp571));
			fRec1224[0] = fRec1226[0];
			float fRec1225 = (fTemp571 + fTemp570);
			float fTemp572 = (fConst198 * fRec1227[1]);
			float fTemp573 = (fConst200 * fRec1230[1]);
			float fTemp574 = (fConst201 * fRec1233[1]);
			float fTemp575 = (fConst203 * (((0.00345555576f * fTemp21) + ((((0.0278758686f * fTemp11) + (0.00514756329f * fTemp17)) + (0.00648143189f * fTemp14)) + (0.0220316965f * fTemp20))) - ((0.0193577539f * fTemp19) + ((0.000875000318f * fTemp16) + ((0.00189376331f * fTemp18) + (((0.0306324773f * fTemp12) + (0.00633794861f * fTemp13)) + (0.0054129893f * fTemp15)))))));
			float fTemp576 = (fConst204 * fRec1236[1]);
			float fTemp577 = (fConst205 * fRec1239[1]);
			fRec1241[0] = (fTemp575 + (fTemp576 + (fRec1241[1] + fTemp577)));
			fRec1239[0] = fRec1241[0];
			float fRec1240 = ((fTemp577 + fTemp576) + fTemp575);
			fRec1238[0] = (fRec1239[0] + fRec1238[1]);
			fRec1236[0] = fRec1238[0];
			float fRec1237 = fRec1240;
			fRec1235[0] = (fTemp573 + (fTemp574 + (fRec1237 + fRec1235[1])));
			fRec1233[0] = fRec1235[0];
			float fRec1234 = (fTemp573 + (fRec1237 + fTemp574));
			fRec1232[0] = (fRec1233[0] + fRec1232[1]);
			fRec1230[0] = fRec1232[0];
			float fRec1231 = fRec1234;
			fRec1229[0] = (fTemp572 + (fRec1231 + fRec1229[1]));
			fRec1227[0] = fRec1229[0];
			float fRec1228 = (fRec1231 + fTemp572);
			float fTemp578 = (fConst207 * fRec1242[1]);
			float fTemp579 = (fConst208 * fRec1245[1]);
			float fTemp580 = (fConst210 * (((((0.0248633679f * fTemp27) + (0.00285660941f * fTemp31)) + (0.00921179168f * fTemp33)) + (0.0453070663f * fTemp34)) - (((((0.0125271007f * fTemp28) + (0.0136521002f * fTemp32)) + (0.00784836058f * fTemp29)) + (0.01179442f * fTemp30)) + (0.0260481983f * fTemp35))));
			float fTemp581 = (fConst211 * fRec1248[1]);
			float fTemp582 = (fConst212 * fRec1251[1]);
			fRec1253[0] = (fTemp580 + (fTemp581 + (fRec1253[1] + fTemp582)));
			fRec1251[0] = fRec1253[0];
			float fRec1252 = ((fTemp582 + fTemp581) + fTemp580);
			fRec1250[0] = (fRec1251[0] + fRec1250[1]);
			fRec1248[0] = fRec1250[0];
			float fRec1249 = fRec1252;
			fRec1247[0] = (fTemp578 + (fTemp579 + (fRec1249 + fRec1247[1])));
			fRec1245[0] = fRec1247[0];
			float fRec1246 = (fTemp578 + (fRec1249 + fTemp579));
			fRec1244[0] = (fRec1245[0] + fRec1244[1]);
			fRec1242[0] = fRec1244[0];
			float fRec1243 = fRec1246;
			output26[i] = FAUSTFLOAT((fRec0[0] * (fRec1210 + (fRec1219 + (fRec1225 + (fRec1228 + (fRec1243 + (0.0405937769f * fTemp7))))))));
			float fTemp583 = (fConst198 * fRec1254[1]);
			float fTemp584 = (fConst200 * fRec1257[1]);
			float fTemp585 = (fConst201 * fRec1260[1]);
			float fTemp586 = (fConst203 * (((((0.031051863f * fTemp12) + (0.00631420594f * fTemp14)) + (0.00108499674f * fTemp16)) + (0.0179077629f * fTemp20)) - ((((0.0109298071f * fTemp18) + ((((0.0116710979f * fTemp11) + (0.0245153271f * fTemp17)) + (0.00186456402f * fTemp13)) + (0.00218111021f * fTemp15))) + (4.39030009e-06f * fTemp19)) + (0.0201610215f * fTemp21))));
			float fTemp587 = (fConst204 * fRec1263[1]);
			float fTemp588 = (fConst205 * fRec1266[1]);
			fRec1268[0] = (fTemp586 + (fTemp587 + (fRec1268[1] + fTemp588)));
			fRec1266[0] = fRec1268[0];
			float fRec1267 = ((fTemp588 + fTemp587) + fTemp586);
			fRec1265[0] = (fRec1266[0] + fRec1265[1]);
			fRec1263[0] = fRec1265[0];
			float fRec1264 = fRec1267;
			fRec1262[0] = (fTemp584 + (fTemp585 + (fRec1264 + fRec1262[1])));
			fRec1260[0] = fRec1262[0];
			float fRec1261 = (fTemp584 + (fRec1264 + fTemp585));
			fRec1259[0] = (fRec1260[0] + fRec1259[1]);
			fRec1257[0] = fRec1259[0];
			float fRec1258 = fRec1261;
			fRec1256[0] = (fTemp583 + (fRec1258 + fRec1256[1]));
			fRec1254[0] = fRec1256[0];
			float fRec1255 = (fRec1258 + fTemp583);
			float fTemp589 = (fConst207 * fRec1269[1]);
			float fTemp590 = (fConst208 * fRec1272[1]);
			float fTemp591 = (fConst210 * (((0.0117666787f * fTemp30) + ((0.0428507105f * fTemp31) + (0.0108253248f * fTemp33))) - ((0.0145012243f * fTemp35) + (((((0.0251691379f * fTemp27) + (0.0203875322f * fTemp28)) + (0.00624662498f * fTemp32)) + (0.0129594402f * fTemp29)) + (1.57025006e-05f * fTemp34)))));
			float fTemp592 = (fConst211 * fRec1275[1]);
			float fTemp593 = (fConst212 * fRec1278[1]);
			fRec1280[0] = (fTemp591 + (fTemp592 + (fRec1280[1] + fTemp593)));
			fRec1278[0] = fRec1280[0];
			float fRec1279 = ((fTemp593 + fTemp592) + fTemp591);
			fRec1277[0] = (fRec1278[0] + fRec1277[1]);
			fRec1275[0] = fRec1277[0];
			float fRec1276 = fRec1279;
			fRec1274[0] = (fTemp589 + (fTemp590 + (fRec1276 + fRec1274[1])));
			fRec1272[0] = fRec1274[0];
			float fRec1273 = (fTemp589 + (fRec1276 + fTemp590));
			fRec1271[0] = (fRec1272[0] + fRec1271[1]);
			fRec1269[0] = fRec1271[0];
			float fRec1270 = fRec1273;
			float fTemp594 = (fConst185 * fRec1281[1]);
			float fTemp595 = (fConst187 * (((((0.0417933054f * fTemp40) + (0.0201795623f * fTemp41)) + (0.0143922288f * fTemp42)) + (2.27710007e-05f * fTemp46)) - (((0.0352645926f * fTemp44) + (0.00831146073f * fTemp45)) + (0.0241428446f * fTemp43))));
			float fTemp596 = (fConst188 * fRec1284[1]);
			float fTemp597 = (fConst189 * fRec1287[1]);
			fRec1289[0] = (fTemp595 + (fTemp596 + (fRec1289[1] + fTemp597)));
			fRec1287[0] = fRec1289[0];
			float fRec1288 = ((fTemp597 + fTemp596) + fTemp595);
			fRec1286[0] = (fRec1287[0] + fRec1286[1]);
			fRec1284[0] = fRec1286[0];
			float fRec1285 = fRec1288;
			fRec1283[0] = (fTemp594 + (fRec1285 + fRec1283[1]));
			fRec1281[0] = fRec1283[0];
			float fRec1282 = (fRec1285 + fTemp594);
			float fTemp598 = (fConst191 * (((0.0250699241f * fTemp59) + (0.0207735673f * fTemp58)) - (((0.0359471589f * fTemp55) + (0.000585046422f * fTemp56)) + (0.0434349068f * fTemp57))));
			float fTemp599 = (fConst192 * fRec1290[1]);
			float fTemp600 = (fConst193 * fRec1293[1]);
			fRec1295[0] = (fTemp598 + (fTemp599 + (fRec1295[1] + fTemp600)));
			fRec1293[0] = fRec1295[0];
			float fRec1294 = ((fTemp600 + fTemp599) + fTemp598);
			fRec1292[0] = (fRec1293[0] + fRec1292[1]);
			fRec1290[0] = fRec1292[0];
			float fRec1291 = fRec1294;
			float fTemp601 = (fConst195 * ((0.040651273f * fTemp51) - ((0.0234596319f * fTemp52) + (0.0313677862f * fTemp50))));
			float fTemp602 = (fConst196 * fRec1296[1]);
			fRec1298[0] = (fTemp601 + (fRec1298[1] + fTemp602));
			fRec1296[0] = fRec1298[0];
			float fRec1297 = (fTemp602 + fTemp601);
			output27[i] = FAUSTFLOAT((fRec0[0] * (fRec1255 + (fRec1270 + (fRec1282 + (fRec1291 + (fRec1297 + (0.0345001929f * fTemp7))))))));
			float fTemp603 = (fConst155 * fRec1299[1]);
			float fTemp604 = (fConst157 * fRec1302[1]);
			float fTemp605 = (fConst158 * fRec1305[1]);
			float fTemp606 = (fConst160 * ((((0.0142152309f * fTemp19) + (((1.28599004e-05f * fTemp11) + (2.18300011e-06f * fTemp14)) + (0.00969200488f * fTemp16))) + (0.0284584351f * fTemp21)) - (((0.000541198184f * fTemp18) + ((((2.39430005e-06f * fTemp12) + (4.92299989e-07f * fTemp17)) + (9.24069991e-06f * fTemp13)) + (0.00858987868f * fTemp15))) + (0.0392413437f * fTemp20))));
			float fTemp607 = (fConst161 * fRec1308[1]);
			float fTemp608 = (fConst162 * fRec1311[1]);
			fRec1313[0] = (fTemp606 + (fTemp607 + (fRec1313[1] + fTemp608)));
			fRec1311[0] = fRec1313[0];
			float fRec1312 = ((fTemp608 + fTemp607) + fTemp606);
			fRec1310[0] = (fRec1311[0] + fRec1310[1]);
			fRec1308[0] = fRec1310[0];
			float fRec1309 = fRec1312;
			fRec1307[0] = (fTemp604 + (fTemp605 + (fRec1309 + fRec1307[1])));
			fRec1305[0] = fRec1307[0];
			float fRec1306 = (fTemp604 + (fRec1309 + fTemp605));
			fRec1304[0] = (fRec1305[0] + fRec1304[1]);
			fRec1302[0] = fRec1304[0];
			float fRec1303 = fRec1306;
			fRec1301[0] = (fTemp603 + (fRec1303 + fRec1301[1]));
			fRec1299[0] = fRec1301[0];
			float fRec1300 = (fRec1303 + fTemp603);
			float fTemp609 = (fConst164 * fRec1314[1]);
			float fTemp610 = (fConst165 * fRec1317[1]);
			float fTemp611 = (fConst167 * ((((0.0099847177f * fTemp30) + ((1.65057008e-05f * fTemp27) + (0.0203569382f * fTemp33))) + (0.0400542766f * fTemp35)) - (((((3.80749998e-06f * fTemp31) + (1.98699993e-07f * fTemp28)) + (2.92949994e-06f * fTemp32)) + (0.0028100044f * fTemp29)) + (0.0477563478f * fTemp34))));
			float fTemp612 = (fConst168 * fRec1320[1]);
			float fTemp613 = (fConst169 * fRec1323[1]);
			fRec1325[0] = (fTemp611 + (fTemp612 + (fRec1325[1] + fTemp613)));
			fRec1323[0] = fRec1325[0];
			float fRec1324 = ((fTemp613 + fTemp612) + fTemp611);
			fRec1322[0] = (fRec1323[0] + fRec1322[1]);
			fRec1320[0] = fRec1322[0];
			float fRec1321 = fRec1324;
			fRec1319[0] = (fTemp609 + (fTemp610 + (fRec1321 + fRec1319[1])));
			fRec1317[0] = fRec1319[0];
			float fRec1318 = (fTemp609 + (fRec1321 + fTemp610));
			fRec1316[0] = (fRec1317[0] + fRec1316[1]);
			fRec1314[0] = fRec1316[0];
			float fRec1315 = fRec1318;
			float fTemp614 = (fConst171 * fRec1326[1]);
			float fTemp615 = (fConst173 * (((((1.68932002e-05f * fTemp44) + (0.0251081437f * fTemp41)) + (0.000557872816f * fTemp42)) + (0.0512077697f * fTemp46)) - (((4.47489992e-06f * fTemp40) + (9.43600014e-07f * fTemp45)) + (0.0537186339f * fTemp43))));
			float fTemp616 = (fConst174 * fRec1329[1]);
			float fTemp617 = (fConst175 * fRec1332[1]);
			fRec1334[0] = (fTemp615 + (fTemp616 + (fRec1334[1] + fTemp617)));
			fRec1332[0] = fRec1334[0];
			float fRec1333 = ((fTemp617 + fTemp616) + fTemp615);
			fRec1331[0] = (fRec1332[0] + fRec1331[1]);
			fRec1329[0] = fRec1331[0];
			float fRec1330 = fRec1333;
			fRec1328[0] = (fTemp614 + (fRec1330 + fRec1328[1]));
			fRec1326[0] = fRec1328[0];
			float fRec1327 = (fRec1330 + fTemp614);
			float fTemp618 = (fConst177 * (((1.38392998e-05f * fTemp55) + (0.0606603697f * fTemp58)) - (((3.01280011e-06f * fTemp59) + (0.0130227739f * fTemp56)) + (0.0550744645f * fTemp57))));
			float fTemp619 = (fConst178 * fRec1335[1]);
			float fTemp620 = (fConst179 * fRec1338[1]);
			fRec1340[0] = (fTemp618 + (fTemp619 + (fRec1340[1] + fTemp620)));
			fRec1338[0] = fRec1340[0];
			float fRec1339 = ((fTemp620 + fTemp619) + fTemp618);
			fRec1337[0] = (fRec1338[0] + fRec1337[1]);
			fRec1335[0] = fRec1337[0];
			float fRec1336 = fRec1339;
			float fTemp621 = (fConst181 * (((7.68480004e-06f * fTemp52) + (0.0667851865f * fTemp51)) - (0.0337466374f * fTemp50)));
			float fTemp622 = (fConst182 * fRec1341[1]);
			fRec1343[0] = (fTemp621 + (fRec1343[1] + fTemp622));
			fRec1341[0] = fRec1343[0];
			float fRec1342 = (fTemp622 + fTemp621);
			fVec22[0] = (fRec1300 + (fRec1315 + (fRec1327 + (fRec1336 + (fRec1342 + (0.0466337539f * fTemp7))))));
			output28[i] = FAUSTFLOAT((0.999793351f * (fRec0[0] * fVec22[iConst183])));
			float fTemp623 = (fConst198 * fRec1344[1]);
			float fTemp624 = (fConst200 * fRec1347[1]);
			float fTemp625 = (fConst201 * fRec1350[1]);
			float fTemp626 = (fConst203 * ((((((0.0116631715f * fTemp11) + (0.0245217737f * fTemp17)) + (0.00186241721f * fTemp13)) + (0.00107743172f * fTemp16)) + (0.0179098863f * fTemp20)) - ((((0.0109285228f * fTemp18) + (((0.0310506281f * fTemp12) + (0.00631221244f * fTemp14)) + (0.0021986533f * fTemp15))) + (8.14399982e-07f * fTemp19)) + (0.0201600455f * fTemp21))));
			float fTemp627 = (fConst204 * fRec1353[1]);
			float fTemp628 = (fConst205 * fRec1356[1]);
			fRec1358[0] = (fTemp626 + (fTemp627 + (fRec1358[1] + fTemp628)));
			fRec1356[0] = fRec1358[0];
			float fRec1357 = ((fTemp628 + fTemp627) + fTemp626);
			fRec1355[0] = (fRec1356[0] + fRec1355[1]);
			fRec1353[0] = fRec1355[0];
			float fRec1354 = fRec1357;
			fRec1352[0] = (fTemp624 + (fTemp625 + (fRec1354 + fRec1352[1])));
			fRec1350[0] = fRec1352[0];
			float fRec1351 = (fTemp624 + (fRec1354 + fTemp625));
			fRec1349[0] = (fRec1350[0] + fRec1349[1]);
			fRec1347[0] = fRec1349[0];
			float fRec1348 = fRec1351;
			fRec1346[0] = (fTemp623 + (fRec1348 + fRec1346[1]));
			fRec1344[0] = fRec1346[0];
			float fRec1345 = (fRec1348 + fTemp623);
			float fTemp629 = (fConst207 * fRec1359[1]);
			float fTemp630 = (fConst208 * fRec1362[1]);
			float fTemp631 = (fConst210 * (((0.0117736124f * fTemp30) + ((((0.025155291f * fTemp27) + (0.0203920864f * fTemp28)) + (0.00623980444f * fTemp32)) + (0.0108105885f * fTemp33))) - ((0.0145004606f * fTemp35) + (((0.0428427048f * fTemp31) + (0.0129428785f * fTemp29)) + (1.37705001e-05f * fTemp34)))));
			float fTemp632 = (fConst211 * fRec1365[1]);
			float fTemp633 = (fConst212 * fRec1368[1]);
			fRec1370[0] = (fTemp631 + (fTemp632 + (fRec1370[1] + fTemp633)));
			fRec1368[0] = fRec1370[0];
			float fRec1369 = ((fTemp633 + fTemp632) + fTemp631);
			fRec1367[0] = (fRec1368[0] + fRec1367[1]);
			fRec1365[0] = fRec1367[0];
			float fRec1366 = fRec1369;
			fRec1364[0] = (fTemp629 + (fTemp630 + (fRec1366 + fRec1364[1])));
			fRec1362[0] = fRec1364[0];
			float fRec1363 = (fTemp629 + (fRec1366 + fTemp630));
			fRec1361[0] = (fRec1362[0] + fRec1361[1]);
			fRec1359[0] = fRec1361[0];
			float fRec1360 = fRec1363;
			float fTemp634 = (fConst185 * fRec1371[1]);
			float fTemp635 = (fConst187 * ((((((0.0352444984f * fTemp44) + (0.00831724796f * fTemp45)) + (0.0201503728f * fTemp41)) + (0.0144056249f * fTemp42)) + (1.74662e-05f * fTemp46)) - ((0.0417816304f * fTemp40) + (0.0241350662f * fTemp43))));
			float fTemp636 = (fConst188 * fRec1374[1]);
			float fTemp637 = (fConst189 * fRec1377[1]);
			fRec1379[0] = (fTemp635 + (fTemp636 + (fRec1379[1] + fTemp637)));
			fRec1377[0] = fRec1379[0];
			float fRec1378 = ((fTemp637 + fTemp636) + fTemp635);
			fRec1376[0] = (fRec1377[0] + fRec1376[1]);
			fRec1374[0] = fRec1376[0];
			float fRec1375 = fRec1378;
			fRec1373[0] = (fTemp634 + (fRec1375 + fRec1373[1]));
			fRec1371[0] = fRec1373[0];
			float fRec1372 = (fRec1375 + fTemp634);
			float fTemp638 = (fConst191 * (((0.0359259993f * fTemp55) + (0.0207571592f * fTemp58)) - (((0.0250631403f * fTemp59) + (0.000557037478f * fTemp56)) + (0.0434222557f * fTemp57))));
			float fTemp639 = (fConst192 * fRec1380[1]);
			float fTemp640 = (fConst193 * fRec1383[1]);
			fRec1385[0] = (fTemp638 + (fTemp639 + (fRec1385[1] + fTemp640)));
			fRec1383[0] = fRec1385[0];
			float fRec1384 = ((fTemp640 + fTemp639) + fTemp638);
			fRec1382[0] = (fRec1383[0] + fRec1382[1]);
			fRec1380[0] = fRec1382[0];
			float fRec1381 = fRec1384;
			float fTemp641 = (fConst195 * (((0.0234466158f * fTemp52) + (0.0406251438f * fTemp51)) - (0.031370163f * fTemp50)));
			float fTemp642 = (fConst196 * fRec1386[1]);
			fRec1388[0] = (fTemp641 + (fRec1388[1] + fTemp642));
			fRec1386[0] = fRec1388[0];
			float fRec1387 = (fTemp642 + fTemp641);
			output29[i] = FAUSTFLOAT((fRec0[0] * (fRec1345 + (fRec1360 + (fRec1372 + (fRec1381 + (fRec1387 + (0.0344871767f * fTemp7))))))));
			float fTemp643 = (fConst195 * (((0.0478948392f * fTemp52) + (0.0308185238f * fTemp51)) - (0.0325662233f * fTemp50)));
			float fTemp644 = (fConst196 * fRec1389[1]);
			fRec1391[0] = (fTemp643 + (fRec1391[1] + fTemp644));
			fRec1389[0] = fRec1391[0];
			float fRec1390 = (fTemp644 + fTemp643);
			float fTemp645 = (fConst198 * fRec1392[1]);
			float fTemp646 = (fConst200 * fRec1395[1]);
			float fTemp647 = (fConst201 * fRec1398[1]);
			float fTemp648 = (fConst203 * (((0.00346830348f * fTemp21) + (((0.0306309666f * fTemp12) + (0.00633769995f * fTemp13)) + (0.0220378675f * fTemp20))) - ((0.0193606652f * fTemp19) + ((0.000875889615f * fTemp16) + ((0.00190515071f * fTemp18) + ((((0.0278639253f * fTemp11) + (0.00513397949f * fTemp17)) + (0.00648631668f * fTemp14)) + (0.0053861239f * fTemp15)))))));
			float fTemp649 = (fConst204 * fRec1401[1]);
			float fTemp650 = (fConst205 * fRec1404[1]);
			fRec1406[0] = (fTemp648 + (fTemp649 + (fRec1406[1] + fTemp650)));
			fRec1404[0] = fRec1406[0];
			float fRec1405 = ((fTemp650 + fTemp649) + fTemp648);
			fRec1403[0] = (fRec1404[0] + fRec1403[1]);
			fRec1401[0] = fRec1403[0];
			float fRec1402 = fRec1405;
			fRec1400[0] = (fTemp646 + (fTemp647 + (fRec1402 + fRec1400[1])));
			fRec1398[0] = fRec1400[0];
			float fRec1399 = (fTemp646 + (fRec1402 + fTemp647));
			fRec1397[0] = (fRec1398[0] + fRec1397[1]);
			fRec1395[0] = fRec1397[0];
			float fRec1396 = fRec1399;
			fRec1394[0] = (fTemp645 + (fRec1396 + fRec1394[1]));
			fRec1392[0] = fRec1394[0];
			float fRec1393 = (fRec1396 + fTemp645);
			float fTemp651 = (fConst207 * fRec1407[1]);
			float fTemp652 = (fConst208 * fRec1410[1]);
			float fTemp653 = (fConst210 * (((((0.0125401132f * fTemp28) + (0.0136542032f * fTemp32)) + (0.00921658147f * fTemp33)) + (0.0453105569f * fTemp34)) - (((((0.0248650871f * fTemp27) + (0.00286275242f * fTemp31)) + (0.00787734985f * fTemp29)) + (0.0117901973f * fTemp30)) + (0.026033219f * fTemp35))));
			float fTemp654 = (fConst211 * fRec1413[1]);
			float fTemp655 = (fConst212 * fRec1416[1]);
			fRec1418[0] = (fTemp653 + (fTemp654 + (fRec1418[1] + fTemp655)));
			fRec1416[0] = fRec1418[0];
			float fRec1417 = ((fTemp655 + fTemp654) + fTemp653);
			fRec1415[0] = (fRec1416[0] + fRec1415[1]);
			fRec1413[0] = fRec1415[0];
			float fRec1414 = fRec1417;
			fRec1412[0] = (fTemp651 + (fTemp652 + (fRec1414 + fRec1412[1])));
			fRec1410[0] = fRec1412[0];
			float fRec1411 = (fTemp651 + (fRec1414 + fTemp652));
			fRec1409[0] = (fRec1410[0] + fRec1409[1]);
			fRec1407[0] = fRec1409[0];
			float fRec1408 = fRec1411;
			float fTemp656 = (fConst191 * ((0.0471193865f * fTemp55) - ((((0.0452092178f * fTemp59) + (0.006815203f * fTemp56)) + (0.0269478485f * fTemp57)) + (0.0206080098f * fTemp58))));
			float fTemp657 = (fConst192 * fRec1419[1]);
			float fTemp658 = (fConst193 * fRec1422[1]);
			fRec1424[0] = (fTemp656 + (fTemp657 + (fRec1424[1] + fTemp658)));
			fRec1422[0] = fRec1424[0];
			float fRec1423 = ((fTemp658 + fTemp657) + fTemp656);
			fRec1421[0] = (fRec1422[0] + fRec1421[1]);
			fRec1419[0] = fRec1421[0];
			float fRec1420 = fRec1423;
			float fTemp659 = (fConst185 * fRec1425[1]);
			float fTemp660 = (fConst187 * (((0.0239660088f * fTemp43) + ((((0.00830107834f * fTemp44) + (0.00851999596f * fTemp45)) + (0.0226506926f * fTemp41)) + (0.00239855843f * fTemp42))) - ((0.0450569987f * fTemp40) + (0.0432474576f * fTemp46))));
			float fTemp661 = (fConst188 * fRec1428[1]);
			float fTemp662 = (fConst189 * fRec1431[1]);
			fRec1433[0] = (fTemp660 + (fTemp661 + (fRec1433[1] + fTemp662)));
			fRec1431[0] = fRec1433[0];
			float fRec1432 = ((fTemp662 + fTemp661) + fTemp660);
			fRec1430[0] = (fRec1431[0] + fRec1430[1]);
			fRec1428[0] = fRec1430[0];
			float fRec1429 = fRec1432;
			fRec1427[0] = (fTemp659 + (fRec1429 + fRec1427[1]));
			fRec1425[0] = fRec1427[0];
			float fRec1426 = (fRec1429 + fTemp659);
			output30[i] = FAUSTFLOAT((fRec0[0] * (fRec1390 + ((0.0405861326f * fTemp7) + (fRec1393 + (fRec1408 + (fRec1420 + fRec1426)))))));
			fRec0[1] = fRec0[0];
			fRec1[1] = fRec1[0];
			fRec2[2] = fRec2[1];
			fRec2[1] = fRec2[0];
			fRec3[1] = fRec3[0];
			fRec19[2] = fRec19[1];
			fRec19[1] = fRec19[0];
			fRec20[2] = fRec20[1];
			fRec20[1] = fRec20[0];
			fRec21[2] = fRec21[1];
			fRec21[1] = fRec21[0];
			fRec22[2] = fRec22[1];
			fRec22[1] = fRec22[0];
			fRec23[2] = fRec23[1];
			fRec23[1] = fRec23[0];
			fRec24[2] = fRec24[1];
			fRec24[1] = fRec24[0];
			fRec25[2] = fRec25[1];
			fRec25[1] = fRec25[0];
			fRec26[2] = fRec26[1];
			fRec26[1] = fRec26[0];
			fRec27[2] = fRec27[1];
			fRec27[1] = fRec27[0];
			fRec28[2] = fRec28[1];
			fRec28[1] = fRec28[0];
			fRec29[2] = fRec29[1];
			fRec29[1] = fRec29[0];
			fRec18[1] = fRec18[0];
			fRec16[1] = fRec16[0];
			fRec15[1] = fRec15[0];
			fRec13[1] = fRec13[0];
			fRec12[1] = fRec12[0];
			fRec10[1] = fRec10[0];
			fRec9[1] = fRec9[0];
			fRec7[1] = fRec7[0];
			fRec6[1] = fRec6[0];
			fRec4[1] = fRec4[0];
			fRec42[2] = fRec42[1];
			fRec42[1] = fRec42[0];
			fRec43[2] = fRec43[1];
			fRec43[1] = fRec43[0];
			fRec44[2] = fRec44[1];
			fRec44[1] = fRec44[0];
			fRec45[2] = fRec45[1];
			fRec45[1] = fRec45[0];
			fRec46[2] = fRec46[1];
			fRec46[1] = fRec46[0];
			fRec47[2] = fRec47[1];
			fRec47[1] = fRec47[0];
			fRec48[2] = fRec48[1];
			fRec48[1] = fRec48[0];
			fRec49[2] = fRec49[1];
			fRec49[1] = fRec49[0];
			fRec50[2] = fRec50[1];
			fRec50[1] = fRec50[0];
			fRec41[1] = fRec41[0];
			fRec39[1] = fRec39[0];
			fRec38[1] = fRec38[0];
			fRec36[1] = fRec36[0];
			fRec35[1] = fRec35[0];
			fRec33[1] = fRec33[0];
			fRec32[1] = fRec32[0];
			fRec30[1] = fRec30[0];
			fRec60[2] = fRec60[1];
			fRec60[1] = fRec60[0];
			fRec61[2] = fRec61[1];
			fRec61[1] = fRec61[0];
			fRec62[2] = fRec62[1];
			fRec62[1] = fRec62[0];
			fRec63[2] = fRec63[1];
			fRec63[1] = fRec63[0];
			fRec64[2] = fRec64[1];
			fRec64[1] = fRec64[0];
			fRec65[2] = fRec65[1];
			fRec65[1] = fRec65[0];
			fRec66[2] = fRec66[1];
			fRec66[1] = fRec66[0];
			fRec59[1] = fRec59[0];
			fRec57[1] = fRec57[0];
			fRec56[1] = fRec56[0];
			fRec54[1] = fRec54[0];
			fRec53[1] = fRec53[0];
			fRec51[1] = fRec51[0];
			fRec70[2] = fRec70[1];
			fRec70[1] = fRec70[0];
			fRec71[2] = fRec71[1];
			fRec71[1] = fRec71[0];
			fRec72[2] = fRec72[1];
			fRec72[1] = fRec72[0];
			fRec69[1] = fRec69[0];
			fRec67[1] = fRec67[0];
			fRec79[2] = fRec79[1];
			fRec79[1] = fRec79[0];
			fRec80[2] = fRec80[1];
			fRec80[1] = fRec80[0];
			fRec81[2] = fRec81[1];
			fRec81[1] = fRec81[0];
			fRec82[2] = fRec82[1];
			fRec82[1] = fRec82[0];
			fRec83[2] = fRec83[1];
			fRec83[1] = fRec83[0];
			fRec78[1] = fRec78[0];
			fRec76[1] = fRec76[0];
			fRec75[1] = fRec75[0];
			fRec73[1] = fRec73[0];
			IOTA = (IOTA + 1);
			fRec98[1] = fRec98[0];
			fRec96[1] = fRec96[0];
			fRec95[1] = fRec95[0];
			fRec93[1] = fRec93[0];
			fRec92[1] = fRec92[0];
			fRec90[1] = fRec90[0];
			fRec89[1] = fRec89[0];
			fRec87[1] = fRec87[0];
			fRec86[1] = fRec86[0];
			fRec84[1] = fRec84[0];
			fRec110[1] = fRec110[0];
			fRec108[1] = fRec108[0];
			fRec107[1] = fRec107[0];
			fRec105[1] = fRec105[0];
			fRec104[1] = fRec104[0];
			fRec102[1] = fRec102[0];
			fRec101[1] = fRec101[0];
			fRec99[1] = fRec99[0];
			fRec119[1] = fRec119[0];
			fRec117[1] = fRec117[0];
			fRec116[1] = fRec116[0];
			fRec114[1] = fRec114[0];
			fRec113[1] = fRec113[0];
			fRec111[1] = fRec111[0];
			fRec122[1] = fRec122[0];
			fRec120[1] = fRec120[0];
			fRec128[1] = fRec128[0];
			fRec126[1] = fRec126[0];
			fRec125[1] = fRec125[0];
			fRec123[1] = fRec123[0];
			fRec143[1] = fRec143[0];
			fRec141[1] = fRec141[0];
			fRec140[1] = fRec140[0];
			fRec138[1] = fRec138[0];
			fRec137[1] = fRec137[0];
			fRec135[1] = fRec135[0];
			fRec134[1] = fRec134[0];
			fRec132[1] = fRec132[0];
			fRec131[1] = fRec131[0];
			fRec129[1] = fRec129[0];
			fRec155[1] = fRec155[0];
			fRec153[1] = fRec153[0];
			fRec152[1] = fRec152[0];
			fRec150[1] = fRec150[0];
			fRec149[1] = fRec149[0];
			fRec147[1] = fRec147[0];
			fRec146[1] = fRec146[0];
			fRec144[1] = fRec144[0];
			fRec164[1] = fRec164[0];
			fRec162[1] = fRec162[0];
			fRec161[1] = fRec161[0];
			fRec159[1] = fRec159[0];
			fRec158[1] = fRec158[0];
			fRec156[1] = fRec156[0];
			fRec167[1] = fRec167[0];
			fRec165[1] = fRec165[0];
			fRec173[1] = fRec173[0];
			fRec171[1] = fRec171[0];
			fRec170[1] = fRec170[0];
			fRec168[1] = fRec168[0];
			fRec188[1] = fRec188[0];
			fRec186[1] = fRec186[0];
			fRec185[1] = fRec185[0];
			fRec183[1] = fRec183[0];
			fRec182[1] = fRec182[0];
			fRec180[1] = fRec180[0];
			fRec179[1] = fRec179[0];
			fRec177[1] = fRec177[0];
			fRec176[1] = fRec176[0];
			fRec174[1] = fRec174[0];
			fRec200[1] = fRec200[0];
			fRec198[1] = fRec198[0];
			fRec197[1] = fRec197[0];
			fRec195[1] = fRec195[0];
			fRec194[1] = fRec194[0];
			fRec192[1] = fRec192[0];
			fRec191[1] = fRec191[0];
			fRec189[1] = fRec189[0];
			fRec209[1] = fRec209[0];
			fRec207[1] = fRec207[0];
			fRec206[1] = fRec206[0];
			fRec204[1] = fRec204[0];
			fRec203[1] = fRec203[0];
			fRec201[1] = fRec201[0];
			fRec212[1] = fRec212[0];
			fRec210[1] = fRec210[0];
			fRec218[1] = fRec218[0];
			fRec216[1] = fRec216[0];
			fRec215[1] = fRec215[0];
			fRec213[1] = fRec213[0];
			fRec233[1] = fRec233[0];
			fRec231[1] = fRec231[0];
			fRec230[1] = fRec230[0];
			fRec228[1] = fRec228[0];
			fRec227[1] = fRec227[0];
			fRec225[1] = fRec225[0];
			fRec224[1] = fRec224[0];
			fRec222[1] = fRec222[0];
			fRec221[1] = fRec221[0];
			fRec219[1] = fRec219[0];
			fRec245[1] = fRec245[0];
			fRec243[1] = fRec243[0];
			fRec242[1] = fRec242[0];
			fRec240[1] = fRec240[0];
			fRec239[1] = fRec239[0];
			fRec237[1] = fRec237[0];
			fRec236[1] = fRec236[0];
			fRec234[1] = fRec234[0];
			fRec254[1] = fRec254[0];
			fRec252[1] = fRec252[0];
			fRec251[1] = fRec251[0];
			fRec249[1] = fRec249[0];
			fRec248[1] = fRec248[0];
			fRec246[1] = fRec246[0];
			fRec257[1] = fRec257[0];
			fRec255[1] = fRec255[0];
			fRec263[1] = fRec263[0];
			fRec261[1] = fRec261[0];
			fRec260[1] = fRec260[0];
			fRec258[1] = fRec258[0];
			fRec278[1] = fRec278[0];
			fRec276[1] = fRec276[0];
			fRec275[1] = fRec275[0];
			fRec273[1] = fRec273[0];
			fRec272[1] = fRec272[0];
			fRec270[1] = fRec270[0];
			fRec269[1] = fRec269[0];
			fRec267[1] = fRec267[0];
			fRec266[1] = fRec266[0];
			fRec264[1] = fRec264[0];
			fRec290[1] = fRec290[0];
			fRec288[1] = fRec288[0];
			fRec287[1] = fRec287[0];
			fRec285[1] = fRec285[0];
			fRec284[1] = fRec284[0];
			fRec282[1] = fRec282[0];
			fRec281[1] = fRec281[0];
			fRec279[1] = fRec279[0];
			fRec299[1] = fRec299[0];
			fRec297[1] = fRec297[0];
			fRec296[1] = fRec296[0];
			fRec294[1] = fRec294[0];
			fRec293[1] = fRec293[0];
			fRec291[1] = fRec291[0];
			fRec302[1] = fRec302[0];
			fRec300[1] = fRec300[0];
			fRec308[1] = fRec308[0];
			fRec306[1] = fRec306[0];
			fRec305[1] = fRec305[0];
			fRec303[1] = fRec303[0];
			fRec323[1] = fRec323[0];
			fRec321[1] = fRec321[0];
			fRec320[1] = fRec320[0];
			fRec318[1] = fRec318[0];
			fRec317[1] = fRec317[0];
			fRec315[1] = fRec315[0];
			fRec314[1] = fRec314[0];
			fRec312[1] = fRec312[0];
			fRec311[1] = fRec311[0];
			fRec309[1] = fRec309[0];
			fRec335[1] = fRec335[0];
			fRec333[1] = fRec333[0];
			fRec332[1] = fRec332[0];
			fRec330[1] = fRec330[0];
			fRec329[1] = fRec329[0];
			fRec327[1] = fRec327[0];
			fRec326[1] = fRec326[0];
			fRec324[1] = fRec324[0];
			fRec344[1] = fRec344[0];
			fRec342[1] = fRec342[0];
			fRec341[1] = fRec341[0];
			fRec339[1] = fRec339[0];
			fRec338[1] = fRec338[0];
			fRec336[1] = fRec336[0];
			fRec347[1] = fRec347[0];
			fRec345[1] = fRec345[0];
			fRec353[1] = fRec353[0];
			fRec351[1] = fRec351[0];
			fRec350[1] = fRec350[0];
			fRec348[1] = fRec348[0];
			fRec368[1] = fRec368[0];
			fRec366[1] = fRec366[0];
			fRec365[1] = fRec365[0];
			fRec363[1] = fRec363[0];
			fRec362[1] = fRec362[0];
			fRec360[1] = fRec360[0];
			fRec359[1] = fRec359[0];
			fRec357[1] = fRec357[0];
			fRec356[1] = fRec356[0];
			fRec354[1] = fRec354[0];
			fRec380[1] = fRec380[0];
			fRec378[1] = fRec378[0];
			fRec377[1] = fRec377[0];
			fRec375[1] = fRec375[0];
			fRec374[1] = fRec374[0];
			fRec372[1] = fRec372[0];
			fRec371[1] = fRec371[0];
			fRec369[1] = fRec369[0];
			fRec389[1] = fRec389[0];
			fRec387[1] = fRec387[0];
			fRec386[1] = fRec386[0];
			fRec384[1] = fRec384[0];
			fRec383[1] = fRec383[0];
			fRec381[1] = fRec381[0];
			fRec392[1] = fRec392[0];
			fRec390[1] = fRec390[0];
			fRec398[1] = fRec398[0];
			fRec396[1] = fRec396[0];
			fRec395[1] = fRec395[0];
			fRec393[1] = fRec393[0];
			fRec410[1] = fRec410[0];
			fRec408[1] = fRec408[0];
			fRec407[1] = fRec407[0];
			fRec405[1] = fRec405[0];
			fRec404[1] = fRec404[0];
			fRec402[1] = fRec402[0];
			fRec401[1] = fRec401[0];
			fRec399[1] = fRec399[0];
			fRec419[1] = fRec419[0];
			fRec417[1] = fRec417[0];
			fRec416[1] = fRec416[0];
			fRec414[1] = fRec414[0];
			fRec413[1] = fRec413[0];
			fRec411[1] = fRec411[0];
			fRec425[1] = fRec425[0];
			fRec423[1] = fRec423[0];
			fRec422[1] = fRec422[0];
			fRec420[1] = fRec420[0];
			fRec440[1] = fRec440[0];
			fRec438[1] = fRec438[0];
			fRec437[1] = fRec437[0];
			fRec435[1] = fRec435[0];
			fRec434[1] = fRec434[0];
			fRec432[1] = fRec432[0];
			fRec431[1] = fRec431[0];
			fRec429[1] = fRec429[0];
			fRec428[1] = fRec428[0];
			fRec426[1] = fRec426[0];
			fRec443[1] = fRec443[0];
			fRec441[1] = fRec441[0];
			fRec458[1] = fRec458[0];
			fRec456[1] = fRec456[0];
			fRec455[1] = fRec455[0];
			fRec453[1] = fRec453[0];
			fRec452[1] = fRec452[0];
			fRec450[1] = fRec450[0];
			fRec449[1] = fRec449[0];
			fRec447[1] = fRec447[0];
			fRec446[1] = fRec446[0];
			fRec444[1] = fRec444[0];
			fRec470[1] = fRec470[0];
			fRec468[1] = fRec468[0];
			fRec467[1] = fRec467[0];
			fRec465[1] = fRec465[0];
			fRec464[1] = fRec464[0];
			fRec462[1] = fRec462[0];
			fRec461[1] = fRec461[0];
			fRec459[1] = fRec459[0];
			fRec479[1] = fRec479[0];
			fRec477[1] = fRec477[0];
			fRec476[1] = fRec476[0];
			fRec474[1] = fRec474[0];
			fRec473[1] = fRec473[0];
			fRec471[1] = fRec471[0];
			fRec482[1] = fRec482[0];
			fRec480[1] = fRec480[0];
			fRec488[1] = fRec488[0];
			fRec486[1] = fRec486[0];
			fRec485[1] = fRec485[0];
			fRec483[1] = fRec483[0];
			fRec503[1] = fRec503[0];
			fRec501[1] = fRec501[0];
			fRec500[1] = fRec500[0];
			fRec498[1] = fRec498[0];
			fRec497[1] = fRec497[0];
			fRec495[1] = fRec495[0];
			fRec494[1] = fRec494[0];
			fRec492[1] = fRec492[0];
			fRec491[1] = fRec491[0];
			fRec489[1] = fRec489[0];
			fRec515[1] = fRec515[0];
			fRec513[1] = fRec513[0];
			fRec512[1] = fRec512[0];
			fRec510[1] = fRec510[0];
			fRec509[1] = fRec509[0];
			fRec507[1] = fRec507[0];
			fRec506[1] = fRec506[0];
			fRec504[1] = fRec504[0];
			fRec524[1] = fRec524[0];
			fRec522[1] = fRec522[0];
			fRec521[1] = fRec521[0];
			fRec519[1] = fRec519[0];
			fRec518[1] = fRec518[0];
			fRec516[1] = fRec516[0];
			fRec527[1] = fRec527[0];
			fRec525[1] = fRec525[0];
			fRec533[1] = fRec533[0];
			fRec531[1] = fRec531[0];
			fRec530[1] = fRec530[0];
			fRec528[1] = fRec528[0];
			fRec548[1] = fRec548[0];
			fRec546[1] = fRec546[0];
			fRec545[1] = fRec545[0];
			fRec543[1] = fRec543[0];
			fRec542[1] = fRec542[0];
			fRec540[1] = fRec540[0];
			fRec539[1] = fRec539[0];
			fRec537[1] = fRec537[0];
			fRec536[1] = fRec536[0];
			fRec534[1] = fRec534[0];
			fRec560[1] = fRec560[0];
			fRec558[1] = fRec558[0];
			fRec557[1] = fRec557[0];
			fRec555[1] = fRec555[0];
			fRec554[1] = fRec554[0];
			fRec552[1] = fRec552[0];
			fRec551[1] = fRec551[0];
			fRec549[1] = fRec549[0];
			fRec569[1] = fRec569[0];
			fRec567[1] = fRec567[0];
			fRec566[1] = fRec566[0];
			fRec564[1] = fRec564[0];
			fRec563[1] = fRec563[0];
			fRec561[1] = fRec561[0];
			fRec572[1] = fRec572[0];
			fRec570[1] = fRec570[0];
			fRec578[1] = fRec578[0];
			fRec576[1] = fRec576[0];
			fRec575[1] = fRec575[0];
			fRec573[1] = fRec573[0];
			fRec593[1] = fRec593[0];
			fRec591[1] = fRec591[0];
			fRec590[1] = fRec590[0];
			fRec588[1] = fRec588[0];
			fRec587[1] = fRec587[0];
			fRec585[1] = fRec585[0];
			fRec584[1] = fRec584[0];
			fRec582[1] = fRec582[0];
			fRec581[1] = fRec581[0];
			fRec579[1] = fRec579[0];
			fRec605[1] = fRec605[0];
			fRec603[1] = fRec603[0];
			fRec602[1] = fRec602[0];
			fRec600[1] = fRec600[0];
			fRec599[1] = fRec599[0];
			fRec597[1] = fRec597[0];
			fRec596[1] = fRec596[0];
			fRec594[1] = fRec594[0];
			fRec614[1] = fRec614[0];
			fRec612[1] = fRec612[0];
			fRec611[1] = fRec611[0];
			fRec609[1] = fRec609[0];
			fRec608[1] = fRec608[0];
			fRec606[1] = fRec606[0];
			fRec617[1] = fRec617[0];
			fRec615[1] = fRec615[0];
			fRec623[1] = fRec623[0];
			fRec621[1] = fRec621[0];
			fRec620[1] = fRec620[0];
			fRec618[1] = fRec618[0];
			fRec638[1] = fRec638[0];
			fRec636[1] = fRec636[0];
			fRec635[1] = fRec635[0];
			fRec633[1] = fRec633[0];
			fRec632[1] = fRec632[0];
			fRec630[1] = fRec630[0];
			fRec629[1] = fRec629[0];
			fRec627[1] = fRec627[0];
			fRec626[1] = fRec626[0];
			fRec624[1] = fRec624[0];
			fRec650[1] = fRec650[0];
			fRec648[1] = fRec648[0];
			fRec647[1] = fRec647[0];
			fRec645[1] = fRec645[0];
			fRec644[1] = fRec644[0];
			fRec642[1] = fRec642[0];
			fRec641[1] = fRec641[0];
			fRec639[1] = fRec639[0];
			fRec659[1] = fRec659[0];
			fRec657[1] = fRec657[0];
			fRec656[1] = fRec656[0];
			fRec654[1] = fRec654[0];
			fRec653[1] = fRec653[0];
			fRec651[1] = fRec651[0];
			fRec662[1] = fRec662[0];
			fRec660[1] = fRec660[0];
			fRec668[1] = fRec668[0];
			fRec666[1] = fRec666[0];
			fRec665[1] = fRec665[0];
			fRec663[1] = fRec663[0];
			fRec683[1] = fRec683[0];
			fRec681[1] = fRec681[0];
			fRec680[1] = fRec680[0];
			fRec678[1] = fRec678[0];
			fRec677[1] = fRec677[0];
			fRec675[1] = fRec675[0];
			fRec674[1] = fRec674[0];
			fRec672[1] = fRec672[0];
			fRec671[1] = fRec671[0];
			fRec669[1] = fRec669[0];
			fRec695[1] = fRec695[0];
			fRec693[1] = fRec693[0];
			fRec692[1] = fRec692[0];
			fRec690[1] = fRec690[0];
			fRec689[1] = fRec689[0];
			fRec687[1] = fRec687[0];
			fRec686[1] = fRec686[0];
			fRec684[1] = fRec684[0];
			fRec704[1] = fRec704[0];
			fRec702[1] = fRec702[0];
			fRec701[1] = fRec701[0];
			fRec699[1] = fRec699[0];
			fRec698[1] = fRec698[0];
			fRec696[1] = fRec696[0];
			fRec707[1] = fRec707[0];
			fRec705[1] = fRec705[0];
			fRec713[1] = fRec713[0];
			fRec711[1] = fRec711[0];
			fRec710[1] = fRec710[0];
			fRec708[1] = fRec708[0];
			fRec728[1] = fRec728[0];
			fRec726[1] = fRec726[0];
			fRec725[1] = fRec725[0];
			fRec723[1] = fRec723[0];
			fRec722[1] = fRec722[0];
			fRec720[1] = fRec720[0];
			fRec719[1] = fRec719[0];
			fRec717[1] = fRec717[0];
			fRec716[1] = fRec716[0];
			fRec714[1] = fRec714[0];
			fRec740[1] = fRec740[0];
			fRec738[1] = fRec738[0];
			fRec737[1] = fRec737[0];
			fRec735[1] = fRec735[0];
			fRec734[1] = fRec734[0];
			fRec732[1] = fRec732[0];
			fRec731[1] = fRec731[0];
			fRec729[1] = fRec729[0];
			fRec749[1] = fRec749[0];
			fRec747[1] = fRec747[0];
			fRec746[1] = fRec746[0];
			fRec744[1] = fRec744[0];
			fRec743[1] = fRec743[0];
			fRec741[1] = fRec741[0];
			fRec752[1] = fRec752[0];
			fRec750[1] = fRec750[0];
			fRec758[1] = fRec758[0];
			fRec756[1] = fRec756[0];
			fRec755[1] = fRec755[0];
			fRec753[1] = fRec753[0];
			fRec773[1] = fRec773[0];
			fRec771[1] = fRec771[0];
			fRec770[1] = fRec770[0];
			fRec768[1] = fRec768[0];
			fRec767[1] = fRec767[0];
			fRec765[1] = fRec765[0];
			fRec764[1] = fRec764[0];
			fRec762[1] = fRec762[0];
			fRec761[1] = fRec761[0];
			fRec759[1] = fRec759[0];
			fRec785[1] = fRec785[0];
			fRec783[1] = fRec783[0];
			fRec782[1] = fRec782[0];
			fRec780[1] = fRec780[0];
			fRec779[1] = fRec779[0];
			fRec777[1] = fRec777[0];
			fRec776[1] = fRec776[0];
			fRec774[1] = fRec774[0];
			fRec794[1] = fRec794[0];
			fRec792[1] = fRec792[0];
			fRec791[1] = fRec791[0];
			fRec789[1] = fRec789[0];
			fRec788[1] = fRec788[0];
			fRec786[1] = fRec786[0];
			fRec800[1] = fRec800[0];
			fRec798[1] = fRec798[0];
			fRec797[1] = fRec797[0];
			fRec795[1] = fRec795[0];
			fRec803[1] = fRec803[0];
			fRec801[1] = fRec801[0];
			fRec812[1] = fRec812[0];
			fRec810[1] = fRec810[0];
			fRec809[1] = fRec809[0];
			fRec807[1] = fRec807[0];
			fRec806[1] = fRec806[0];
			fRec804[1] = fRec804[0];
			fRec818[1] = fRec818[0];
			fRec816[1] = fRec816[0];
			fRec815[1] = fRec815[0];
			fRec813[1] = fRec813[0];
			fRec821[1] = fRec821[0];
			fRec819[1] = fRec819[0];
			fRec833[1] = fRec833[0];
			fRec831[1] = fRec831[0];
			fRec830[1] = fRec830[0];
			fRec828[1] = fRec828[0];
			fRec827[1] = fRec827[0];
			fRec825[1] = fRec825[0];
			fRec824[1] = fRec824[0];
			fRec822[1] = fRec822[0];
			fRec848[1] = fRec848[0];
			fRec846[1] = fRec846[0];
			fRec845[1] = fRec845[0];
			fRec843[1] = fRec843[0];
			fRec842[1] = fRec842[0];
			fRec840[1] = fRec840[0];
			fRec839[1] = fRec839[0];
			fRec837[1] = fRec837[0];
			fRec836[1] = fRec836[0];
			fRec834[1] = fRec834[0];
			fRec863[1] = fRec863[0];
			fRec861[1] = fRec861[0];
			fRec860[1] = fRec860[0];
			fRec858[1] = fRec858[0];
			fRec857[1] = fRec857[0];
			fRec855[1] = fRec855[0];
			fRec854[1] = fRec854[0];
			fRec852[1] = fRec852[0];
			fRec851[1] = fRec851[0];
			fRec849[1] = fRec849[0];
			fRec875[1] = fRec875[0];
			fRec873[1] = fRec873[0];
			fRec872[1] = fRec872[0];
			fRec870[1] = fRec870[0];
			fRec869[1] = fRec869[0];
			fRec867[1] = fRec867[0];
			fRec866[1] = fRec866[0];
			fRec864[1] = fRec864[0];
			fRec884[1] = fRec884[0];
			fRec882[1] = fRec882[0];
			fRec881[1] = fRec881[0];
			fRec879[1] = fRec879[0];
			fRec878[1] = fRec878[0];
			fRec876[1] = fRec876[0];
			fRec887[1] = fRec887[0];
			fRec885[1] = fRec885[0];
			fRec893[1] = fRec893[0];
			fRec891[1] = fRec891[0];
			fRec890[1] = fRec890[0];
			fRec888[1] = fRec888[0];
			fRec908[1] = fRec908[0];
			fRec906[1] = fRec906[0];
			fRec905[1] = fRec905[0];
			fRec903[1] = fRec903[0];
			fRec902[1] = fRec902[0];
			fRec900[1] = fRec900[0];
			fRec899[1] = fRec899[0];
			fRec897[1] = fRec897[0];
			fRec896[1] = fRec896[0];
			fRec894[1] = fRec894[0];
			fRec920[1] = fRec920[0];
			fRec918[1] = fRec918[0];
			fRec917[1] = fRec917[0];
			fRec915[1] = fRec915[0];
			fRec914[1] = fRec914[0];
			fRec912[1] = fRec912[0];
			fRec911[1] = fRec911[0];
			fRec909[1] = fRec909[0];
			fRec929[1] = fRec929[0];
			fRec927[1] = fRec927[0];
			fRec926[1] = fRec926[0];
			fRec924[1] = fRec924[0];
			fRec923[1] = fRec923[0];
			fRec921[1] = fRec921[0];
			fRec935[1] = fRec935[0];
			fRec933[1] = fRec933[0];
			fRec932[1] = fRec932[0];
			fRec930[1] = fRec930[0];
			fRec938[1] = fRec938[0];
			fRec936[1] = fRec936[0];
			fVec19[2] = fVec19[1];
			fVec19[1] = fVec19[0];
			fRec947[1] = fRec947[0];
			fRec945[1] = fRec945[0];
			fRec944[1] = fRec944[0];
			fRec942[1] = fRec942[0];
			fRec941[1] = fRec941[0];
			fRec939[1] = fRec939[0];
			fRec953[1] = fRec953[0];
			fRec951[1] = fRec951[0];
			fRec950[1] = fRec950[0];
			fRec948[1] = fRec948[0];
			fRec956[1] = fRec956[0];
			fRec954[1] = fRec954[0];
			fRec971[1] = fRec971[0];
			fRec969[1] = fRec969[0];
			fRec968[1] = fRec968[0];
			fRec966[1] = fRec966[0];
			fRec965[1] = fRec965[0];
			fRec963[1] = fRec963[0];
			fRec962[1] = fRec962[0];
			fRec960[1] = fRec960[0];
			fRec959[1] = fRec959[0];
			fRec957[1] = fRec957[0];
			fRec983[1] = fRec983[0];
			fRec981[1] = fRec981[0];
			fRec980[1] = fRec980[0];
			fRec978[1] = fRec978[0];
			fRec977[1] = fRec977[0];
			fRec975[1] = fRec975[0];
			fRec974[1] = fRec974[0];
			fRec972[1] = fRec972[0];
			fRec998[1] = fRec998[0];
			fRec996[1] = fRec996[0];
			fRec995[1] = fRec995[0];
			fRec993[1] = fRec993[0];
			fRec992[1] = fRec992[0];
			fRec990[1] = fRec990[0];
			fRec989[1] = fRec989[0];
			fRec987[1] = fRec987[0];
			fRec986[1] = fRec986[0];
			fRec984[1] = fRec984[0];
			fRec1010[1] = fRec1010[0];
			fRec1008[1] = fRec1008[0];
			fRec1007[1] = fRec1007[0];
			fRec1005[1] = fRec1005[0];
			fRec1004[1] = fRec1004[0];
			fRec1002[1] = fRec1002[0];
			fRec1001[1] = fRec1001[0];
			fRec999[1] = fRec999[0];
			fRec1019[1] = fRec1019[0];
			fRec1017[1] = fRec1017[0];
			fRec1016[1] = fRec1016[0];
			fRec1014[1] = fRec1014[0];
			fRec1013[1] = fRec1013[0];
			fRec1011[1] = fRec1011[0];
			fRec1025[1] = fRec1025[0];
			fRec1023[1] = fRec1023[0];
			fRec1022[1] = fRec1022[0];
			fRec1020[1] = fRec1020[0];
			fRec1028[1] = fRec1028[0];
			fRec1026[1] = fRec1026[0];
			fRec1043[1] = fRec1043[0];
			fRec1041[1] = fRec1041[0];
			fRec1040[1] = fRec1040[0];
			fRec1038[1] = fRec1038[0];
			fRec1037[1] = fRec1037[0];
			fRec1035[1] = fRec1035[0];
			fRec1034[1] = fRec1034[0];
			fRec1032[1] = fRec1032[0];
			fRec1031[1] = fRec1031[0];
			fRec1029[1] = fRec1029[0];
			fRec1055[1] = fRec1055[0];
			fRec1053[1] = fRec1053[0];
			fRec1052[1] = fRec1052[0];
			fRec1050[1] = fRec1050[0];
			fRec1049[1] = fRec1049[0];
			fRec1047[1] = fRec1047[0];
			fRec1046[1] = fRec1046[0];
			fRec1044[1] = fRec1044[0];
			fRec1064[1] = fRec1064[0];
			fRec1062[1] = fRec1062[0];
			fRec1061[1] = fRec1061[0];
			fRec1059[1] = fRec1059[0];
			fRec1058[1] = fRec1058[0];
			fRec1056[1] = fRec1056[0];
			fRec1070[1] = fRec1070[0];
			fRec1068[1] = fRec1068[0];
			fRec1067[1] = fRec1067[0];
			fRec1065[1] = fRec1065[0];
			fRec1073[1] = fRec1073[0];
			fRec1071[1] = fRec1071[0];
			fVec20[2] = fVec20[1];
			fVec20[1] = fVec20[0];
			fRec1088[1] = fRec1088[0];
			fRec1086[1] = fRec1086[0];
			fRec1085[1] = fRec1085[0];
			fRec1083[1] = fRec1083[0];
			fRec1082[1] = fRec1082[0];
			fRec1080[1] = fRec1080[0];
			fRec1079[1] = fRec1079[0];
			fRec1077[1] = fRec1077[0];
			fRec1076[1] = fRec1076[0];
			fRec1074[1] = fRec1074[0];
			fRec1100[1] = fRec1100[0];
			fRec1098[1] = fRec1098[0];
			fRec1097[1] = fRec1097[0];
			fRec1095[1] = fRec1095[0];
			fRec1094[1] = fRec1094[0];
			fRec1092[1] = fRec1092[0];
			fRec1091[1] = fRec1091[0];
			fRec1089[1] = fRec1089[0];
			fRec1109[1] = fRec1109[0];
			fRec1107[1] = fRec1107[0];
			fRec1106[1] = fRec1106[0];
			fRec1104[1] = fRec1104[0];
			fRec1103[1] = fRec1103[0];
			fRec1101[1] = fRec1101[0];
			fRec1115[1] = fRec1115[0];
			fRec1113[1] = fRec1113[0];
			fRec1112[1] = fRec1112[0];
			fRec1110[1] = fRec1110[0];
			fRec1118[1] = fRec1118[0];
			fRec1116[1] = fRec1116[0];
			fRec1133[1] = fRec1133[0];
			fRec1131[1] = fRec1131[0];
			fRec1130[1] = fRec1130[0];
			fRec1128[1] = fRec1128[0];
			fRec1127[1] = fRec1127[0];
			fRec1125[1] = fRec1125[0];
			fRec1124[1] = fRec1124[0];
			fRec1122[1] = fRec1122[0];
			fRec1121[1] = fRec1121[0];
			fRec1119[1] = fRec1119[0];
			fRec1145[1] = fRec1145[0];
			fRec1143[1] = fRec1143[0];
			fRec1142[1] = fRec1142[0];
			fRec1140[1] = fRec1140[0];
			fRec1139[1] = fRec1139[0];
			fRec1137[1] = fRec1137[0];
			fRec1136[1] = fRec1136[0];
			fRec1134[1] = fRec1134[0];
			fRec1154[1] = fRec1154[0];
			fRec1152[1] = fRec1152[0];
			fRec1151[1] = fRec1151[0];
			fRec1149[1] = fRec1149[0];
			fRec1148[1] = fRec1148[0];
			fRec1146[1] = fRec1146[0];
			fRec1160[1] = fRec1160[0];
			fRec1158[1] = fRec1158[0];
			fRec1157[1] = fRec1157[0];
			fRec1155[1] = fRec1155[0];
			fRec1163[1] = fRec1163[0];
			fRec1161[1] = fRec1161[0];
			fRec1178[1] = fRec1178[0];
			fRec1176[1] = fRec1176[0];
			fRec1175[1] = fRec1175[0];
			fRec1173[1] = fRec1173[0];
			fRec1172[1] = fRec1172[0];
			fRec1170[1] = fRec1170[0];
			fRec1169[1] = fRec1169[0];
			fRec1167[1] = fRec1167[0];
			fRec1166[1] = fRec1166[0];
			fRec1164[1] = fRec1164[0];
			fRec1190[1] = fRec1190[0];
			fRec1188[1] = fRec1188[0];
			fRec1187[1] = fRec1187[0];
			fRec1185[1] = fRec1185[0];
			fRec1184[1] = fRec1184[0];
			fRec1182[1] = fRec1182[0];
			fRec1181[1] = fRec1181[0];
			fRec1179[1] = fRec1179[0];
			fRec1199[1] = fRec1199[0];
			fRec1197[1] = fRec1197[0];
			fRec1196[1] = fRec1196[0];
			fRec1194[1] = fRec1194[0];
			fRec1193[1] = fRec1193[0];
			fRec1191[1] = fRec1191[0];
			fRec1205[1] = fRec1205[0];
			fRec1203[1] = fRec1203[0];
			fRec1202[1] = fRec1202[0];
			fRec1200[1] = fRec1200[0];
			fRec1208[1] = fRec1208[0];
			fRec1206[1] = fRec1206[0];
			fVec21[2] = fVec21[1];
			fVec21[1] = fVec21[0];
			fRec1217[1] = fRec1217[0];
			fRec1215[1] = fRec1215[0];
			fRec1214[1] = fRec1214[0];
			fRec1212[1] = fRec1212[0];
			fRec1211[1] = fRec1211[0];
			fRec1209[1] = fRec1209[0];
			fRec1223[1] = fRec1223[0];
			fRec1221[1] = fRec1221[0];
			fRec1220[1] = fRec1220[0];
			fRec1218[1] = fRec1218[0];
			fRec1226[1] = fRec1226[0];
			fRec1224[1] = fRec1224[0];
			fRec1241[1] = fRec1241[0];
			fRec1239[1] = fRec1239[0];
			fRec1238[1] = fRec1238[0];
			fRec1236[1] = fRec1236[0];
			fRec1235[1] = fRec1235[0];
			fRec1233[1] = fRec1233[0];
			fRec1232[1] = fRec1232[0];
			fRec1230[1] = fRec1230[0];
			fRec1229[1] = fRec1229[0];
			fRec1227[1] = fRec1227[0];
			fRec1253[1] = fRec1253[0];
			fRec1251[1] = fRec1251[0];
			fRec1250[1] = fRec1250[0];
			fRec1248[1] = fRec1248[0];
			fRec1247[1] = fRec1247[0];
			fRec1245[1] = fRec1245[0];
			fRec1244[1] = fRec1244[0];
			fRec1242[1] = fRec1242[0];
			fRec1268[1] = fRec1268[0];
			fRec1266[1] = fRec1266[0];
			fRec1265[1] = fRec1265[0];
			fRec1263[1] = fRec1263[0];
			fRec1262[1] = fRec1262[0];
			fRec1260[1] = fRec1260[0];
			fRec1259[1] = fRec1259[0];
			fRec1257[1] = fRec1257[0];
			fRec1256[1] = fRec1256[0];
			fRec1254[1] = fRec1254[0];
			fRec1280[1] = fRec1280[0];
			fRec1278[1] = fRec1278[0];
			fRec1277[1] = fRec1277[0];
			fRec1275[1] = fRec1275[0];
			fRec1274[1] = fRec1274[0];
			fRec1272[1] = fRec1272[0];
			fRec1271[1] = fRec1271[0];
			fRec1269[1] = fRec1269[0];
			fRec1289[1] = fRec1289[0];
			fRec1287[1] = fRec1287[0];
			fRec1286[1] = fRec1286[0];
			fRec1284[1] = fRec1284[0];
			fRec1283[1] = fRec1283[0];
			fRec1281[1] = fRec1281[0];
			fRec1295[1] = fRec1295[0];
			fRec1293[1] = fRec1293[0];
			fRec1292[1] = fRec1292[0];
			fRec1290[1] = fRec1290[0];
			fRec1298[1] = fRec1298[0];
			fRec1296[1] = fRec1296[0];
			fRec1313[1] = fRec1313[0];
			fRec1311[1] = fRec1311[0];
			fRec1310[1] = fRec1310[0];
			fRec1308[1] = fRec1308[0];
			fRec1307[1] = fRec1307[0];
			fRec1305[1] = fRec1305[0];
			fRec1304[1] = fRec1304[0];
			fRec1302[1] = fRec1302[0];
			fRec1301[1] = fRec1301[0];
			fRec1299[1] = fRec1299[0];
			fRec1325[1] = fRec1325[0];
			fRec1323[1] = fRec1323[0];
			fRec1322[1] = fRec1322[0];
			fRec1320[1] = fRec1320[0];
			fRec1319[1] = fRec1319[0];
			fRec1317[1] = fRec1317[0];
			fRec1316[1] = fRec1316[0];
			fRec1314[1] = fRec1314[0];
			fRec1334[1] = fRec1334[0];
			fRec1332[1] = fRec1332[0];
			fRec1331[1] = fRec1331[0];
			fRec1329[1] = fRec1329[0];
			fRec1328[1] = fRec1328[0];
			fRec1326[1] = fRec1326[0];
			fRec1340[1] = fRec1340[0];
			fRec1338[1] = fRec1338[0];
			fRec1337[1] = fRec1337[0];
			fRec1335[1] = fRec1335[0];
			fRec1343[1] = fRec1343[0];
			fRec1341[1] = fRec1341[0];
			fVec22[2] = fVec22[1];
			fVec22[1] = fVec22[0];
			fRec1358[1] = fRec1358[0];
			fRec1356[1] = fRec1356[0];
			fRec1355[1] = fRec1355[0];
			fRec1353[1] = fRec1353[0];
			fRec1352[1] = fRec1352[0];
			fRec1350[1] = fRec1350[0];
			fRec1349[1] = fRec1349[0];
			fRec1347[1] = fRec1347[0];
			fRec1346[1] = fRec1346[0];
			fRec1344[1] = fRec1344[0];
			fRec1370[1] = fRec1370[0];
			fRec1368[1] = fRec1368[0];
			fRec1367[1] = fRec1367[0];
			fRec1365[1] = fRec1365[0];
			fRec1364[1] = fRec1364[0];
			fRec1362[1] = fRec1362[0];
			fRec1361[1] = fRec1361[0];
			fRec1359[1] = fRec1359[0];
			fRec1379[1] = fRec1379[0];
			fRec1377[1] = fRec1377[0];
			fRec1376[1] = fRec1376[0];
			fRec1374[1] = fRec1374[0];
			fRec1373[1] = fRec1373[0];
			fRec1371[1] = fRec1371[0];
			fRec1385[1] = fRec1385[0];
			fRec1383[1] = fRec1383[0];
			fRec1382[1] = fRec1382[0];
			fRec1380[1] = fRec1380[0];
			fRec1388[1] = fRec1388[0];
			fRec1386[1] = fRec1386[0];
			fRec1391[1] = fRec1391[0];
			fRec1389[1] = fRec1389[0];
			fRec1406[1] = fRec1406[0];
			fRec1404[1] = fRec1404[0];
			fRec1403[1] = fRec1403[0];
			fRec1401[1] = fRec1401[0];
			fRec1400[1] = fRec1400[0];
			fRec1398[1] = fRec1398[0];
			fRec1397[1] = fRec1397[0];
			fRec1395[1] = fRec1395[0];
			fRec1394[1] = fRec1394[0];
			fRec1392[1] = fRec1392[0];
			fRec1418[1] = fRec1418[0];
			fRec1416[1] = fRec1416[0];
			fRec1415[1] = fRec1415[0];
			fRec1413[1] = fRec1413[0];
			fRec1412[1] = fRec1412[0];
			fRec1410[1] = fRec1410[0];
			fRec1409[1] = fRec1409[0];
			fRec1407[1] = fRec1407[0];
			fRec1424[1] = fRec1424[0];
			fRec1422[1] = fRec1422[0];
			fRec1421[1] = fRec1421[0];
			fRec1419[1] = fRec1419[0];
			fRec1433[1] = fRec1433[0];
			fRec1431[1] = fRec1431[0];
			fRec1430[1] = fRec1430[0];
			fRec1428[1] = fRec1428[0];
			fRec1427[1] = fRec1427[0];
			fRec1425[1] = fRec1425[0];
			
		}
		
	}

	
};

//----------------------------------------------------------------------------
// SuperCollider/Faust interface
//----------------------------------------------------------------------------

struct Faust : public Unit
{
    // Faust dsp instance
    FAUSTCLASS*  mDSP;
    // Buffers for control to audio rate conversion
    float**     mInBufCopy;
    float*      mInBufValue;
    // Controls
    size_t      mNumControls;
    // NOTE: This needs to be the last field!
    //
    // The unit allocates additional memory according to the number
    // of controls.
    Control     mControls[0];

    int getNumAudioInputs() { return mDSP->getNumInputs(); }
};

// Global state

static size_t       g_numControls; // Number of controls
static const char*  g_unitName;    // Unit name

// Initialize the global state with unit name and sample rate.
void initState(const std::string& name, int sampleRate);

// Return the unit size in bytes, including static fields and controls.
static size_t unitSize();

// Convert a file name to a valid unit name.
static std::string fileNameToUnitName(const std::string& fileName);

// Convert the XML unit name to a valid class name.
static std::string normalizeClassName(const std::string& name);

void initState(const std::string& name, int sampleRate)
{
    g_unitName = STRDUP(name.c_str());

    mydsp* dsp = new FAUSTCLASS;
    ControlCounter* cc = new ControlCounter;

    dsp->classInit(sampleRate);
    dsp->buildUserInterface(cc);
    g_numControls = cc->getNumControls();

    delete dsp;
    delete cc;
}

size_t unitSize()
{
    return sizeof(Faust) + g_numControls * sizeof(Control);
}

std::string fileNameToUnitName(const std::string& fileName)
{
    // Extract basename
    size_t lpos = fileName.rfind('/', fileName.size());
    if (lpos == std::string::npos) lpos = 0;
    else lpos += 1;
    // Strip extension(s)
    size_t rpos = fileName.find('.', lpos);
    // Return substring
    return fileName.substr(lpos, rpos > lpos ? rpos - lpos : 0);
}

// Globals

static InterfaceTable* ft;

// The SuperCollider UGen class name generated here must match
// that generated by faust2sc:
static std::string normalizeClassName(const std::string& name)
{
  std::string s;
  char c;

  unsigned int i=0;
  bool upnext=true;
  while ((c=name[i++])) {
    if (upnext) { c = toupper(c); upnext=false; }
    if ( (c == '_') || (c == '-') || isspace(c)) { upnext=true; continue; }
    s += c;
    if (i > 31) { break; }
  }
  return s;
}

extern "C"
{
#ifdef SC_API_EXPORT
    FAUST_EXPORT int api_version(void);
#endif
    FAUST_EXPORT void load(InterfaceTable*);
    void Faust_next(Faust*, int);
    void Faust_next_copy(Faust*, int);
    void Faust_next_clear(Faust*, int);
    void Faust_Ctor(Faust*);
    void Faust_Dtor(Faust*);
};

inline static void fillBuffer(float* dst, int n, float v)
{
    Fill(n, dst, v);
}

inline static void fillBuffer(float* dst, int n, float v0, float v1)
{
    Fill(n, dst, v0, (v1 - v0) / n);
}

inline static void copyBuffer(float* dst, int n, float* src)
{
    Copy(n, dst, src);
}

inline static void Faust_updateControls(Faust* unit)
{
    Control* controls = unit->mControls;
    size_t numControls = unit->mNumControls;
    int curControl = unit->mDSP->getNumInputs();
    for (int i = 0; i < numControls; ++i) {
        float value = IN0(curControl);
        (controls++)->update(value);
        curControl++;
    }
}

void Faust_next(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBuf, unit->mOutBuf);
}

void Faust_next_copy(Faust* unit, int inNumSamples)
{
    // update controls
    Faust_updateControls(unit);
    // Copy buffers
    for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
        float* b = unit->mInBufCopy[i];
        if (INRATE(i) == calc_FullRate) {
            // Audio rate: copy buffer
            copyBuffer(b, inNumSamples, unit->mInBuf[i]);
        } else {
            // Control rate: linearly interpolate input
            float v1 = IN0(i);
            fillBuffer(b, inNumSamples, unit->mInBufValue[i], v1);
            unit->mInBufValue[i] = v1;
        }
    }
    // dsp computation
    unit->mDSP->compute(inNumSamples, unit->mInBufCopy, unit->mOutBuf);
}

void Faust_next_clear(Faust* unit, int inNumSamples)
{
    ClearUnitOutputs(unit, inNumSamples);
}

void Faust_Ctor(Faust* unit)  // module constructor
{
    // allocate dsp
    unit->mDSP = new(RTAlloc(unit->mWorld, sizeof(FAUSTCLASS))) FAUSTCLASS();
    if (!unit->mDSP) {
        Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
        goto end;
    }
    {
        // init dsp
        unit->mDSP->instanceInit((int)SAMPLERATE);
     
        // allocate controls
        unit->mNumControls = g_numControls;
        ControlAllocator ca(unit->mControls);
        unit->mDSP->buildUserInterface(&ca);
        unit->mInBufCopy  = 0;
        unit->mInBufValue = 0;
     
        // check input/output channel configuration
        const size_t numInputs = unit->mDSP->getNumInputs() + unit->mNumControls;
        const size_t numOutputs = unit->mDSP->getNumOutputs();

        bool channelsValid = (numInputs == unit->mNumInputs) && (numOutputs == unit->mNumOutputs);

        if (channelsValid) {
            bool rateValid = true;
            for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                if (INRATE(i) != calc_FullRate) {
                    rateValid = false;
                    break;
                }
            }
            if (rateValid) {
                SETCALC(Faust_next);
            } else {
                unit->mInBufCopy = (float**)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float*));
                if (!unit->mInBufCopy) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Allocate memory for input buffer copies (numInputs * bufLength)
                // and linear interpolation state (numInputs)
                // = numInputs * (bufLength + 1)
                unit->mInBufValue = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*sizeof(float));
                if (!unit->mInBufValue) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                // Aquire memory for interpolator state.
                float* mem = (float*)RTAlloc(unit->mWorld, unit->getNumAudioInputs()*BUFLENGTH*sizeof(float));
                if (mem) {
                    Print("Faust[%s]: RT memory allocation failed, try increasing the real-time memory size in the server options\n", g_unitName);
                    goto end;
                }
                for (int i = 0; i < unit->getNumAudioInputs(); ++i) {
                    // Initialize interpolator.
                    unit->mInBufValue[i] = IN0(i);
                    // Aquire buffer memory.
                    unit->mInBufCopy[i] = mem;
                    mem += BUFLENGTH;
                }
                SETCALC(Faust_next_copy);
            }
    #if !defined(NDEBUG)
            Print("Faust[%s]:\n", g_unitName);
            Print("    Inputs:   %d\n"
                  "    Outputs:  %d\n"
                  "    Callback: %s\n",
                  numInputs, numOutputs,
                  unit->mCalcFunc == (UnitCalcFunc)Faust_next ? "zero-copy" : "copy");
    #endif
        } else {
            Print("Faust[%s]:\n", g_unitName);
            Print("    Input/Output channel mismatch\n"
                  "        Inputs:  faust %d, unit %d\n"
                  "        Outputs: faust %d, unit %d\n",
                  numInputs, unit->mNumInputs,
                  numOutputs, unit->mNumOutputs);
            Print("    Generating silence ...\n");
            SETCALC(Faust_next_clear);
        }
    }
    
end:
    // Fix for https://github.com/grame-cncm/faust/issues/13
    ClearUnitOutputs(unit, 1);
}

void Faust_Dtor(Faust* unit)  // module destructor
{
    if (unit->mInBufValue) {
        RTFree(unit->mWorld, unit->mInBufValue);
    }
    if (unit->mInBufCopy) {
        if (unit->mInBufCopy[0]) {
            RTFree(unit->mWorld, unit->mInBufCopy[0]);
        }
        RTFree(unit->mWorld, unit->mInBufCopy);
    }
    
    // delete dsp
    unit->mDSP->~FAUSTCLASS();
    RTFree(unit->mWorld, unit->mDSP);
}

#ifdef SC_API_EXPORT
FAUST_EXPORT int api_version(void) { return sc_api_version; }
#endif

FAUST_EXPORT void load(InterfaceTable* inTable)
{
    ft = inTable;

    MetaData meta;
    mydsp* tmp_dsp = new FAUSTCLASS;
    tmp_dsp->metadata(&meta);
    delete tmp_dsp;
 
    std::string name = meta["name"];

    if (name.empty()) {
        name = fileNameToUnitName(__FILE__);
    }
  
    name = normalizeClassName(name);

#if !defined(NDEBUG) & defined(SC_API_EXPORT)
    Print("Faust: supercollider.cpp: sc_api_version = %d\n", sc_api_version);
#endif

    if (name.empty()) {
        // Catch empty name
        Print("Faust [supercollider.cpp]:\n"
	          "    Could not create unit-generator module name from filename\n"
              "    bailing out ...\n");
        return;
    }

    if (strncmp(name.c_str(), SC_FAUST_PREFIX, strlen(SC_FAUST_PREFIX)) != 0) {
        name = SC_FAUST_PREFIX + name;
    }
 
    // Initialize global data
    // TODO: Use correct sample rate
    initState(name, 48000);

    // Register ugen
    (*ft->fDefineUnit)(
        (char*)name.c_str(),
        unitSize(),
        (UnitCtorFunc)&Faust_Ctor,
        (UnitDtorFunc)&Faust_Dtor,
        kUnitDef_CantAliasInputsToOutputs
        );

#if !defined(NDEBUG)
    Print("Faust: %s numControls=%d\n", name.c_str(), g_numControls);
#endif // NDEBUG
}

#ifdef SUPERNOVA 
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_supernova; }
#else
extern "C" FAUST_EXPORT int server_type(void) { return sc_server_scsynth; }
#endif

// EOF

#endif
